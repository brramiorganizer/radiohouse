<?php

namespace app\helper;

use Yii;
use app\models\AccountMember;
use app\models\PageContent;
use app\models\Content;
use app\models\PlanPackage;
use app\models\PlanFeatures;
use app\models\RadioPlan;
use app\models\Plan;
use app\models\Radio;
use app\models\RadioContent;
use app\models\Data;
use app\models\AdvertismentRadioSeven;
use app\models\AdvertismentRadioTen;
use app\models\AdvertismentNewsEvent;
use app\models\AdvertismentRadioMenu;
use app\models\AdvertismentNeChannel;
use app\models\EventJoinConfirmation;
  class Helper {
    public static function base_url(){
      return Yii::$app->params['base_url'];
    }

    public static function content($id){
      $data = Content::findOne($id);
      return $data->content;
    }

    public static function getUserLogged(){
      $user = Yii::$app->session->get('fronttoken');
      if($user['type'] == 'member'){
        $return = AccountMember::findOne(['token'=>$user['token']]);
      }else{
        return Yii::$app->response->redirect(Yii::$app->homeUrl);
      }
      return $return;
    }
    public static function getRadioLogged(){
      $user = Yii::$app->session->get('radiotoken');
      $return = Radio::findOne(['id'=>$user]);
      return $return;
    }

    public static function getPageContent($id){
      $data = PageContent::findOne(['id'=>$id]);
      return $data;
    }
    public static function breadcrumb($array){
      return Yii::$app->view->render('@app/views/layouts/breadcrumb',['breadcrumb'=>$array]);
    }
    public static function convertDateTime($date){
      $tahun = substr($date,0,4);
      $bulan = substr($date,5,2);
      $tanggal = substr($date,8,2);
      $time = substr($date,11,8);
      $nama_bulan = ['','January','February','March','April','May','June','July','August','September','October','November','December'];

      return $tanggal . ' ' . $nama_bulan[(int)$bulan] . ' ' . $tahun . ' ' . $time;
    }
    public static function convertDate($date){
      $tahun = substr($date,0,4);
      $bulan = substr($date,5,2);
      $tanggal = substr($date,8,2);
      $nama_bulan = ['','January','February','March','April','May','June','July','August','September','October','November','December'];

      return $tanggal . ' ' . $nama_bulan[(int)$bulan] . ' ' . $tahun;
    }

    public static function convertDateAmerican($date){
      $tahun = substr($date,0,4);
      $bulan = substr($date,5,2);
      $tanggal = substr($date,8,2);
      $nama_bulan = ['','January','February','March','April','May','June','July','August','September','October','November','December'];

      return  $nama_bulan[(int)$bulan] . ' ' . $tanggal. ', ' . $tahun;
    }

    public static function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])){
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_X_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }
        else if(isset($_SERVER['HTTP_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        }
        else if(isset($_SERVER['REMOTE_ADDR'])){
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }
        else{
            $ipaddress = 'UNKNOWN';
        }
        return $ipaddress;
    }


    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => $ipdat->geoplugin_city,
                            "state"          => $ipdat->geoplugin_regionName,
                            "country"        => $ipdat->geoplugin_countryName,
                            "country_code"   => $ipdat->geoplugin_countryCode,
                            "continent"      => $continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => $ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = $ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = $ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = $ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = $ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = $ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }





      public static function getDevice(){
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
          $bname = 'Unknown';
          $platform = 'Unknown';
          $version= "";

          //First get the platform?
          if (preg_match('/linux/i', $u_agent)) {
              $platform = 'linux';
          }
          elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
              $platform = 'mac';
          }
          elseif (preg_match('/windows|win32/i', $u_agent)) {
              $platform = 'windows';
          }

          // Next get the name of the useragent yes seperately and for good reason
          if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
          {
              $bname = 'Internet Explorer';
              $ub = "MSIE";
          }
          elseif(preg_match('/Firefox/i',$u_agent))
          {
              $bname = 'Mozilla Firefox';
              $ub = "Firefox";
          }
          elseif(preg_match('/OPR/i',$u_agent))
          {
              $bname = 'Opera';
              $ub = "Opera";
          }
          elseif(preg_match('/Chrome/i',$u_agent))
          {
              $bname = 'Google Chrome';
              $ub = "Chrome";
          }
          elseif(preg_match('/Safari/i',$u_agent))
          {
              $bname = 'Apple Safari';
              $ub = "Safari";
          }
          elseif(preg_match('/Netscape/i',$u_agent))
          {
              $bname = 'Netscape';
              $ub = "Netscape";
          }

          // finally get the correct version number
          $known = array('Version', $ub, 'other');
          $pattern = '#(?<browser>' . join('|', $known) .
          ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
          if (!preg_match_all($pattern, $u_agent, $matches)) {
              // we have no matching number just continue
          }

          // see how many we have
          $i = count($matches['browser']);
          if ($i != 1) {
              //we will have two since we are not using 'other' argument yet
              //see if version is before or after the name
              if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                  $version= $matches['version'][0];
              }
              else {
                  $version= $matches['version'][1];
              }
          }
          else {
              $version= $matches['version'][0];
          }

          // check if we have a number
          if ($version==null || $version=="") {$version="?";}

          $data =  array(
              'userAgent' => $u_agent,
              'name'      => $bname,
              'version'   => $version,
              'platform'  => $platform,
              'pattern'    => $pattern
          );
          return $data['name'] . ' version ' . $data['version'] . ' platform '.$data['platform'];
      }

      public static function checkPlan($plan_id,$plan_features_id){
        $data = PlanPackage::findOne(['plan_id'=>$plan_id,'plan_features_id'=>$plan_features_id]);
        return ($data ? true : false);
      }

      public static function getPlanFeature($plan_id){
        $data = PlanPackage::find()
                ->andWhere(['plan_id'=>$plan_id])
                ->orderBy('plan_features_id asc')
                ->all();
        return $data;
      }


      public static function getPlan($radio_id){
        $radio_plan = RadioPlan::findOne(['radio_id'=>$radio_id]);
        $plan = Plan::findOne(['id'=>$radio_plan->plan_id]);
        return $plan;
      }


      public static function checkMemberLogged(){
        if($session = Yii::$app->session->get('fronttoken')){
          $member = AccountMember::findOne(['token'=>$session]);
          if(!$member){
            return Yii::$app->response->redirect(Yii::$app->homeUrl);
          }else{
            return $member;
          }
        }else{
          return Yii::$app->response->redirect(Yii::$app->homeUrl);
        }
      }




      public static function slugify($text){
          // replace non letter or digits by -
          $text = preg_replace('~[^\pL\d]+~u', '-', $text);

          // transliterate
          $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

          // remove unwanted characters
          $text = preg_replace('~[^-\w]+~', '', $text);

          // trim
          $text = trim($text, '-');

          // remove duplicated - symbols
          $text = preg_replace('~-+~', '-', $text);

          // lowercase
          $text = strtolower($text);

          if (empty($text)) {
            return 'n-a';
          }

          return $text;
      }


      public static function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
      }

      public static function getOneRadio($radio_id){
        $data = Radio::findOne(['id'=>$radio_id]);
        return $data;
      }


      public static function getDataContent($id){
        $data = Data::findOne(['id'=>$id]);
        if($data){
          return $data->content;
        }else{
          return '<i>not set<i>';
        }
      }

      public static function getAdvertismentOne($sort){
        $model = AdvertismentRadioSeven::findOne(['sort'=>$sort]);
        if($sort == 1){
          $image = Helper::base_url().'demos/800x1100.png';
        }else{
          $image = Helper::base_url().'demos/800x800.png';
        }

        if($model->expired_date != '' and $model->radio->status == 'active' and $model->expired_date > date('Y-m-d H:i:s')){
          if($model->banner_type == 'custom'){
            $image = Helper::base_url().'media/advertisment/'.$model->banner;
          }else{
            $radiocontent = RadioContent::findOne(['radio_id'=>$model->radio_id]);
            if($radiocontent){
              if($radiocontent->photo_profile != null or $radiocontent->photo_profile != ''){
                $image = Helper::base_url().'media/radio/profile/'.$radiocontent->photo_profile;
              }
            }

          }

        }
        return $image;
      }

      public static function getAdvertismentOneName($sort){
        $model = AdvertismentRadioSeven::findOne(['sort'=>$sort]);
        $text[0] = '';
        $text[1] = '';

        if($model->radio_id != '' and $model->radio->status == 'active' and $model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
          $text[0] = $model->radio->radio_name;
          $text[1] = $model->radio->city;
        }
        return $text;
      }

      public static function getAdvertismentOneLink($sort){
        $model = AdvertismentRadioSeven::findOne(['sort'=>$sort]);
        $text = '#';

        if($model->radio_id != '' and $model->radio->status == 'active' and $model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
          $text = Yii::$app->params['base_url'].'channel/'.$model->radio->slug;
        }
        return $text;
      }












      public static function getTwoAdvertismentOne($sort){
        $model = AdvertismentRadioTen::findOne(['sort'=>$sort]);

          $image = Helper::base_url().'demos/800x800.png';


        if($model->expired_date != '' and $model->radio->status == 'active' and $model->expired_date > date('Y-m-d H:i:s')){
          if($model->banner_type == 'custom'){
            $image = Helper::base_url().'media/advertisment/'.$model->banner;
          }else{
            $radiocontent = RadioContent::findOne(['radio_id'=>$model->radio_id]);
            if($radiocontent){
              if($radiocontent->photo_profile != null or $radiocontent->photo_profile != ''){
                $image = Helper::base_url().'media/radio/profile/'.$radiocontent->photo_profile;
              }
            }

          }

        }
        return $image;
      }

      public static function getTwoAdvertismentOneName($sort){
        $model = AdvertismentRadioTen::findOne(['sort'=>$sort]);
        $text[0] = '';
        $text[1] = '';

        if($model->radio_id != '' and $model->radio->status == 'active' and $model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
          $text[0] = $model->radio->radio_name;
          $text[1] = $model->radio->city;
        }
        return $text;
      }

      public static function getTwoAdvertismentOneLink($sort){
        $model = AdvertismentRadioTen::findOne(['sort'=>$sort]);
        $text = '#';

        if($model->radio_id != '' and $model->radio->status == 'active' and $model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
          $text = Yii::$app->params['base_url'].'channel/'.$model->radio->slug;
        }
        return $text;
      }








      public static function getAdvertismentNeOne($sort){
        $model = AdvertismentNewsEvent::findOne(['sort'=>$sort,'status'=>'active']);

          $return['image'] = Helper::base_url().'demos/830x557.png';
          $return['title'] = '';
          $return['description'] = '';
          $return['link'] = '';

        if($model){
          if($model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
            $return['image'] = Helper::base_url().'media/banner-news-event/'.$model->ne->banner;
            $return['title'] = $model->ne->title;
            $return['description'] = self::limit_text(strip_tags($model->ne->description),15);
            $return['link'] = Helper::base_url().$model->ne->type.'/'.$model->ne->slug;
          }
        }
        return $return;
      }



      public static function getAdvertismentOneNeLink($sort){
        $model = AdvertismentRadioTen::findOne(['sort'=>$sort]);
        $text = '#';

        if($model->radio_id != '' and $model->expired_date != '' and $model->expired_date > date('Y-m-d H:i:s')){
          $text = Yii::$app->params['base_url'].'channel/'.$model->radio->slug;
        }
        return $text;
      }

      public static function checkedOrNot($radio_id){
        $model = AdvertismentRadioMenu::find()
        ->andWhere(['radio_id'=>$radio_id])
        ->andWhere(['>=','expired',date('Y-m-d H:i:s')])
        ->one();
        $status = false;
        if($model){
          $status = true;
        }
        return $status;
      }


      public static function getAdvMenu(){
        $model = AdvertismentRadioMenu::find()
        ->joinWith('radio')
        ->andWhere(
         ['=', 'radio.status', 'active']

        )->andWhere(['>=','expired',date('Y-m-d H:i:s')])
        ->all();
        return $model;
      }

      public static function getNeChannel($id){
        $advertisment = AdvertismentNeChannel::findOne($id);
        return $advertisment;
      }

      public static function buyingTicketCount(){
        $data = EventJoinConfirmation::find()->andWhere(['status'=>'pending'])->count();
        return $data;
      }

  }
?>
