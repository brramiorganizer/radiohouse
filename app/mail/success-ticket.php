<?php
use app\helper\Helper;
?>
<h3>Ticket Code : RH-<?= $join->code ?></h3>

<p><?= Helper::getDataContent(15) ?></p>
<div class="invoice success-ye">


<h4 style="margin:20px 0 10px 7px;">Acara</h4>
<table style="margin-bottom:25px" class="standarttb">
  <tr>
    <td>Nama Acara</td>
    <td>:</td>
    <td><?= $join->event->title ?></td>
  </tr>
  <tr>
    <td>Url Detail Acara</td>
    <td>:</td>
    <td><a href="<?= Yii::$app->params['base_url'].'event/'.$join->event->slug ?>"><?= Yii::$app->params['base_url'].'event/'.$join->event->slug ?></a> </td>
  </tr>
</table>

<h4 style="margin:20px 0 10px 7px;">Informasi Tiket</h4>
<table style="margin-bottom:25px" class="standarttb">
  <tr>
    <td>Nama Lengkap</td>
    <td>:</td>
    <td><?= $join->name?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td><?= $join->email ?></td>
  </tr>
  <tr>
    <td>Nomer Telephone</td>
    <td>:</td>
    <td><?= $join->no_phone ?></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><?= $join->address ?></td>
  </tr>
  <tr>
    <td>Kategori Tiket</td>
    <td>:</td>
    <td><?= $join->ticket->category ?></td>
  </tr>
  <tr>
    <td>Harga Satuan</td>
    <td>:</td>
    <td>Rp. <?= number_format($join->ticket->price) ?></td>
  </tr>
  <tr>
    <td>Quantity</td>
    <td>:</td>
    <td><?= $join->quantity ?></td>
  </tr>
  <tr>
    <td>Total Price</td>
    <td>:</td>
    <td>Rp. <?= number_format($join->total_price) ?></td>
  </tr>
</table>

<p style="color:red; font-size:12px; margin-top:20px; font-style:italic">* Tunjukan email ini saat anda akan melakukan check in pada event tersebut</p>
</div>
