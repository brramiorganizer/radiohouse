<?php
use app\helper\Helper;
?>
<p><?= Helper::getDataContent(6) ?></p>

<table class="standarttb" style="margin-top:30px">
  <tr>
    <td>Radio Name</td>
    <td>:</td>
    <td><?= $model->radio->radio_name ?></td>
  </tr>
  <tr>
    <td>Bank Tujuan</td>
    <td>:</td>
    <td><?= $model->bank->name ?></td>
  </tr>
  <tr>
    <td>Atas Nama Rekening</td>
    <td>:</td>
    <td><?= $model->rekening_name ?></td>
  </tr>
  <tr>
    <td>Nomer Rekening</td>
    <td>:</td>
    <td><?= $model->rekening_number ?></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td><?= $model->description ?></td>
  </tr>
</table>
