<?php
use app\helper\Helper;
?>
<h5>Hi, <?= $personal->name ?></h5>
<p><?= Helper::getDataContent(12); ?></p>
<div class="banks">
  <?php foreach($banks as $bank){ ?>
  <div class="item">
    <img src="http://lumbungschool.com/<?= $bank->image ?>" alt="">
    <p style="font-style:italic">(<?= $bank->rekening_name ?> : <?= $bank->rekening_number ?>)</p>
  </div>
<?php } ?>
</div>

<div class="invoice">
  <table style="border-collapse:collapse">
    <tr>
      <td colspan="2" style="width:100%; padding:20px; text-align:right"> <h3>Invoice</h3> </td>
    </tr>
    <tr>
      <td style="width:50%; padding:20px;">
        <h4>Paket Utama</h4>
        <h4><?= $plan->name ?></h4>
        <?php foreach($packages as $package){ ?>
        <p style="margin:0"><?= $package->planfeature->name ?></p>
        <?php } ?>
      </td>
      <td style="text-align:center;width:50%; padding:20px;">
        Rp. <?= number_format($plan->price) ?> / tahun
      </td>
    </tr>
    <tr>
      <td style="text-align:center; background:#000; color:#fff; font-style:bold;width:50%; padding-right:20px"><h4>Total</h4></td>
      <td style="text-align:center; font-style:bold;width:50%;"><h4>Rp. <?= number_format($plan->price) ?></h4></td>
    </tr>
  </table>
</div>
<p style="text-align:center; margin-top:25px">
  <a href="<?= Yii::$app->params['base_url'] ?>pricing/confirm?token=<?= $token ?>" class="btn-default">Konfirmasi Pembayaran</a>
</p>
