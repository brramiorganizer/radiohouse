<?php
use app\helper\Helper;
?>
<h5><?= Helper::getDataContent(7) ?></h5>

<table>
	<tr>
		<td>Nama</td>
		<td>:</td>
		<td><?= $model->name ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td><?= $model->email ?></td>
	</tr>
	<tr>
		<td>Pesan</td>
		<td>:</td>
		<td><?= $model->message ?></td>
	</tr>
	<tr>
		<td>Device</td>
		<td>:</td>
		<td><?= $model->device ?></td>
	</tr>
</table>
