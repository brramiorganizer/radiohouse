<?php
use app\helper\Helper;
?>
<p><?= Helper::getDataContent(9) ?></p>




<h4 style="margin:20px 0 10px;">Data Informasi Pembeli</h4>
<table style="margin-bottom:25px" class="standarttb">
	<tr>
		<td>Nama Lengkap</td>
		<td>:</td>
		<td><?= $join->name?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td><?= $join->email ?></td>
	</tr>
	<tr>
		<td>Nomer Telephone</td>
		<td>:</td>
		<td><?= $join->no_phone ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td><?= $join->address ?></td>
	</tr>
</table>

<h4 style="margin:0px 0 10px;">Data Informasi Tiket</h4>
<table style="margin-bottom:25px" class="standarttb">
	<tr>
		<td>Event</td>
		<td>:</td>
		<td><a href="<?= Yii::$app->params['base_url'] ?>event/<?= $join->event->slug ?>"><?= $join->event->title ?></a></td>
	</tr>
	<tr>
		<td>Kategori Tiket</td>
		<td>:</td>
		<td><?= $join->ticket->category ?></td>
	</tr>
	<tr>
		<td>Harga Satuan</td>
		<td>:</td>
		<td>Rp. <?= number_format($join->ticket->price) ?></td>
	</tr>
	<tr>
		<td>Quantity</td>
		<td>:</td>
		<td><?= $join->quantity ?></td>
	</tr>
	<tr>
		<td>Total Price</td>
		<td>:</td>
		<td>Rp. <?= number_format($join->total_price) ?></td>
	</tr>
</table>


<h4 style="margin:0px 0 10px;">Data Informasi Konfirmasi Pembayaran</h4>
<table style="margin-bottom:15px" class="standarttb">
	<tr>
		<td>Tujuan Bank</td>
		<td>:</td>
		<td><?= $confirmation->bank->name ?> (<?= $confirmation->bank->rekening_number ?>)</td>
	</tr>
	<tr>
		<td>Nomer Rekening</td>
		<td>:</td>
		<td><?= $confirmation->rekening_number ?></td>
	</tr>
	<tr>
		<td>Atas Nama Rekening</td>
		<td>:</td>
		<td><?= $confirmation->rekening_name ?></td>
	</tr>
  <tr>
		<td>Catatan</td>
		<td>:</td>
		<td><?= $confirmation->description ?></td>
	</tr>
	<tr>
		<td>Total</td>
		<td>:</td>
		<td>Rp. <?= number_format($confirmation->total) ?></td>
	</tr>
</table>
