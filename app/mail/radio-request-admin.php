<?php
use app\helper\Helper;
?>
<p><?= Helper::getDataContent(11) ?></p>

<table class="standarttb" style="margin-top:30px">
  <tr>
    <td>Company Name</td>
    <td>:</td>
    <td><?= $model->company_name ?></td>
  </tr>
  <tr>
    <td>Radio Name</td>
    <td>:</td>
    <td><?= $model->radio_name ?></td>
  </tr>
  <tr>
    <td>Name</td>
    <td>:</td>
    <td><?= $personal->name ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td><?= $personal->email ?></td>
  </tr>
  <tr>
    <td>Telephone</td>
    <td>:</td>
    <td><?= $personal->telephone ?></td>
  </tr>
  <tr>
    <td>Handphone</td>
    <td>:</td>
    <td><?= $personal->handphone ?></td>
  </tr>
</table>
