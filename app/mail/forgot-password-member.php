<h5>Hi, <?= $model->user->firstname . ' ' . $model->user->lastname?></h5>
<p>untuk mengembalikan akun anda, silahkan klik tombol di bawah ini untuk merubah password</p>

<p style="margin:50px 0; text-align:center">
  <a href="<?= Yii::$app->params['base_url']?>member/change-password-forgot?id=<?= $model->token ?>" class="btn-default">Password Baru</a>
</p>
