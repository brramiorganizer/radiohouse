<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style media="screen">
      .wrapper{
        width: 600px;
        background: #eee;
        padding: 50px;
      }
      .header{
        background: #333;
        padding: 35px;
        text-align: center
      }
      .header img{
        height: 70px;
        margin: auto
      }
      .content{
        background: #fff;
        padding: 35px;
      }
      .footer{
        background: #333333;
        padding: 5px;
        color:#fff
      }
      .footer p{
        text-align: center;
        font-size: 12px
      }
      .btn-default{
        background: #17c405;
        display: inline-block;
        padding: 10px 25px;
        font-size: 12px;
        text-transform: uppercase;
        letter-spacing: 1px;
        text-decoration: none;
        color:#fff
      }
      .btn-default:hover{
        text-decoration: none
      }
      .banks{
        overflow: hidden;
        margin: 45px 0;
      }
      .banks .item{
        width: 50%;
        text-align: center;
        float: left;
        margin-top: 15px
      }
      .banks .item img{
        margin:auto
      }
      .invoice{
        padding: 20px;
        overflow: hidden;
      }
      .invoice table{
        width: 100%
      }
      .invoice table td{
        border:1px solid #000
      }
      .standarttb td{
        padding: 3px 7px
      }
      .success-ye table{
        border-collapse: collapse;
        margin-left: -7px
      }
      .success-ye table td{
        border:none !important;
        padding:5px 7px
      }
    </style>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrapper">
      <div class="header">
        <!-- <img src="<?= Yii::$app->params['base_url']  ?>web/images/logo-white.png" alt=""> -->
        <img src="http://lumbungschool.com/radiohouse.png" alt="">

      </div>
      <div class="content">
        <?= $content ?>
      </div>
      <div class="footer">
        <p>Copyright © <?= Date('Y') ?>. RADIOHOUSE.com</p>
      </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
