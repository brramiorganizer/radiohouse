<?php
use app\helper\Helper;
?>
<p><?= Helper::getDataContent(21) ?></p>

<h4 style="margin:20px 0 10px;">Data Informasi Pembeli</h4>
<table style="margin-bottom:25px" class="standarttb">
	<tr>
		<td>Nama Lengkap</td>
		<td>:</td>
		<td><?= $join->name?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td><?= $join->email ?></td>
	</tr>
	<tr>
		<td>Nomer Telephone</td>
		<td>:</td>
		<td><?= $join->no_phone ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td><?= $join->address ?></td>
	</tr>
</table>

<h4 style="margin:0px 0 10px;">Data Informasi Tiket</h4>
<table style="margin-bottom:25px" class="standarttb">
	<tr>
		<td>Kategori Tiket</td>
		<td>:</td>
		<td><?= $join->ticket->category ?></td>
	</tr>
	<tr>
		<td>Harga Satuan</td>
		<td>:</td>
		<td>Rp. <?= number_format($join->ticket->price) ?></td>
	</tr>
	<tr>
		<td>Quantity</td>
		<td>:</td>
		<td><?= $join->quantity ?></td>
	</tr>
	<tr>
		<td>Total Price</td>
		<td>:</td>
		<td>Rp. <?= number_format($join->total_price) ?></td>
	</tr>
</table>
