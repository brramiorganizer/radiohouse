<?php
use app\helper\Helper;
?>
<h5>Hi, <?= $model->radio_name ?></h5>

<p><?= Helper::getDataContent(10) ?></p>
<h4>Package Plan : <?= $plan->plan->name?></h4>
<ul style="padding:0 !important">
  <?php foreach($packages as $package){ ?>
    <li><?= $package->planfeature->name ?></li>
  <?php } ?>
</ul>
<p>Berikut URL akses login untuk Radio Member : <a href="<?= Yii::$app->params['base_url'].'radio/login' ?>"><?= Yii::$app->params['base_url'].'radio/login' ?></a> </p>
<p> Jika ada yang ingin di tanyakan, anda bisa langsung hubungi kami.</p>

<p style="margin-top:40px">Terima kasih.</p>
