<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'dashboard' => [
            'class' => 'app\modules\dashboard\Dashboard',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '4vR7cG0QKY5_sJKxBFwI2KQg5EaXWmG-',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              'logout-member' => 'site/logout-member',
              'member-active' => 'site/member-active',
              'informasi-akun' => 'member/index',
              'news/<slug>' => 'news/detail',
              'event/<slug>' => 'event/detail',
              'channel/<slug>' => 'channel/detail',
              'complete' => 'radio/complete',
              '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],


        'authClientCollection' => [
          'class' => 'yii\authclient\Collection',
          'clients' => [
            'facebook' => [
              'class' => 'yii\authclient\clients\Facebook',
              'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
              'clientId' => '868087446682940',
              'clientSecret' => 'c49eea27128706a8cd55ba9246667f65',
              'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
            ],
            'google' => [
                  'class' => 'yii\authclient\clients\Google',
                  'clientId' => '322979677720-eurcib8j4slhe3jr8j0qvp330uv7qkh4.apps.googleusercontent.com',
                  'clientSecret' => 'mjayj3D7s21y_y8uTJYw0iHd',
              ],
          ],
        ],




        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                // 'username' => 'agungseptiyadi.developer@gmail.com',
                // 'password' => 'wikramagung',
                'username' => 'radiohouse.id@gmail.com',
                'password' => 'uvhvjtkyfsbzskiw',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],




    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
