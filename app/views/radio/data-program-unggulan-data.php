<?php foreach($datum as $data){ ?>
<tr>
  <td></td>
  <td><?= $data->name ?></td>
  <td><?= $data->listener ?></td>
  <td><?= $data->kind_program ?></td>
  <td><?= $data->music ?></td>
  <td class="action">
    <a href="#" data-id="<?= $data->id ?>" class="edit-program"><i class="fa fa-pencil"></i></a>
    <a href="#" data-id="<?= $data->id ?>" class="delete-program"><i class="fa fa-trash" ></i></a>
  </td>
</tr>
<?php } ?>
<script type="text/javascript">
var tc = $('#program-unggulan-data').DataTable( {
  "columnDefs": [ {
      "searchable": false,
      "orderable": false,
      "targets": [0,5]
  } ],
  // "order": [[ 1, 'asc' ]]
} );

tc.on( 'order.dt search.dt', function () {
  tc.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      cell.innerHTML = i+1;
  } );
} ).draw();


$('.delete-program').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var thisis = $(this).parents('tr');
  if(confirm('Anda yakin untuk menghapus nya ?')){
    $.ajax({
      url : '<?= Yii::$app->params['base_url']?>radio/delete-program',
      type : 'POST',
      data : { id : id , _csrf : $('#gils').val()},
      success : function(){
        tc.row(thisis).remove().draw();
      }
    });
  }
});


$('.edit-program').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $.ajax({
    url : '<?= Yii::$app->params['base_url']?>radio/edit-program',
    type : 'POST',
    data : { id : id , _csrf : $('#gils').val()},
    dataType : 'JSON',
    success : function(data){
      $('#f_data_program_unggulan input[name=id]').val(data.id);
      $('#f_data_program_unggulan input[name=name]').val(data.name);
      $('#f_data_program_unggulan input[name=listener]').val(data.listener);
      $('#f_data_program_unggulan input[name=kind_program]').val(data.kind_program);
      $('#f_data_program_unggulan input[name=music]').val(data.music);
      tinymce.get('descriptions').setContent(data.content_information);
      $('#f_data_program_unggulan input[name=average_listener]').val(data.average_listener);
    }
  });
});
</script>
<style>
#f_data_program_unggulan{
  margin-bottom:50px
}
  #program-unggulan-data td , #program-unggulan-data th{
    text-align: center !important
  }
</style>
