<h3>Data Personal</h3>
<form id="f_data_personal">
  <div class="form-group">
    <label for="">Name</label>
    <input type="text" name="name" class="form-control" value="<?= $personal->name ?>">
  </div>
  <div class="form-group">
    <label for="">Email</label>
    <input type="email" disabled name="email" class="form-control" value="<?= $personal->email ?>">
  </div>
  <div class="form-group">
    <label for="">Telephone</label>
    <input type="text" name="telephone" class="form-control" value="<?= $personal->telephone ?>">
  </div>
  <div class="form-group">
    <label for="">Handphone</label>
    <input type="text" name="handphone" class="form-control" value="<?= $personal->handphone ?>">
  </div>
  <div class="form-group">
    <label for="">Address</label>
    <textarea name="address" class="form-control"><?= $personal->address ?></textarea>
  </div>
  <div class="form-group">

    <button type="submit" class="btn btn-success" name="button">Save All</button>
  </div>
</form>

<script type="text/javascript">
  $('#f_data_personal').validate({
    rules : {
      name : {
        required : true
      },
      email : {
        required : true
      },
      telephone : {
        required : true
      },
      handphone : {
        required : true
      },
      address : {
        required : true
      }
    },
    messages : {
      name : {
        required : 'please fill your name'
      },
      email : {
        required : 'please fill your email'
      },
      telephone : {
        required : 'please fill the telephone number'
      },
      handphone : {
        required : 'please fill the handphone number'
      },
      address : {
        required : 'please fill the final address'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val();
      $('#f_data_personal button').prop('disabled',true).html('saving ...');
      $('#f_data_personal .alert').remove();
      $.ajax({
        url : '<?= Yii::$app->params['base_url']?>radio/change-personal',
        data : dataform,
        type : 'POST',
        success : function(data){
          $('#f_data_personal button').prop('disabled',false).html('Save All');
          $('#f_data_personal').append('<div class="alert alert-success">Data tersimpan</div>');
        }

      });
      return false;
    }
  });
</script>
