<div class="container content">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <p>Untuk mengaktifkan Radio anda, isi form dibawah ini dengan benar.</p>
      <form id="activate-radio">
        <div class="form-group">
          <label for="">Password <span class="required">*</span></label><br/>
          <input type="password" class="form-control" name="password" id="password" value="">
        </div>
        <div class="form-group">
          <label for="">Ulang Password <span class="required">*</span></label><br/>
          <input type="password" class="form-control" name="repassword" value="">
        </div>
        
        <div class="form-group">
          <input type="hidden" name="radio_id" value="<?= $radio_id ?>">
          <button type="submit" name="button" class="btn btn-asli btn-block">F I N I S H</button>
        </div>
      </form>
    </div>
  </div>
</div>
