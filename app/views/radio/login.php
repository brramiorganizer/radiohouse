<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Admin management | Radio House</title>
    <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/favicon.png" rel="shortcut icon" type="image/png">
 <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon.png" rel="apple-touch-icon">
 <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
 <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
 <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <style media="screen">
      body{
        background: #6ABC52
      }
      .background {
	width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
	background-color: yellow;
}
.content img{
  margin-bottom: 10px
}
label.error{
  color: red;
  text-align: left !important;
  display: block;
  margin-left: 3px;
  font-size: 13px
}
.content {
	width: 300px;
  height: 345px;
    max-height: 300px;
    text-align: center;

	position:absolute;
    left:0; right:0;
    top:0; bottom:0;
	margin:auto;

	max-width:100%;
	max-height:100%;
	overflow:auto;
  background: #ffffff;
    box-shadow: 0px 0px 31px 0.5px #aaa;
    padding: 50px

}
#login-radio input{
display: block;
  background:#F2F2F2;
  padding: 15px;
  outline: none;
  border:none;
  width: 100%;
  box-sizing: border-box;
  font-size: 14px;
  margin-top: 15px
}
#login-radio button{
  background: #6ABC52;
width: 100%;
padding: 10px;
border: none;
color: #fff;
font-size: 14px;
letter-spacing: 1px;
cursor: pointer;
    box-shadow: 0px 1px 1px #176b59;
    transition: 0.1s;
    margin-top: 15px;
    outline: none
}
#login-radio button:hover{
  opacity: 0.9;
  box-shadow: none
}
#login-radio .form-group{
  height: 62px
}
.alert.alert-danger{
  background: rgba(236,100,75,0.8);
    margin-top: 5px;
    padding: 10px;
    color: white;
    text-transform: uppercase;
    font-size: 12px;
    letter-spacing: 1px;
}
    </style>
  </head>
  <body>
    <input type="hidden" id="base" value="<?= Yii::$app->params['base_url'] ?>">
    <input type="hidden" id="gils" value="<?= Yii::$app->request->csrfToken?>">
    <div class="background">
		    <div class="content">
          <img class="img-responsive" src="<?= Yii::$app->params['base_url'] ?>images/logo.png" alt="">
          <form id="login-radio" method="post">
            <div class="form-group">
              <input type="email" name="email"  placeholder="email">
            </div>
            <div class="form-group">
              <input type="password" name="password"  placeholder="password">
            </div>

            <button type="submit" name="button">LOGIN</button>
          </form>
          <p style="text-align:left"><a style="color:green; text-decoration:none;letter-spacing:1px;font-size:13px" href="<?= Yii::$app->params['base_url'] ?>radio/forgot-password">forgot password ?</a> </p>
        </div>
    </div>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.validate.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/login-radio.js"></script>
  </body>
</html>
