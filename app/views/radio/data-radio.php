<div class="profileChanneln mt-40">
        <div class="tableProfile">
            <h3 class="p-0 mb-20 mt-0">A. Data Personal</h3>
            <div class="table-responsive">
                <table class="table mb-10">
                    <tbody>
                        <tr>
                            <th style="width: 200px;">Nama Lengkap</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Email</th>
                            <td style="width: 10px;">:</td>
                            <td><a href="mailto:<?= $personal->name ?>"><?= $personal->name ?></a> </td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Alamat Lengkap</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->address ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Telephone &amp; Ext</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->telephone ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Handphone</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->handphone ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tableProfile mt-30">
            <h3 class="p-0 mb-20 mt-0">B. Data Perusahaan</h3>
            <div class="table-responsive">
                <table class="table mb-0">
                    <tbody>
                      <tr>
                          <th style="width: 200px;">Nomor ISR &amp; IPP</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $radio->number_isr_ipp ?></td>
                      </tr>
                        <tr>
                            <th style="width: 200px;">Nama Radio</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->radio_name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Nama Perusahaan</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->company_name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Alamat Lengkap Perusahaan</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->address ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Telephone &amp; Ext</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->telepon ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tableProfile mt-30">
            <h3 class="p-0 mb-0 mt-0">C. Data Pengurus</h3>
            <div class="table-responsive">
              <?php foreach($committees  as $x){ ?>
                <table class="table mb-30 mt-10">
                    <tbody>
                        <tr>
                            <th style="width: 200px;"><?= $x->position->name ?></th>
                            <td style="width: 10px;">:</td>
                            <td><?= $x->name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Alamat Lengkap</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $x->address ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Telephone/HP</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $x->telephone ?></td>
                        </tr>
                    </tbody>
                </table>
              <?php } ?>
            </div>
        </div>

        <a href="#" class="text-green mt-20" id="viewMoreProfile"><i class="fa fa-plus-square-o"></i> View More</a>

    </div>
