
<form id="form-edit-news-event" enctype="multipart/form-data" style="margin-top:50px">
  <input type="hidden" name="id" value="">
  <div class="row">
    <div class="col-md-8 col-sm-8">
      <div class="form-group">
        <label for="">Judul</label>
        <input type="text" class="form-control" name="title" value="">
      </div>
      <div class="form-group">
        <label for="">Deskripsi</label>
        <textarea id="description_cux" name="description" class="editortextarea-update"></textarea>
      </div>
    </div>
    <div class="col-md-4 col-sm-4">
      <div class="form-group">
        <label for="">Banner</label>
        <input type="file" id="file-imageya" name="banner" value="" class="setpreview">
        <img src="" class="getpreview img-responsive" >
      </div>
      <p style="color: red;font-size: 12px;margin-top: 40px;">* search engine optimization (SEO)</p>
      <div class="form-group">
        <label for="">Meta Description</label>
        <textarea name="meta_description" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <label for="">Meta keywords</label>
        <input type="text" name="meta_keywords" class="form-control taginput"/>
      </div>
    </div>
  </div>



  <div class="form-group">
    <input type="hidden" name="radio_id" value="<?= Yii::$app->session->get('radiotoken') ?>">
    <button type="submit" name="button" class="btn btn-success">simpan</button>
  </div>
</form>
<style media="screen">
  .tagsinput{
    width: 100% !important
  }
  .mce-notification-error{
    display: none !important
  }
</style>
<script type="text/javascript">
setTimeout(function(){



  $('.taginput').tagsInput();

  $('#form-edit-news-event').validate({
    rules : {
      title : {
        required : true
      },
      description : {
        required : true
      }
    },
    messages : {
      title : {
        required : 'judul wajib di isi'
      },
      description : {
        required : 'konten wajib di isi'
      }
    },
    submitHandler : function(form){
      $('.loading').show();
      var kuy = $('#form-edit-news-event')[0];
      var formData = new FormData(kuy);
      // if( document.getElementById("file-imageya").files.length != 0 ){
      //   formData.append('zip_file', $('input[type=file]')[0].files[0]);
      // }
      formData.append('_csrf', $('#gils').val());
      formData.append('content', tinymce.get('description_cux').getContent());

      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>radio/update-news-event',
        type : 'POST',
        //dataType : 'JSON',
        contentType: false,
        cache: false,
        processData:false,
        data : formData,
        success : function(data){
          setTimeout(function(){
            location.reload();
          },1000);
        }
      });

      return false;
    }
  });
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.getpreview').attr('src', e.target.result).css('margin','15px 0');
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $(".setpreview").change(function(){
      readURL(this);
  });


},2000);

</script>
