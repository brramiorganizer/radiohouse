<div class="container content">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <p>Password Baru.</p>
      <form id="new-password-radio">
        <input type="hidden" name="radio_id" value="<?= $model->radio_id ?>">
        <div class="form-group">
          <label for="">New Password <span class="required">*</span></label><br/>
          <input type="password" class="form-control" name="password" id="password" value="">
        </div>
        <div class="form-group">
          <label for="">Retype New Password <span class="required">*</span></label><br/>
          <input type="password" class="form-control" name="repassword" id="repassword" value="">
        </div>

        <div class="form-group">
          <button type="submit" name="button" class="btn btn-asli btn-block">F I N I S H</button>
        </div>
      </form>
    </div>
  </div>
</div>
