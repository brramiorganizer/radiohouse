<h3>Data Perusahaan</h3>
<form id="f_data_company">
  <div class="form-group">
    <label for="">Number ISR & IPP</label>
    <input type="text" name="number_isr_ipp" class="form-control" value="<?= $model->number_isr_ipp ?>">
  </div>
  <div class="form-group">
    <label for="">Nama Perusahaan</label>
    <input type="text"  name="company_name" class="form-control" value="<?= $model->company_name ?>">
  </div>
  <div class="form-group">
    <label for="">Nama Radio</label>
    <input type="text" name="radio_name" class="form-control" value="<?= $model->radio_name ?>">
  </div>
  <div class="form-group">
    <label for="">Telephone / Handphone</label>
    <input type="text" name="telepon" class="form-control" value="<?= $model->telepon ?>">
  </div>
  <div class="form-group">
    <label for="">Address</label>
    <textarea name="address" class="form-control"><?= $model->address ?></textarea>
  </div>
  <div class="form-group">
    <label for="">Deskripsi Profil</label>
    <textarea  id="desprofil" class="form-control"><?= $model->description ?></textarea>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success" id="saveall" name="button">Save All</button>
  </div>
</form>

<script type="text/javascript">
tinymce.init({
  selector: '#desprofil',
  height: 300,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
  $('#f_data_company').validate({
    rules : {
      number_isr_ipp : {
        required : true
      },
      company_name : {
        required : true
      },
      radio_name : {
        required : true
      },
      telepon : {
        required : true
      },
      address : {
        required : true
      }
    },
    messages : {
      number_isr_ipp : {
        required : 'please fill number ISP & IPP'
      },
      company_name : {
        required : 'please fill company name'
      },
      radio_name : {
        required : 'please fill radio name'
      },
      telepon : {
        required : 'please fill phone number'
      },
      address : {
        required : 'please fill the address'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val()+'&description='+tinymce.get('desprofil').getContent();
      $('#f_data_company #saveall').prop('disabled',true).html('saving ...');
      $('#f_data_company .alert').remove();
      $.ajax({
        url : '<?= Yii::$app->params['base_url']?>radio/change-company',
        data : dataform,
        type : 'POST',
        success : function(data){
          $('#f_data_company #saveall').prop('disabled',false).html('Save All');
          $('#f_data_company').append('<div class="alert alert-success">Data tersimpan</div>');
        }

      });
      return false;
    }
  });
</script>
