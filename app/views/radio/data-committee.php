<h3>Data Pengurus</h3>
<form id="f_data_committee">
  <h4 style="margin-top:30px; margin-bottom:0">Direktur Utama</h4>
  <div class="form-group">
    <label for="">Nama</label>
    <input type="text" name="name_1" class="form-control" value="<?= $direktur->name ?>">
  </div>
  <div class="form-group">
    <label for="">Alamat</label>
    <input type="text"  name="address_1" class="form-control" value="<?= $direktur->address ?>">
  </div>
  <div class="form-group">
    <label for="">No Telephone / Handphone</label>
    <input type="text" name="telephone_1" class="form-control" value="<?= $direktur->telephone ?>">
  </div>
  <hr>

  <h4 style="margin-top:30px; margin-bottom:0">Marketing Manager</h4>
  <div class="form-group">
    <label for="">Nama</label>
    <input type="text" name="name_2" class="form-control" value="<?= $marketing->name ?>">
  </div>
  <div class="form-group">
    <label for="">Alamat</label>
    <input type="text"  name="address_2" class="form-control" value="<?= $marketing->address ?>">
  </div>
  <div class="form-group">
    <label for="">No Telephone / Handphone</label>
    <input type="text" name="telephone_2" class="form-control" value="<?= $marketing->telephone ?>">
  </div>

<hr>

  <h4 style="margin-top:30px; margin-bottom:0">Personal Ditunjuk</h4>
  <div class="form-group">
    <label for="">Nama</label>
    <input type="text" name="name_3" class="form-control" value="<?= $personal->name ?>">
  </div>
  <div class="form-group">
    <label for="">Alamat</label>
    <input type="text"  name="address_3" class="form-control" value="<?= $personal->address ?>">
  </div>
  <div class="form-group">
    <label for="">No Telephone / Handphone</label>
    <input type="text" name="telephone_3" class="form-control" value="<?= $personal->telephone ?>">
  </div>

  <div class="form-group">

    <button type="submit" class="btn btn-success" name="button">Save All</button>
  </div>
</form>
<style media="screen">
  .form-group label{
    font-size: 13px
  }
</style>
<script type="text/javascript">
  $('#f_data_committee').validate({
    rules : {
      name_1 : {
        required : true
      },
      name_2 : {
        required : true
      },
      name_3 : {
        required : true
      },
      address_1 : {
        required : true
      },
      address_2 : {
        required : true
      },
      address_3 : {
        required : true
      },
      telephone_1 : {
        required : true
      },
      telephone_2 : {
        required : true
      },
      telephone_3 : {
        required : true
      }
    },
    messages : {
      name_1 : {
        required : 'nama direktur utama harus diisi'
      },
      name_2 : {
        required : 'nama marketing manager harus diisi'
      },
      name_3 : {
        required : 'nama personal ditunjuk harus diisi'
      },
      address_1 : {
        required : 'alamat direktur utama harus diisi'
      },
      address_2 : {
        required : 'alamat marketing manager harus diisi'
      },
      address_3 : {
        required : 'alamat personal ditunjuk harus diisi'
      },
      telephone_1 : {
        required : 'no telephone / handphone direktur utama harus diisi'
      },
      telephone_2 : {
        required : 'no telephone / handphone marketing manager harus diisi'
      },
      telephone_3 : {
        required : 'no telephone / handphone personal ditunjuk harus diisi'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val();
      $('#f_data_committee button').prop('disabled',true).html('saving ...');
      $('#f_data_committee .alert').remove();
      $.ajax({
        url : '<?= Yii::$app->params['base_url']?>radio/change-committee',
        data : dataform,
        type : 'POST',
        success : function(data){
          $('#f_data_committee button').prop('disabled',false).html('Save All');
          $('#f_data_committee').append('<div class="alert alert-success">Data tersimpan</div>');
        }

      });
      return false;
    }
  });
</script>
