<div class="wrap-dataticket">
<table id="ticket-data-datatable" class="table">
  <thead>
    <tr>
      <th>#</th>
      <th>Nama Pembeli</th>
      <th>Kode Tiket</th>
      <th>Event</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($datum as $d){ ?>
    <tr>
      <td></td>
      <td><?= $d->name ?></td>
      <td>RH-<?= $d->code ?></td>
      <td><?= $d->event->title ?></td>
      <th><?= $d->status ?></th>
      <td>
        <a href="#" data-id="<?= $d->id ?>" class="detail-ticket">detail</a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<div class="wrap-detailticket">
  <a href="#" id="backtodata"><i class="fa fa-caret-left"></i> kembali</a>

  <div class="content-data" style="margin-top:10px">
    <p>loading ...</p>
  </div>
</div>

<style media="screen">
  .wrap-detailticket{
    display: none
  }
</style>
<script type="text/javascript">
$('#backtodata').on('click',function(e){
  e.preventDefault();
  $('.wrap-detailticket').hide();
  $('.wrap-dataticket').show();

});
$('.detail-ticket').on('click',function(e){
  e.preventDefault();
  $('.wrap-detailticket').show();
  $('.wrap-dataticket').hide();
  $('#ticket-data-datatable .wrap-detailticket .content-data').html('loading ...');
  var id = $(this).data('id');
  $.ajax({
    url : '<?= Yii::$app->params['base_url'] ?>radio/data-ticket-detail',
    type : 'POST',
    data : {
      id : id,
      _csrf : $('#gils').val()
    },
    success : function(data){
      $('.wrap-detailticket .content-data').html(data);
    }
  });
});
var hg = $('#ticket-data-datatable').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,4,5]
    } ],
    "order": [[ 1, 'asc' ]]
} );

hg.on( 'order.dt search.dt', function () {
    hg.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();
</script>
