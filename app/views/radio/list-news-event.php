<table id="list-news-event2" class="table">
  <thead>
    <tr>
      <th>#</th>
      <th>Tipe</th>
      <th>Judul</th>
      <th>URL</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($newsevents as $z){ ?>
    <tr>
      <td></td>
      <td><?= $z->type ?></td>
      <td><?= $z->title ?></td>
      <td><a target="_blank" href="<?= Yii::$app->params['base_url'].$z->type.'/'.$z->slug ?>">detail</a> </td>
      <td class="actnya">
        <a data-id="<?= $z->id ?>" class="delete-ne" href="#">hapus</a>
        <a data-id="<?= $z->id ?>" class="edit-ne" href="#">edit</a>
        <?php if($z->type == 'event'){ ?>
        <a data-id="<?= $z->id ?>" href="#"  class="open-ticket">tiket</a>
        <?php } ?>
      </td>
    </tr>
  <?php } ?>
  </tbody>
</table>
<style media="screen">
#list-news-event2 td a{
  color:#17c405
}
  #list-news-event2 th , #list-news-event2 td{
    text-align: center;
  }
  #list-news-event2 th{
    background: #17c405;
    color:#fff;
  }
  #list-news-event2{
    border:1px solid #17c405
  }
  .actnya a{
    display: inline-block;
    padding:3px 5px;
    border-bottom: 1px dotted transparent;
  }
  .actnya a:hover{
    border-bottom: 1px dotted #17c405;
  }
  .modal{
    overflow-y: auto
  }
</style>
<script type="text/javascript">
setTimeout(function(){
  var t = $('#list-news-event2').DataTable( {
      "columnDefs": [ {
          "searchable": false,
          "orderable": false,
          "targets": [0,4]
      } ]
  } );

  t.on( 'order.dt search.dt', function () {
      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
  } ).draw();


},1000);


$('.actnya .delete-ne').on('click',function(e){
  e.preventDefault();
  var tablenya = $('#list-news-event2').DataTable().row($(this).parents('tr'));
  var id = $(this).data('id');
  if(confirm('Anda yakin akan menghapusnya ?')){
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>news-events/delete',
      type : 'POST',
      data : {
        id : id,
        _csrf : $('#gils').val()
      },
      dataType : 'JSON',
      success : function(data){
        if(data.status){
          tablenya.remove().draw();
        }else{
          location.reload();
        }
      }
    });
  }
});

$('.open-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $('#ticket-event').modal('show');
  $('#list-news-event').modal('hide');
  $.ajax({
    url : '<?= Yii::$app->params['base_url'] ?>radio/modal-ticket',
    type : 'POST',
    data : {
      id : id,
      _csrf : $('#gils').val()
    },
    success : function(data){
      $('#ticket-event .modal-body').html(data);
    }
  });
});


$('.edit-ne').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $('#edit-news-event').modal('show');
  $('#list-news-event').modal('hide');

  $.ajax({
    url : '<?= Yii::$app->params['base_url'] ?>radio/get-news-event',
    type : 'POST',
    data : {
      id : id,
      _csrf : $('#gils').val()
    },
    dataType : 'JSON',
    success : function(data){

      setTimeout(function(){
        $('#form-edit-news-event input[name=id]').val(data.data.id);
        $('#form-edit-news-event input[name=title]').val(data.data.title);
        $('#description_cux').html(data.data.description);
        $('#form-edit-news-event textarea[name=meta_description]').html(data.data.meta_description);
        $('input[name=meta_keywords]').importTags(data.data.meta_keywords);
        $('#form-edit-news-event .getpreview').attr('src','<?= Yii::$app->params['base_url'] ?>media/banner-news-event/'+data.data.banner);

        tinymce.init({
  selector: '.editortextarea-update',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });


      },100);
    }
  });

});
</script>
