<section class="notification pt-60">
    <div class="container">
        <div class="row m-0">
            <div class="col-md-12 success-message text-center bg-success">
                <p><i class="fa fa-check"></i></p>
                <p>Terima Kasih telah melakukan pembelian tiket di Radio House</p>
            </div>
        </div>
    </div>
</section>

<section class="pagePembayaran pt-60 pb-70 pt-sm-60">

    <div class="container page-information-process page-payment">
        <div id="processRent">
            <form method="" action="">
                <div class="col-md-7">
                    <h2 class="mt-0 mb-0">Detail Pemesanan</h2>
                    <div class="table-responsive">
                        <h3 class="mt-30"><i class="fa fa-user mr-10"></i>Personal Data</h3>
                        <table class="table">
                            <tr>
                                <td class="pl-25"><strong>Nama Lengkap</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $event->name ?></td>
                            </tr>
                            <tr>
                                <td class="pl-25"><strong>Email</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $event->email ?></td>
                            </tr>
                            <!-- <tr>
                                <td class="pl-25"><strong>No Identitas</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;">0987654321</td>
                            </tr> -->
                            <tr>
                                <td class="pl-25"><strong>No Handphone</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $event->no_phone ?></td>
                            </tr>
                            <tr>
                                <td class="pl-25"><strong>Alamat Lengkap</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $event->address ?></td>
                            </tr>
                        </table>
                        <h3 class="mt-15"><i class="fa fa-ticket mr-10"></i>Tiket</h3>
                        <table class="table">
                            <tr>
                                <td class="pl-25"><strong>Event</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $model->title ?></td>
                            </tr>
                            <!-- <tr>
                                <td class="pl-25"><strong>Tanggal Event</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;">9 November 2017</td>
                            </tr> -->
                            <tr>
                                <td class="pl-25"><strong>Kategori Tiket</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $ticket->category ?></td>
                            </tr>
                            <tr>
                                <td class="pl-25"><strong>Jumlah</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;"><?= $event->quantity ?> Tiket</td>
                            </tr>
                            <!-- <tr>
                                <td class="pl-25"><strong>Kursi</strong></td>
                                <td class="text-center">:</td>
                                <td class="" style="width: 450px;">VIP A 01</td>
                            </tr> -->
                        </table>
                    </div>

                    <h2 class="mt-40 mb-30">Jumlah Transfer</h2>
                    <div class="TotalTrasfer">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="text-green T1">IDR <?= number_format($event->total_price) ?></p>
                            </div>
                            <div class="col-sm-6">
                                <p class="well">
                                    Mohon transfer sesuai jumlah yang tertera (termasuk 3 digit terakhir)
                                </p>
                            </div>
                        </div>
                    </div>

                    <h2 class="mt-40">Bank Tujuan Transfer</h2>
                    <div class="contentPilihPembayaran mt-20">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="mainPembayaran text-center">
                                    <img src="<?= Yii::$app->params['base_url'] ?>media/bank/<?= $bank->image ?>" class="" />
                                    <p ><?= $bank->name ?></p>
                                    <p><?= $bank->rekening_number ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2 class="mt-40">Konfirmasi Pembayaran</h2>
                    <div class="konfTrans marginbottom-40">
                        <p>Setelah Anda melakukan transfer, segera konfirmasikan dengan menekan tombol di bawah ini.</p>
                        <a class="btn btn-primary display-block font-18" href="<?= Yii::$app->params['base_url'] ?>konfirmasi?token=<?= $event->code ?>">Konfirmasi Pembayaran</a>
                    </div>

                </div>

                <div class="col-md-5">
                    <div class="sidebar-booking discount">
                        <h3 class="m-0 p-0 text-green">Invoice</h3>
                        <p class="text-right">RH-<?= $event->code ?></p>
                    </div>
                    <div class="sidebar-booking">
                        <h3 class="mt-0 mb-20 p-0 text-green">Keranjang</h3>
                        <div class="main-sidebar-booking">
                            <div class="row noteRent">
                                <div class="col-xs-5">
                                    <h4 class="mb-5 pr-0">Jumlah Tiket</h4>
                                    <!-- <p class="small">Tanggal Event : <strong>09/11/2017</strong></p> -->
                                </div>
                                <div class="col-xs-1 text-center"><?= $event->quantity ?></div>
                                <div class="col-xs-6 pl-0">IDR <?= number_format($ticket->price) ?></div>
                            </div>
                            <hr />
                            <!--<div class="row">
                                <div class="col-xs-5">
                                    <p class="font-15">Shipping</p>
                                </div>
                                <div class="col-xs-1 text-center">
                                </div>
                                <div class="col-xs-6">
                                    <p>IDR 100.000</p>
                                </div>
                            </div>
                            <hr />-->
                            <span class="totalPrice"><strong>Total</strong> IDR <?= number_format($event->total_price) ?></span>
                        </div>
                    </div>
                    <!--<div class="sidebar-booking discount">
                        <h3>KODE PROMOSI</h3>
                        <div class="form-group">
                            <label class="sr-only">Kode Promosi</label>
                            <input type="text" class="form-control" placeholder="Kode Promosi"/>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Apply</button>
                    </div>-->
                </div>
            </form>
        </div>
</div><!-- end container -->

</section><!--end white-wrapper -->
