<?php
  use app\helper\Helper;
?>
<audio class="hidden" controls="controls"><source src="<?= $radio->api ?>" type="audio/mpeg" /></audio>
<?php $banner = ($content->banner != '' ? Helper::base_url().'media/radio/banner/'.$content->banner : Helper::base_url().'media/general/radio-detail.jpg') ?>
<section class="banner-img withText" style="background-image: url(<?= $banner ?>);">

    <div class="change-banner-radio">
        <a href="#"><i class="fa fa-image"></i> ubah banner</a>
    </div>

    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row m-0">
            <div class="breadcrumb-banner detail-page-other">
                <ul class="m-0">
                    <li><a href="<?= Yii::$app->params['base_url'] ?>"><i class="fa fa-home"></i></a></li>
                    <li>Tentang Radio House</li>
                </ul>
                <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
            </div>
        </div>
    </div>
</section><!-- end post-wrapper-top -->

<!-- Radio Player -->
<section class="radio-palyer bg-green-primary">
    <div class="container">
        <div class="row m-0">
            <div class="col-sm-4 img-thumbRadioChannel hidden-mobileTablet">
                <div class="widget pl-15 pr-60">

                    <div class="containereditingprofile" style="z-index:4">
                      <div class="editingprofile" id="edit-photo-profile">
                        <h5>ubah foto profil</h5>
                      </div>
                      <?php if($content){ ?>
                        <?php if($content->photo_profile != ''){ ?>
                          <img style="height:305px; width:305px" src="<?= Helper::base_url() ?>media/radio/profile/<?= $content->photo_profile ?>" class="img-responsive" />
                        <?php }else{ ?>
                          <img src="/demos/800x800.png" class="img-responsive" />
                        <?php } ?>
                      <?php }else{ ?>
                        <img src="/demos/800x800.png" class="img-responsive" />
                      <?php } ?>

                    </div>

                </div>
            </div>
            <div class="col-sm-8 pt-15 pb-15 pr-sm-0 pl-sm-0 overflowMobile playerDetailChannel">
                <div id="audio-player">
                    <div id="buttons">
                <span>
                  <button id="play"></button>
                  <button id="pause"></button>
                </span>
              </div>
              <div id="tracker">
                        <div class="hidden-dekstop">
                            <h3 class="p-0 m-0">Radio House</h3>
                            <p class="">Jakarta</p>
                        </div>
                <div id="progress-bar">
                  <span id="progress"></span>
                            <p class="timer-player">0:00</p>
                </div>
              </div>
                    <div id="volume">
                        <span class="dropdown-hover">
                            <button id="volumeup"></button>
                  <button id="volumemute"></button>
                        </span>
                        <div class="dropdown-menu-volume">
                            <div id="master" style="width:100px;"></div>
                        </div>
                    </div>
                    <div class="button-share-radio ml-xs-10">
                        <span class="dropdown">
                            <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span></span></a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                              <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= Yii::$app->params['base_url'] ?>channel/<?= $radio->slug ?>" target="_blank" class="face"><i class="fa fa-facebook"></i></a></li>
                              <li><a target="_blank"  href="https://twitter.com/intent/tweet?text=<?= strip_tags($radio->radio_name) . ' ' .Yii::$app->params['base_url'].'channel/'.$radio->slug?>" data-size="large"class="twit"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </span>
                    </div>
                    <div class="radio-change">
                      <a href="#">change api</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- end Radio Player -->

<section class="page-under pt-60 pb-60 detail-channel">
  <div class="container">
        <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-165 mt-sm-0 pr-sm-15">

            <!-- images Radio Channel Mobile -->
            <div class="imgRadioChannel">
                <div class="widget mb-30 mb-sm-40 hidden-dekstop bannerRadio">
                    <img src="/demos/800x800.png" class="img-responsive" />
                </div>
            </div>
            <!-- end image radio channel mobile -->

            <!-- images banner sale Dekstop -->
            <div class="imageBannerSale hiddenMobile ">
                <div class="widget iklan-ke-1 mb-30" style="margin-top:167px">
                  <div class="containereditingprofile iklan1" data-of="1" style="z-index:4">
                    <div class="editingprofile">
                      <h5>ubah iklan 1</h5>
                    </div>
                  </div>
                  <?php $adv1 =  $content->advertisment_1 == '' ? '/demos/800x800.png' :  Helper::base_url().'media/radio/iklan/'.$content->advertisment_1?>
                    <img style="height:305px; width:305px" src="<?= $adv1 ?>" class="img-responsive" />
                </div>
                <div class="widget iklan-ke-2 mb-30">
                  <div class="containereditingprofile iklan2" data-of="2" style="z-index:4">
                    <div class="editingprofile" >
                      <h5>ubah iklan 2</h5>
                    </div>
                  </div>
                  <?php $adv2 =  $content->advertisment_2 == '' ? '/demos/800x800.png' :  Helper::base_url().'media/radio/iklan/'.$content->advertisment_2?>
                    <a href=""><img style="height:305px; width:305px" src="<?= $adv2 ?>" class="img-responsive" style="display:block !important" /></a>
                </div>
            </div>
            <!-- end images banner sale -->

        </div><!-- end left-sidebar -->

      <div id="content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 radio-channel pb-0">
            <div class="row m-0 header-detail-channel">
                <?php if(Yii::$app->session->hasFlash('success_news')){ ?>
                  <div class="alert alert-success">
                    Artikel berita anda berhasil di pos
                  </div>
                <?php } ?>
                <?php if(Yii::$app->session->hasFlash('update_oke')){ ?>
                  <div class="alert alert-success">
                    Artikel berita anda berhasil di ter ubah
                  </div>
                <?php } ?>
                <div class="breadcrumb-content" style="overflow:hidden">
                    <ul class="m-0" style="display:inline-block; float:left">
                        <li><a href="<?= Yii::$app->params['base_url'] ?>"><i class="fa fa-home"></i></a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>channel">Channel</a></li>
                        <li class="text-green"><?= ucwords($radio->radio_name) ?></li>

                    </ul>

                    <a href="#" style="float:right" id="managedata">manage data</a>
                </div>
                <div class="title">
                    <h2 class="mt-10 p-0 mb-0"><?= ucwords($radio->radio_name) ?></h2>
                    <!-- <p>Banyuwangi</p> -->
                </div><!-- end title -->



                <?= $this->render('data-radio',[
                  'personal' => $personal,
                  'radio' => $radio,
                  'committees' => $committees
                  ]) ?>




                <div class="description">
                    <h2 class="p-0 mb-5">Description <span class="text-gray-silver"><?= ucwords($radio->radio_name) ?></span>
                      <a class="pull-right editbuttonlive editcontentradio"href="#" data-toggle="tooltip" title="edit konten"><i class="fa fa-pencil"></i> </a>
                    </h2>
                    <?= $content->description ?>
                    <!-- <a href="#" class="text-green" id="viewMore"><i class="fa fa-plus-square-o"></i> View More</a> -->
                </div>
            </div>
            <div class="row news-detail-channel m-0">
                <div class="well mt-40 pr-xs-10 pl-xs-10">
                    <div class="title-well">
                       <h2 class="p-0 mb-5 mt-0">News &amp; Events   <!-- <span class="text-gray-silver"><?php // ucwords($radio->radio_name) ?></span> -->
                         <a href="#" class="list-news-event"><i class="fa fa-list"></i> list data news dan event</a>
                          <a href="#" class="new-news-event"><i class="fa fa-plus"></i> news atau event</a>
                          <a href="#" id="purchase-ticket"><i class="fa fa-ticket"></i> purchase tickets</a>
                        </h2>
                    </div>
                    <div class="slide-news-event">
                        <div id="owl-blog" class="owl-carousel mt-35 mt-xs-70">


                          <?php foreach($newsevents as $z){ ?>

                            <div class="blog-carousel">
                                <div class="entry" style="background:#fff;border-bottom: 1px dashed #eee;">
                                    <a title="" href="<?php echo 'detail-news-event.php'; ?>"><img width="325" height="218" src="<?= Helper::base_url() ?>media/banner-news-event/<?= $z->banner ?>" alt=""></a>
                                </div><!-- end entry -->
                                <div class="bg-white p-20">
                                    <div class="blog-carousel-header">
                                        <h3 class="p-0 mt-0 mb-0"><a title="" href="<?php echo Helper::base_url().$z->type.'/'.$z->slug ?>"><?= ucwords($z->title) ?></a></h3>
                                        <div class="blog-carousel-meta">
                                            <span><i class="fa fa-calendar text-green"></i> <?= Helper::convertDateAmerican($z->created_on) ?></span>
                                        </div><!-- end blog-carousel-meta -->
                                    </div><!-- end blog-carousel-header -->
                                    <div class="blog-carousel-desc">
                                        <p><?= Helper::limit_text(strip_tags($z->description),15)?></p>

                                        <a href="<?php echo Helper::base_url().$z->type.'/'.$z->slug ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                    </div><!-- end blog-carousel-desc -->
                                </div>
                            </div><!-- end blog-carousel -->
                          <?php } ?>




                        </div><!-- end owl-blog -->
                    </div>
                </div>
            </div>
            <div class="row m-0">

                <!-- images banner sale -->
                <div class="imageBannerSale hidden-dekstop viewMobile mt-40 border-top-1px">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->

            </div>
        </div><!-- end content -->

  </div><!-- end container -->
</section><!--end white-wrapper -->

<?php if($radio){ ?>

<div id="iklan" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin:0; padding:0">Ubah Iklan</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p style="color: red;font-style: italic;font-size: 12px;">*note : disarankan untuk foto ukuran 800px X 800px</p>
            </div>
            <div class="col-md-10 col-md-offset-1">
              <form method="post" enctype="multipart/form-data" action="<?= Helper::base_url() ?>radio/change-iklan">
                <div class="openmedia">

                  <h6>Open Media <i class="fa fa-image"></i>  </h6>
                  <img class="getpreview img-responsive" style="display:none">
                </div>
                <input style="visibility:hidden; height:1px" class="choose-pp setpreview" type="file" name="file" value="">
                <input type="hidden" name="id" value="<?= $radio->id ?>">
                <input type="hidden" name="iklan" id="number_iklan" value="">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
                <button style="margin-top:-8px" type="submit" class="btn btn-success btn-block" name="button">simpan</button>

              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        </div>
      </div>

    </div>
  </div>

<div id="change-banner" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin:0; padding:0">Ubah Banner Profile Radio</h4>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-md-10 col-md-offset-1">
              <p style="color: red;font-style: italic;font-size: 12px;">*note : disarankan untuk foto ukuran 1920px × 378px</p>
              <form method="post" enctype="multipart/form-data" action="<?= Helper::base_url() ?>radio/change-banner">
                <div class="openmedia">

                  <h6>Open Media <i class="fa fa-image"></i>  </h6>
                  <img class="getpreview img-responsive" style="display:none">
                </div>
                <input style="visibility:hidden; height:1px" class="choose-pp setpreview" type="file" name="file" value="">
                <input type="hidden" name="id" value="<?= $radio->id ?>">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
                <button style="margin-top:-8px" type="submit" class="btn btn-success btn-block" name="button">simpan</button>

              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
        </div>
      </div>

    </div>
  </div>


<div id="change-photo-profile" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Ubah Photo Profile Radio</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <p style="color: red;font-style: italic;font-size: 12px;">*note : disarankan untuk foto ukuran 800px X 800px</p>
          </div>
          <div class="col-md-8 col-md-offset-2">
            <form method="post" enctype="multipart/form-data" action="<?= Helper::base_url() ?>radio/change-profile-image">
              <div class="openmedia">

                <h6>Open Media <i class="fa fa-image"></i>  </h6>
                <img class="getpreview img-responsive" style="display:none">
              </div>
              <input style="visibility:hidden; height:1px" class="choose-pp setpreview" type="file" name="file" value="">
              <input type="hidden" name="id" value="<?= $radio->id ?>">
              <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
              <button style="margin-top:-8px" type="submit" class="btn btn-success btn-block" name="button">simpan</button>

            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>
<div id="change-content-profile" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Ubah Konten Profile Radio</h4>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12">
            <form method="post" enctype="multipart/form-data" action="<?= Helper::base_url() ?>radio/change-profile-content">
              <textarea name="description" class="editortextarea"><?= $content->description ?></textarea>
              <input type="hidden" name="id" value="<?= $radio->id ?>">
              <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
              <br/>
              <button type="submit" class="btn btn-success" name="button">simpan</button>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>
<div id="list-news-event" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">List News and Event</h4>
      </div>
      <div class="modal-body">
        <p>loading ...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>
<div id="news-and-event-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">

        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Create News or Event</h4>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12">
            <select class="form-control" id="ask-news-event">
              <option value="">-- pilih apa yang akan dibuat --</option>
              <option value="news">News</option>
              <option value="event">Event</option>
            </select>
            <div class="place-news-event">
              <div class="news">
                <?= $this->render('form-news') ?>
              </div>
              <div class="event">
                <?= $this->render('form-event') ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>

<div id="ticket-event" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">

        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Ticket</h4>
      </div>
      <div class="modal-body">
        <p>loading ...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>



<div id="edit-news-event" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">
        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Edit News or Event</h4>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-12">
            <?= $this->render('edit-news-event') ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>




<div id="modal-api" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">
        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">API Url Radio</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="">URL Streaming</label>
            <input type="text" id="apinya" placeholder="contoh : http://45.64.98.181:8323/stream" class="form-control" name="api" value="<?= $radio->api?>">
          </div>
          <div class="form-group">
            <button type="submit" name="button" class="btn btn-primary">save</button>
          </div>
        </form>
      </div>

    </div>

  </div>
</div>




<div id="modalmanagedata" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">
        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Manage Data</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-3 col-sm-3">
          <ul class="controlmenu">
            <li><a href="#" data-to="data-personal">Data Personal</a></li>
            <li><a href="#" data-to="data-company">Data Perusahaan</a></li>
            <li><a href="#" data-to="data-program">Data Program</a></li>
            <li><a href="#" data-to="data-committee">Data Pengurus</a></li>
            <li><a href="#" data-to="program-superior">Program Unggulan</a></li>
          </ul>
        </div>
        <div class="col-md-9 col-sm-9" id="here-edit">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>






<div id="purchase-ticket-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">
        </div>
      </div>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="margin:0; padding:0">Success Purchase Ticket</h4>
      </div>
      <div class="modal-body">
        <p>loading ...</p>
      </div>

    </div>

  </div>
</div>


<?php $message = Helper::getDataContent(18); ?>
<?php $file = Helper::getDataContent(17); ?>
<div id="modaltutorial" style="background: rgba(0,0,0,0.7);" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="loading">
        <div class="loader-oke">
        </div>
      </div>

      <div class="modal-body" style="overflow:hidden">
        <div class="col-md-12">
          <h3 style="margin-bottom:0" class="text-center">Tutorial Mengubah Konten</h3>
          <p class="text-center"><?= $message ?></p>
          <p class="text-center"><a href="#" class="download-tutorial btn btn-primary" data-file="<?= $file ?>">Download File</a></p>
        </div>
      </div>

    </div>

  </div>
</div>


<style media="screen">
.modal{
  background: rgba(0,0,0,0.7);

}
  #managedata{
    color:#17c405;
    border-bottom :1px dotted #17c405;
    display: none
  }
  .new-news-event , .list-news-event , #purchase-ticket{
    font-size: 13px;
    display: inline-block;
    margin-left: 20px;
    color: #17c405 !important;
    font-weight: 100 !important;
    border-bottom: 1px dashed transparent
  }
  .new-news-event:hover , .list-news-event:hover , #purchase-ticket:hover{
    background: transparent;
    color:#17c405 !important;
    border-bottom: 1px dashed #17c405
  }
  .change-banner-radio{
    position: absolute;
    bottom: 30px;
    left: 30px;
    z-index: 10;
    display: none
  }
  .change-banner-radio a{
    display: inline-block;
    padding: 0px 20px 0px 5px;
    border: 1px dashed #ddd;
    background: rgba(0,0,0,0.4);
    color: #ffff;
    font-size: 12px;
    letter-spacing: 1px
  }
  .change-banner-radio a:hover{
    background: rgba(0,0,0,0.6);
  }
  section.banner-img:hover > .change-banner-radio{
    display: block
  }
  .place-news-event > div{
    display: none
  }
  .tableProfile .table tr th,
  .tableProfile .table tr td {
      font-size: 14px;
  }
  .tableProfile {
      display: none;
  }
  #modalmanagedata , #modal-api{
    background: rgba(0,0,0,0.6)
  }
  #modalmanagedata .modal-body{
    overflow: hidden;
    padding: 0;
    display: flex
  }
  #modalmanagedata .modal-body > .col-md-3{
    background: #eee;
    padding: 0px
  }
  #modalmanagedata .modal-body > .col-md-3 li{
    list-style: none
  }
  #modalmanagedata .modal-body > .col-md-3 li a{
    color:#333;
    font-size: 13px;
    display: block;
    padding: 5px 20px
  }
  #modalmanagedata .modal-body > .col-md-3 li a:hover{
    background: #ccc
  }
  .radio-change{
    float: left;
margin-top: 20px;
margin-left: 20px;

  }
  .radio-change a{
    color:#fff;
    border-bottom: 1px dashed #fff;
    display: inline-block;
    padding-bottom: 1px
  }
  .radio-change a:hover{
    color:#eee;
    border-bottom: 1px dotted #eee;
  }
</style>
<?php if(Yii::$app->session->getFlash('download_file')){ ?>
<script type="text/javascript">
setTimeout(function(){
  $('#modaltutorial').modal('show');
},2000);
</script>
<?php } ?>
<script type="text/javascript">
  setTimeout(function(){
    function load_ticket(){
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>radio/data-ticket',
        success : function(data){
          $('#purchase-ticket-modal .modal-body').html(data);
        }
      });
    }
    $('#purchase-ticket').on('click',function(e){
      e.preventDefault();
      $('#purchase-ticket-modal').modal('show');
      load_ticket();
    });


    $('.radio-change').on('click',function(e){
      e.preventDefault();
      $('#modal-api').modal('show');
    });

    $('#modal-api form').validate({
      rules : {
        api : {
          required : true,
          url : true
        }
      },
      messages : {
        api : {
          required : 'tolong untuk diisi',
          url : 'tolong diisi dengan URL yang benar'
        }
      },
      submitHandler : function(form){
        $('#modal-api form .alert').remove();
        $.ajax({
          url : '<?= Yii::$app->params['base_url']?>radio/api-radio',
          data : {
            apinya : $('#apinya').val()
          },
          success : function(data){
            $('#modal-api form').append('<div class="alert alert-success">data telah ter ubah</div>');
            setTimeout(function(){
              location.reload();
            },1000);
          }
        });


        return false;
      }
    });

    $('.controlmenu a').on('click',function(e){
      e.preventDefault();
      var to = $(this).data('to');
      $.ajax({
        url : '<?= Yii::$app->params["base_url"] ?>radio/' + to,
        success : function(data){
          $('#here-edit').html(data);
        }
      });
    });
    $('#managedata').show();
    $('#managedata').on('click',function(e){
      e.preventDefault();
      $('#modalmanagedata').modal('show');
      setTimeout(function(){
        $('.controlmenu li:first-child a').trigger('click');
      },500);
    });

    $(".tableProfile").slice(0, 1).show();
    $("#viewMoreProfile").on('click', function (e) {
        e.preventDefault();
        $(".tableProfile:hidden").slice(0, 100000).slideDown();
        if ($(".tableProfile:hidden").length == 0) {}
    });
    $("#viewMoreProfile").click(function(){
        $("#viewMoreProfile").addClass("hidden");
    });

    $('.list-news-event').on('click',function(e){
      e.preventDefault();
      $('#list-news-event').modal('show');
      $.ajax({
        url : '<?= Helper::base_url() ?>radio/list-news-event',
        type : 'POST',
        data : {
          gen : '<?= $radio->id ?>',
          _csrf : $('#gils').val()
        },
        success : function(data){
          $('#list-news-event .modal-body').html(data);
        }
      });

    });


    $('.new-news-event').on('click',function(e){
      e.preventDefault();
      $('#news-and-event-modal').modal('show');
    });

    $('#ask-news-event').on('change',function(){

      var valuenya = $(this).val();
      if(valuenya == 'news'){
        $('.place-news-event > .news').fadeIn();
        $('.place-news-event > .event').fadeOut();
      }else{
        $('.place-news-event > .news').fadeOut();
        $('.place-news-event > .event').fadeIn();
      }
      // $('.place-news-event').html('loading ...');
      // if(valuenya == 'news'){
      //   $.ajax({
      //     url : '<?php Helper::base_url() ?>radio/get-form-news',
      //     type : 'GET',
      //     success : function(data){
      //       $('.place-news-event').html(data);
      //     }
      //   });
      // }else{
      //     $.ajax({
      //       url : '<?php Helper::base_url() ?>radio/get-form-event',
      //       type : 'GET',
      //       success : function(data){
      //         $('.place-news-event').html(data);
      //       }
      //     });
      // }
    });


    $('#edit-photo-profile').on('click',function(){
      $('#change-photo-profile').modal('show');
    });

    $('#change-photo-profile .openmedia').on('click',function(){
      $('#change-photo-profile .choose-pp').trigger('click');
    });

    $('#change-banner .openmedia').on('click',function(){
      $('#change-banner .choose-pp').trigger('click');
    });

    $('#iklan .openmedia').on('click',function(){
      $('#iklan .choose-pp').trigger('click');
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.getpreview').attr('src', e.target.result).css('margin-bottom','15px');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".setpreview").change(function(){
        $('.openmedia h6').hide();
        $('.openmedia img').show();
        $('#file-error').hide();
        readURL(this);
    });

    $('#change-photo-profile form').validate({
      rules : {
        file : {
          required : true
        }
      },
      messages : {
        file : {
          required : 'image tidak boleh kosong'
        }
      },
      submitHandler : function(form){
        return true;
      }
    });



    $('#change-banner form').validate({
      rules : {
        file : {
          required : true
        }
      },
      messages : {
        file : {
          required : 'image tidak boleh kosong'
        }
      },
      submitHandler : function(form){
        return true;
      }
    });


    $('#iklan1 form').validate({
      rules : {
        file : {
          required : true
        }
      },
      messages : {
        file : {
          required : 'image tidak boleh kosong'
        }
      },
      submitHandler : function(form){
        return true;
      }
    });


    $('#iklan2 form').validate({
      rules : {
        file : {
          required : true
        }
      },
      messages : {
        file : {
          required : 'image tidak boleh kosong'
        }
      },
      submitHandler : function(form){
        return true;
      }
    });




    $('.editcontentradio').on('click',function(e){
      e.preventDefault();
      $('#change-content-profile').modal('show');

    });


    $('.change-banner-radio a').on('click',function(e){
      e.preventDefault();
      $('#change-banner').modal("show");
    });

    $('.iklan1 , .iklan2').on('click',function(){
      $('#iklan').modal('show');
      $('#number_iklan').val($(this).data('of'));
    });




  },1000);

  var setremove = setInterval(function(){
    if($('#mceu_29').length){
      $('#mceu_29').remove();
      clearInterval(setremove);
    }

  },1000);

</script>
<?php } ?>
