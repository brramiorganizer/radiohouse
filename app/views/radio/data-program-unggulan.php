<h3>Data Program Unggulan</h3>
<form id="f_data_program_unggulan">
  <div class="form-group">
    <label for="">Nama Program</label>
    <input type="text" name="name" class="form-control" value="">
  </div>
  <div class="form-group">
    <label for="">Pendengar</label>
    <input type="text"  name="listener" class="form-control" value="">
  </div>
  <div class="form-group">
    <label for="">Jenis Program</label>
    <input type="text" name="kind_program" class="form-control" value="">
  </div>
  <div class="form-group">
    <label for="">Musik</label>
    <input type="text" name="music" class="form-control" value="">
  </div>
  <div class="form-group">
    <label for="">Konten Informasi</label>
    <textarea id="descriptions" name="description" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <label for="">Rata Rata Pendengar</label>
    <input type="text" name="average_listener" class="form-control" value="">
  </div>
  <div class="form-group">
    <input type="hidden" name="id" value="">
    <button type="submit" class="btn btn-success" id="saveallprog" name="button">Save All</button>
  </div>
</form>

<table id="program-unggulan-data" class="table">
  <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Pendengar</th>
      <th>Jenis</th>
      <th>Musik</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>

  </tbody>
</table>
<style>
.action a{
  margin: 0 5px;
  display: inline-block;
}
.action .fa-trash{
  color:red
}
.action .fa-pecil{
  color:blue
}
</style>
<script type="text/javascript">

  function load_table(){
    $.ajax({
      url : '<?= Yii::$app->params['base_url']?>radio/data-program-superior',
      type : 'GET',
      success : function(data){
        $('#program-unggulan-data tbody').html(data);
      }
    });
  }
  load_table();
  $('#f_data_program_unggulan').validate({
    rules : {
      name : {
        required : true
      },
      listener : {
        required : true
      },
      kind_program : {
        required : true
      }
    },
    messages : {
      name : {
        required : 'plese fill name of program'
      },
      listener : {
        required : 'please fill target listener'
      },
      kind_program : {
        required : 'please fill kind of program'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val();
      $('#f_data_program_unggulan #saveallprog').prop('disabled',true).html('saving ...');
      $.ajax({
        url : '<?= Yii::$app->params['base_url']?>radio/insert-program-unggulan',
        data : dataform,
        type : 'POST',
        success : function(data){
          $('#f_data_program_unggulan #saveallprog').prop('disabled',false).html('Save All');
          $('textarea , #f_data_program_unggulan input[type=text] , #f_data_program_unggulan input[type=hidden]').val('');
          $('#program-unggulan-data').DataTable().destroy();

          load_table();
        }

      });
      return false;
    }
  });
</script>
