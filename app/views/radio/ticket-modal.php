<p><a href="#" class="btn btn-warning back-to-list"><i class="fa fa-caret-left"></i> kembali</a></p>
<br/>
<table id="list-ticket" class="table">
  <thead>
    <tr>
      <th>#</th>
      <th>Kategori Tiket</th>
      <th>Harga</th>
      <th>Ketersediaan</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($datum as $data){ ?>
    <tr>
      <td></td>
      <td><?= $data->category ?></td>
      <td>Rp. <?=  number_format($data->price) ?></td>
      <td>
        <?= $data->available ?>
      </td>
      <td class="actnya">
        <?php if($data->is_sold == 0){ ?>
        <a href="#" class="delete-ticket" data-id="<?= $data->id ?>">hapus</a>
        <a href="#" class="sold-ticket" data-id="<?= $data->id ?>">sold out</a>
      <?php }else{ ?>
        <i class="sold">sold out</i>
      <?php } ?>
      </td>
    </tr>
  <?php } ?>
  </tbody>
</table>
<style media="screen">
#list-ticket td a{
  color:#17c405
}
  #list-ticket th , #list-ticket td{
    text-align: center;
  }
  #list-ticket th{
    background: #17c405;
    color:#fff;
  }
  #list-ticket{
    border:1px solid #17c405
  }
  .actnya a{
    display: inline-block;
    padding:3px 5px;
    border-bottom: 1px dotted transparent;
  }
  .actnya a:hover{
    border-bottom: 1px dotted #17c405;
  }
  .sold{
    color:red
  }
</style>
<script type="text/javascript">
setTimeout(function(){
  var v = $('#list-ticket').DataTable( {
      "columnDefs": [ {
          "searchable": false,
          "orderable": false,
          "targets": [0,4]
      } ]
  } );

  v.on( 'order.dt search.dt', function () {
      v.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
  } ).draw();


},1000);


$('.delete-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var tablenya = $('#list-ticket').DataTable().row($(this).parents('tr'));
  if(confirm('Anda yakin akan menghapus tiket ini ?')){
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>radio/delete-ticket',
      type : 'POST',
      data : {
        id : id,
        _csrf : $('#gils').val()
      },
      dataType : 'JSON',
      success : function(data){
        if(data.status){
          tablenya.remove().draw();
        }else{
          location.reload();
        }
      }
    });
  }

});



$('.sold-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var thisc = $(this).parents('tr').find('.actnya');
  if(confirm('Anda yakin tiket ini sudah habis ?')){
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>radio/sold-ticket',
      type : 'POST',
      data : {
        id : id,
        _csrf : $('#gils').val()
      },
      dataType : 'JSON',
      success : function(data){
        if(data.status){
          thisc.html('<i class="sold">sold out</i>')
        }else{
          location.reload();
        }
      }
    });
  }

});

$('.back-to-list').on('click',function(e){
  e.preventDefault();
  $('#ticket-event').modal('hide');
  $('.list-news-event').trigger('click');
});
</script>
