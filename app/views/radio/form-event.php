
<form id="form-event" enctype="multipart/form-data" style="margin-top:50px">
  <div class="row">
    <div class="col-md-8 col-sm-8">
      <div class="form-group">
        <label for="">Judul</label>
        <input type="text" class="form-control" name="title" value="">
      </div>
      <div class="form-group">
        <label for="">Deskripsi</label>
        <textarea id="description_goks" name="description" class="editortextarea"></textarea>
      </div>

      <div class="form-group" style="overflow:hidden">
        <p style="color:#17c405; margin-top:30px; margin-bottom:0">jual ticket event ini secara online disini ?</p>
        <div class="form-ticket-online">
          <p class="text-center">Ticket Berdasarkan Kategori</p>
          <div class="col-md-5 col-sm-5">
            <label for="">Nama Kategori Tiket</label>
            <input type="text" id="category-ticket" name="category" value="" class="form-control">
          </div>
          <div class="col-md-5 col-sm-5">
            <label for="">Harga Tiket</label>
            <span class="rupiah">Rp.</span>
            <input style="padding-left:50px" id="price-ticket" type="text" name="price" value="" class="form-control">

          </div>
          <div class="col-md-2 col-sm-2">
            <label for="">&nbsp; </label><br/>
            <input type="hidden" id="is_edit" value="">
            <button type="button" name="button" id="save-ticket"><i class="fa fa-save"></i></button>
          </div>
          <div class="col-md-12 errorthis">
            <label class="error"></label>
          </div>
          <div class="col-md-12">
            <table class="table table-ticket">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Category Tiket</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>


      </div>

    </div>
    <div class="col-md-4 col-sm-4">
      <div class="form-group">
        <label for="">Banner</label>
        <input type="file" name="banner" value="" class="setpreview">
        <img src="" class="getpreview img-responsive" >
      </div>
      <p style="color: red;font-size: 12px;margin-top: 40px;">* search engine optimization (SEO)</p>
      <div class="form-group">
        <label for="">Meta Description</label>
        <textarea name="meta_description" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <label for="">Meta keyword</label>
        <input type="text" name="meta_keywords" class="form-control taginput"/>
      </div>
    </div>
  </div>



  <div class="form-group" style="margin-top:40px">
    <input type="hidden" name="radio_id" value="<?= Yii::$app->session->get('radiotoken') ?>">
    <button type="submit" name="button" class="btn btn-success">simpan</button>
  </div>
</form>
<style media="screen">
  .tagsinput{
    width: 100% !important
  }
  .form-ticket-online > p{
    background: #17c405;
    margin-bottom: 0;
    padding: 5px;
    color:white;
    margin-bottom: 20px
  }
  .form-ticket-online{
    border:1px solid #17c405;
    margin-top:20px;
    overflow: hidden;
    padding-bottom: 20px
  }
  .rupiah{
    display: inline-block;
position: absolute;
bottom: 0px;
left: 15px;
background: #17c405;
color: #fff;
padding: 3px 10px;
  }
  #save-ticket{
    background: #17c405;
    color:#fff;
    border: 0;
border-radius: 0;
padding: 3px 11px;
}
.table-ticket{
  margin-top: 30px
}
  .table-ticket thead , .table-ticket tbody{
    border: 1px solid #17c405
  }
  .table-ticket td{
    text-align: center;
  }
  .table-ticket th{
    background: #17c405;
    color:white;
    text-align: center;
    font-weight: 100;

  }
  .table-ticket tbody a{
    display: inline-block;
    margin: 0 5px
  }
  .mce-notification-error{
    display: none !important
  }
</style>
<script type="text/javascript">

setTimeout(function(){



function convertToRupiah(angka)
{
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}

var tickets = [];
$('#save-ticket').on('click',function(){
  var category = $('#category-ticket').val();
  var price = $('#price-ticket').val();
  var is_edit = $('#is_edit').val();
  if(category == ''){
    $('.errorthis label').html('kategori tiket harus di isi degan benar');
  }else if(price == '' || $.isNumeric(price) == false){
    $('.errorthis label').html('harga tiket harus di isi dengan benar');
  }else{
    $('.errorthis label').html('');
    $('#category-ticket').val('');
    $('#price-ticket').val('');
    $('#is_edit').val('');
    if(is_edit == ''){
      tickets.push([category,price]);
    }else{
      tickets[is_edit][0] = category;
      tickets[is_edit][1] = price;
    }

    var result = '';
    for (var i = 0; i < tickets.length; i++) {
        var numberis = i + 1;
        result += '<tr><td>'+ numberis +'</td><td>'+ tickets[i][0]+'</td><td>'+ convertToRupiah(tickets[i][1]) +'</td><td><a href="#" class="delete-ticket" data-list="'+i+'"><i class="fa fa-trash"></i></a> <a href="#" class="edit-ticket" data-list="'+i+'"><i class="fa fa-pencil"></i></a> </td></tr>';
    }
    console.log(tickets);
    $('.table-ticket tbody').html(result);
  }
});

$(document).on('click','.delete-ticket',function(e){
  e.preventDefault();
  var id = $(this).data('list');
  tickets.splice(id,1);
  $('#category-ticket').val('');
  $('#price-ticket').val('');
  $('#is_edit').val('');
  var result = '';
  for (var i = 0; i < tickets.length; i++) {
      var numberis = i + 1;
      result += '<tr><td>'+ numberis +'</td><td>'+ tickets[i][0]+'</td><td>'+ tickets[i][1] +'</td><td><a href="#" class="delete-ticket" data-list="'+i+'"><i class="fa fa-trash"></i></a> <a href="#" class="edit-ticket" data-list="'+i+'"><i class="fa fa-pencil"></i></a> </td></tr>';
  }
  console.log(tickets);
  $('.table-ticket tbody').html(result);
});


$(document).on('click','.edit-ticket',function(e){
  e.preventDefault();
  var id = $(this).data('list');
  var data = tickets[id];
  $('#category-ticket').val(data[0]);
  $('#price-ticket').val(data[1]);
  $('#is_edit').val(id);
});


  $('.taginput').tagsInput();

  $('#form-event').validate({
    rules : {
      title : {
        required : true
      },
      description : {
        required : true
      },
      banner : {
        required : true
      }
    },
    messages : {
      title : {
        required : 'judul wajib di isi'
      },
      description : {
        required : 'konten wajib di isi'
      },
      banner : {
        required : 'banner wajib di isi'
      }
    },
    submitHandler : function(form){
      $('.loading').show();
      var kuy = $('#form-event')[0];
      var formData = new FormData(kuy);
      formData.append('zip_file', $('input[type=file]')[0].files[0]);
      formData.append('_csrf', $('#gils').val());
      formData.append('content', tinymce.get('description_goks').getContent());
      formData.append('tickets', JSON.stringify(tickets));

      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>radio/post-form-event',
        type : 'POST',
        //dataType : 'JSON',
        contentType: false,
        cache: false,
        processData:false,
        data : formData,
        success : function(data){

          setTimeout(function(){
            location.reload();
          },1000);
        }
      });

      return false;
    }
  });
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.getpreview').attr('src', e.target.result).css('margin','15px 0');
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $(".setpreview").change(function(){
      readURL(this);
  });

},2000);


</script>
