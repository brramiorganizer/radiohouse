<h3>Data Program</h3>
<form id="f_data_program">
  <div class="form-group">
    <label for="">Format Siaran</label>
    <input type="text" name="format_siaran" class="form-control" value="<?= ($model ? $model->format_siaran : '' );?>">
  </div>
  <div class="form-group">
    <label for="">Target Pendengar</label>
    <input type="text"  name="target_pendengar" class="form-control" value="<?= ($model ? $model->target_pendengar : ''); ?>">
  </div>
  <div class="form-group">
    <label for="">Coverage Area</label>
    <input type="text" name="coverage_area" class="form-control" value="<?= ($model ? $model->coverage_area : ''); ?>">
  </div>
  <div class="form-group">
    <label for="">Musik Yang Diputar</label>
    <input type="text" name="musik" class="form-control" value="<?= ($model ? $model->musik : ''); ?>">
  </div>
  <div class="form-group">

    <button type="submit" class="btn btn-success" name="button">Save All</button>
  </div>
</form>

<script type="text/javascript">
  $('#f_data_program').validate({
    rules : {
      format_siaran : {
        required : true
      },
      target_pendengar : {
        required : true
      },
      coverage_area : {
        required : true
      },
      musik : {
        required : true
      }
    },
    messages : {
      format_siaran : {
        required : 'please fill the broadcast format'
      },
      target_pendengar : {
        required : 'please fill the target audience'
      },
      coverage_area : {
        required : 'please fill the coverage area'
      },
      musik : {
        required : 'please fillt the music playing'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val();
      $('#f_data_program button').prop('disabled',true).html('saving ...');
      $('#f_data_program .alert').remove();
      $.ajax({
        url : '<?= Yii::$app->params['base_url']?>radio/change-program',
        data : dataform,
        type : 'POST',
        success : function(data){
          $('#f_data_program button').prop('disabled',false).html('Save All');
          $('#f_data_program').append('<div class="alert alert-success">Data tersimpan</div>');
        }

      });
      return false;
    }
  });
</script>
