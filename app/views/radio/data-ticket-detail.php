<table class="table-detail-ticket-on">
  <tr>
    <td>Event</td>
    <td>:</td>
    <td><?= $data->event->title ?></td>
  </tr>
  <tr>
    <td>Kode Tiket</td>
    <td>:</td>
    <td>RH-<?= $data->code ?></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td><?= $data->name ?></td>
  </tr>
  <tr>
    <td>Email</td>
    <td>:</td>
    <td><?= $data->email ?></td>
  </tr>
  <tr>
    <td>Nomer Telepon / Handphone</td>
    <td>:</td>
    <td><?= $data->no_phone ?></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><?= $data->address ?></td>
  </tr>
  <tr>
    <td>Jumlah Tiket</td>
    <td>:</td>
    <td><?= $data->quantity ?></td>
  </tr>
  <tr>
    <td>Total Harga</td>
    <td>:</td>
    <td>Rp. <?= number_format($data->total_price) ?></td>
  </tr>
  <tr>
    <td>Status Pembelian</td>
    <td>:</td>
    <td><?= $data->status ?></td>
  </tr>
</table>
<style media="screen">
  .table-detail-ticket-on td{
    padding: 4px 10px
  }
</style>
