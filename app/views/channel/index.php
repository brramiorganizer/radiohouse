<?php
use app\helper\Helper;
$content = Helper::getPageContent(4);
$this->title = $content->meta_title;
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $content->meta_description
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $content->meta_keywords
]);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner detail-page-other">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']?>"><i class="fa fa-home"></i></a></li>
                      <li>Tentang Radio House</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="page-under pt-80 pt-sm-60 pb-40">
    <div class="container">
          <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pr-sm-15">
              <div class="breadcrumb-sidebar">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url'] ?>"><i class="fa fa-home"></i></a></li>
                      <li class="text-green">Channel</li>
                  </ul>
              </div>
              <div class="widget mb-30">
                  <div class="title">
                      <h2 class="mt-10 p-0 mb-5">Channel</h2>
                      <?= $content->description ?>
                  </div><!-- end title -->
              </div>
              <div class="widget mb-30">
                  <div id="accordion-second" class="clearfix">
                      <div class="accordion" id="accordion3">
                        <div class="accordion-group">
                          <div class="accordion-heading active">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseToggle1">
                              <em class="fa fa-minus icon-fixed-width"></em>Channel
                            </a>
                          </div>
                          <div id="collapseToggle1" class="accordion-body collapse in">
                            <div class="accordion-inner">
                              <div class="engine-search">
                                  <form method="" accept-charset="">
                                      <div class="form-group">
                                          <input type="text" id="filter-channel" class="form-control" name="" placeholder="Search" />
                                          <button type="button" class="btn-search"><i class="fa fa-search"></i></button>
                                      </div>
                                  </form>
                              </div>
                              <ul id="filter-channel-data">
                                  <?php foreach($radios as $radio){ ?>
                                  <li><a href="<?= Yii::$app->params['base_url'] ?>channel/<?= $radio->slug ?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?= ucwords($radio->radio_name) ?></a></li>
                                  <?php } ?>

                              </ul>
                              <p class="hidden no-data-channel text-center">no data available</p>
                            </div>
                          </div>
                        </div>
                        <!-- <div class="accordion-group">
                          <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseToggle2">
                              <em class="fa fa-plus icon-fixed-width"></em>Kategori Channel
                            </a>
                          </div>
                          <div id="collapseToggle2" class="accordion-body collapse">
                            <div class="accordion-inner">
                              <div class="engine-search">
                                  <form method="" accept-charset="">
                                      <div class="form-group">
                                          <input type="text" class="form-control" name="" placeholder="Search" />
                                          <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                      </div>
                                  </form>
                              </div>
                              <ul>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                  <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                              </ul>
                            </div>
                          </div>
                        </div> -->
                        <div class="accordion-group">
                          <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseToggle3">
                              <em class="fa fa-plus icon-fixed-width"></em>Daerah / Kota
                            </a>
                          </div>
                          <div id="collapseToggle3" class="accordion-body collapse">
                            <div class="accordion-inner">
                              <div class="engine-search">
                                  <form method="" accept-charset="">
                                      <div class="form-group">
                                          <input type="text" id="filter-city" class="form-control" name="" placeholder="Search" />
                                          <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                      </div>
                                  </form>
                              </div>
                              <ul id="filter-city-data">
                                <?php foreach($cities as $x){ ?>

                                <li><a href="<?= Yii::$app->params['base_url'] ?>channel?city=<?= strtolower($x->city) ?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?= ucwords($x->city) ?></a></li>
                                <?php } ?>
                              </ul>
                              <p class="hidden no-data-city text-center">no data available</p>
                            </div>
                          </div>
                        </div>
                      </div><!-- end accordion -->
                  </div><!-- end accordion first -->
              </div>

              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hiddenMobile">
                <div class="widget mb-30">
                  <?php if(Helper::getNeChannel(3)->image != '' and Helper::getNeChannel(3)->expired >= date('Y-m-d H:i:s')){ ?>
                    <?php $link = (Helper::getNeChannel(3)->link != '' ? Helper::getNeChannel(3)->link : '#') ?>
                    <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(3)->image ?>" class="img-responsive" /></a>
                  <?php }else{ ?>
                    <img class="img-responsive"  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">

                  <?php } ?>

                </div>
                <div class="widget mb-30">
                  <?php if(Helper::getNeChannel(4)->image != '' and Helper::getNeChannel(4)->expired >= date('Y-m-d H:i:s')){ ?>
                    <?php $link = (Helper::getNeChannel(4)->link != '' ? Helper::getNeChannel(4)->link : '#') ?>
                    <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(4)->image ?>" class="img-responsive" /></a>
                  <?php }else{ ?>
                    <img class="img-responsive" src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">

                  <?php } ?>
                </div>
              </div>
              <!-- end images banner sale -->

          </div><!-- end left-sidebar -->

        <div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 radio-channel pb-0">
              <div class="row img-radio-channel">


                  <?php foreach($channels as $z){ ?>
                  <div class="col-sm-4 col-md-4 col-grid">
                      <div class="portfolio_item">
                  <div class="entry">
                              <a href="<?= Helper::base_url() ?>channel/<?= $z->slug ?>">
                      <?php $adv =  $z->content->photo_profile == '' ? '/demos/800x800.png' :  Helper::base_url().'media/radio/profile/'.$z->content->photo_profile?>
                        <img style="width:223px !important; height:223px !important" src="<?= $adv ?>" alt="" class="img-responsive">
                        <div class="magnifier">
                          <div class="buttons">
                            <h3 class="p-0 mt-0 mb-5"><?= $z->radio_name ?></h3>
                                          <h4 class="p-0 m-0"><?= ucwords($z->city) ?></h4>
                          </div><!-- end buttons -->
                        </div><!-- end magnifier -->
                              </a>
                  </div><!-- end entry -->
                </div><!-- end portfolio_item -->
                  </div>
                <?php } ?>


              </div>

              <div class="pagination_wrapper text-center">
                  <!-- Pagination Normal -->
                  <!-- Pagination Normal -->
                  <?php $getnya = (isset($_GET['s']) ? 'channel?s='.$_GET['s'].'&page=' : 'channel?page='); ?>
                  <ul class="pagination">
                      <?php if(isset($_GET['page']) and $_GET['page'] > 1){ ?>
                        <li><a href="<?= Yii::$app->params['base_url'] .$getnya . ($_GET['page'] - 1) ?>">«</a></li>
                      <?php } ?>
                      <?php for($i = 1; $i <= $pagination; $i++){ ?>

                      <?php
                        $active = '';
                        if(isset($_GET['page'])){
                          if($_GET['page'] == $i){
                            $active = 'active';
                          }
                        }else{
                          if($i == 1){
                            $active = 'active';
                          }
                        }
                      ?>
                      <li class="<?= $active ?>"><a href="<?= Yii::$app->params['base_url'] .$getnya. $i ?>"><?= $i ?></a></li>
                      <?php } ?>
                      <?php $next = (isset($_GET['page']) ? $_GET['page'] + 1 : 2); ?>
                      <?php if($pagination > ($next - 1)){ ?>
                        <li><a href="<?= Yii::$app->params['base_url'] .$getnya. $next ?>">»</a></li>
                      <?php } ?>
                  </ul>
              </div><!-- end pagination_wrapper -->

          </div><!-- end content -->

          <div class="clearfix"></div>

          <div class="row m-0 pl-15 pr-15">
              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40 border-top-1px">
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                  </div>
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                  </div>
              </div>
              <!-- end images banner sale -->
          </div>

    </div><!-- end container -->
  </section><!--end white-wrapper -->
