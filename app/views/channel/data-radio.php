<div class="profileChanneln mt-40">
        <div class="tableProfile">
            <h3 class="p-0 mb-20 mt-0">A. Data Personal</h3>
            <div class="table-responsive">
                <table class="table mb-10">
                    <tbody>
                        <tr>
                            <th style="width: 200px;">Nama Lengkap</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Email</th>
                            <td style="width: 10px;">:</td>
                            <td><a href="mailto:<?= $personal->name ?>"><?= $personal->name ?></a> </td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Alamat Lengkap</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->address ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Telephone &amp; Ext</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->telephone ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Handphone</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $personal->handphone ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="tableProfile mt-30">
            <h3 class="p-0 mb-20 mt-0">B. Data Perusahaan</h3>
            <div class="table-responsive">
                <table class="table mb-0">
                    <tbody>
                      <!-- <tr>
                          <th style="width: 200px;">Nomor ISR &amp; IPP</th>
                          <td style="width: 10px;">:</td>
                          <td> // $radio->number_isr_ipp</td>
                      </tr> -->
                        <tr>
                            <th style="width: 200px;">Nama Radio</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->radio_name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Nama Perusahaan</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->company_name ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Alamat Lengkap Perusahaan</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->address ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">No Telephone &amp; Ext</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $radio->telepon ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <?php if($program){ ?>
        <div class="tableProfile mt-30">
            <h3 class="p-0 mb-20 mt-0">B. Program Radio</h3>
            <div class="table-responsive">
                <table class="table mb-0">
                    <tbody>
                      <!-- <tr>
                          <th style="width: 200px;">Nomor ISR &amp; IPP</th>
                          <td style="width: 10px;">:</td>
                          <td> // $radio->number_isr_ipp</td>
                      </tr> -->
                        <tr>
                            <th style="width: 200px;">Format Siaran</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $program->format_siaran ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Target Pendengar</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $program->target_pendengar ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Cakupan Area</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $program->coverage_area ?></td>
                        </tr>
                        <tr>
                            <th style="width: 200px;">Musik</th>
                            <td style="width: 10px;">:</td>
                            <td><?= $program->musik ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
      <?php } ?>




      <?php if(count($unggulan) > 0){ ?>
      <div class="tableProfile mt-30">
          <h3 class="p-0 mb-20 mt-0">B. Program Unggulan</h3>
          <div class="table-responsive">
            <?php foreach($unggulan as $a){ ?>
              <table class="table mb-30 mt-10">
                  <tbody>
                      <tr>
                          <th style="width: 200px;">Nama Program</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $a->name ?></td>
                      </tr>
                      <tr>
                          <th style="width: 200px;">Pendengar</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $a->listener ?></td>
                      </tr>
                      <tr>
                          <th style="width: 200px;">Jenis Program</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $a->kind_program ?></td>
                      </tr>
                      <tr>
                          <th style="width: 200px;">Musik</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $a->music ?></td>
                      </tr>
                      <tr>
                          <th style="width: 200px;">Rata Rata Pendengar</th>
                          <td style="width: 10px;">:</td>
                          <td><?= $a->average_listener ?></td>
                      </tr>
                      <tr>
                          <th style="width: 200px;">Informasi</th>
                          <td style="width: 10px;">:</td>
                          <td><?= htmlentities($a->content_information) ?></td>
                      </tr>
                  </tbody>
              </table>
            <?php } ?>
          </div>
      </div>
    <?php } ?>
        <a href="#" class="text-green mt-20" id="viewMoreProfile"><i class="fa fa-plus-square-o"></i> View More</a>

    </div>
