<section class="page-under pt-20 pt-sm-60 pb-50 pb-sm-60">
  <div class="container">

        <div class="row">
            <div class="col-md-8 p-0 p-sm-0 col-md-offset-2">
                <div class="doc">
                    <div class="widget">
                        <div class="about_tabbed" style="padding-top:50px">
                          <div class="alert" style="padding:80px 20px; background:#eee">
                            <h1 style="font-size:30px; margin-bottom:10px; margin-top:0" class="text-center">Terima Kasih</h1>
                            <p class="text-center mb-20" style="line-height:20px" >Sudah mendaftar menjadi member, silahkan cek email untuk melakukan pembayaran sebelum kami aktivasi.</p>
                              <p class="text-center"><a class="btn btn-primary" href="<?= Yii::$app->params['base_url'] ?>pricing/confirm?token=<?= $token ?>">Konfirmasi Pembayaran</a></p>
                          </div>

                        </div><!-- end about tabbed -->
                    </div><!-- end widget -->
                </div><!-- end doc -->
            </div>
        </div>

  </div><!-- end container -->
</section><!--end white-wrapper -->

<div class="contact-us" style="background:">
  <h2 style="color:#fff">Any Question ? <a href="<?= Yii::$app->params['base_url']?>hubungi-kami">Contact Us</a> </h2>
</div>

<style media="screen">
  .contact-us{
    background: #4e4e4e;
    padding: 80px 0
  }
  .contact-us h2{
    text-align: center;
    font-size: 30px;
    margin-bottom: 0;
    font-weight: 100;
  }
  .contact-us h2 a{
    color:#fff !important;
    text-decoration: underline !important;
    font-weight: bold
  }
  .contact-us h2 a:hover{
    background: transparent !important;
    color:#bbb !important;
  }
</style>
