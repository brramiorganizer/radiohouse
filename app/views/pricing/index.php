<?php
use app\helper\Helper;
$content = Helper::getPageContent(7);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                    <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                    <li class="text-green"><?= $content->meta_title ?></li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg"><?= $content->meta_title ?></h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

    <section class="page-under pt-80 pb-0">
    	<div class="container">
            <div class="content-about-up">
                <p>
                  <?= $content->description ?>
                </p>
            </div>
            
        </div>
    </section><!--end white-wrapper -->

    <div class="wrapper-section pt-60 pb-60">
        <section class="white-wrapper pb-40 pt-0 sec-member">
        	<div class="container">
            	<div class="general-title pb-60">
                	<h2 class="mt-0 p-0">Harga Member</h2>
                    <hr class="border-green"/>
                </div><!-- end general title -->
                <div class="doc member-price">
                    <div id="owl-testimonial-widget" class="owl-carousel pricing_boxes mainEqual p-0">

                      <?php foreach($plans as $plan){ ?>
                        <div class="owl-item col-lg-4 col-sm-4">
                            <div class="pricing_detail">

                                <header>
                                    <h3><?= strtoupper($plan->name) ?></h3>
                                    <h2 class="mt-0 p-0 mb-30">Rp <?= number_format($plan->price) ?> / Tahun</h2>
                                </header><!-- end header -->
                                <div class="pricing_info text-center">
                                    <ul class="equal-1">

                                        <?php
                                        $no = 0;
                                        $batas = 4;
                                        foreach(Helper::getPlanFeature($plan->id) as $b){
                                          $no++;
                                          if($no <= $batas){
                                        ?>
                                          <li class="pb-5"><?= $b->planfeature->name ?></li>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php if($no > $batas){ ?>
                                        <li class="pb-5"><a data-to="<?= $plan->slug ?>" data-id="<?= $plan->id ?>" class="getMemberGold detailfeatures text-green" href="#packageis">Detail...</a></li>
                                      <?php } ?>
                                    </ul>
                                    <footer class="mt-20">
                                        <a href="#packageis" data-to="<?= $plan->slug ?>" data-id="<?= $plan->id ?>" class="btn btn-primary btn-lg get-form-besic">Get Member</a>
                                    </footer>
                                </div><!-- end pricing-info -->
                            </div><!-- end pricing_detail -->
                        </div><!-- end col-lg-3 -->
                      <?php } ?>




                    </div>
    			</div><!-- end doc -->
    		</div><!-- end container -->
        </section><!-- end white-wrapper -->

        <section id="packageis" class="white-wrapper pb-0 pt-0" style="display:none">
          <p style="height:80px"> &nbsp; </p>
            <div class="container">
                <div class="col-md-12 pb-20">
                  <div class="benefit-goldPremium">
                      <div class="featuresis silver">
                        <h2 class="mt-0 p-0">Benefit Silver</h2>
                        <ul class="mb-20">
                            <?php foreach(Helper::getPlanFeature(1) as $gold){ ?>
                              <li><i class="fa fa-check text-green mr-10"></i><?= $gold->planfeature->name ?></li>
                            <?php } ?>
                        </ul>
                      </div>
                      <div class="featuresis gold">
                        <h2 class="mt-0 p-0">Benefit Gold</h2>
                        <ul class="mb-20">
                            <?php foreach(Helper::getPlanFeature(2) as $gold){ ?>
                              <li><i class="fa fa-check text-green mr-10"></i><?= $gold->planfeature->name ?></li>
                            <?php } ?>
                        </ul>
                      </div>
                      <div class="featuresis premium">
                        <h2 class="mt-0 p-0">Benefit Premium</h2>
                        <ul class="mb-20">
                            <?php foreach(Helper::getPlanFeature(3) as $gold){ ?>
                              <li><i class="fa fa-check text-green mr-10"></i><?= $gold->planfeature->name ?></li>
                            <?php } ?>
                        </ul>
                      </div>

                  </div>
                    <p>Isi form di bawah ini untuk mendaftarkan stasiun radio Anda</p>
                    <div class="row">
                      <form id="newmember" method="post">
                        <div class="col-md-6">
                            <div class="form-member">

                                <input type="hidden" name="plan_id" class="plan_id" value="">
                                <h2 class="mt-10 mb-10 p-0">Data Personal</h2>
                                <div class="form-group">
                                    <label>Nama <em>(wajib)</em></label>
                                    <input type="text" class="form-control" name="p_name" />
                                </div>
                                <div class="form-group">
                                    <label>Email <em>(wajib)</em></label>
                                    <input type="email" id="p_email" class="form-control" name="p_email" id="p_email" />
                                </div>

                                <div class="form-group">
                                    <label>Alamat Lengkap <em>(wajib)</em></label>
                                    <input type="text" name="p_address" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label>No Handphone <em>(wajib)</em></label>
                                    <input type="text" name="p_handphone" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label>No Telepon</label>
                                    <input type="text" name="p_telephone" class="form-control" value="">
                                </div>


                                  <h2 class="mt-50 mb-10 p-0">Data Perusahaan</h2>
                                  <div class="form-group">
                                      <label>Nama Radio <em>(wajib)</em></label>
                                      <input type="text" class="form-control" name="radio_name" id="radio_name" />
                                  </div>
                                  <div class="form-group">
                                      <label>Nama Perusahaan <em>(wajib)</em></label>
                                      <input type="text" class="form-control" name="company_name" />
                                  </div>
                                  <div class="form-group">
                                      <label>Alamat <em>(wajib)</em></label>
                                      <textarea class="form-control" rows="3" name="address"></textarea>
                                  </div>
                                  <div class="form-group">
                                      <label>Kota / Wilayah <em>(wajib)</em></label>
                                      <input type="text" class="form-control" name="kota" value="">
                                  </div>
                                  <div class="form-group">
                                      <label>No Telepon <em>(wajib)</em></label>
                                      <input type="text" class="form-control" name="telepon" />
                                  </div>






                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 data-pengurus">
                          <h2 class="mt-10 mb-10 p-0">Data Pengurus</h2>
                          <div class="form-group">
                              <label>Nomor ISR & IPP <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="isr_ipp" />
                          </div>

                          <div class="form-group p-0" style="overflow:hidden">
                            <div class="col-md-12 p-0">
                              <h3 class="mt-10 mb-10 p-0">1. Direktur Utama</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0">
                              <label>Nama Lengkap <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="name_p1">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Nomer Telepon/Handphone <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="handphone_p1">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Alamat Lengkap <em>(wajib)</em></label>
                              <textarea class="form-control" rows="3" name="alamat_p1"></textarea>
                            </div>
                          </div>

                          <div class="form-group mt-15 p-0" style="overflow:hidden">
                            <div class="col-md-12 p-0">
                              <h3 class="mt-10 mb-10 p-0">2. Marketing Manager</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0">
                              <label>Nama Lengkap <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="name_p2">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Nomer Telepon/Handphone <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="handphone_p2">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Alamat Lengkap <em>(wajib)</em></label>
                              <textarea class="form-control" rows="3" name="alamat_p2"></textarea>
                            </div>
                          </div>


                          <div class="form-group mt-15 p-0" style="overflow:hidden">
                            <div class="col-md-12 p-0">
                              <h3 class="mt-10 mb-10 p-0">3. Personal yang Ditunjuk</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0">
                              <label>Nama Lengkap <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="name_p3">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Nomer Telepon/Handphone <em>(wajib)</em></label>
                              <input type="text" class="form-control" name="handphone_p3">
                            </div>
                            <div class="col-md-12 col-sm-12 pl-20 pr-0 mt-15">
                              <label>Alamat Lengkap <em>(wajib)</em></label>
                              <textarea class="form-control" rows="3" name="alamat_p3"></textarea>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 pl-20 pr-0">


                          <div class="form-group" style="position:relative">
                              <label class="font-weight-100" >
                                  <input type="checkbox" name="checklist" class="mr-10"/><a href="" style="color: blue;" target="_blank">Syarat dan Ketentuan.</a>
                              </label>
                          </div>
                          <button type="submit" class="btn btn-primary btn-lg">Kirim</button>
                          </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <style media="screen">
      label.error{
        color:red

      }
      #checklist-error{
        position: absolute;
top: 20px;
      }
      .data-pengurus label{
        font-size: 14px
      }
      .data-pengurus em{
        font-size: 12px;
        font-weight: 100
      }
    </style>
