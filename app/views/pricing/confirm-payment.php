<?php
use app\helper\Helper;
$content = Helper::getPageContent(16);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: Yii::$app->params['base_url'].'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $content->meta_description
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $content->meta_keywords
]);
?>
<section class="banner-img whiteText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                      <li><a href="<?= Helper::base_url() ?>"><i class="fa fa-home"></i></a></li>
                      <li>Konfirmasi Pembayaran</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Konfirmasi Pembayaran</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="pageAccount pt-70 pb-60 pt-sm-60">

      <div class="container page-information-process page-konfirmasi">
          <div class="success-message text-center">
              <h1 class="mt-0 font-26">Konfirmasi Pembayaran</h1>
          </div>
          <div class="col-sm-8 col-sm-offset-2 main-konfirmasi well margintop-30">
              <form id="konfirmasi-pembayaran-radio">
                  <p>Silahkan Melakukan Konfirmasi pembayaran jika sudah mentransfer pembayarannya.</p>

                  <div class="form-group">
                      <label>Transfer Ke</label>
                      <select class="form-control" id="destination" name="bank">
                          <option value="">-- Pilih Bank --</option>
                          <?php foreach($banks as $bank){ ?>
                            <option  value="<?= $bank->id ?>"><?= $bank->name . ' ' . '('.$bank->rekening_number.' a/n '.$bank->rekening_name.')' ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>No Rekening Anda</label>
                      <input type="text" name="rekening_number" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <label>Nama Pemilik Rekening</label>
                      <input type="text" name="rekening_name" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <label>Jumlah yang di Transfer</label>
                      <input readonly  value="Rp. <?= number_format($plan->plan->price) ?>" type="text" name="total" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <label>Keterangan</label>
                      <textarea name="keterangan" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                      <label>Bukti Transfer</label>
                      <input type="file" name="image" value="" accept="image/*">
                  </div>
                  <input type="hidden" name="id" value="<?= $plan->radio_id ?>">
                  <button type="submit" class="btn btn-primary btnKonfrim">Konfirmasi Pembayaran</button>
              </form>
          </div>
  </div><!-- end container -->

  </section><!--end white-wrapper -->
