<?php
use app\helper\Helper;
$content = Helper::getPageContent(3);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
?>

  <section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                      <li class="text-green"><?= $content->meta_title ?></li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg"><?= $content->meta_title ?></h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

  <section class="page-under pt-60 pb-0">
    <div class="container">

          <div class="content-about-up mb-60">
              <?= $content->description ?>
          </div>

          <div class="content-about-middel">
              <div class="row mb-80">
                  <div class="stat f-container">
            <div class="f-element col-lg-4 col-md-4">
              <div class="milestone-counter mb-sm-20">
                            <i class="fa fa-user fa-3x text-green"></i>
                              <div class="milestone-details text-4e4e4e mt-30"><?= Helper::getDataContent(23) ?></div>
              </div>
            </div>
            <div class="f-element col-lg-4 col-md-4">
              <div class="milestone-counter mb-sm-20">
                            <i class="fa fa-coffee fa-3x text-green"></i>
                              <div class="milestone-details text-4e4e4e mt-30"><?= Helper::getDataContent(24) ?></div>
              </div>
            </div>
            <div class="f-element col-lg-4 col-md-4 ">
              <div class="milestone-counter mb-sm-20">
                            <i class="fa fa-trophy fa-3x text-green"></i>
                              <div class="milestone-details text-4e4e4e mt-30"><?= Helper::getDataContent(25) ?></div>
              </div>
            </div>
          </div><!-- stat -->
              </div>
          </div>
    </div><!-- end container -->


      <!-- <div class="row m-0">
          <div class="col-md-12 p-0">
              <a href="#"><img src="demos/1600x300.png" class="img-responsive" /></a>
          </div>
      </div> -->

      <div class="row ml-0 mr-0" >
          <div class="col-sm-12 p-0">
              <div class="bg-gray pt-50 pb-50" style="background:rgba(0, 0, 0, 0.7) !important">
                  <h1 class="m-0 p-0 text-center text-white font-36"><span class="font-familly-proximareg">ANY QUESTION?</span> <strong><a href="<?= Yii::$app->params['base_url'];?>hubungi-kami" class="contact-about">CONTACT US</a></strong></h1>
              </div>
          </div>
      </div>
  </section><!--end white-wrapper -->
