<?php
use app\helper\Helper;
$content = Helper::getPageContent(13);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: Yii::$app->params['base_url'].'demos/1920x300.png');
$this->title = $model->title . ' | Radio House';
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_description
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_keywords
]);
?>
<section class="banner-img whiteText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url'] ?>"><i class="fa fa-home"></i></a></li>
                      <li>Pembelian Tiket</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Pembelian Tiket</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="pageAccount pt-70 pb-60 pt-sm-60">

      <div class="container page-information-process page-pembelianTiket">
          <div class="success-message text-center">
              <h1 class="mt-0 font-26">Pembelian Tiket (<?= $model->title ?>)</h1>
          </div>
          <div class="col-sm-10 col-sm-offset-1 main-konfirmasi well margintop-30 pr-40 pb-40 pl-40 pt-30 pr-sm-20 pl-sm-20 pb-sm-20 pt-sm-20">
              <form id="buy-ticket">
                  <p>Silahkan mengisi form pembelian tiket dibawah ini.</p>
                  <div class="headTiket mt-20">
                      <h2 class="mt-0 pb-0">Personal Data</h2>
                      <hr />
                  </div>
                  <div class="form-group">
                      <label>Nama Lengkap <span class="sparator">*</span></label>
                      <input type="text" name="name" value="" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <label>Email <span class="sparator">*</span></label>
                      <input type="email" value="" name="email" class="form-control"/>
                  </div>
                  <!-- <div class="form-group">
                      <label>No Identitas <span class="sparator">*</span></label>
                      <input type="text" name="identity" class="form-control"/>
                  </div> -->
                  <div class="form-group">
                      <label>No Handphone <span class="sparator">*</span></label>
                      <input type="text" value="" name="handphone" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <label>Alamat Lengkap <span class="sparator">*</span></label>
                      <textarea name="address" class="form-control" rows="3"></textarea>
                  </div>

                  <div class="headTiket mt-40">
                      <h2 class="mt-0 pb-0">Tiket</h2>
                      <hr />
                  </div>
                  <div class="form-group">
                      <label>Kategori Tiket <span class="sparator">*</span></label>
                      <select class="form-control" name="category_ticket">
                          <option value="">-- Pilih Kategori --</option>
                          <?php foreach($tickets as $ticket){ ?>
                            <option value="<?= $ticket->id ?>"><?= $ticket->category ?> (Rp.<?= number_format($ticket->price) ?>)</option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Jumlah <span class="sparator">*</span></label>
                      <input type="number" value="1" min="1" name="total_ticket" class="form-control"/>
                  </div>
                  <input type="hidden" name="news_event_id" value="<?= $model->id ?>">
                  <button type="submit" class="btn btn-primary btnKonfrim mt-20">Beli Tiket</button>
              </form>
          </div>
  </div><!-- end container -->

  </section><!--end white-wrapper -->
