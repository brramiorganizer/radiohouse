<?php
use app\helper\Helper;
$content = Helper::getPageContent(9);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']?>"><i class="fa fa-home"></i></a></li>
                      <li class="text-green">FAQ</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">FAQ</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="page-under pt-70 pb-50">
    <div class="container">

          <div class="col-md-12 p-0 p-sm-0">
              <div class="doc">
                  <div class="widget">
                      <div class="about_tabbed">
                          <div class="panel-group" id="accordion2">
                              <?php $no=1;foreach($datum as $data){ ?>
                                <?php
                                $active = ($no == 1 ? 'active' : '');
                                $in = ($no == 1 ? 'in' : '');
                                ?>
                              <div class="panel panel-default">
                                  <div class="panel-heading <?= $active ?>">
                                      <h4 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?= $no ?>"><?= $data->question ?></a>
                                      </h4>
                                  </div><!-- end panel-heading -->
                                  <div id="collapse<?= $no ?>" class="panel-collapse collapse <?= $in ?>">
                                      <div class="panel-body">
                                          <p><?= htmlentities($data->answer) ?></p>
                                      </div><!-- end panel-body -->
                                  </div><!-- end collapseOne -->
                              </div><!-- end panel -->
                              <?php $no++;} ?>

                              
                          </div><!-- end panel-group -->
                      </div><!-- end about tabbed -->
                  </div><!-- end widget -->
              </div><!-- end doc -->
          </div>

    </div><!-- end container -->
  </section><!--end white-wrapper -->
