<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <div class="container" style="background:white;">
        <div class="row" style="margin:40px 0">
          <div class="col-md-6 col-sm-6 col-lg-6">
            <div style="margin-left:50px; margin-top:50px">
              <h1 style="font-size:60px; font-weight:100 !important">Oops!</h1>
              <p>Halaman tidak tersedia, silahkan anda kembali ke beranda dengan klik <a style="color:#17c405" href="<?= Yii::$app->params['base_url'] ?>">disini</a></p>
              <p style="color:red; font-size:12px">Error code: 404</p>
            </div>

          </div>
          <div class="col-md-6 col-sm-6 col-lg-6">
            <div style="margin-left:50px">
              <img src="<?= Yii::$app->params['base_url'] ?>/images/notfound.gif" class="pull-right" alt="page not found">
            </div>
          </div>
        </div>
    </div>

</div>
