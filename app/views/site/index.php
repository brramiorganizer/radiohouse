<?php
use app\helper\Helper;
?>
<input type="hidden" id="thisindex">
<?php if($main){ ?>
<audio class="hidden" controls="controls"><source src="<?= $main->radio->api ?>" type="audio/mpeg" /></audio>
<?php } ?>
<section class="slider-wrapper">

    <div class="tp-banner-container">
        <div class="tp-banner" >
            <div class="flexslider">
                <ul class="slides">
                  <?php foreach($sliders as $slider){
                    if(date('Y-m-d H:i:s') < $slider->expired){
                    ?>
                    <li>
                        <img src="<?= Yii::$app->params['base_url'] ?>media/slider-home/<?= $slider->file ?>" alt="" class="img-responsive">
                        <div class="flex-caption">
                            <h2 class="mb-10"><?= $slider->title ?></h2>
                            <p><?= $slider->description ?></p>
                            <?php if($slider->link != ''){ ?>
                            <a href="<?= $slider->link ?>" class="btn btn-primary mt-10">View More</a>
                          <?php } ?>
                        </div>
                        <div class="overlay"></div>
                    </li>
                  <?php } } ?>
                </ul><!-- end slides -->
            </div><!-- end post-slider -->
        </div>
    </div>
</section><!-- end slider-wrapper -->
    <!-- Radio Player -->
    <?php if($main){ ?>
    <section class="radio-palyer bg-green-primary">
        <div class="container">
            <div class="row m-xs-0">
                <div class="col-sm-3 col-sm-offset-2 border-right pt-20 pb-15 hidden-xs">
                    <div class="img-radio">
                      <?php $imgprof = ($main->radio->content->photo_profile == '' ? Yii::$app->params['base_url'].'demos/800x800.png' : Yii::$app->params['base_url'].'media/radio/profile/'.$main->radio->content->photo_profile) ?>
                        <img width="70" src="<?= $imgprof ?>" class="img-responsive" />
                    </div>
                    <div class="title-radio">
                        <h3 class="p-0 m-0"><?= $main->radio->radio_name ?></h3>
                        <p class=""><?= $main->radio->city  ?></p>
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12 pt-20 pt-xs-30 pb-xs-30 pl-40 pl-xs-0 pr-xs-0">
                    <div id="audio-player">
                        <div id="buttons">
            				<span>
            					<button id="play"></button>
            					<button id="pause"></button>
            				</span>
            			</div>
            			<div id="tracker">
                            <div class="hidden-dekstop">
                                <h3 class="p-0 m-0"><?= $main->radio->radio_name ?></h3>
                                <p class=""><?= $main->radio->city  ?></p>
                            </div>
            				<div id="progress-bar">
            					<span id="progress"></span>
                                <p class="timer-player">0:00</p>
            				</div>
            			</div>
                        <div id="volume">
                            <span class="dropdown-hover">
                                <button id="volumeup"></button>
            					<button id="volumemute"></button>
                            </span>
                            <div class="dropdown-menu-volume">
                                <div id="master" style="width:100px;"></div>
                            </div>
                        </div>
                        <div class="button-share-radio ml-xs-10">
                            <span class="dropdown">
                                <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span></span></a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">

                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= Yii::$app->params['base_url'] ?>channel/<?= $main->radio->slug ?>" target="_blank" class="face"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank"  href="https://twitter.com/intent/tweet?text=<?= strip_tags($main->radio->radio_name) . ' ' .Yii::$app->params['base_url'].'channel/'.$main->radio->slug?>" data-size="large"class="twit"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end Radio Player -->
   <?php } ?>
    <!-- Radio Channel-->
    <section class="radio-channel pt-125 pb-95 pt-sm-60 pb-sm-60">
        <div class="container">
            <div class="row">
                <div class="img-radio-channel">
                    <div class="col-sm-4">
                        <div class="portfolio_item wow fadeInLeft">
                    <div class="entry">
                                <a href="<?= Helper::getAdvertismentOneLink(1)?>" target="_blank" >
                                    <div class="square-img-bc bg-img" style="background-image: url('<?= Helper::getAdvertismentOne(1);?>');">
                              <div class="magnifier">
                                <div class="buttons">
                                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(1)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(1)[0]) ?></h3>
                                  <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(1)[1]  ?></h4>
                                </div><!-- end buttons -->
                              </div><!-- end magnifier -->
                                    </div>
                                </a>
                    </div><!-- end entry -->
                  </div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(2)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(2);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(2)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(2)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(2)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(3)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(3);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(3)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(3)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(3)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(4)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(4);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(4)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(4)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(4)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(5)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(5);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(5)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(5)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(5)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(6)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(6);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(6)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(6)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(6)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                            <div class="entry">
                                        <a href="<?= Helper::getAdvertismentOneLink(7)?>" target="_blank" >
                                  <img src="<?= Helper::getAdvertismentOne(7);?>" alt="" class="img-responsive">
                                  <div class="magnifier">
                                    <div class="buttons">
                                      <h3 class="p-0 mt-0 mb-5"><?= (Helper::getAdvertismentOneName(7)[0] == '' ? 'Radio House' : Helper::getAdvertismentOneName(7)[0]) ?></h3>
                                      <h4 class="p-0 m-0"><?= Helper::getAdvertismentOneName(7)[1]  ?></h4>
                                    </div><!-- end buttons -->
                                  </div><!-- end magnifier -->
                                        </a>
                            </div><!-- end entry -->
                          </div><!-- end portfolio_item -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="text-radio-channel text-center mt-10">
                    <div class="row m-xs-0">
                        <div class="col-sm-offset-2 col-sm-8">

                            <a href="<?= Yii::$app->params['base_url'] ?>channel" target="_blank"  class="btn btn-default btn-lg mt-30 hvr-sweep-to-right-green wow fadeInUp">Radio Channel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end Radio Channel -->

    <!-- News & Event -->
    <section class="radio-news-event">
          <div class="container-fluid p-0">
              <div class="row m-0 mainEqual m-xs-0">


                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item title-news-event bg-green-primary p-40 wow fadeInLeft" data-wow-offset="200" data-wow-duration="1500ms">
                          <h1 class="m-0 p-0 text-white">Berita &amp; Event</h1>
                          <hr class="m-0" />
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
                          <a href="<?= Yii::$app->params['base_url'] ?>news-events" target="_blank"  class="btn btn-default btn-lg hvr-sweep-to-right">Lihat Semua Berita &amp; Event</a>
                      </div>
                  </div>


                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item wow fadeInLeft" data-wow-offset="260" data-wow-duration="1500ms">
          				<div class="entry">
          					<img src="<?= Helper::getAdvertismentNeOne(1)['image']; ?>" alt="" class="img-responsive">
          					<div class="magnifier m-15 m-xs-0">
          						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                      <h3><?= (Helper::getAdvertismentNeOne(1)['title'] == '' ? 'Radio House' : Helper::getAdvertismentNeOne(1)['title']) ?></h3>
                                      <?php if(Helper::getAdvertismentNeOne(1)['description'] != ''){ ?>
                                      <p><?= Helper::getAdvertismentNeOne(1)['description'] ?></p>
          							<a href="<?= Helper::getAdvertismentNeOne(1)['link'] ?>" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
                      <?php } ?>
                      </div><!-- end buttons -->
          					</div><!-- end magnifier -->
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>


                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item wow fadeInLeft" data-wow-offset="320" data-wow-duration="1500ms">
          				<div class="entry">
          					<img src="<?= Helper::getAdvertismentNeOne(2)['image']; ?>" alt="" class="img-responsive">
          					<div class="magnifier m-15 m-xs-0">
          						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                      <h3><?= (Helper::getAdvertismentNeOne(2)['title'] == '' ? 'Radio House' : Helper::getAdvertismentNeOne(2)['title']) ?></h3>
                                      <?php if(Helper::getAdvertismentNeOne(2)['description'] != ''){ ?>
                                      <p><?= Helper::getAdvertismentNeOne(2)['description'] ?></p>
          							<a href="<?= Helper::getAdvertismentNeOne(2)['link'] ?>" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
                      <?php } ?>
          						</div><!-- end buttons -->
          					</div><!-- end magnifier -->
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item wow fadeInRight" data-wow-offset="260" data-wow-duration="1500ms">
          				<div class="entry">
          					<img src="<?= Helper::getAdvertismentNeOne(3)['image']; ?>" alt="" class="img-responsive">
          					<div class="magnifier m-15 m-xs-0">
          						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                      <h3><?= (Helper::getAdvertismentNeOne(3)['title'] == '' ? 'Radio House' : Helper::getAdvertismentNeOne(3)['title']) ?></h3>
                                      <?php if(Helper::getAdvertismentNeOne(3)['description'] != ''){ ?>
                                      <p><?= Helper::getAdvertismentNeOne(3)['description'] ?></p>
          							<a href="<?= Helper::getAdvertismentNeOne(3)['link'] ?>" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
                      <?php } ?>
          						</div><!-- end buttons -->
          					</div><!-- end magnifier -->
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item wow fadeInRight" data-wow-offset="300" data-wow-duration="1500ms">
          				<div class="entry">
          					<img src="<?= Helper::getAdvertismentNeOne(4)['image']; ?>" alt="" class="img-responsive">
          					<div class="magnifier m-15 m-xs-0">
          						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                      <h3><?= (Helper::getAdvertismentNeOne(4)['title'] == '' ? 'Radio House' : Helper::getAdvertismentNeOne(4)['title']) ?></h3>
                                      <?php if(Helper::getAdvertismentNeOne(4)['description'] != ''){ ?>
                                      <p><?= Helper::getAdvertismentNeOne(4)['description'] ?></p>
          							<a href="<?= Helper::getAdvertismentNeOne(4)['link'] ?>" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
                      <?php } ?>
          						</div><!-- end buttons -->
          					</div><!-- end magnifier -->
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-6 col-md-4 p-0 equal-1">
                      <div class="portfolio_item wow fadeInRight" data-wow-offset="350" data-wow-duration="1500ms">
          				<div class="entry">
          					<img src="<?= Helper::getAdvertismentNeOne(5)['image']; ?>" alt="" class="img-responsive">
          					<div class="magnifier m-15 m-xs-0">
          						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                      <h3><?= (Helper::getAdvertismentNeOne(5)['title'] == '' ? 'Radio House' : Helper::getAdvertismentNeOne(5)['title']) ?></h3>
                                      <?php if(Helper::getAdvertismentNeOne(5)['description'] != ''){ ?>
                                      <p><?= Helper::getAdvertismentNeOne(5)['description'] ?></p>
          							<a href="<?= Helper::getAdvertismentNeOne(5)['link'] ?>" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
                      <?php } ?>
          						</div><!-- end buttons -->
          					</div><!-- end magnifier -->
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
              </div>
          </div>

      </section><!-- end white-wrapper -->

    <!-- Category Channel -->
	<!-- <section id="one-parallax" class="parallax category-channel" style="background-image: url('demos/parallax_04.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay pt-125 pb-95">
        	<div class="container">
				<div class="stat f-container">
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-20">
                        	<i class="fa fa-user fa-3x text-green"></i>
                            <div class="milestone-details">Happy Customers</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-20">
                        	<i class="fa fa-coffee fa-3x text-green"></i>
                            <div class="milestone-details">Ordered Coffee's</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-20">
                        	<i class="fa fa-trophy fa-3x text-green"></i>
                            <div class="milestone-details">Awards Win</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-20">
                        	<i class="fa fa-camera fa-3x text-green"></i>
                            <div class="milestone-details">Photos Taken</div>
						</div>
					</div>
				</div> stat
            </div>end container
    	</div>end overlay
    </section> end transparent-bg -->

    <!-- Logo-Logo Channel -->



    <section class="white-wrapper channel-logo radio-channel pt-125 pb-95">
      	<div class="container">
          	<div class="row text-center m-xs-0 img-radio-channel">
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
          				<div class="entry">
                              <a href="<?= Helper::getTwoAdvertismentOneLink(1)?>" target="_blank" >
              					<img src="<?= Helper::getTwoAdvertismentOne(1);?>" alt="" class="img-responsive">
              					<div class="magnifier">
              						<div class="buttons">
              							<h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(1)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(1)[0] ) ?></h3>
                            <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(1)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(2)?>" target="_blank" >
                    <img src="<?= Helper::getTwoAdvertismentOne(2);?>" alt="" class="img-responsive">
                    <div class="magnifier">
                      <div class="buttons">
                        <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(2)[0]  == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(2)[0] ) ?></h3>
                        <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(2)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(3)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(3);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(3)[0]  == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(3)[0]) ?></h3>
                      <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(3)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(4)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(4);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(4)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(4)[0]) ?></h3>
                                <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(4)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(5)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(5);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(5)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(5)[0]) ?></h3>
                      <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(5)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(6)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(6);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(6)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(6)[0]) ?></h3>
                          <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(6)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(7)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(7);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(7)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(7)[0]) ?></h3>
                          <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(7)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(8)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(8);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(8)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(8)[0]) ?></h3>
                          <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(8)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(9)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(9);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(9)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(9)[0]) ?></h3>
                          <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(9)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
                  <div class="col-sm-2">
                      <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
          				<div class="entry">
                    <a href="<?= Helper::getTwoAdvertismentOneLink(10)?>" target="_blank" >
              <img src="<?= Helper::getTwoAdvertismentOne(10);?>" alt="" class="img-responsive">
              <div class="magnifier">
                <div class="buttons">
                  <h3 class="p-0 mt-0 mb-5"><?= (Helper::getTwoAdvertismentOneName(10)[0] == '' ? 'Radio House' : Helper::getTwoAdvertismentOneName(10)[0]) ?></h3>
                          <h4 class="p-0 m-0"><?= Helper::getTwoAdvertismentOneName(10)[1]  ?></h4>
              						</div><!-- end buttons -->
              					</div><!-- end magnifier -->
                              </a>
          				</div><!-- end entry -->
          			</div><!-- end portfolio_item -->
                  </div>
              </div>
  		</div><!-- end container -->
      </section><!-- end grey-wrapper -->

      <section id="one-parallax" class="parallax category-channel"  data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
      <div class="overlay pt-125 pb-95">
              <div class="container-fluid p-0">
                  <div class="row ml-0 mr-0 secAnyQuestion">
                      <div class="col-sm-12 p-0">
                          <div class="pl-xs-15 pr-xs-15">
                              <h1 class="m-0 p-0 text-center text-white font-36"><span class="font-familly-proximareg">ANY QUESTION?</span> <strong><a href="<?= Yii::$app->params['base_url']?>hubungi-kami" class="contact-about">CONTACT US</a></strong></h1>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <style media="screen">
        .portfolio_item img{
          width: 100%;
          height: 230px
        }
      </style>
