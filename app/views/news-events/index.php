<?php
use app\helper\Helper;
$content = Helper::getPageContent(5);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: Yii::$app->params['base_url'].'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $content->meta_description
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $content->meta_keywords
]);
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner detail-page-other">
                  <ul class="m-0">
                      <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                      <li class="text-green">Tentang Radio House</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="page-under pt-60 pb-40 newsEvent">
    <div class="container">
          <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pr-sm-15">
              <div class="breadcrumb-sidebar">
                  <ul class="m-0">
                      <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                      <li>News &amp; Events</li>
                  </ul>
              </div>
              <div class="widget mb-30">
                  <div class="title">
                      <h2 class="mt-10 p-0 mb-5">News &amp; Events</h2>
                      <?= $content->description ?>
                  </div><!-- end title -->
              </div>
              <div class="widget mb-30">
                  <h3 class="mt-0 mb-10 pb-5">Radio Channel</h3>
                  <div class="search-news-event">
                      <form method="" action="">
                          <div class="form-group">
                              <input type="text" id="filter-channel" name="" class="form-control" placeholder="Search" />
                              <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                          </div>
                      </form>
                  </div>
                  <div class="list-radioChannel">
                    <ul id="filter-channel-data">
                        <?php foreach($radios as $radio){ ?>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>channel/<?= $radio->slug ?>"><i class="fa fa-angle-right" aria-hidden="true"></i><?= ucwords($radio->radio_name) ?></a></li>
                        <?php } ?>

                    </ul>
                    <p class="hidden no-data-channel text-center">no data available</p>
                      <!-- <ul>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                          <li><a href="" id="viewMore"><i class="fa fa-plus"></i>View More  </a></li>
                      </ul> -->
                  </div>
              </div>

              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hiddenMobile">
                  <div class="widget mb-30">
                    <?php if(Helper::getNeChannel(1)->image != ''  and Helper::getNeChannel(1)->expired >= date('Y-m-d H:i:s')){ ?>
                      <?php $link = (Helper::getNeChannel(1)->link != '' ? Helper::getNeChannel(1)->link : '#') ?>
                      <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(1)->image ?>" class="img-responsive" /></a>
                    <?php }else{ ?>
                      <img class="img-responsive"  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">

                    <?php } ?>

                  </div>
                  <div class="widget mb-30">
                    <?php if(Helper::getNeChannel(2)->image != '' and Helper::getNeChannel(2)->expired >= date('Y-m-d H:i:s')){ ?>
                      <?php $link = (Helper::getNeChannel(2)->link != '' ? Helper::getNeChannel(2)->link : '#') ?>
                      <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(2)->image ?>" class="img-responsive" /></a>
                    <?php }else{ ?>
                      <img class="img-responsive" src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">

                    <?php } ?>
                  </div>
              </div>
              <!-- end images banner sale -->

          </div><!-- end left-sidebar -->

        <div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-news-event pb-0 mt-xs-40">

              <div class="row portfolio-filter mt-0 ">
                  <div class="text-left">
                      <a href="<?= Yii::$app->params['base_url'] ?>news-events">All</a>
                      <a href="<?= Yii::$app->params['base_url'] ?>news-events?type=news">News</a>
                      <a href="<?= Yii::$app->params['base_url'] ?>news-events?type=event">Events</a>
                  </div>
              </div>

              <div class="row content-news-event">
                  <div class="grid mainEqual">
                      <?php foreach($newsevents as $newsevent){ ?>
                      <div class="col-sm-6 equal-1 mb-30 filter news">
                          <div class="blog-carousel" >
                              <div class="entry">
                                  <a href="<?= Helper::base_url().$newsevent->type.'/'.$newsevent->slug ?>">
                                    <img src="<?= Helper::base_url() ?>media/banner-news-event/<?= $newsevent->banner ?>" alt="" class="img-responsive">
                                  </a>
                              </div><!-- end entry -->
                              <div class="blog-carousel-header">
                                  <h3 class="p-0 mb-0"><a title="" href="<?= Helper::base_url().$newsevent->type.'/'.$newsevent->slug ?>"><?= $newsevent->title ?></a></h3>
                                  <div class="blog-carousel-meta">
                                      <span><i class="fa fa-calendar text-green"></i> <?= Helper::convertDateAmerican($newsevent->created_on) ?></span>
                                  </div><!-- end blog-carousel-meta -->
                              </div><!-- end blog-carousel-header -->
                              <div class="blog-carousel-desc">
                                  <p><?= Helper::limit_text(strip_tags($newsevent->description),15)?></p>
                                  <a href="<?= Helper::base_url().$newsevent->type.'/'.$newsevent->slug ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                              </div><!-- end blog-carousel-desc -->
                          </div><!-- end blog-carousel -->
                      </div><!-- end col-lg-4 -->
                    <?php } ?>



                  </div><!-- end blog-masonry -->

                  <div class="clearfix"></div>

                  <div class="pagination_wrapper text-center">
                      <!-- Pagination Normal -->
                      <?php $getnya = (isset($_GET['type']) ? 'news-events?type='.$_GET['type'].'&page=' : 'news-events?page='); ?>
                      <ul class="pagination">
                          <?php if(isset($_GET['page']) and $_GET['page'] > 1){ ?>
                            <li><a href="<?= Yii::$app->params['base_url'] .$getnya . ($_GET['page'] - 1) ?>">«</a></li>
                          <?php } ?>
                          <?php for($i = 1; $i <= $pagination; $i++){ ?>

                          <?php
                            $active = '';
                            if(isset($_GET['page'])){
                              if($_GET['page'] == $i){
                                $active = 'active';
                              }
                            }else{
                              if($i == 1){
                                $active = 'active';
                              }
                            }
                          ?>
                          <li class="<?= $active ?>"><a href="<?= Yii::$app->params['base_url'] .$getnya. $i ?>"><?= $i ?></a></li>
                          <?php } ?>
                          <?php $next = (isset($_GET['page']) ? $_GET['page'] + 1 : 2); ?>
                          <?php if($pagination > ($next - 1)){ ?>
                            <li><a href="<?= Yii::$app->params['base_url'] .$getnya. $next ?>">»</a></li>
                          <?php } ?>
                      </ul>
                  </div><!-- end pagination_wrapper -->

              </div>
          </div><!-- end content -->

          <div class="clearfix"></div>

          <div class="row m-0 pl-15 pr-15">
              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40 border-top-1px">
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                  </div>
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                  </div>
              </div>
              <!-- end images banner sale -->
          </div>

    </div><!-- end container -->
  </section><!--end white-wrapper -->
