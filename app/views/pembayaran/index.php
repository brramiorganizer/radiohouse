<?php
use app\helper\Helper;
$content = Helper::getPageContent(14);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: Yii::$app->params['base_url'].'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $content->meta_description
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $content->meta_keywords
]);
?>
<section class="banner-img whiteText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                      <li><a href="<?=  Yii::$app->params['base_url'] ?>"><i class="fa fa-home"></i></a></li>
                      <li>Pilih Pembayaran</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Pilih Pembayaran</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="pagePembayaran pt-70 pb-70 pt-sm-60">

      <div class="container page-information-process page-payment">
          <div id="processRent">
              <form id="choose-payment">
                  <div class="col-md-7">
                      <h2 class="mt-0 mb-0">Informasi Pemesanan <a href="" class="text-green pull-right font-14 editInfo" data-toggle="modal" data-target="#edit-ticket"><i class="fa fa-edit"></i> Edit</a></h2>
                      <div class="table-responsive">
                          <h3 class="mt-30"><i class="fa fa-user mr-10"></i>Personal Data</h3>
                          <table class="table">
                              <tr>
                                  <td class="pl-25"><strong>Nama Lengkap</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $event->name ?></td>
                              </tr>
                              <tr>
                                  <td class="pl-25"><strong>Email</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $event->email ?></td>
                              </tr>
                              <!-- <tr>
                                  <td class="pl-25"><strong>No Identitas</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;">0987654321</td>
                              </tr> -->
                              <tr>
                                  <td class="pl-25"><strong>No Handphone</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $event->no_phone ?></td>
                              </tr>
                              <tr>
                                  <td class="pl-25"><strong>Alamat Lengkap</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $event->address ?></td>
                              </tr>
                          </table>
                          <h3 class="mt-15"><i class="fa fa-ticket mr-10"></i>Tiket</h3>
                          <table class="table">
                              <tr>
                                  <td class="pl-25"><strong>Event</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $model->title ?></td>
                              </tr>
                              <!-- <tr>
                                  <td class="pl-25"><strong>Tanggal Event</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;">9 November 2017</td>
                              </tr> -->
                              <tr>
                                  <td class="pl-25"><strong>Kategori Tiket</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $ticket->category ?></td>
                              </tr>
                              <tr>
                                  <td class="pl-25"><strong>Jumlah</strong></td>
                                  <td class="text-center">:</td>
                                  <td class="" style="width: 450px;"><?= $event->quantity ?> Tiket</td>
                              </tr>
                          </table>
                      </div>

                      <h2 class="mt-40">Pilih Pembayaran</h2>
                      <div class="contentPilihPembayaran mt-20">
                          <div class="row">

                            <?php $nov = 1;foreach($banks as $bank){ ?>
                              <div class="col-sm-4">
                                  <div class="mainPembayaran text-center">
                                      <div class="radio">
                                          <label>
                                            <?php $active = ($nov == 1 ? 'checked="checked"' : '')?>
                                              <input <?= $active ?> type="radio" name="bank" id="optionsRadios1" value="<?= $bank->id ?>">
                                          </label>
                                      </div>
                                      <div class="contBank">
                                          <img src="<?= Yii::$app->params['base_url'] ?>media/bank/<?= $bank->image ?>" class="" />
                                          <p>Radio House</p>
                                          <p>0987654321</p>
                                      </div>
                                  </div>
                              </div>
                            <?php $nov++;} ?>
                          </div>
                      </div>

                  </div>

                  <div class="col-md-5">
                      <div class="sidebar-booking discount">
                          <h3 class="m-0 p-0 text-green">Invoice</h3>
                          <p class="text-right">RH-<?= $event->code ?></p>
                      </div>
                      <div class="sidebar-booking">
                          <h3 class="mt-0 mb-20 p-0 text-green">Keranjang</h3>
                          <div class="main-sidebar-booking">
                              <div class="row noteRent">
                                  <div class="col-xs-5">
                                      <h4 class="mb-5 pr-0">Jumlah Tiket</h4>
                                      <p class="small"><?= $model->title ?></p>
                                      <!-- <p class="small">Tanggal Event : <strong>09/11/2017</strong></p> -->
                                  </div>
                                  <div class="col-xs-1 text-center"><?= $event->quantity ?></div>
                                  <div class="col-xs-6 pl-0">IDR <?= number_format($ticket->price) ?></div>
                              </div>
                              <hr />
                              <!--<div class="row">
                                  <div class="col-xs-5">
                                      <p class="font-15">Shipping</p>
                                  </div>
                                  <div class="col-xs-1 text-center">
                                  </div>
                                  <div class="col-xs-6">
                                      <p>IDR 100.000</p>
                                  </div>
                              </div>
                              <hr />-->
                              <span class="totalPrice"><strong>Total</strong> IDR <?= number_format($ticket->price * $event->quantity) ?></span>
                          </div>
                      </div>
                      <!-- <div class="clearfix">
                          <div class="sidebar-booking discount sidebarKodeVoucher">
                              <h3>Kode Voucher</h3>
                              <div class="form-group">
                                  <label class="sr-only">Kode Voucher</label>
                                  <input type="text" class="form-control" placeholder=""/>
                                  <button type="submit" class="btn btn-primary pull-right btn-disc">Apply</button>
                              </div>
                          </div>
                      </div> -->
                      <div class="btnProses">
                        <input type="hidden" name="id" value="<?= $event->id ?>">
                          <button type="submit" class="btn btn-primary pull-right btn-lg">Pesan Sekarang</a>
                      </div>
                  </div>
              </form>
          </div>
  </div><!-- end container -->


      <!-- Modal Edit Information -->
      <div class="modal fade s1" style="background:rgba(0,0,0,0.5)" id="edit-ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form>
                    <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Edit Pemesanan</h4>
                    </div>
                    <div class="modal-body">
                          <div class="row">
                              <div class="col-md-12">

                                      <p>Silahkan mengisi form pembelian tiket dibawah ini.</p>
                                      <div class="headTiket mt-20 mb-20">
                                          <h2 class="mt-0 pb-0">Personal Data</h2>
                                      </div>
                                      <div class="form-group">
                                          <label>Nama Lengkap <span class="sparator">*</span></label>
                                          <input type="text" value="<?= $event->name ?>" name="name" class="form-control"/>
                                      </div>
                                      <div class="form-group">
                                          <label>Email <span class="sparator">*</span></label>
                                          <input type="email" readonly value="<?= $event->email ?>" name="email" class="form-control"/>
                                      </div>
                                      <!-- <div class="form-group">
                                          <label>No Identitas <span class="sparator">*</span></label>
                                          <input type="text" name="" class="form-control"/>
                                      </div> -->
                                      <div class="form-group">
                                          <label>No Handphone <span class="sparator">*</span></label>
                                          <input type="text" name="no_phone" value="<?= $event->no_phone ?>" class="form-control"/>
                                      </div>
                                      <div class="form-group">
                                          <label>Alamat Lengkap <span class="sparator">*</span></label>
                                          <textarea class="form-control" name="address" rows="3"><?= $event->address ?></textarea>
                                      </div>

                                      <div class="headTiket mt-40 mb-20">
                                          <h2 class="mt-0 pb-0">Tiket</h2>
                                      </div>
                                      <!-- <div class="form-group">
                                          <label>Event <span class="sparator">*</span></label>
                                          <select class="form-control">
                                              <option>-- Pilih Event --</option>
                                              <option>Lorem Ipsum - 9 September 2017</option>
                                              <option>Lorem Ipsum - 10 November 2017</option>
                                          </select>
                                      </div> -->
                                      <div class="form-group">
                                          <label>Kategori Tiket <span class="sparator">*</span></label>
                                          <select class="form-control" name="category_ticket">
                                              <option value="">-- Pilih Kategori --</option>
                                              <?php foreach($tickets as $tic){ ?>
                                                <?php $selected  = ($tic->id == $event->event_ticket_id ? 'selected="selected"' : ''); ?>
                                                <option <?= $selected ?> value="<?= $tic->id ?>"><?= $tic->category ?> (<?= $tic->price ?>)</option>
                                              <?php } ?>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Jumlah <span class="sparator">*</span></label>
                                          <input type="number" value="<?= $event->quantity ?>" name="total_ticket" class="form-control"/>
                                      </div>


                              </div>
                          </div>
                    </div>
                    <div class="modal-footer">
                      <input type="hidden" name="id" value="<?= $event->id ?>">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

  </section><!--end white-wrapper -->
