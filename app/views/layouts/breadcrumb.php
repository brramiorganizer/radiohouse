<div id="content-header">
  <div id="breadcrumb">
    <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
    <?php foreach($breadcrumb as $x){ ?>
      <?php $current = ($x['current'] == true ? 'current' : '') ?>
      <a href="<?= Yii::$app->params['base_url'].'dashboard/'.$x['url'] ?>" title="Go to <?= $x['page'] ?>" class="<?= $current ?>"><?= $x['page'] ?></a>
    <?php } ?>
  </div>
</div>
