<?php
use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?= Html::encode($this->title) ?></title>


  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta property="og:type" content="website" />
  <?php $this->head() ?>
  <meta name="author" content="Radio House">

  <!-- Favicon and Touch Icons -->
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/favicon.png" rel="shortcut icon" type="image/png">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

  <!-- Styles -->
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/bootstrap.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/datatables/datatables.min.css" rel="stylesheet">

  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/style.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/owl-carousel.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/animate.min.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/responsive.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/customs.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/flexslider.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/custom-bootstrap-margin-padding.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/utility-classes.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/css/jquery-ui.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/royalslider/royalslider.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/royalslider/skins/default-inverted/rs-default-inverted.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/taginput.css" rel="stylesheet">
  <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/rs-plugin/css/settings.css" media="screen"  rel="stylesheet" type="text/css"  />
  </head>
  <body>

    <?= $this->render('backstage-header') ?>
    <?= $content ?>
    <?= $this->render('backstage-footer') ?>
    <!-- Main Scripts-->
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.validate.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/datatables/datatables.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/glory.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/bootstrap.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery-ui.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/menu.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/owl.carousel.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.simple-text-rotator.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/wow.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/custom.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.isotope.min.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/custom-portfolio.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/jquery.flexslider.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5ayt8aat70ykxs52ge9x8tughowxi9zwzfii0lwm3nkmmimi"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/taginput.js"></script>
    <script src="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/js/main.js"></script>
    <?php if(Yii::$app->session->getFlash('success_regis_member')){ ?>
      <script type="text/javascript">
        setTimeout(function(){
          $('#myModal').modal('show');
          $('#loginform').prepend('<div class="alert alert-success">akun anda sudah aktif, silahkan masuk!</div>');
        },1000);
      </script>
    <?php } ?>
  </body>
</html>
<?php $this->endPage() ?>
