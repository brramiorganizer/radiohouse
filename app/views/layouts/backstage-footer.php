<footer id="footer-style-1" class="p-0 ">
    	<div class="container-fluid p-0">
            <div class="row menu-footer pl-70 pr-70 m-0 m-xs-0 pl-md-15 pr-md-15 pt-xs-20 pb-xs-20">
                <div class="nav-foot col-sm-10 col-xs-8 p-0">
                    <ul class="main-menu-foot mb-0">
                        <li><a href="<?= Yii::$app->params['base_url'] ?>about" class="hvr-underline-reveal">Tentang Radio House</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>channel" class="hvr-underline-reveal">Channel</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>news-events" class="hvr-underline-reveal">News &amp; Event</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>hubungi-kami" class="hvr-underline-reveal">Hubungi Kami</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>pricing" class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>syarat-ketentuan" class="hvr-underline-reveal">Syarat &amp; Ketentuan</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>faq" class="hvr-underline-reveal">FAQ</a></li>
                        <li><a href="<?= Yii::$app->params['base_url'] ?>privacy" class="hvr-underline-reveal">Privacy</a></li>
                    </ul>
                </div>
                <div class="social-media col-sm-2 col-xs-4 p-0">
                    <ul class="pull-right mb-0">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        	<div id="copyrights" class="bg-green-primary">
            	<div class="container p-0 m-0">
                	<div class="copyright-text pl-70 pr-70 pl-md-15 pr-md-15">
                        <p>Copyright © 2017. RADIOHOUSE.com</p>
                    </div><!-- end copyright-text -->
                </div><!-- end container -->
            </div><!-- end copyrights -->
    	</div><!-- end container -->
	   <div class="dmtop">Scroll to Top</div>

    </footer><!-- end #footer-style-1 -->
