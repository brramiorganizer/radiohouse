<?php
use app\helper\Helper;
?>
<input type="hidden" id="base" value="<?= Yii::$app->params['base_url'] ?>">
<input type="hidden" id="gils" value="<?= Yii::$app->request->csrfToken?>">

<!-- <audio class="hidden" controls="controls"><source src="all-we-know.mp3" type="audio/mpeg" /></audio> -->
    <div id="topbar" class="clearfix">

    </div><!-- end topbar -->

    <header id="header-style-1">
		<div class="container-fluid">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?= Yii::$app->params['base_url'] ?>" class="navbar-brand m-0 pl-50 pl-xs-0">
                      <?php $logo = Helper::getDataContent(16); ?>
                        <img src="<?= Yii::$app->params['base_url'] ?>images/<?= $logo ?>" alt="Radiohouse" class="m-0" width="180" />
                    </a>
                    <div class="main-menu hidden-xs">
                        <div class="dropdown">
                          <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MENU
                            <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <!--<li><a href="index.php">HOME</a></li>-->
                            <li><a href="<?= Helper::base_url() ?>about">TENTANG RADIO HOUSE</a></li>
                            <li class="dropdown-submenu">
                                <a href="<?= Helper::base_url() ?>channel">CHANNELS <span class="caret ml-10"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= Helper::base_url() ?>channel">All Channels</a></li>
                                    <?php foreach(Helper::getAdvMenu() as $a){ ?>
                                    <li><a href="<?= Helper::base_url().'channel/'.$a->radio->slug?>"><i class="fa fa-microphone mr-10"></i><?= $a->radio->radio_name?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li><a href="<?= Helper::base_url() ?>news-events">BERITA &amp; EVENT</a></li>
                          </ul>
                        </div>
                    </div>
                    <div class="search-header">
                        <form method="GET" action="<?= Yii::$app->params['base_url'] ?>channel">
                            <div class="form-group m-0">
                                <input class="form-control input-lg" name="s" type="text" placeholder="Search...">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
        		</div><!-- end navbar-header -->

				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right mr-20 mr-xs-0">
                    <ul class="nav navbar-nav hidden-xs navDesktop">
                      <?php if(!Yii::$app->session->get('radiotoken')){ ?>
                        <li><a href="<?= Yii::$app->params['base_url']?>pricing" class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li><a href="<?= Yii::$app->params['base_url']?>radio/login" class="hvr-underline-reveal">Login radio</a></li>

                        <?php //if(!Yii::$app->session->get('fronttoken')){ ?>
                          <!-- <li><a href="#" class="hvr-underline-reveal" data-toggle="modal" data-target="#myModal">Masuk / Register</a></li> -->
                        <?php //}else{ ?>
                          <?php //$member = Helper::getUserLogged();?>
                          <!-- <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="hvr-underline-reveal dropdown-toggle">Hi, <?php // $member->firstname ?></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php //  Helper::base_url()?>informasi-akun">Informasi Akun</a></li>
                                <li><a href="<?php //  Helper::base_url()?>history">History Transaksi</a></li>
                                <li><a href="<?php //  Helper::base_url()?>konfirmasi-pembayaran">Konfirmasi Pembayaran</a></li>
                                <li><a href="<?php // Yii::$app->params['base_url']?>site/logout-member">Log Out</a></li>
                            </ul>
                          </li> -->
                        <?php //} ?>
                      <?php }else{ ?>
                        <?php $member = Helper::getRadioLogged();?>
                        <li class="dropdown">
                          <a href="#" data-toggle="dropdown" class="hvr-underline-reveal dropdown-toggle" href="<?= Yii::$app->params['base_url']?>radio">Hi, <?= $member->radio_name ?></a>
                          <ul class="dropdown-menu" role="menu">
                              <li><a href="<?= Yii::$app->params['base_url']?>radio">Profile</a></li>
                              <li><a href="<?= Yii::$app->params['base_url']?>radio/logout">Log Out</a></li>
                          </ul>
                        </li>
                      <?php } ?>

                      <!--<li><a href="#" class="hvr-underline-reveal" data-toggle="modal" data-target="#myModal">Register</a></li>-->
                    </ul>
					<ul class="nav navbar-nav hidden-dekstop">
						<!--<li><a href="index.php">Home</a></li>-->
                        <li><a href="about.php">Tentang Radio House</a></li>
						<li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">CHANNELS <span class="caret ml-10"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="channel.php">ALL CHANNELS</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="#"><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                            </ul>
                        </li>
                        <li><a href="news-event.php">NEWS &amp; EVENTS</a></li>
                        <li><a href="menjadi-member.php">MENJADI MEMBER</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">MASUK</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">REGISTER</a></li>
					</ul><!-- end navbar-nav -->
				</div><!-- #navbar-collapse-1 -->
            </nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->

        <!--Modal Login & Register-->
        <?php if(!Yii::$app->session->get('fronttoken')){ ?>
        <!-- <div style="background:rgba(0,0,0,0.5)" class="modal fade loginnReg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title pb-0" id="myModalLabel">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-right" aria-hidden="true"></i></a> </h4>
              </div>
              <div class="modal-body">
                <div class="wrapper">
                    <div id="mask">
                        <div id="page-1" class="page">
                            <div class="widget">
                                <div class="title">
                                	<h3 class="mt-0 text-capitalize pt-0 pb-10">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-circle-right ml-5"></i></a></h3>
                                 </div>
                                <form id="loginform" method="post" name="loginform">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group mt-25">
                                        <button type="submit" class="btn btn-primary btn-lg">Masuk</button>
                                        <a href="<?= Yii::$app->params['base_url'] ?>member/forgot-password" class="lp-password">Lupa Password</a>
                                    </div>
                                </form>
                                <div class="orLogin">
                                    <div class="lineLogin">
                                        <hr />
                                        <span>Atau</span>
                                    </div>
                                    <div class="row option-login">
                                        <div class="col-sm-6">
                                            <a title="Gmail" href="<?= Yii::$app->params['base_url'] ?>site/auth?authclient=google" class="gmail"><i class="fa fa-google"></i>Masuk dengan Google</a>
                                        </div>
                                        <div class="col-sm-6">
                                            <a title="Facebook" href="<?= Yii::$app->params['base_url'] ?>site/auth?authclient=facebook" class="facebook"><i class="fa fa-facebook"></i>Masuk dengan Facebook</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="page-2" class="page">
                            <div class="page-login">
                                <a name="item2"></a>
                                <div class="widget last">
                                	<div class="title">
                                    	<h3 class="mt-0 pt-0 pb-10 text-capitalize">Register</h3>
                                    </div>
                                    <form id="registerform" method="post" name="registerform" action="detail-lelang.html">
                                        <div class="form-group">
                                            <input type="text" name="firstname" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="lastname" class="form-control" placeholder="Last name">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" id="registerpassword" name="password" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password"  name="repassword" class="form-control" placeholder="Re-enter password">
                                        </div>
                                        <div class="form-group mb-0">
                                            <a href="#page-1" class="scrollitem bc-login"><i class="fa fa-angle-left mr-5"></i>Back Login</a>
                                            <button type="submit" class="btn btn-primary btn-lg pull-right mt-15">SUBMIT</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
      <?php } ?>
        <!--End Modal Login & Register-->

	</header><!-- end header-style-1 -->

  <style media="screen">
    .hvr-underline-reveal:hover{
      color:#333 !!important
    }
  </style>
