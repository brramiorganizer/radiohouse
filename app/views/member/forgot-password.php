<div class="container content">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <p>Silahkan isi email anda.</p>
      <form id="forgot-password-member">
        <div class="form-group">
          <label for="">Email <span class="required">*</span></label><br/>
          <input type="email" class="form-control" name="email" id="email" value="">
        </div>

        <div class="form-group">

          <button type="submit" name="button" class="btn btn-asli btn-block">F I N I S H</button>
        </div>
      </form>
    </div>
  </div>
</div>
