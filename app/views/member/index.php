<?php
use app\helper\Helper;
$content = Helper::getPageContent(11);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
?>
<section class="banner-img whiteText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                      <li><a href="<?=  Yii::$app->params['base_url']?>"><i class="fa fa-home"></i></a></li>
                      <li>Informasi Akun</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Informasi Akun</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="pageAccount pt-70 pb-60 pt-sm-60">
    <div class="container">
          <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <ul class="list-group">
                  <li class="list-group-item"><span class="text-green"><strong>Hi Nama User,</strong></span></li>
                  <li class="list-group-item"><a href="<?= Helper::base_url()?>informasi-akun" class="text-black-444">Informasi Akun</a></li>
                  <li class="list-group-item"><a href="<?= Helper::base_url()?>history" class="text-black-444">History Transaksi</a></li>
                  <!-- <li class="list-group-item"><a href="<?= Helper::base_url()?>konfirmasi-pembayaran" class="text-black-444">Konfirmasi Pembayaran</a></li> -->
              </ul>
          </div><!-- end left-sidebar -->

        <div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-detail-news-event pb-0 mt-xs-40 mb-xs-40">
              <div class="row">
                  <div class="col-sm-6">
                      <?php  if(Yii::$app->session->hasFlash('success_change_profile')){?>
                        <div class="alert alert-success">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          Akun anda terbaharui
                        </div>
                      <?php } ?>
                      <div class="list-group">
                          <div class="list-group-item">
                              <div class="row">
                                  <div class="col-xs-6">
                                      <h4 class="m-0 p-0">Informasi Akun</h4>
                                  </div>
                                  <div class="col-xs-6 text-right">
                                      <a href="#" class="text-green" data-toggle="modal" data-target="#editAccount">
                                          <span class="fa fa-edit"></span>
                                          Edit
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <div class="list-group-item">
                              <div class="dsk">
                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">Nama Depan</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0"><?= $user->firstname ?></p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">Nama Belakang</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0"><?= ($user->lastname != '' ? $user->lastname : '-') ?></p>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">No Handphone</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0"><?= ($user->number_phone != '' ? $user->number_phone : '-') ?></p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">Email</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0"><?= $user->email ?></p>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">Alamat</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0"><?= ($user->address != '' ? $user->address : '-')?></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <!-- Modal Edit Account -->
                      <div class="modal fade change" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <form id="edit-profil-visitor">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title p-0">Edit Informasi Akun</h4>
                                    </div>
                                    <div class="modal-body pb-0">
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Nama Depan <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="firstname" value="<?= $user->firstname ?>" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Nama Belakang</label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="lastname" value="<?= $user->lastname ?>" class="form-control"/>
                                              </div>

                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Jenis Kelamin <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <?php
                                                  $checked_l = ($user->gender == 'l' ? 'checked="checked"' : '');
                                                  $checked_p = ($user->gender == 'p' ? 'checked="checked"' : '');

                                                   ?>
                                                  <div class="radio">
                                                    <label class="mr-20">
                                                      <input type="radio" <?= $checked_l ?> name="gender" id="optionsRadios1" value="l" >
                                                      Pria
                                                    </label>
                                                    <label>
                                                      <input type="radio" <?= $checked_p ?> name="gender" id="optionsRadios1" value="p" >
                                                      Wanita
                                                    </label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Tempat Lahir <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="place_of_birth" value="<?= $user->place_of_birth ?>" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Tanggal Lahir <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="date_of_birth" value="<?= $user->date_of_birth ?>" class="form-control datepicker" readonly/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">Email <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <input type="email" name="email" value="<?= $user->email ?>" class="form-control" readonly/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">No KTP</label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="ktp" value="<?= $user->ktp ?>" class="form-control"/>
                                              </div>

                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">No. Handphone</label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="number_phone" value="<?= $user->number_phone ?>" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">Alamat <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <textarea class="form-control" name="address" rows="3"><?= $user->address ?></textarea>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Kota <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="city" value="<?= $user->city ?>" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Provinsi <span class="required">*</span></label>
                                              <div class="col-sm-8">
                                                  <select class="form-control" name="province">
                                                      <option value="">-- Pilih Provinsi --</option>
                                                      <?php foreach($provinces as $province){ ?>
                                                        <?php $selected = ($province->id == $user->province ? 'selected="selected"' : '') ?>
                                                        <option <?= $selected ?> value="<?= $province->id ?>"><?= $province->name ?></option>
                                                      <?php } ?>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentname">Kode Pos</label>
                                              <div class="col-sm-8">
                                                  <input type="text" name="zip_code" value="<?= $user->zip_code ?>" class="form-control"/>
                                              </div>
                                          </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="modal-footer">
                                      <input type="hidden" name="id" value="<?= $user->id ?>">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                              </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                      </div><!-- /.modal -->
                  </div>
                  <div class="col-sm-6">
                    <?php if(Yii::$app->session->hasFlash('success_change_password')){ ?>
                      <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        password anda berhasil diubah
                      </div>
                    <?php } ?>
                      <div class="list-group">
                          <div class="list-group-item">
                              <div class="row">
                                  <div class="col-xs-6">
                                      <h4 class="m-0 p-0">Password</h4>
                                  </div>
                                  <div class="col-xs-6 text-right">
                                      <a href="" class="text-green" data-toggle="modal" data-target="#editPassword">
                                          <span class="fa fa-edit"></span>
                                          Edit
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <div class="list-group-item">
                              <div class="dsk">
                                  <div class="row">
                                      <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                          <p class="m-0 font-weight-900">Password</p>
                                      </div>
                                      <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                          <p class="m-0">*****</p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <!-- Modal Edit Account -->
                      <div class="modal fade change" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <form id="edit-password-member">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title p-0">Edit Password</h4>
                                    </div>
                                    <div class="modal-body pb-0">
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">Old Password</label>
                                              <div class="col-sm-8">
                                                  <input type="password" name="old_password" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">New Password</label>
                                              <div class="col-sm-8">
                                                  <input type="password" name="new_password" id="new_password" class="form-control"/>
                                              </div>
                                          </div>
                                          <div class="form-group mb-15">
                                              <label class="col-sm-4" for="#currentaddress">Retype new Password</label>
                                              <div class="col-sm-8">
                                                  <input type="password" name="retype_new_password" class="form-control"/>
                                              </div>
                                          </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="modal-footer">
                                      <input type="hidden" name="id" value="<?= $user->id ?>">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                              </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                      </div><!-- /.modal -->
                  </div>
              </div>
          </div><!-- end content -->

          <!-- Modal -->
          <div class="modal fade" id="myModalJoinEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title pb-0" id="myModalLabel">Join Event <span class="text-grey text-gray-silver ml-5">Nama Event</span></h4>
                </div>
                <form method="" action="">
                    <div class="modal-body">
                      <p>Silahkan isi form pendaftaran di bawah ini untuk berpartisipasi dalam event ini.</p>
                      <div class="form-group mt-15">
                          <label>Nama Lengkap</label>
                          <input type="text" class="form-control" name="" placeholder="Jhon Doe" />
                      </div>
                      <div class="form-group mt-15">
                          <label>Email</label>
                          <input type="email" class="form-control" name="" placeholder="email@example.com" />
                      </div>
                      <div class="form-group mt-15">
                          <label>No Telepon / HP</label>
                          <input type="text" class="form-control" name="" placeholder="0***" />
                      </div>
                      <div class="form-group mt-15">
                          <label>Alamat Lengkap</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                      <div class="form-group mt-15">
                          <label>Password</label>
                          <input type="password" class="form-control" name="" placeholder="***" />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" name="" class="btn btn-primary btn-lg pull-left">Kirim</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

    </div><!-- end container -->
  </section><!--end white-wrapper -->
