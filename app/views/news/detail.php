<?php
use app\helper\Helper;
$content = Helper::getPageContent(12);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: Yii::$app->params['base_url'].'demos/1920x300.png');
$this->title = $model->title.' | Radio House';
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => strip_tags($model->meta_description)
]);
Yii::$app->view->registerMetaTag([
    'name' => 'og:description',
    'content' => strip_tags($model->meta_description)
]);
Yii::$app->view->registerMetaTag([
    'name' => 'og:title',
    'content' => strip_tags($model->title)
]);
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => strip_tags($model->meta_keywords)
]);
Yii::$app->view->registerMetaTag([
    'name' => 'og:image',
    'content' => Helper::base_url().'media/banner-news-event/'.$model->banner
]);
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner detail-page-other">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                      <li>Tentang Radio House</li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="page-under pt-70 pb-60 pt-sm-60">
    <div class="container">
          <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 detail-news col-xs-12 pr-xs-15 pr-sm-0">
              <div class="breadcrumb-sidebar">
                  <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                      <li class="title-detail-news text-green"><?= $model->title ?></li>
                  </ul>
              </div>
              <div class="widget mt-20 mb-40 pageChannel hiddenMobile">
                  <div class="title">
                      <h2 class="mt-10 p-0 mb-5">The Latest News &amp; Events</h2>
                  </div><!-- end title -->
                  <div class="value-latest mt-30 pageChannel hiddenMobile">

                    <?php foreach($latest as $late){ ?>

                      <div class="row media mb-20">
                          <a href="<?php echo Helper::base_url().$late->type.'/'.$late->slug ?>">
                              <div class="media-left">
                                  <img class="media-object ml-15" src="<?= Helper::base_url() ?>media/banner-news-event/<?= $late->banner ?>" style="width: 100px;" />
                              </div>
                              <div class="media-body">
                                  <h4 class="media-heading"><?= $late->title ?></h4>
                              </div>
                          </a>
                      </div>
                      <?php } ?>

                  </div>
              </div>

              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hiddenMobile">
                <div class="widget mb-30">
                  <?php if(Helper::getNeChannel(1)->image == ''){ ?>
                    <img class="img-responsive"  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
                  <?php }else{ ?>
                    <?php $link = (Helper::getNeChannel(1)->link != '' ? Helper::getNeChannel(1)->link : '#') ?>
                    <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(1)->image ?>" class="img-responsive" /></a>
                  <?php } ?>

                </div>
                <div class="widget mb-30">
                  <?php if(Helper::getNeChannel(2)->image == ''){ ?>
                    <img class="img-responsive" src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
                  <?php }else{ ?>
                    <?php $link = (Helper::getNeChannel(2)->link != '' ? Helper::getNeChannel(2)->link : '#') ?>
                    <a href="<?= $link ?>"><img src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(2)->image ?>" class="img-responsive" /></a>
                  <?php } ?>
                </div>
              </div>
              <!-- end images banner sale -->

          </div><!-- end left-sidebar -->

        <div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-detail-news-event pb-0 mt-xs-40 mb-xs-40">

              <div class="row content-news-event m-0">
                  <div class="heading-detail-ne">
                      <h2 class="p-0 mb-0 mt-0 font-26"><?= $model->title ?></h2>
                      <div class="blog-carousel-meta">
                          <span><i class="fa fa-user text-green"></i>
                            <?php if($model->radio_id == 0){ ?>
                              Administrator
                            <?php }else{ ?>
                              <?= Helper::getOneRadio($model->radio_id)->radio_name ?>
                            <?php } ?>
                          </span>
                          <span><i class="fa fa-calendar text-green"></i> <?= Helper::convertDateAmerican($model->created_on) ?></span>
                          <span class="dropdown">
                              <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share text-green"></i> Share</a>
                              <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= Yii::$app->params['base_url'] ?>event/<?= $model->slug ?>" target="_blank" class="face"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank"  href="https://twitter.com/intent/tweet?text=<?= strip_tags($model->title) . ' ' .Yii::$app->params['base_url'].'channel/'.$model->slug?>" data-size="large"class="twit"><i class="fa fa-twitter"></i></a></li>
                              </ul>
                              </ul>
                          </span>
                      </div><!-- end blog-carousel-meta -->
                  </div>
              </div>
              <div class="ne-carousel-desc mt-20">
                  <div class="desc-img mb-20">
                      <img style="margin:auto" src="<?= Helper::base_url() ?>media/banner-news-event/<?= $model->banner ?>" class="img-responsive" />
                  </div>
                  <div class="desc-content">

                    <?= $model->description ?>
                  </div>
              </div>
          </div><!-- end content -->

          <!-- Modal -->
          <div class="modal fade" id="myModalJoinEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title pb-0" id="myModalLabel">Join Event <span class="text-grey text-gray-silver ml-5">Nama Event</span></h4>
                </div>
                <form method="" action="join-event.php">
                    <div class="modal-body">
                      <p>Silahkan isi form pendaftaran di bawah ini untuk berpartisipasi dalam event ini.</p>
                      <div class="form-group mt-15">
                          <label>Nama Lengkap</label>
                          <input type="text" class="form-control" name="" placeholder="Jhon Doe" />
                      </div>
                      <div class="form-group mt-15">
                          <label>Email</label>
                          <input type="email" class="form-control" name="" placeholder="email@example.com" />
                      </div>
                      <div class="form-group mt-15">
                          <label>No Telepon / HP</label>
                          <input type="text" class="form-control" name="" placeholder="0***" />
                      </div>
                      <div class="form-group mt-15">
                          <label>Alamat Lengkap</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                      <div class="form-group mt-15">
                          <label>Password</label>
                          <input type="password" class="form-control" name="" placeholder="***" />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" name="" class="btn btn-primary btn-lg pull-left">Kirim</button>
                    </div>
                </form>
              </div>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="row m-0 pl-15 pr-15">
              <div class="title hidden-dekstop viewMobile pageChannel detailNews border-top-1px">
                  <h2 class="mt-10 p-0 mb-5">The Latest News &amp; Events</h2>
              </div><!-- end title -->
              <div class="value-latest mt-30 pageChannel hidden-dekstop viewMobile">
                <?php foreach($latest as $late2){ ?>
                <div class="row media mb-20">
                    <a href="<?php echo Helper::base_url().$late2->type.'/'.$late2->slug ?>">
                        <div class="media-left">
                            <img class="media-object ml-15" src="<?= Helper::base_url() ?>media/banner-news-event/<?= $late2->banner ?>" style="width: 100px;" />
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?= $late2->title ?></h4>
                        </div>
                    </a>
                </div>
                <?php } ?>

              </div>

              <!-- images banner sale Dekstop -->
              <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40">
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                  </div>
                  <div class="widget mb-30">
                      <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                  </div>
              </div>
              <!-- end images banner sale -->
          </div>

    </div><!-- end container -->
  </section><!--end white-wrapper -->
