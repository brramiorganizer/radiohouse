<?php
use app\helper\Helper;
$content = Helper::getPageContent(6);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
?>
<section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
      <div class="bg-overlay"></div>
      <div class="container">
          <div class="row m-0">
              <div class="breadcrumb-banner">
                  <ul class="m-0">
                    <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                    <li class="text-green"><?= $content->meta_title ?></li>
                  </ul>
                  <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg"><?= $content->meta_title ?></h1>
              </div>
          </div>
      </div>
  </section><!-- end post-wrapper-top -->

  <section class="white-wrapper nopadding">
    <div id="map">
          <iframe src="<?= Helper::getDataContent(22) ?>" width="100%" height="450px" style="border:none;"></iframe>
      </div>

      <div class="clearfix"></div>

      <div class="container pt-60 pb-40">

          <div class="contact_form m-0">
              <div id="message">

                <?php if(Yii::$app->session->hasFlash('contact')){ ?>
                  <div class="alert alert-success">

                    Pesan anda sudah terkirim, terimakasih
                  </div>
                <?php } ?>
              </div>
              <form id="contactform" action="contact.php" name="contactform" method="post">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <textarea class="form-control" name="comment" id="comments" rows="6" placeholder="Message"></textarea>
            <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-primary pull-right mt-20">Kirim</button>
          </div>
              </form>
          </div><!-- end contact-form -->

          <div class="clearfix"></div>

          <div class="row padding-top margin-top">
        <div class="contact_details">
          <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
            <div class="text-center">
              <div class="">
                <div class="contact-icon">
                  <a href="#" class=""> <i class="fa fa-map-marker fa-3x text-green"></i> </a>
                </div><!-- end dm-icon-effect-1 -->
                               <p><?= Helper::getDataContent(1) ?></p>
              </div><!-- end service-icon -->
            </div><!-- end miniboxes -->
          </div><!-- end col-lg-4 -->

          <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
            <div class="text-center">
              <div class="">
                <div class="contact-icon">
                  <a href="#" class=""> <i class="fa fa-phone fa-3x text-green"></i> </a>
                </div><!-- end dm-icon-effect-1 -->
                               <p><strong>Phone:</strong> <?= Helper::getDataContent(2) ?><br>
                <strong>Phone:</strong> <?= Helper::getDataContent(3) ?></p>
              </div><!-- end service-icon -->
            </div><!-- end miniboxes -->
          </div><!-- end col-lg-4 -->

          <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
            <div class="text-center">
              <div class="">
                <div class="contact-icon">
                  <a href="#" class=""> <i class="fa fa-envelope-o fa-3x text-green"></i> </a>
                </div><!-- end dm-icon-effect-1 -->
                               <p><strong>Email:</strong> <?= Helper::getDataContent(4) ?></p>
              </div><!-- end service-icon -->
            </div><!-- end miniboxes -->
          </div><!-- end col-lg-4 -->
        </div><!-- end contact_details -->
          </div><!-- end margin-top -->
      </div><!-- end container -->
  </section><!-- end map wrapper -->
