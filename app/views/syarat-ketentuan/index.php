<?php
use app\helper\Helper;
$content = Helper::getPageContent(8);
$banner = ($content->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$content->banner: 'demos/1920x300.png');
$this->title = ($content->meta_title != '' ? $content->meta_title .' | Radio House' : 'Radio House');
?>
  <section class="banner-img withText" style="background-image: url('<?= $banner ?>');">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                      <li><a href="<?= Yii::$app->params['base_url']; ?>"><i class="fa fa-home"></i></a></li>
                      <li class="text-green"><?= $content->meta_title ?></li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg"><?= $content->meta_title ?></h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="page-under pt-60 pb-60">
      <div class="container">

            <div class="content-about-up">
                <?= $content->description ?>
            </div>
      </div><!-- end container -->
    </section><!--end white-wrapper -->
