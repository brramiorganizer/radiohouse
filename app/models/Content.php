<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $banner
 * @property string $content
 * @property string $created_on
 * @property string $updated_on
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'required'],
            [['page_id'], 'integer'],
            [['content'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['banner'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'banner' => 'Banner',
            'content' => 'Content',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
