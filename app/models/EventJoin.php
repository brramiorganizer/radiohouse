<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_join".
 *
 * @property integer $id
 * @property integer $news_event_id
 * @property string $status
 * @property integer $code
 * @property string $name
 * @property string $email
 * @property string $no_phone
 * @property string $address
 * @property integer $event_ticket_id
 * @property integer $quantity
 * @property string $total_price
 * @property string $created_on
 * @property string $updated_on
 */
class EventJoin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_join';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_event_id', 'code', 'event_ticket_id', 'quantity'], 'required'],
            [['news_event_id', 'code', 'event_ticket_id', 'quantity','account_member_id'], 'integer'],
            [['status', 'address'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name', 'total_price'], 'string', 'max' => 255],
            [['email', 'no_phone'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_event_id' => 'News Event ID',
            'status' => 'Status',
            'code' => 'Code',
            'name' => 'Name',
            'email' => 'Email',
            'no_phone' => 'No Phone',
            'address' => 'Address',
            'event_ticket_id' => 'Event Ticket ID',
            'quantity' => 'Quantity',
            'total_price' => 'Total Price',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getEvent(){
        return $this->hasOne(NewsEvent::className(), ['id'=>'news_event_id']);
    }
    // public function getUser(){
    //     return $this->hasOne(AccountMember::className(), ['id'=>'account_member_id']);
    // }
    public function getTicket(){
        return $this->hasOne(EventTicket::className(), ['id'=>'event_ticket_id']);
    }
}
