<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_forgot".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $status
 * @property string $token
 * @property string $created_on
 * @property string $updated_on
 */
class RadioForgot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_forgot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'status' => 'Status',
            'token' => 'Token',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getRadio(){
        return $this->hasOne(Radio::className(), ['id'=>'radio_id']);
    }
}
