<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisment_ne_channel".
 *
 * @property integer $id
 * @property string $page
 * @property integer $sort
 * @property string $image
 * @property string $created_on
 * @property string $updated_on
 */
class AdvertismentNeChannel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisment_ne_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page'], 'string'],
            [['sort'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page' => 'Page',
            'sort' => 'Sort',
            'image' => 'Image',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
