<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_change_password".
 *
 * @property integer $id
 * @property string $account_type
 * @property string $old_password
 * @property string $new_password
 * @property string $created_on
 * @property string $updated_on
 */
class AccountChangePassword extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_change_password';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['old_password', 'new_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_type' => 'Account Type',
            'old_password' => 'Old Password',
            'new_password' => 'New Password',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
