<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_token".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $token
 * @property string $expired_date
 * @property string $created_on
 */
class RadioToken extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['expired_date', 'created_on'], 'safe'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'token' => 'Token',
            'expired_date' => 'Expired Date',
            'created_on' => 'Created On',
        ];
    }
}
