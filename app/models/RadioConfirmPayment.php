<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_confirm_payment".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property integer $bank_id
 * @property string $rekening_name
 * @property string $rekening_number
 * @property string $description
 * @property string $status
 * @property string $created_on
 * @property string $updated_on
 */
class RadioConfirmPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_confirm_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id', 'bank_id'], 'required'],
            [['radio_id', 'bank_id'], 'integer'],
            [['description', 'status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['rekening_name', 'rekening_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'bank_id' => 'Bank ID',
            'rekening_name' => 'Rekening Name',
            'rekening_number' => 'Rekening Number',
            'description' => 'Description',
            'status' => 'Status',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
    public function getRadio(){
        return $this->hasOne(Radio::className(), ['id'=>'radio_id']);
    }
    public function getBank(){
        return $this->hasOne(Bank::className(), ['id'=>'bank_id']);
    }
}
