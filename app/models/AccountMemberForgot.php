<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_member_forgot".
 *
 * @property integer $id
 * @property integer $account_member_id
 * @property string $status
 * @property string $expired_date
 * @property string $token
 * @property string $created_on
 * @property string $updated_on
 */
class AccountMemberForgot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_member_forgot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_member_id', 'expired_date'], 'required'],
            [['account_member_id'], 'integer'],
            [['status'], 'string'],
            [['expired_date', 'created_on', 'updated_on'], 'safe'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_member_id' => 'Account Member ID',
            'status' => 'Status',
            'expired_date' => 'Expired Date',
            'token' => 'Token',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getUser(){
        return $this->hasOne(AccountMember::className(), ['id'=>'account_member_id']);
    }
}
