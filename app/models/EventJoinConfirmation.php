<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_join_confirmation".
 *
 * @property integer $id
 * @property integer $event_join_id
 * @property integer $bank_id
 * @property string $rekening_name
 * @property string $rekening_number
 * @property string $total
 * @property string $description
 * @property string $created_on
 * @property string $updated_on
 * @property string $status
 */
class EventJoinConfirmation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_join_confirmation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_join_id', 'bank_id'], 'required'],
            [['event_join_id', 'bank_id'], 'integer'],
            [['description', 'status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['rekening_name', 'rekening_number'], 'string', 'max' => 255],
            [['total'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_join_id' => 'Event Join ID',
            'bank_id' => 'Bank ID',
            'rekening_name' => 'Rekening Name',
            'rekening_number' => 'Rekening Number',
            'total' => 'Total',
            'description' => 'Description',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'status' => 'Status',
        ];
    }
    public function getBank(){
        return $this->hasOne(Bank::className(), ['id'=>'bank_id']);
    }
}
