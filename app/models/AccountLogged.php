<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_logged".
 *
 * @property integer $id
 * @property string $account
 * @property integer $account_id
 * @property string $ip
 * @property string $device
 * @property string $created_on
 */
class AccountLogged extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_logged';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account'], 'string'],
            [['account_id'], 'required'],
            [['account_id'], 'integer'],
            [['created_on'], 'safe'],
            [['ip', 'device'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account' => 'Account',
            'account_id' => 'Account ID',
            'ip' => 'Ip',
            'device' => 'Device',
            'created_on' => 'Created On',
        ];
    }
}
