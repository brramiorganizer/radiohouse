<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio".
 *
 * @property integer $id
 * @property integer $radio_category_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $company_name
 * @property string $radio_name
 * @property string $description
 * @property string $category
 * @property string $telepon
 * @property string $address
 * @property string $password
 * @property string $status
 * @property string $api
 * @property string $created_on
 * @property string $updated_on
 */
class Radio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_category_id'], 'integer'],
            [['description', 'address', 'status', 'api'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name', 'company_name', 'radio_name', 'password'], 'string', 'max' => 255],
            [['username', 'email'], 'string', 'max' => 70],
            [['category', 'telepon'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_category_id' => 'Radio Category ID',
            'name' => 'Name',
            'username' => 'Username',
            'email' => 'Email',
            'company_name' => 'Company Name',
            'radio_name' => 'Radio Name',
            'description' => 'Description',
            'category' => 'Category',
            'telepon' => 'Telepon',
            'address' => 'Address',
            'password' => 'Password',
            'status' => 'Status',
            'api' => 'Api',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getContent(){
        return $this->hasOne(RadioContent::className(), ['radio_id'=>'id']);
    }
    public function getPersonal(){
        return $this->hasOne(RadioPersonal::className(), ['radio_id'=>'id']);
    }


}
