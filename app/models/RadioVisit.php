<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_visit".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $device
 * @property string $ip
 * @property string $created_on
 */
class RadioVisit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_visit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['created_on'], 'safe'],
            [['device', 'ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'device' => 'Device',
            'ip' => 'Ip',
            'created_on' => 'Created On',
        ];
    }
}
