<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_program".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $format_siaran
 * @property string $target_pendengar
 * @property string $coverage_area
 * @property string $musik
 * @property string $created_on
 * @property string $updated_on
 */
class RadioProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['format_siaran', 'target_pendengar', 'coverage_area', 'musik'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'format_siaran' => 'Format Siaran',
            'target_pendengar' => 'Target Pendengar',
            'coverage_area' => 'Coverage Area',
            'musik' => 'Musik',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
