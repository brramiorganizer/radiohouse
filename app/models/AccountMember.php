<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_member".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $number_phone
 * @property string $address
 * @property string $gender
 * @property string $place_of_birth
 * @property string $date_of_birth
 * @property string $ktp
 * @property string $city
 * @property integer $province
 * @property string $zip_code
 * @property string $password
 * @property string $token
 * @property string $type
 * @property integer $is_active
 * @property integer $is_delete
 * @property string $created_on
 * @property string $updated_on
 */
class AccountMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'gender', 'type'], 'string'],
            [['date_of_birth', 'created_on', 'updated_on'], 'safe'],
            [['province', 'is_active', 'is_delete'], 'integer'],
            [['token'], 'required'],
            [['firstname', 'lastname', 'email', 'ktp'], 'string', 'max' => 50],
            [['number_phone'], 'string', 'max' => 20],
            [['place_of_birth', 'city', 'password', 'token'], 'string', 'max' => 255],
            [['zip_code'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'number_phone' => 'Number Phone',
            'address' => 'Address',
            'gender' => 'Gender',
            'place_of_birth' => 'Place Of Birth',
            'date_of_birth' => 'Date Of Birth',
            'ktp' => 'Ktp',
            'city' => 'City',
            'province' => 'Province',
            'zip_code' => 'Zip Code',
            'password' => 'Password',
            'token' => 'Token',
            'type' => 'Type',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
