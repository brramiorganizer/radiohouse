<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_plan".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property integer $plan_id
 * @property string $status
 * @property string $active_date
 * @property string $expired_date
 * @property string $created_on
 */
class RadioPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id', 'plan_id'], 'required'],
            [['radio_id', 'plan_id'], 'integer'],
            [['status'], 'string'],
            [['active_date', 'expired_date', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'plan_id' => 'Plan ID',
            'status' => 'Status',
            'active_date' => 'Active Date',
            'expired_date' => 'Expired Date',
            'created_on' => 'Created On',
        ];
    }
    public function getPlan(){
        return $this->hasOne(Plan::className(), ['id'=>'plan_id']);
    }
}
