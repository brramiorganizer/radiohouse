<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_personal".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $name
 * @property string $email
 * @property string $address
 * @property string $telephone
 * @property string $handphone
 * @property string $created_on
 * @property string $updated_on
 */
class RadioPersonal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_personal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id', 'address'], 'required'],
            [['radio_id'], 'integer'],
            [['address'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['telephone', 'handphone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'name' => 'Name',
            'email' => 'Email',
            'address' => 'Address',
            'telephone' => 'Telephone',
            'handphone' => 'Handphone',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
