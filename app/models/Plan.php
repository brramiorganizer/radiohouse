<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property integer $id
 * @property string $name
 * @property integer $price
 * @property string $content
 * @property string $slug
 * @property integer $sort
 * @property string $created_on
 * @property string $updated_on
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'sort'], 'required'],
            [['price', 'sort'], 'integer'],
            [['content'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'content' => 'Content',
            'slug' => 'Slug',
            'sort' => 'Sort',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
