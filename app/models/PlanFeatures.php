<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan_features".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_on
 * @property string $updated_on
 */
class PlanFeatures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
