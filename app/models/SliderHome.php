<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider_home".
 *
 * @property integer $id
 * @property string $title
 * @property string $file
 * @property string $description
 * @property string $link
 * @property string $type
 * @property integer $radio_id
 * @property integer $sort
 * @property string $created_on
 * @property string $updated_on
 * @property string $status
 */
class SliderHome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_home';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'type', 'status'], 'string'],
            [['radio_id', 'sort'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['title', 'file', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'file' => 'File',
            'description' => 'Description',
            'link' => 'Link',
            'type' => 'Type',
            'radio_id' => 'Radio ID',
            'sort' => 'Sort',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'status' => 'Status',
        ];
    }
}
