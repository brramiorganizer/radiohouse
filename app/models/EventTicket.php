<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_ticket".
 *
 * @property integer $id
 * @property integer $news_event_id
 * @property string $category
 * @property string $price
 * @property string $available
 * @property string $created_on
 * @property string $updated_on
 */
class EventTicket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_event_id'], 'required'],
            [['news_event_id'], 'integer'],
            [['available'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['category'], 'string', 'max' => 255],
            [['price'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_event_id' => 'News Event ID',
            'category' => 'Category',
            'price' => 'Price',
            'available' => 'Available',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
