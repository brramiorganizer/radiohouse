<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_event".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $type
 * @property string $title
 * @property string $banner
 * @property string $description
 * @property string $slug
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $join_active
 * @property string $created_on
 * @property string $updated_on
 */
class NewsEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['type', 'description', 'meta_keywords', 'meta_description', 'join_active'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['title', 'banner', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'type' => 'Type',
            'title' => 'Title',
            'banner' => 'Banner',
            'description' => 'Description',
            'slug' => 'Slug',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'join_active' => 'Join Active',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getRadio(){
        return $this->hasOne(Radio::className(), ['id'=>'radio_id']);
    }
}
