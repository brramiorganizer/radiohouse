<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_admin".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $token
 * @property string $created_on
 * @property string $updated_on
 */
class AccountAdmin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_on', 'updated_on'], 'safe'],
            [['fullname', 'email', 'password', 'token'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
