<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisment_radio_seven".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $banner_type
 * @property string $banner
 * @property integer $sort
 * @property string $expired_date
 * @property string $created_on
 * @property string $updated_on
 */
class AdvertismentRadioSeven extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisment_radio_seven';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id', 'sort'], 'required'],
            [['radio_id', 'sort'], 'integer'],
            [['banner_type'], 'string'],
            [['expired_date', 'created_on', 'updated_on'], 'safe'],
            [['banner'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'banner_type' => 'Banner Type',
            'banner' => 'Banner',
            'sort' => 'Sort',
            'expired_date' => 'Expired Date',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getRadio(){
      return $this->hasOne(Radio::className(), ['id'=>'radio_id']);
    }
}
