<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisment_radio".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $status
 * @property string $created_on
 * @property string $updated_on
 */
class AdvertismentRadio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisment_radio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'status' => 'Status',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getRadio(){
        return $this->hasOne(Radio::className(), ['id'=>'radio_id']);
    }
}
