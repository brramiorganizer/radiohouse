<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account_admin_logged".
 *
 * @property integer $id
 * @property integer $account_admin_id
 * @property string $device
 * @property string $ip
 * @property string $created_on
 */
class AccountAdminLogged extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_admin_logged';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_admin_id'], 'required'],
            [['account_admin_id'], 'integer'],
            [['created_on'], 'safe'],
            [['device', 'ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_admin_id' => 'Account Admin ID',
            'device' => 'Device',
            'ip' => 'Ip',
            'created_on' => 'Created On',
        ];
    }
}
