<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_join_payment".
 *
 * @property integer $id
 * @property integer $event_join_id
 * @property integer $bank_id
 * @property string $created_on
 * @property string $updated_on
 */
class EventJoinPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_join_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_join_id', 'bank_id'], 'required'],
            [['event_join_id', 'bank_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_join_id' => 'Event Join ID',
            'bank_id' => 'Bank ID',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
