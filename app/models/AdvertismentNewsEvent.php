<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "advertisment_news_event".
 *
 * @property integer $id
 * @property integer $news_event_id
 * @property integer $sort
 * @property string $created_on
 * @property string $updated_on
 * @property string $status
 */
class AdvertismentNewsEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisment_news_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_event_id', 'sort'], 'required'],
            [['news_event_id', 'sort'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['status','expired_date'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_event_id' => 'News Event ID',
            'sort' => 'Sort',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'status' => 'Status',
        ];
    }

    public function getNe(){
      return $this->hasOne(NewsEvent::className(), ['id'=>'news_event_id']);
    }
}
