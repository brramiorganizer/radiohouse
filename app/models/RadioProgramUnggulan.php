<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_program_unggulan".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $name
 * @property string $listener
 * @property string $kind_program
 * @property string $music
 * @property string $content_information
 * @property string $average_listener
 * @property string $created_on
 * @property string $updated_on
 */
class RadioProgramUnggulan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_program_unggulan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['content_information'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name', 'listener', 'kind_program', 'music', 'average_listener'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'name' => 'Name',
            'listener' => 'Listener',
            'kind_program' => 'Kind Program',
            'music' => 'Music',
            'content_information' => 'Content Information',
            'average_listener' => 'Average Listener',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
