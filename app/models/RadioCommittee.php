<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_committee".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property integer $position_id
 * @property string $name
 * @property string $address
 * @property string $telephone
 * @property string $created_on
 * @property string $updated_on
 */
class RadioCommittee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_committee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id', 'position_id', 'name'], 'required'],
            [['radio_id', 'position_id'], 'integer'],
            [['address'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['telephone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'position_id' => 'Position ID',
            'name' => 'Name',
            'address' => 'Address',
            'telephone' => 'Telephone',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getPosition(){
        return $this->hasOne(RadioCommitteePosition::className(), ['id'=>'position_id']);
    }
}
