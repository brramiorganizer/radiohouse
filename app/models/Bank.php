<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property string $name
 * @property string $rekening_name
 * @property string $rekening_number
 * @property string $image
 * @property integer $is_delete
 * @property string $created_on
 * @property string $updated_on
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_delete'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name', 'rekening_name', 'image'], 'string', 'max' => 255],
            [['rekening_number'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rekening_name' => 'Rekening Name',
            'rekening_number' => 'Rekening Number',
            'image' => 'Image',
            'is_delete' => 'Is Delete',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
