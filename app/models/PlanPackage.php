<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plan_package".
 *
 * @property integer $id
 * @property integer $plan_id
 * @property integer $plan_features_id
 * @property integer $created_on
 * @property integer $updated_on
 */
class PlanPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'plan_id', 'plan_features_id', 'created_on', 'updated_on'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_id' => 'Plan ID',
            'plan_features_id' => 'Plan Features ID',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getPlanfeature(){
        return $this->hasOne(PlanFeatures::className(), ['id'=>'plan_features_id']);
    }
}
