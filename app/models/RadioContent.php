<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "radio_content".
 *
 * @property integer $id
 * @property integer $radio_id
 * @property string $photo_profile
 * @property string $advertisment_1
 * @property string $advertisment_2
 * @property string $description
 * @property string $created_on
 * @property string $updated_on
 */
class RadioContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'radio_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['radio_id'], 'required'],
            [['radio_id'], 'integer'],
            [['description'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['banner','photo_profile', 'advertisment_1', 'advertisment_2'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'radio_id' => 'Radio ID',
            'photo_profile' => 'Photo Profile',
            'advertisment_1' => 'Advertisment 1',
            'advertisment_2' => 'Advertisment 2',
            'description' => 'Description',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
