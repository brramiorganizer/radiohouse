(function($) {
  "use strict";
$(window).load(function(){
  $('.flexslider').flexslider({
    animation: "fade",
  controlNav: true,
    start: function(slider){
      $('body').removeClass('loading');
    }
  });
});
})(jQuery);

var audioplaying = $('audio');
$( function() {
    // setup master volume

    $('#master').slider({
      orientation: "horizontal",
      value: audioplaying.prop('volume'),
      min: 0,
      max: 1,
      range: 'min',
      animate: true,
      step: .1,
      slide: function(e, ui) {
          audioplaying.prop('volume',ui.value);
          if(ui.value > 0){
            $('#volumeup').css('display','inline-block');
            $('#volumemute').css('display','none');
          }else{
            $('#volumemute').css('display','inline-block');
            $('#volumeup').css('display','none');
          }
      }
  });

});

//var audio = new Audio('media/Linkin Park - Papercut.mp3');
//Hide Pause
$('#pause').hide();

//Play button
$('#play').click(function(){
    $('audio').trigger('play');
    $('#play').hide();
    $('#pause').show();
    showDuration();
});

//Pause button
$('#pause').click(function(){
    $('audio').trigger('pause');
    $('#play').show();
    $('#pause').hide();
});

//hide volumemute
$('#volumemute').hide();

//volume
var volume = 0;
$('#volumeup').click(function(){
    volume = audioplaying.prop('volume');
    audioplaying.prop('volume',0);
    $("#master").slider('value',0);
    $('#volumeup').hide();
    $('#volumemute').show();
    showDuration();
});

//volume
$('#volumemute').click(function(){
    audioplaying.prop('volume',volume);
    $("#master").slider('value',volume);
    $('#volumeup').show();
    $('#volumemute').hide();
});


$('#widget').draggable();


$(document).ready(function () {
    if($('#thisindex').length){
      $('#play').trigger('click');
    }

    var sildeNum = $('.page').length,
        wrapperWidth = 100 * sildeNum,
        slideWidth = 100/sildeNum;
    //$('.wrapper').width(wrapperWidth + '%');
    //$('.page').width(slideWidth + '%');

    $('a.scrollitem').click(function(){
        $('a.scrollitem').removeClass('selected');
        $(this).addClass('selected');

        var slideNumber = $($(this).attr('href')).index('.page'),
            margin = slideNumber * -100 + '%';

        $('.wrapper').animate({marginLeft: margin},100);
        return false;
    });
});

$(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');

        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');

        }
    });

    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");



$('[data-toggle="tooltip"]').tooltip();



tinymce.init({
  selector: '.editortextarea',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });



});
