var main_url = $('#base').val();
var gils = $('#gils').val();

$(document).ready(function(){


  $('#forgot-password-radio').validate({
    rules : {
      email : {
        required : true,
        remote : {
            url: main_url + "radio/check-email",
            type: "post",
            data: {
              email: function() {
                return $("#forgot-password-radio #email" ).val();
              },
              _csrf : gils
            }
         }
      }
    },
    messages : {
      email : {
        required : 'please fill the email',
        remote : 'email is not exist'
      }
    },
    submitHandler : function(form){
      $("#forgot-password-radio button").prop('disabled',true).html('Proccessing ...');
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url: main_url + "radio/insert-forgot",
        type: "post",
        data : dataform,
        success : function(data){
          $( "#forgot-password-radio button").prop('disabled',false).html('Proccess');
          $( "#forgot-password-radio #email").val('');
            $( "#forgot-password-radio .alert").remove();
          $( "#forgot-password-radio").prepend('<div class="alert alert-success">Cek email anda untuk mendapatkan password baru</div>');
        }
      });
    }
  });



  



  //login
  $('#login-radio').validate({
    rules : {
      email : {
        required : true
      },
      password : {
        required : true
      }
    },

    messages :{
      email : {
        required : 'please fill your email'
      },
      password : {
        required : 'please fill your password'
      }
    },

    submitHandler : function(form){
      $('#login-radio button').html('Logging ...').prop("disabled", true);
      $.ajax({
        url : main_url + 'radio/logining',
        type : 'POST',
        data : $(form).serialize() + '&_csrf='+gils,
        dataType : 'JSON',
        success : function(data){
          $('#login-radio button').html('Login').prop("disabled", false);
          $('.content .alert').remove();
          if(data.status){
            location.reload();
          }else{
            $('.content').append('<div class="alert alert-danger">username or password is wrong</div>')
          }
        }
      });
      return false;
    }
  });

});
