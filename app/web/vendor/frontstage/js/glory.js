var main_url = $('#base').val();
var gils = $('#gils').val();


$(document).ready(function(){

  //Forgot Password Member
  $('#forgot-password-member').validate({
    rules : {
      email : {
        required : true,
        remote : {
            url: main_url + "member/check-email",
            type: "post",
            data: {
              email: function() {
                return $("#forgot-password-member #email" ).val();
              },
              _csrf : gils
            }
         }
      }
    },
    messages : {
      email : {
        required : 'please fill the email',
        remote : 'email is not exist'
      }
    },
    submitHandler : function(form){
      $( "#forgot-password-member button").prop('disabled',true);
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url: main_url + "member/insert-forgot",
        type: "post",
        data : dataform,
        success : function(data){
          $( "#forgot-password-member button").prop('disabled',false);
          $( "#forgot-password-member #email").val('');
            $( "#forgot-password-member .alert").remove();
          $( "#forgot-password-member").prepend('<div class="alert alert-success">Cek email anda untuk mendapatkan password baru</div>');
        }
      });
    }
  });



  $('#new-password-member').validate({
    rules : {
      password : {
        required : true,
        minlength : 6
      },
      repassword : {
        required : true,
        minlength : 6,
        equalTo : '#password'
      }
    },
    messages : {
      password : {
        required : 'please fill the new password',
        minlength : 'min 6 character'
      },
      repassword : {
        required : 'please fill retype password',
        minlength : 'min 6 character',
        equalTo : 'password does not match'
      }
    },
    submitHandler : function(form){
      $( "#new-password-member button").prop('disabled',true);
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url: main_url + "member/new-forgot",
        type: "post",
        data : dataform,
        success : function(data){
          $( "#new-password-member").html('<div class="alert alert-success">Password anda berhasil ter ubah, silahkan masuk dengan password baru anda</div>');
        }
      });
    }
  });



  $('#new-password-radio').validate({
    rules : {
      password : {
        required : true,
        minlength : 6
      },
      repassword : {
        required : true,
        minlength : 6,
        equalTo : '#password'
      }
    },
    messages : {
      password : {
        required : 'please fill the new password',
        minlength : 'min 6 character'
      },
      repassword : {
        required : 'please fill retype password',
        minlength : 'min 6 character',
        equalTo : 'password does not match'
      }
    },
    submitHandler : function(form){
      $( "#new-password-radio button").prop('disabled',true);
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url: main_url + "radio/new-forgot",
        type: "post",
        data : dataform,
        success : function(data){
          $( "#new-password-radio").html('<div class="alert alert-success">Password anda berhasil ter ubah, silahkan masuk dengan password baru anda</div>');
        }
      });
    }
  });

  // Regiter Form Member
  $('#registerform').validate({
    rules : {
      firstname : {
        required : true
      },
      email : {
        required : true
      },
      password : {
        required : true,
        minlength: 6
      },
      repassword : {
        required : true,
        equalTo: "#registerpassword"
      }
    },
    messages : {
      firstname : {
        required : 'please fill your firstname'
      },
      email : {
        required : 'please fill your email'
      },
      password : {
        required : 'please fill the password'
      },
      repassword : {
        required : 'please fill the password',
        equalTo: 'password does not match'
      }
    },
    submitHandler : function(form){
      $('#registerform button').html('Proccessing ...').prop('disabled',true);
      $.ajax({
        url : main_url + 'site/register-member',
        type : 'POST',
        dataType : 'JSON',
        data : $(form).serialize()+ '&_csrf='+gils,
        success : function(data){
          $('#registerform input').val('');
          $('#registerform button').html('SUBMIT').prop('disabled',false);
          if(data.status){
            $('#loginform').prepend('<div class="alert alert-success">Anda berhasil terdaftar, silahkan cek email anda untuk aktifasi akun anda</div>');
            $('.scrollitem.bc-login').trigger('click');
          }else{
            location.reload();
          }
        }

      })

      return false;
    }
  });


  //Login Form Member
  $('#loginform').validate({
    rules : {
      email : {
        required : true
      },
      password : {
        required : true
      }
    },
    messages : {
      email : {
        required : 'please fill your email'
      },
      password : {
        required : 'please fill the password'
      },
    },
    submitHandler : function(form){
      $('#loginform button').html('Proccessing ...').prop('disabled',true);
      $.ajax({
        url : main_url + 'site/login-member',
        type : 'POST',
        dataType : 'JSON',
        data : $(form).serialize()+ '&_csrf='+gils,
        success : function(data){
          $('#loginform input[type=password]').val('');
          $('#loginform button').html('SUBMIT').prop('disabled',false);
          $('#loginform .alert').remove();
          if(data.status){
            location.reload();
          }else{
            $('#loginform').prepend('<div class="alert alert-danger">email atau password yang anda masukan salah</div>')
          }
        }

      })

      return false;
    }
  });



  //Register Radio Member
  $('#newmember').validate({
    rules : {
      p_name : {
        required : true
      },
      p_email : {
        required : true,
        remote : {
            url: main_url + "pricing/check-email",
            type: "post",
            data: {
              email: function() {
                return $( "#p_email" ).val();
              },
              _csrf : gils
            }
         }
      },
      p_address : {
        required : true
      },
      p_handphone : {
        required : true
      },


      company_name : {
        required :  true
      },
      radio_name : {
        required : true,
        remote : {
            url: main_url + "pricing/check-name",
            type: "post",
            data: {
              email: function() {
                return $( "#radio_name" ).val();
              },
              _csrf : gils
            }
         }
      },
      address : {
        required : true
      },
      telepon : {
        required : true
      },
      kota : {
        required : true
      },

      isr_ipp : {
        required : true
      },
      name_p1 : {
        required : true
      },
      handphone_p1 : {
        required : true
      },
      alamat_p1 : {
        required : true
      },

      name_p2 : {
        required : true
      },
      handphone_p2 : {
        required : true
      },
      alamat_p2 : {
        required : true
      },

      name_p3 : {
        required : true
      },
      handphone_p3 : {
        required : true
      },
      alamat_p3 : {
        required : true
      },
      checklist : {
        required : true
      }
    },
    messages : {
      p_name : {
        required : 'tolong isi personal name anda'
      },
      p_email : {
        required : 'tolong isi email anda',
        remote : 'email sudah digunakan'
      },
      p_address : {
        required : 'tolong isi alamat anda'
      },
      p_handphone : {
        required : 'tolong isi nomer handphone anda'
      },


      company_name : {
        required :  'tolong isi nama perusahaan radio'
      },
      radio_name : {
        required : 'tolong isi nama radio'
      },
      address : {
        required : 'tolong isi alamat radio'
      },
      telepon : {
        required : 'tolong isi nomer telephone radio'
      },
      kota : {
        required : 'tolong isi nama kota / wilayah'
      },
      isr_ipp : {
        required : 'tolong isi nomer ISR & IPP'
      },
      name_p1 : {
        required : 'field ini tidak boleh kosong'
      },
      handphone_p1 : {
        required :'field ini tidak boleh kosong'
      },
      alamat_p1 : {
        required : 'field ini tidak boleh kosong'
      },

      name_p2 : {
        required : 'field ini tidak boleh kosong'
      },
      handphone_p2 : {
        required : 'field ini tidak boleh kosong'
      },
      alamat_p2 : {
        required : 'field ini tidak boleh kosong'
      },

      name_p3 : {
        required : 'field ini tidak boleh kosong'
      },
      handphone_p3 : {
        required : 'field ini tidak boleh kosong'
      },
      alamat_p3 : {
        required : 'field ini tidak boleh kosong'
      },
      checklist : {
        required : 'tolong checklist untuk persetujuan'
      }
    },
    submitHandler : function(form){
      $('#newmember button').prop('disabled',true).html('proccessing ...');
      var dataform = $(form).serialize();
      $.ajax({
        url : main_url + "pricing/request",
        type : 'POST',
        data : $(form).serialize() + '&_csrf=' + gils,
        success : function(data){
          window.location = main_url + 'pricing/finish-request';
        }
      });
      return false;
    }
  });


  $('#activate-radio').validate({
    rules : {
      password : {
        required : true
      },
      repassword : {
        required : true,
        equalTo : '#password'
      }
    },
    messages : {
      password : {
        required : 'tolong isi password anda'
      },
      repassword : {
        required : 'tolong ketikan kembali password anda',
        equalTo : 'password tidak sama'
      }
    },
    submitHandler : function(form){
      $('#activate-radio button').prop('disabled', true).html('Proccessing ...');
      var data = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url : main_url + 'radio/set-activate',
        type : 'POST',
        data : data,
        dataType : 'JSON',
        success : function(data){
          window.location = main_url + 'radio/login';
        }
      });
      return false;
    }
  });


  $('#edit-profil-visitor').validate({
    rules : {
      firstname : {
        required : true
      },
      gender : {
        required : true
      },
      place_of_birth : {
        required : true
      },
      date_of_birth : {
        required : true
      },
      address : {
        required : true
      },
      city : {
        required : true
      },
      province : {
        required : true
      },
      zip_code : {
        required : true
      }
    },
    messages : {
      firstname : {
        required : 'tolong isi nama anda'
      },
      gender : {
        required : 'tolong pilih jenis kelamin anda'
      },
      place_of_birth : {
        required : 'tolong isi tempat lahir anda'
      },
      date_of_birth : {
        required : 'tolong isi tanggal lahir anda'
      },
      address : {
        required : 'tolong isi alamat anda'
      },
      city : {
        required : 'tolong isi kota anda'
      },
      province : {
        required : 'tolong isi provinsi anda'
      },
      zip_code : {
        required : 'tolong isi kode pos anda'
      }
    },
    submitHandler : function(form){
      $.ajax({
        url : main_url + 'member/change-profile',
        data : $(form).serialize() + '&_csrf=' + gils,
        dataType : 'JSON',
        type : 'POST',
        success : function(data){
          location.reload();
        }
      });
      return false;
    }
  });


  $('#edit-password-member').validate({
    rules : {
      old_password : {
        required : true
      },
      new_password : {
        required : true
      },
      retype_new_password : {
        required : true,
        equalTo : '#new_password'
      }
    },
    messages : {
      old_password : {
        required : 'tolong isi password lama anda'
      },
      new_password : {
        required : 'tolong isi password baru anda'
      },
      retype_new_password : {
        required : 'tolong isi password baru anda',
        equalTo : 'password baru anda tidak sama'
      }
    },
    submitHandler : function(form){
      $.ajax({
        url : main_url + 'member/change-password',
        data : $(form).serialize() + '&_csrf=' + gils,
        dataType : 'JSON',
        type : 'POST',
        success : function(data){
          if(data.status){
            if(data.old_valid){
              location.reload();
            }else{
              $('#edit-password-member > .alert').remove();
              $('#edit-password-member').prepend('<div class="alert alert-danger">password lama anda salah</div>');
            }
          }else{
            location.reload();
          }
        }
      });
      return false;
    }
  });






  //HUBUNGI KAMI

  $('#contactform').validate({
    rules : {
      name : {
        required : true
      },
      email :  {
        required : true
      },
      comment : {
        required : true
      }
    },
    messages : {
      name : {
        required : 'please fill your name'
      },
      email :  {
        required : 'please fill your email'
      },
      comment : {
        required : 'please fill your comment'
      }
    },
    submitHandler : function(form){
      $('#contactform button').prop('disabled',true).html('sending ...');
      $.ajax({
        url : main_url+'hubungi-kami/post',
        type : 'POST',
        data : $(form).serialize() + '&_csrf='+$('#gils').val(),
        dataType : 'JSON',
        success : function(data){
          $('#contactform button').html('redirecting ...');
          location.reload();
        }
      });
      return false;
    }
  });





  //BUY TICKET
  $('#buy-ticket').validate({
    rules : {
      name : {
        required : true
      },
      email : {
        required : true
      },
      handphone : {
        required  : true
      },
      address : {
        required : true
      },
      category_ticket : {
        required : true
      },
      total_ticket : {
        required : true,
        min : 1,
        max : 7,
        digits: true
      }
    },
    messages  : {
      name : {
        required : 'nama anda harus diisi'
      },
      email : {
        required : 'email anda harus diisi'
      },
      handphone : {
        required  : 'nomer handphone anda harus diisi'
      },
      address : {
        required : 'alamat anda harus diisi'
      },
      category_ticket : {
        required : 'pilih kategori tiket'
      },
      total_ticket : {
        required : 'isi total ticket',
        min : 'minimal 1 tiket',
        max : 'maksimal 7 tiket',
        digits: 'masukan angka digit total dengan benar'
      }
    },
    submitHandler : function(form){
      $('#buy-ticket button').prop('disabled',true).html('Proccessing ...');
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url : main_url + 'radio/buy-ticket',
        type : 'POST',
        dataType : 'JSON',
        data :dataform,
        success : function(data){
          window.location = main_url + 'pembayaran?token='+data.code;
        }
      });
      return false;
    }
  });


  //Choose payment
  $('#choose-payment').validate({
    url : {
      bank : {
        required : true
      }
    },
    messages : {
      required : 'pilih pembayaran anda'
    },
    submitHandler : function(form){
      var formdata = $(form).serialize() + '&_csrf='+gils;
      $('#choose-payment button').prop('disabled',true).html('Proccessing ...');
      $.ajax({
        url : main_url + 'radio/choose-payment',
        type : 'POST',
        dataType : 'JSON',
        data :formdata,
        success : function(data){
          window.location = main_url + 'complete';
        }
      });
      return false;
    }
  });


    //EDIT TICKET
  $('#edit-ticket form').validate({
    rules : {
      name : {
        required : true
      },
      email : {
        required : true
      },
      no_phone : {
        required  : true
      },
      address : {
        required : true
      },
      category_ticket : {
        required : true
      },
      total_ticket : {
        required : true,
        min : 1,
        max : 7,
        digits: true
      }
    },
    messages  : {
      name : {
        required : 'nama anda harus diisi'
      },
      email : {
        required : 'email anda harus diisi'
      },
      no_phone : {
        required  : 'nomer handphone anda harus diisi'
      },
      address : {
        required : 'alamat anda harus diisi'
      },
      category_ticket : {
        required : 'pilih kategori tiket'
      },
      total_ticket : {
        required : 'isi total ticket',
        min : 'minimal 1 tiket',
        max : 'maksimal 7 tiket',
        digits: 'masukan angka digit total dengan benar'
      }
    },
    submitHandler : function(form){
      $('#edit-ticket form button').html('Proccessing ...').prop('disabled',true);
      var dataform = $(form).serialize() + '&_csrf='+gils;
      $.ajax({
        url : main_url + 'radio/edit-ticket',
        type : 'POST',
        dataType : 'JSON',
        data :dataform,
        success : function(data){
          $('#edit-ticket form button').prop('disabled',true);
          location.reload();
        }
      });
      return false;
    }
  });

  //konfirmasi pembayaran radio
  $('#konfirmasi-pembayaran-radio').validate({
    rules : {
      bank : {
        required :true
      },
      rekening_number : {
        required : true
      },
      rekening_name : {
        required : true
      }
    },
    messages : {
      bank : {
        required :'tolong pilih bank yang dituju'
      },
      rekening_number : {
        required : 'masukan nomer rekening anda'
      },
      rekening_name : {
        required : 'masukan nama rekening anda'
      }
    },
    submitHandler : function(form){
      $('#konfirmasi-pembayaran-radio button').prop('disabled',true).html('Proccessing ...');
      var kuy = $('#konfirmasi-pembayaran-radio')[0];
      var formData = new FormData(kuy);
      formData.append('banner', $('#konfirmasi-pembayaran-radio input[type=file]')[0].files[0]);
      formData.append('_csrf', $('#gils').val());

      $.ajax({
        url : main_url + 'pricing/insert-confirm',
        type : 'POST',
        data : formData,
        contentType: false,
        cache: false,
        processData:false,
        success : function(data){

            $('#konfirmasi-pembayaran-radio button').prop('disabled',false).html('Konfirmasi Pembayaran');
          if($('.main-konfirmasi .alert').length){
            $('.main-konfirmasi .alert').remove();
          }
          $('#konfirmasi-pembayaran-radio select , #konfirmasi-pembayaran-radio textarea , #konfirmasi-pembayaran-radio input[name=rekening_number] , #konfirmasi-pembayaran-radio input[name=rekening_name]').val('');
          $('.main-konfirmasi').prepend('<div class="alert alert-success text-center">Konfirmasi anda akan segera kami proses, kami akan mengirimkan balasan selanjutnya melalui email</div>');

        }
      });
      return false;
    }
  });


  //Konfirmasi Pembayaran
  $('#konfirmasi-pembayaran').validate({
    rules : {
      bank : {
        required :true
      },
      rekening_number : {
        required : true
      },
      rekening_name : {
        required : true
      }
    },
    messages : {
      bank : {
        required :'tolong pilih bank yang dituju'
      },
      rekening_number : {
        required : 'masukan nomer rekening anda'
      },
      rekening_name : {
        required : 'masukan nama rekening anda'
      }
    },
    submitHandler : function(form){
      $('#konfirmasi-pembayaran button').prop('disabled',true).html('proccessing ...');

      var kuy = $('#konfirmasi-pembayaran')[0];
      var formData = new FormData(kuy);
      formData.append('banner', $('#konfirmasi-pembayaran input[type=file]')[0].files[0]);
      formData.append('_csrf', $('#gils').val());
      $.ajax({
        url : main_url + 'konfirmasi/post',
        type : 'POST',
        data : formData,
        contentType: false,
        cache: false,
        processData:false,
        success : function(data){
          $('#konfirmasi-pembayaran').html('<div class="alert alert-success text-center">Konfirmasi Pembayaran anda akan kami segera proses, kami akan mengirimkan status pembelian anda melalui Email yang terdaftar</div>');
        }
      });
      return false;
    }
  });







  $("#filter-channel").keyup(function(){

        var filter = $(this).val(), count = 0;
        $("#filter-channel-data li").each(function(){

            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();

            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

        // Update the count
        var numberItems = count;
        console.log(numberItems);
        if(numberItems == 0){
          $('.no-data-channel').removeClass('hidden');
        }else{
          $('.no-data-channel').addClass('hidden');
        }

        //$("#filter-count").text("Number of Comments = "+count);
    });




    $("#filter-city").keyup(function(){

          var filter = $(this).val(), count = 0;
          $("#filter-city-data li").each(function(){

              // If the list item does not contain the text phrase fade it out
              if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                  $(this).fadeOut();

              // Show the list item if the phrase matches and increase the count by 1
              } else {
                  $(this).show();
                  count++;
              }
          });

          // Update the count
          var numberItems = count;
          console.log(numberItems);
          if(numberItems == 0){
            $('.no-data-city').removeClass('hidden');
          }else{
            $('.no-data-city').addClass('hidden');
          }

          //$("#filter-count").text("Number of Comments = "+count);
      });


      $('.download-tutorial').on('click',function(e){
        e.preventDefault();
        var file =  $(this).data('file');
        window.location.href = '/images/' + file;
      });


});
