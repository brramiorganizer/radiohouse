$( function() {
  $( ".datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
} );
$(document).ready(function() {
    $('a.get-form-besic , .detailfeatures').on('click',function(e){
      var to = $(this).data('to');
      var id = $(this).data('id');
      $('#packageis').fadeIn();
      $('.featuresis').hide();
      $('.'+to).fadeIn();
      $('.plan_id').val(id);
    });
});

$(function () {
    $(".p-grid").slice(0, 2).show();
    $("#viewMore").on('click', function (e) {
        e.preventDefault();
        $(".p-grid:hidden").slice(0, 100000).slideDown();
        if ($(".p-grid:hidden").length == 0) {}
    });
    $("#viewMore").click(function(){
        $("#viewMore").addClass("hidden");
    });
});
$(function () {
    $(".r-grid").slice(0, 6).show();
    $("#viewMore").on('click', function (e) {
        e.preventDefault();
        $(".r-grid:hidden").slice(0, 1000000).slideDown();
        if ($(".r-grid:hidden").length == 0) {}
    });
    $("#viewMore").click(function(){
        $("#viewMore").addClass("hidden");
    });
});

/* Thanks to CSS Tricks for pointing out this bit of jQuery
https://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.mainEqual .equal-1');
});


$(window).resize(function(){
  equalheight('.mainEqual .equal-1');
});

(function($) {
 "use strict"

	// Page Preloader
	$(window).load(function() {
		$(".loader").delay(300).fadeOut();
		$(".animationload").delay(600).fadeOut("slow");
	});

// Header Aff
	$("#header-style-1").affix({
		offset: {
			top: 500,
			bottom: function () {
			return (this.bottom = $('#copyrights').outerHeight(true))
			}
		}
	})

// OWL Carousel
	$("#owl-testimonial").owlCarousel({
		items : 1,
		lazyLoad : true,
		navigation : false,
		autoPlay: true
    });

	$("#owl-testimonial-widget").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });

    $("#owl-blog").owlCarousel({
		items : 2,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });

	$("#owl_blog_three_line, #owl_portfolio_two_line, #owl_blog_two_line").owlCarousel({
		items : 2,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });

	$("#owl_shop_carousel, #owl_shop_carousel_1").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : true,
		pagination : false,
		autoPlay: true
    });

	$("#services").owlCarousel({
		items : 3,
		lazyLoad : true,
		navigation : false,
		pagination : true,
		autoPlay: true
    });

	$(".buddy_carousel").owlCarousel({
		items : 9,
		lazyLoad : true,
		navigation : false,
		pagination : true,
		autoPlay: true
    });


	$('.buddy_tooltip').popover({
		container: '.buddy_carousel, .buddy_members'
	});

// Parallax
	$(window).bind('body', function() {
		parallaxInit();
	});
	function parallaxInit() {
		$('#one-parallax').parallax("30%", 0.1);
		$('#two-parallax').parallax("30%", 0.1);
		$('#three-parallax').parallax("30%", 0.1);
		$('#four-parallax').parallax("30%", 0.4);
		$('#five-parallax').parallax("30%", 0.4);
		$('#six-parallax').parallax("30%", 0.4);
		$('#seven-parallax').parallax("30%", 0.4);
	}


// Fun Facts
	function count($this){
		var current = parseInt($this.html(), 10);
		current = current + 1; /* Where 50 is increment */

		$this.html(++current);
			if(current > $this.data('count')){
				$this.html($this.data('count'));
			} else {
				setTimeout(function(){count($this)}, 50);
			}
		}

		$(".stat-count").each(function() {
		  $(this).data('count', parseInt($(this).html(), 10));
		  $(this).html('0');
		  count($(this));
	});

// WOW
	new WOW(
		{ offset: 300 }
	).init();

// DM Top
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 1) {
			jQuery('.dmtop').css({bottom:"40px"});
		} else {
			jQuery('.dmtop').css({bottom:"-100px"});
		}
	});
	jQuery('.dmtop').click(function(){
		jQuery('html, body').animate({scrollTop: '0px'}, 800);
		return false;
	});

// Rotate Text
	$(".rotate").textrotator({
		animation: "flipUp",
		speed: 2300
	});



// TOOLTIP
    $('.social-icons, .bs-example-tooltips').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })

// Accordion Toggle Items
   var iconOpen = 'fa fa-minus',
        iconClose = 'fa fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
        var $target = $(e.target)
          $target.siblings('.accordion-heading')
          .find('em').toggleClass(iconOpen + ' ' + iconClose);
          if(e.type == 'show')
              $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
          if(e.type == 'hide')
              $(this).find('.accordion-toggle').not($target).removeClass('active');
    });

})(jQuery);
