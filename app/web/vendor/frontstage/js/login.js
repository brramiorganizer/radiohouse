var main_url = $('#base').val();
var gils = $('#gils').val();

$(document).ready(function(){

  //login
  $('#login').validate({
    rules : {
      username : {
        required : true
      },
      password : {
        required : true
      }
    },

    messages :{
      username : {
        required : 'please fill your username'
      },
      password : {
        required : 'please fill your password'
      }
    },

    submitHandler : function(form){
      $('#login button').html('Logging ...').prop("disabled", true);
      $.ajax({
        url : main_url + 'backstage/logining',
        type : 'POST',
        data : $(form).serialize() + '&_csrf='+gils,
        dataType : 'JSON',
        success : function(data){
          $('#login button').html('Login').prop("disabled", false);
          $('.content .alert').remove();
          if(data.status){
            location.reload();
          }else{
            $('.content').append('<div class="alert alert-danger">username or password is wrong</div>')
          }
        }
      });
      return false;
    }
  });

});
