var base = $('#base').val();
var gils = $('#gils').val();

function general_modal(title,url){
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-header h4').html(title);
  $.ajax({
    url : url,
    type : 'GET',
    success : function(data){
      $('#generalmodal .modal-body').html(data);
    }
  });
}

var t = jQuery('#datatable-page').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,2]
    } ],
    "order": [[ 1, 'asc' ]]
} );

t.on( 'order.dt search.dt', function () {
    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


var a = jQuery('#datatable-slider-home').DataTable( {
    "columnDefs": [ {
        "searchable": true,
        "orderable": false,
        "targets": [0,3,5]
    } ],
    "order": [[ 5, 'asc' ]]
} );

a.on( 'order.dt search.dt', function () {
    a.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();



//Member Visitor
var b = jQuery('#datatable-member-visitor').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,4,5,6]
    } ],
    "order": [[ 1, 'asc' ]]
} );

b.on( 'order.dt search.dt', function () {
    b.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


// Radio Category

var radiocat = jQuery('#datatable-radio-category').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,2]
    } ],
    "order": [[ 1, 'asc' ]]
} );

radiocat.on( 'order.dt search.dt', function () {
    radiocat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();

$('.addnewradiocat').on('click',function(e){
  e.preventDefault();
  general_modal('Add New Category',base + 'radio-category/new');
});

$('.edit-radio-category').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var value = $(this).data('value');
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-header h4').html('Edit Category Radio');
  $.ajax({
    url : base + 'radio-category/new',
    type : 'GET',
    success : function(data){
      $('#generalmodal .modal-body').html(data);
      setTimeout(function(){
        $('#newcat input[type=hidden]').val(id);
        $('#newcat input[type=text]').val(value);
      },500);
    }
  });
});

$('.lihat-visitor').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-header h4').html('Detail Visitor');
  $.ajax({
    url : base + 'member-visitor/view?id='+id,
    type : 'GET',
    success : function(data){
      $('#generalmodal .modal-body').html(data);
    }
  });
});




// Request Radio
var reqrad = jQuery('#datatable-request-radio').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,3]
    } ],
    "order": [[ 1, 'asc' ]]
} );

reqrad.on( 'order.dt search.dt', function () {
    reqrad.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


$('.detailradio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  general_modal('Detail Radio',base + 'request-radio/getone?id='+id);
});

$('.visitradio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  general_modal('Detail Radio',base + 'request-radio/visit?id='+id);
});

$('.inactiveradio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure to inactive this radio ?')){
    $.ajax({
      url : base + 'radio/inactive',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        location.reload();
      }
    });
  }
});


$('.activeradio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure Active this radio ?')){
    $.ajax({
      url : base + 'radio/active',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        location.reload();
      }
    });
  }
});

$('.hapusradioreq').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure to delete this radio ?')){
    $.ajax({
      url : base + 'request-radio/delete-radio',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        location.reload();
      }
    });
  }
});

$('#payment-pending').on('click',function(e){
  e.preventDefault();
  general_modal('Confirm Payment',base + 'request-radio/payment-pending');
});


$('.delete-slider-home').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure to delete this slider ? ')){
    $.ajax({
      url : base + 'slider-home/delete',
      data : {
        id : id,
        _csrf : gils
      },
      type : 'POST',
      success : function(data){
        window.location = base + 'slider-home';
      }
    });
  }

});





$('.taginput').tagsInput();

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.getpreview').attr('src', e.target.result).css('margin-bottom','15px');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".setpreview").change(function(){
    readURL(this);
});


$('#sliderhome-type').change(function(){
  var value = $(this).val();
  if(value == 'radio'){
    $('.field-sliderhome-radio_id').show();
  }else{
    $('.field-sliderhome-radio_id').hide();
  }
});









//validation


$('#slider-home').validate({
  rules : {
    file : {
      required : function(element){
        return $('#updateya').length < 1;
      },
    },
    // title : {
    //   required : true
    // },
    // description : {
    //   required : true
    // },
    link : {
      url : true
    },
    type : {
      required : true
    },
    radio : {
      required : function(element) {
        return $("#sliderhome-type").val() == 'radio';
      }
    },
    expired : {
      required : true
    }
  },
  messages : {
    file : {
      required : 'please choose image'
    },
    // title : {
    //   required : 'please fill the title'
    // },
    // description : {
    //   required : 'please fill the description'
    // },
    link : {
      url : 'please fill the correct url'
    },
    type : {
      required : 'please fill type of slider'
    },
    radio : {
      required : 'please choose the radio'
    },
    expired : {
      required : 'please fill the expired date'
    }
  }
});


$("#sortableslider").sortable({
  stop : function(event, ui){
    var result = $(this).sortable('serialize') + '&_csrf='+gils;

    $.ajax({
      type : 'post',
      url : base + 'slider-home/sort',
      data : result
    });
  }
});
$("#sortableslider").disableSelection();


$("#sortable-faq").sortable({
  stop : function(event, ui){
    var result = $(this).sortable('serialize') + '&_csrf='+gils;

    $.ajax({
      type : 'post',
      url : base + 'faq/sort',
      data : result
    });
  }
});
$("#sortable-faq").disableSelection();





// Data Content

var reqrad = jQuery('#datatable-data').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,4]
    } ],
    //"order": [[ 1, 'asc' ]]
} );

reqrad.on( 'order.dt search.dt', function () {
    reqrad.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();





var mr = jQuery('#main-radio').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,2]
    } ],
    "pageLength": 100
    //"order": [[ 1, 'asc' ]]
} );

mr.on( 'order.dt search.dt', function () {
    mr.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();



$('.set-main-radio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var title = $(this).data('title');
  $('#modalmainradio').modal('show');
  $('#modalmainradio #title-radio').html(title);
  $('#modalmainradio input[type=hidden]').val(id);
});

$('#main-radio-form').validate({
  rules : {
    expired : {
      required : true
    }
  },
  messages : {
    expired : {
      required : 'please fill the expired date'
    }
  },
  submitHandler : function(form){
    var dataform = $(form).serialize() + '&_csrf='+gils;
    $.ajax({
        url : base + 'main-radio/set',
        type : 'POST',
        data : dataform,
        success : function(){
          location.reload();
        }
      });
    return false;
  }
});


$('.unset-main-radio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var kuy = $(this).parents('tr');
  if(confirm('Are you sure to unset this radio become main radio ?')){
    $.ajax({
      url : base + 'main-radio/unset',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(){
        location.reload();
      }
    });
  }
});






var na = jQuery('#news-admin').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,2]
    } ],
    "pageLength": 100
    //"order": [[ 1, 'asc' ]]
} );

na.on( 'order.dt search.dt', function () {
    na.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


$('#create-news').validate({
  rules : {
    type : {
      required : true
    },
    title : {
      required : true
    },
    banner : {
      required : true
    }
  },
  messages : {
    type : {
      required : 'please select type first'
    },
    title : {
      required : 'please fill the title'
    },
    banner : {
      required : 'please fill the banner'
    }
  },
  submitHandler : function(form){
    $('#create-news button').prop('disabled',true).html('saving ...');
    var kuy = $('#create-news')[0];
    var formData = new FormData(kuy);
    formData.append('banner', $('input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());
    formData.append('content', tinymce.get('description_goks').getContent());

    $.ajax({
      url : base + 'news/create',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'news';
      }
    });
    return false;
  }
});

$('#edit-news-data').validate({
  rules : {
    type : {
      required : true
    },
    title : {
      required : true
    }
  },
  messages : {
    type : {
      required : 'please select type first'
    },
    title : {
      required : 'please fill the title'
    }
  },
  submitHandler : function(form){
    $('#edit-news-data button').prop('disabled',true).html('saving ...');
    var kuy = $('#edit-news-data')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());
    formData.append('content', tinymce.get('description_goks').getContent());

    $.ajax({
      url : base + 'data-news-events/create',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'data-news-events';
      }
    });
    return false;
  }
});

$('#edit-news').validate({
  rules : {
    type : {
      required : true
    },
    title : {
      required : true
    }
  },
  messages : {
    type : {
      required : 'please select type first'
    },
    title : {
      required : 'please fill the title'
    }
  },
  submitHandler : function(form){
    $('#edit-news button').prop('disabled',true).html('saving ...');
    var kuy = $('#edit-news')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());
    formData.append('content', tinymce.get('description_goks').getContent());

    $.ajax({
      url : base + 'news/create',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'news';
      }
    });
    return false;
  }
});


$('.delete-news-admin').click(function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sire to delete this news ?')){
    $.ajax({
      url : base + 'news/delete',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        location.reload();
      }
    });
  }
});


var dt = jQuery('#data-tickets').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,4]
    } ],
    "pageLength": 100
    //"order": [[ 1, 'asc' ]]
} );

dt.on( 'order.dt search.dt', function () {
    dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


$('#tickets-event').validate({
  rules : {
    category : {
      required : true
    },
    price : {
      required : true,
      digits : true
    }
  },
  messages : {
    category : {
      required : 'please fill the category name'
    },
    price : {
      required : 'please fill the price',
        digits : 'please fill the right price'
    }
  },
  submitHandler : function(form){
    $('#tickets-event button').prop('disabled',true);
    var formData = $(form).serialize() + '&_csrf='+gils;
    $.ajax({
      url : base + 'news/insert-ticket',
      type : 'POST',
      data : formData,
      success : function(data){
        location.reload();
      }
    });
    return false;
  }
});


$('.edit-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $.ajax({
    url : base + 'news/edit-ticket',
    type : 'POST',
    data : {
      id : id,
      _csrf : gils
    },
    dataType : 'JSON',
    success : function(data){
      $('#tickets-event input[name=id]').val(id);
      $('#tickets-event input[name=category]').val(data.category);
      $('#tickets-event input[name=price]').val(data.price);
    }
  });
});


$('.delete-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var thisis = $(this).parents('tr');
  if(confirm('Are you sure to delete this ticket ?')){
    $.ajax({
      url : base + 'news/delete-ticket',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        dt.row(thisis).remove().draw();
      }
    });
  }

});


$('.sold-ticket').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure to change status sold this ticket ?')){
    $.ajax({
      url : base + 'news/sold-ticket',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        location.reload();
      }
    });
  }

});


var dc = jQuery('#data-contact').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,3]
    } ],
    "pageLength": 100
    //"order": [[ 1, 'asc' ]]
} );

dc.on( 'order.dt search.dt', function () {
    dc.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();


$('.item-adv .change').on('click',function(){
  var id = $(this).data('id');
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-title').html('Change Image');
  $.ajax({
    url : base + 'banner-news-event-channel/change',
    type : 'POST',
    data : { id : id , _csrf : gils },
    success : function(data){
      $('#generalmodal .modal-body').html(data);
    }
  });
});



var bank = jQuery('#data-bank').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,5]
    } ],
    "pageLength": 100
    //"order": [[ 1, 'asc' ]]
} );

bank.on( 'order.dt search.dt', function () {
    bank.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();

$('#add-new-bank').on('click',function(e){
  e.preventDefault();
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-title').html('New Bank');
  $.ajax({
    url : base + 'bank/form-new',
    success : function(data){
      $('#generalmodal .modal-body').html(data);
    }
  })
});

$('.edit-bank').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  $('#generalmodal').modal('show');
  $('#generalmodal .modal-title').html('Edit Bank');
  $.ajax({
    url : base + 'bank/form-edit',
    data : {
      id : id
    },
    success : function(data){
      $('#generalmodal .modal-body').html(data);
    }
  });
});

$('.delete-bank').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var thisis = $(this).parents('tr');
  if(confirm('Are you sure to delete this Bank ?')){
    $.ajax({
      url : base + 'bank/delete',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(data){
        bank.row(thisis).remove().draw();
      }
    });
  }
});


$('.taginput').tagsInput();

tinymce.init({ selector:'.editortextarea' ,height : '300'});


$('#editlogo').validate({
  rules : {
    image : {
      required : true
    }
  },
  messages : {
    image : {
      required : 'please choose the logo'
    }
  },
  submitHandler : function(form){
    $('#editlogo button').prop('disabled',true).html('saving ...');
    var kuy = $('#editlogo')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('#editlogo input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());

    $.ajax({
      url : base + 'logo/change',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        $('#editlogo .alert').remove();
        $('#editlogo').prepend('<div class="alert alert-success">Logo has been changed</div>');
        $('#editlogo input[type=file]').val('');
        $('#editlogo button').prop('disabled',false).html('save');
      }
    });
    return false;
  }
});

$('.delete-slider').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  if(confirm('Are you sure to delete this slider ?')){
    $.ajax({
      url : base + 'slider-home/delete',
      type : 'POST',
      data : {
        id : id,
        _csrf : gils
      },
      success : function(){
        location.reload();
      }
    });
  }
});



$('#edittutorial').validate({
  rules : {
    image : {
      required : true
    }
  },
  messages : {
    image : {
      required : 'please choose the file'
    }
  },
  submitHandler : function(form){
    $('#edittutorial button').prop('disabled',true).html('saving ...');
    var kuy = $('#edittutorial')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('#edittutorial input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());

    $.ajax({
      url : base + 'tutorial/change',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        location.reload();
      }
    });
    return false;
  }
});


$('.download-tutorial').on('click',function(e){
  e.preventDefault();
  var file =  $(this).data('file');
  window.location.href = '/images/' + file;
});
