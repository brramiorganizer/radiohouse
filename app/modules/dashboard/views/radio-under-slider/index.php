<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <div class="row wrap-radio-adv">
        <div class="col-md-4 col-sm-4 radio-adv utama" data-sort="1">

          <img src="<?= Helper::getAdvertismentOne(1);?>" class="img-responsive" style="height:402px">

            <div class="add-radio">
              <?php if(Helper::getAdvertismentOneName(1)[0] != ''){ ?>
                <h5><?= Helper::getAdvertismentOneName(1)[0] ?></h5>
              <?php } ?>
              <p><i class="icon icon-plus"></i> Change</p>
            </div>
        </div>
        <div class="col-md-8 col-sm-8">
          <div class="row">
            <div class="col-md-4 radio-adv" data-sort="2">
              <img src="<?= Helper::getAdvertismentOne(2);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(2)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(2)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="3">
              <img src="<?= Helper::getAdvertismentOne(3);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(3)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(3)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="4">
              <img src="<?= Helper::getAdvertismentOne(4);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(4)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(4)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="5" style="margin-top:10px">
              <img src="<?= Helper::getAdvertismentOne(5);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(5)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(5)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="6" style="margin-top:10px">
              <img src="<?= Helper::getAdvertismentOne(6);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(6)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(6)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="7" style="margin-top:10px">
              <img src="<?= Helper::getAdvertismentOne(7);?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentOneName(7)[0] != ''){ ?>
                  <h5><?= Helper::getAdvertismentOneName(7)[0] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
          </div>
        </div>
      </div>



    </div>

  </div>
</div>



<div id="adv1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin:0; padding:0">Ubah Iklan</h4>
        </div>
        <div class="modal-body">
          <form id="adv-1">
          <div class="form-group">
            <label for="">Radio</label>
            <select class="form-control" name="radio" id="radio">
              <option value="">-- Select Radio --</option>
              <?php foreach($radios as $radio){ ?>
                <option value="<?= $radio->id ?>"><?= $radio->radio_name ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label for="">Image</label><br/>
            <label style="margin-right:30px" for="">
              <input type="radio" class="who-image" name="image" value="old" checked> Image from Radio photo profile
            </label>
            <label for="">
              <input type="radio" class="who-image" name="image" value="new"> New Image
            </label>
            <div class="filing">
              <div class="old">
                <img src="" alt="" style="width:200px">
              </div>
              <div class="new" style="display:none">
                <label class="error" id="pesan-ukuran"></label>
                <input type="file" name="file" value="" onchange="imageShow(this);">
                <img id="blah" src="<?= Helper::base_url() ?>demos/800x800.png" alt="" style="width:200px; margin:20px 0">
              </div>
            </div>

          </div>
          <div class="form-group">
            <label for="">Date Expired</label>
            <input type="text" readonly name="date" id="datepicker" class="form-control" value="">
          </div>
          <div class="form-group">
            <input type="hidden" name="sort" id="sort" value="">
            <button type="submit" class="btn btn-primary" name="button">save</button>
          </div>
        </form>
        </div>

      </div>

    </div>
  </div>

<style media="screen">
.radio-adv h5{
  color: white;
  text-align: center;
  line-height: 100%;
  position: absolute;
  width: 100%;
  top: 5%;
  text-transform: uppercase;
  letter-spacing: 3px;
  font-size: 8px
}
.utama h5{
    font-size: 10px
}

  .wrap-radio-adv > div{
    position: relative;
  }
  .add-radio{
    position: absolute;
    left: 10%;
    top: 5%;
    width: 80%;
    height: 90%;
    background: rgba(0,0,0,0.8);
    border-radius: 5px;
    border: 3px dashed #FFF;
    cursor: pointer;
    display: none
  }
  .add-radio p{
    color: white;
    text-align: center;
    line-height: 100%;
    position: absolute;
    width: 100%;
    top: 48%;

    text-transform: uppercase;
    letter-spacing: 3px;
  }
  .col-md-4 p{
    font-size: 18px;
  }
  .col-md-8 .col-md-4 p{
    font-size: 14px
  }
  .col-md-4:hover .add-radio{
    display: block;
  }
  #blah{
    display: none
  }
  .col-md-4.radio-adv img{
    width: 200px;
    height: 200px;
  }
  .utama img{
    width: 100% !important;
    height: 410px !important;
  }
</style>
<script type="text/javascript">


function imageShow(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result).fadeIn();
        }

        reader.readAsDataURL(input.files[0]);
    }
}
setTimeout(function(){
  $( "#datepicker" ).datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat: 'yy-mm-dd'
});

  $('#adv-1').validate({
    rules : {
      radio : {
        required : true
      },
      file : {
        required : function(){
          return $('input[name=image]:checked').val() == 'new';
        }
      },
      date : {
        required : true
      }
    },
    messages : {
      radio : {
        required : 'please choose the radio'
      },
      file : {
        required : 'please choose the image'
      },
      date : {
        required : 'please select date expired'
      }
    },
    submitHandler : function(form){
      var kuy = $('#adv-1')[0];
      var formData = new FormData(kuy);
      formData.append('banner', $('input[name=file]')[0].files[0]);
      formData.append('_csrf', $('#gils').val());
      $('#adv-1 button').html('saving ...');
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-under-slider/advertisment-one',
        type : 'POST',
        contentType: false,
        cache: false,
        processData:false,
        data : formData,
        success : function(data){

            location.reload();
        }
      });

      return false;
    }
  });

  $('#radio').on('change',function(){
    var valuenya = $(this).val();
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-under-slider/get-one-radio',
      data : {
        id : valuenya,
        _csrf : $('#gils').val()
       },
      type : 'POST',
      dataType : 'JSON',
      success : function(data){
        if(data.photo_profile == null || data.photo_profile == ''){
          if($('#sort').val() == 1){
            $('.filing .old img').attr('src','<?= Yii::$app->params['base_url'] ?>demos/800x1100.png');
          }else{
            $('.filing .old img').attr('src','<?= Yii::$app->params['base_url'] ?>demos/800x800.png');
          }
        }else{
          $('.filing .old img').attr('src','<?= Yii::$app->params['base_url'] ?>media/radio/profile/'+data.photo_profile);
        }
      }
    });
  });
  $('.radio-adv').on('click',function(){
    $('#adv1').modal('show');
    var sortnya = $(this).data('sort');
    $('#sort').val(sortnya);
    $('.filing .old img').attr('src','');
    $('select#radio').val('');
    if(sortnya == 1){
      $('#pesan-ukuran').html('* image size 800px x 1100px');
    }else{
      $('#pesan-ukuran').html('* image size 800px x 800px');
    }

  });
  $('.who-image').on('change',function(){
    var valuenya = $(this).val();
    $('.filing > div').hide();
    $('.filing > .'+valuenya).fadeIn();
  });
},1000);

</script>
