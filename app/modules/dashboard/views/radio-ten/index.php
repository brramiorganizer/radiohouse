<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

    <table id="datatable-radio-menu">
      <thead>
        <tr>
          <th>choose</th>
          <th>Radio Name</th>
          <th>Company Name</th>
          <th>Personal</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($radios as $radio){ ?>
        <tr>
          <td class="text-center">
            <?php $checked = (Helper::checkedOrNot($radio->id) ? 'checked="checked"' : ''); ?>
            <input <?= $checked ?> class="topone" type="checkbox" value="<?= $radio->id ?>"> </td>
          <td><?= $radio->radio_name ?></td>
          <td><?= $radio->company_name ?></td>
          <td><?= $radio->personal->name ?></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>


    </div>

  </div>
</div>


<div id="modalten" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Expired Date</h4>
      </div>
      <div class="modal-body" style="overflow:hidden">
        <form id="oke-ten">
          <div class="form-group">
            <input type="hidden" name="radio_id" value="">
            <label for="">Expired Date</label>
            <input type="text" class="form-control dateya" name="expired" readonly value="">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="button">Save</button>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  setTimeout(function(){



    var t = jQuery('#datatable-radio-menu').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": [0]
        } ]
    } );

    
    $('.topone').on('click',function(){
      var id = $(this).val();

      if($(this).prop('checked') == true){
        $(this).prop('checked' , false);
        $('#modalten').modal('show');
        $('#modalten input[type=hidden]').val(id);
      }else{
        $(this).prop('checked' , true);
        if(confirm('Are you sure to inactive this radio on the menu ?')){
          $.ajax({
            url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-ten/delete',
            type : 'POST',
            data :{
              id : id,
              _csrf : gils
            },
            success : function(data){
              location.reload();
            }
          });
        }
      }

    });

    $('#oke-ten').validate({
      rules : {
        expired : {
          required : true
        }
      },
      messages : {
        expired : {
          required : 'please fill the date expired'
        }
      },
      submitHandler : function(form){
        var dataform = $(form).serialize() + '&_csrf='+gils;
        $.ajax({
          url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-ten/insert',
          type : 'POST',
          data :dataform,
          success : function(data){
            location.reload();
          }
        });
        return false;
      }
    });


  },1000);
</script>
