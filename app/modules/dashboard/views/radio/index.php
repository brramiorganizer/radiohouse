<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">



      <table id="datatable-request-radio">
        <thead>
          <tr>
            <th>No</th>
            <th>Radio Name</th>
            <th>Company</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($datum as $data){ ?>
            <tr>
              <td></td>
              <td><?= $data->radio_name ?></td>
              <td><?= $data->company_name ?></td>
              <td><?= ucwords($data->status) ?></td>
              <td>
                <a href="#" class="edit visitradio" data-id="<?= $data->id ?>">visitor</a>
                <a href="#" class="lihat detailradio" data-id="<?= $data->id ?>">view</a>
                <?php if($data->status == 'active'){ ?>
                  <a href="#" class="hapus inactiveradio" data-id="<?= $data->id ?>">inactive</a>
                <?php }else{ ?>
                  <a href="#" class="edit activeradio" data-id="<?= $data->id ?>">active</a>
                <?php } ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>


    </div>

  </div>
</div>
