<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

        <table id="data-contact">
        	<thead>
        		<tr>
        			<th>#</th>
        			<th>Name</th>
        			<th>Email</th>
        			<th>action</th>
        		</tr>
        	</thead>
        	<tbody>
        		<?php foreach($datum as $data){ ?>
        		<tr>
        			<td></td>
        			<td><?= $data->name ?></td>
        			<td><?= $data->email ?></td>
        			<td>
        				<a href="#" class="delete-contact hapus" data-id="<?= $data->id ?>">delete</a>
                <a href="#" class="lihat view-contact" data-id="<?= $data->id ?>">view</a>

        			</td>
        		</tr>
        		<?php } ?>
        	</tbody>
        </table>

    </div>

  </div>
</div>

<script type="text/javascript">
  setTimeout(function(){
    $('.delete-contact').on('click',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var thisis = $(this).parents('tr');
      if(confirm('Are you sure to delete this message ?')){
        $.ajax({
          url : base + 'contact/delete',
          type : 'POST',
          data : {
            id : id,
            _csrf : gils
          },
          success : function(data){
            $('#data-contact').DataTable().row(thisis).remove().draw();
          }
        });
      }
    });

    $('.view-contact').on('click',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      $.ajax({
        url : base + 'contact/view',
        type : 'POST',
        data : {
          id : id,
          _csrf : gils
        },
        success : function(data){
          $('#generalmodal').modal('show');
          $('#generalmodal .modal-title').html('Detail Message');
          $('#generalmodal .modal-body').html(data);

        }
      });
    });
  },2000);
</script>
