<?php


use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <div class="row">
        <div class="col-md-4 col-sm-4"></div>
        <div class="col-md-4 col-sm-4">
          <form id="editlogo">
            <div class="pil" style="margin-bottom:5px">
              <img class="img-responsive getpreview" src="<?= Yii::$app->params['base_url'] ?>images/<?= $model ?>" alt="">
            </div>
            <input class="setpreview" type="file" name="image" value="">
            <div class="form-group" style="margin-top:10px">
              <label class="error">* image size 275px X 82px</label>
              <button type="submit" class="btn btn-primary btn-block">save</button>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-sm-4"></div>
      </div>


    </div>

  </div>
</div>
<style media="screen">
  .getpreview{
    max-width: 100%;
    height: auto;
  }
</style>
