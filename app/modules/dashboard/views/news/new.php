<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <form id="create-news">
        <div class="form-group">
          <label for="">Type</label>
          <select class="form-control" name="type">
            <option value="">-- Select Type --</option>
            <option value="news">News</option>
            <option value="event">Event</option>
          </select>
        </div>
        <div class="form-group">
          <label for="">Title</label>
          <input type="text" class="form-control" name="title" value="">
        </div>
        <div class="form-group">
          <label for="">Description</label>
          <textarea class="editortextarea" id="description_goks"></textarea>
        </div>
        <div class="form-group">
          <label for="">Banner</label>
          <input type="file" name="banner" class="setpreview" value="">
          <img src="" class="getpreview img-responsive" >
        </div>
        <p style="font-size:12px; color:red; margin-top:30px">* search engine optimization (SEO)</p>
        <div class="form-group">
          <label for="">Meta Description</label>
          <textarea class="form-control" name="meta_description" rows="4" cols="80"></textarea>
        </div>
        <div class="form-group">
          <label for="">Meta Keyword</label>
          <textarea class="form-control taginput" name="meta_keywords" rows="4" cols="80"></textarea>
        </div>
        <div class="form-group">
          <button type="submit" name="button" class="btn btn-primary">save</button>
        </div>
      </form>


    </div>

  </div>
</div>


<script type="text/javascript">
  setTimeout(function(){
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.getpreview').attr('src', e.target.result).css('margin','15px 0');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".setpreview").change(function(){
        readURL(this);
    });
  },2000);
</script>
