<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <div class="row">
        <div class="col-md-8">
          <h3 class="titinya"><?= $model->title ?></h3>

          <div class="content">
            <?= $model->description ?>
          </div>

          <div class="tickets">
            <h4>Tickets</h4>
            <div class="isi">
              <form id="tickets-event">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="news_event_id" value="<?= $model->id ?>">
                <div class="form-group">
                  <label for="">Category Ticket</label>
                  <input type="text" class="form-control" name="category" value="">
                </div>
                <div class="form-group">
                  <label for="">Price</label>
                  <input type="text" class="form-control" name="price" value="">
                </div>
                <div class="form-group">
                  <button type="submit" name="button" class="btn btn-primary">save</button>
                </div>
              </form>

              <div class="data-tickets">
                <table id="data-tickets">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Category</th>
                      <th>Price</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($tickets as $t){ ?>
                    <tr>
                      <td></td>
                      <td><?= $t->category ?></td>
                      <td>Rp. <?= number_format($t->price) ?></td>
                      <td>
                        <?php if($t->available == 'yes'){ ?>
                          <span class="label label-primary">Availabel</span>
                        <?php }else{ ?>
                          <span class="label label-danger">Sold</span>
                        <?php } ?>
                      </td>
                      <td class="action">
                        <?php if($t->available == 'yes'){ ?>
                        <a href="#" class="hapus delete-ticket" data-id="<?= $t->id ?>">delete</a>
                        <a href="#" class="edit edit-ticket" data-id="<?= $t->id ?>">edit</a>
                        <a href="#" class="sold-ticket" data-id="<?= $t->id ?>" style="border-bottom:1px dashed #333333">sold</a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <h4 class="h-banner">Banner Image</h4>
          <img style="width:100%" src="<?= Yii::$app->params['base_url'] ?>media/banner-news-event/<?= $model->banner ?>" alt="">
          <table class="ketmeta">
            <tr>
              <td>Meta Description</td>
              <td>:</td>
              <td><?= $model->meta_description ?></td>
            </tr>
            <tr>
              <td>Meta Keywords</td>
              <td>:</td>
              <td><?= $model->meta_keywords ?></td>
            </tr>
          </table>
          <a href="<?= Yii::$app->params['base_url'] ?>dashboard/news/edit?id=<?= $model->id ?>" class="btn btn-primary btn-block">edit</a>
        </div>
      </div>


    </div>

  </div>
</div>
<style media="screen">
.titinya{
  color:#333
}
  .h-banner{
    background: #ddd;
    text-align: center;
    margin: 0;
    padding: 10px 5px
  }
  .content{
    margin-top: 30px
  }
  .ketmeta{
    width: 100%;
    margin-top: 30px;
    margin-bottom: 30px
  }
  .ketmeta td{
    padding: 4px
  }
  .tickets{
    margin-top: 40px
  }
  .tickets h4{
    text-align: center;
background: #333333;
color: #fff;
padding: 15px;
text-transform: uppercase;
letter-spacing: 2px;
font-size: 14px;
margin:0
  }
  .tickets .isi{
    padding: 15px;
border: 1px solid #333333;
  }
  .data-tickets .row{
    margin: 0 !important
  }
</style>
