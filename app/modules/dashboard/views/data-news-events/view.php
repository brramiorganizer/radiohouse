<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <div class="row">
        <div class="col-md-8">
          <h3 class="titinya"><?= $model->title ?></h3>

          <div class="content">
            <?= $model->description ?>
          </div>
        </div>
        <div class="col-md-4">
          <h4 class="h-banner">Banner Image</h4>
          <img style="width:100%" src="<?= Yii::$app->params['base_url'] ?>media/banner-news-event/<?= $model->banner ?>" alt="">
          <table class="ketmeta">
            <tr>
              <td>Meta Description</td>
              <td>:</td>
              <td><?= $model->meta_description ?></td>
            </tr>
            <tr>
              <td>Meta Keywords</td>
              <td>:</td>
              <td><?= $model->meta_keywords ?></td>
            </tr>
          </table>
          <a href="<?= Yii::$app->params['base_url'] ?>dashboard/news/edit?id=<?= $model->id ?>" class="btn btn-primary btn-block">edit</a>
        </div>
      </div>


    </div>

  </div>
</div>
<style media="screen">
.titinya{
  color:#333
}
  .h-banner{
    background: #ddd;
    text-align: center;
    margin: 0;
    padding: 10px 5px
  }
  .content{
    margin-top: 30px
  }
  .ketmeta{
    width: 100%;
    margin-top: 30px;
    margin-bottom: 30px
  }
  .ketmeta td{
    padding: 4px
  }
</style>
