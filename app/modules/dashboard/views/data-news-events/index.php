<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <table id="news-admin">
        <thead>
          <tr>
            <th>#</th>
            <th>Type</th>
            <th>Title</th>
            <th>Author</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($datum as $data){ ?>
          <tr>
            <td></td>
            <td><?= $data->type ?></td>
            <td><?= $data->title ?></td>
            <td>
              <?php if($data->author == 'admin'){ ?>
                Admin
              <?php }else{ ?>
                <?= $data->radio->radio_name ?>
              <?php } ?>
            </td>
            <td class="action">
              <a href="#" class="hapus delete-news-admin" data-id="<?= $data->id ?>">delete</a>
              <a href="<?= Yii::$app->params['base_url'] ?>dashboard/data-news-events/edit?id=<?= $data->id ?>" class="edit">edit</a>
              <?php $tonya = ($data->type == 'news' ? 'view' : 'ticket') ?>
              <a href="<?= Yii::$app->params['base_url'] ?>dashboard/data-news-events/<?= $tonya ?>?id=<?= $data->id ?>" class="lihat">view</a>

            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>


    </div>

  </div>
</div>
