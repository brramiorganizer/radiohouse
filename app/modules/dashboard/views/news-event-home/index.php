<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

      <div class="row wrap-radio-adv">

        <div class="col-md-12 col-sm-12">
          <div class="row">
            <div class="col-md-4 radio-adv" data-sort="1">
              <img src="<?= Helper::getAdvertismentNeOne(1)['image'];?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentNeOne(1)['title'] != ''){ ?>
                  <h5><?= Helper::getAdvertismentNeOne(1)['title'] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="2">
              <img src="<?= Helper::getAdvertismentNeOne(2)['image'];?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentNeOne(2)['title'] != ''){ ?>
                  <h5><?= Helper::getAdvertismentNeOne(2)['title'] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="3">
              <img src="<?= Helper::getAdvertismentNeOne(3)['image'];?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentNeOne(3)['title'] != ''){ ?>
                  <h5><?= Helper::getAdvertismentNeOne(3)['title'] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="4">
              <img src="<?= Helper::getAdvertismentNeOne(4)['image'];?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentNeOne(4)['title'] != ''){ ?>
                  <h5><?= Helper::getAdvertismentNeOne(4)['title'] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>
            <div class="col-md-4 radio-adv" data-sort="5" >
              <img src="<?= Helper::getAdvertismentNeOne(5)['image'];?>" class="img-responsive">
              <div class="add-radio">
                <?php if(Helper::getAdvertismentNeOne(5)['title'] != ''){ ?>
                  <h5><?= Helper::getAdvertismentNeOne(5)['title'] ?></h5>
                <?php } ?>
                <p><i class="icon icon-plus"></i> Change</p>
              </div>
            </div>


          </div>
        </div>
      </div>



    </div>

  </div>
</div>



<div id="adv1" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xs">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin:0; padding:0">Ubah Iklan</h4>
        </div>
        <div class="modal-body">
          <form id="adv-2">
          <div class="form-group">
            <label for="">News / Event</label>
            <select class="form-control" name="ne" id="radio">
              <option value="">-- Select News Event --</option>
              <?php foreach($newsevents as $o){ ?>
                <option value="<?= $o->id ?>"><?= $o->title?></option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label for="">Date Expired</label>
            <input type="text" readonly name="date" id="datepicker" class="form-control" value="">
          </div>

          <div class="form-group">
            <input type="hidden" name="sort" id="sort" value="">
            <button type="submit" class="btn btn-primary" name="button">save</button>
          </div>
        </form>
        </div>

      </div>

    </div>
  </div>

<style media="screen">
.radio-adv{
  margin-bottom: 10px
}
.radio-adv h5{
  color: white;
  text-align: center;
  line-height: 100%;
  position: absolute;
  width: 100%;
  top: 5%;
  text-transform: uppercase;
  letter-spacing: 3px;
  font-size: 8px
}
.utama h5{
    font-size: 10px
}

  .wrap-radio-adv > div{
    position: relative;
  }
  .add-radio{
    position: absolute;
    left: 10%;
    top: 5%;
    width: 80%;
    height: 90%;
    background: rgba(0,0,0,0.8);
    border-radius: 5px;
    border: 3px dashed #FFF;
    cursor: pointer;
    display: none
  }
  .add-radio p{
    color: white;
    text-align: center;
    line-height: 100%;
    position: absolute;
    width: 100%;
    top: 48%;

    text-transform: uppercase;
    letter-spacing: 3px;
  }
  .col-md-4 p{
    font-size: 18px;
  }
  .col-md-8 .col-md-4 p{
    font-size: 14px
  }
  .col-md-4:hover .add-radio{
    display: block;
  }
  #blah{
    display: none
  }
  .col-md-4.radio-adv img{
    width: 100%;
    height: 250px;
  }
  .utama img{
    width: 100% !important;
    height: 410px !important;
  }
  .col-md-2.radio-adv{
    width: 20%;
    margin-top: 10px
  }
  .col-md-2.radio-adv img{
    width: 172px;
    height: 172px
  }
</style>
<script type="text/javascript">


setTimeout(function(){
  $( "#datepicker" ).datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat: 'yy-mm-dd',
  minDate : 0
});

  $('#adv-2').validate({
    rules : {
      ne : {
        required : true
      }
    },
    messages : {
      ne : {
        required : 'please choose the news or event'
      }
    },
    submitHandler : function(form){
      var formdata = $(form).serialize()+'&_csrf='+$('#gils').val();
      $('#adv-2').prop('disabled',true);
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-before-footer/advertisment-ne',
        type : 'POST',
        data : formdata,
        success : function(data){
            location.reload();
        }
      });

      return false;
    }
  });


  $('.radio-adv').on('click',function(){
    $('#adv1').modal('show');
    var sortnya = $(this).data('sort');
    $('#sort').val(sortnya);
    $('select#radio').val('');

  });

},1000);

</script>
