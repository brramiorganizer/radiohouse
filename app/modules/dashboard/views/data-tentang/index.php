<?php
use app\helper\Helper;
?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
<form style=" display:none" id="about-form">
  <div class="form-group">
    <label for="">Icon 1</label>
    <input type="text" name="icon1" class="form-control" value="<?= Helper::getDataContent(23) ?>">
  </div>
  <div class="form-group">
    <label for="">Icon 2</label>
    <input type="text" name="icon2" class="form-control" value="<?= Helper::getDataContent(24) ?>">
  </div>
  <div class="form-group">
    <label for="">Icon 3</label>
    <input type="text" name="icon3" class="form-control" value="<?= Helper::getDataContent(25) ?>">
  </div>

  <div class="form-group">
  <button type="submit" name="button" class="btn btn-primary">Save</button>
  </div>
</form>
</div>
</div>
</div>
<script type="text/javascript">
setTimeout(function(){

  $('#about-form').fadeIn();
  $('#about-form').validate({
    rules : {
      icon1 : {
        required : true
      },
      icon2 : {
        required : true
      },
      icon3 : {
        required : true
      }
    },
    messages : {
      icon1 : {
        required : 'please fill the name of first icon'
      },
      icon2 : {
        required : 'please fill the name of second icon'
      },
      icon3 : {
        required : 'please fill the name of third icon'
      }
    },
    submitHandler : function(form){
      $('#about-form button').html('Saving ...').prop('disabled',true);
      var dataform = $(form).serialize() + '&_csrf='+ $('#gils').val();
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/page/content-about',
        type : 'POST',
        data : dataform,
        dataType : 'JSON',
        success : function(data){
          $('#about-form button').html('Save').prop('disabled',false);
          $('#about-form').prepend('<div class="alert alert-success">Data has been updated</div>');
          setTimeout(function(){
            $('#about-form .alert').remove();
          },3000);
        }
      });
      return false;
    }
  });
  },1000);
</script>
