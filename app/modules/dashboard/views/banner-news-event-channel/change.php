<form id="change-adv-ne-channel">
  <input type="hidden" name="id" value="<?= $data->id ?>">
  <div class="form-group">
    <label for="">Link <span class="note">* exp <?= Yii::$app->params['base_url'] ?></span></label>
    <input type="text" class="form-control" name="link" value="<?= $data->link ?>">
  </div>
  <div class="form-group">
    <label for="">Expired</label>
    <input type="text" class="form-control datenow" readonly name="expired"  value="">
  </div>
  <div class="form-group">
    <label for="">Image  <span class="note">* image size must be 800px X 800px</span></label>
    <input type="file" name="image" class="setpreview" value="">
  </div>
  <div class="form-group">
    <?php if($data->image == ''){ ?>
      <img class="getpreview" src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
    <?php }else{ ?>
      <img class="getpreview"  src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= $data->image ?>" alt="">
    <?php } ?>
  </div>
  <input type="hidden" id="ri" value="<?= $data->image ?>">
  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="button">Save</button>
  </div>
</form>

<style media="screen">
  #change-adv-ne-channel img{
    width: 200px
  }
  .note{
    font-size: 11px;
    color:red
  }
</style>
<script type="text/javascript">

function readThis(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.getpreview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".setpreview").change(function(){
    readURL(this);
});


$('#change-adv-ne-channel').validate({
  rules : {
    image : {
      required : function(){
        return $("#ri").val() == '';
      }
    },
    link : {
      url : true
    },
    expired : {
      required : true
    }
  },
  messages : {
    image : {
      required : 'please choose the image'
    },
    link : {
      url : 'please fill the right URL'
    },
    expired : {
      required : 'please fill the expired date'
    }
  },
  submitHandler : function(data){

    $('#change-adv-ne-channel button').prop('disabled',true).html('saving ...');
    var kuy = $('#change-adv-ne-channel')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('#change-adv-ne-channel input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());

    $.ajax({
      url :  '<?= Yii::$app->params['base_url'] ?>dashboard/'+ 'banner-news-event-channel/insert',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'banner-news-event-channel';
      }
    });

    return false;
  }
});

$( ".datenow" ).datepicker({
changeMonth: true,
changeYear: true,
dateFormat: 'yy-mm-dd',
minDate : 1
});
</script>
