<?php

use app\helper\Helper;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

        <div class="row">
          <div class="col-md-6">
            <h4 class="text-center">Advertisment News & Event Page</h4>
            <div class="item-adv">
              <div class="wrap-img">
                <?php if(Helper::getNeChannel(1)->image == ''){ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
                <?php }else{ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(1)->image ?>" alt="">
                <?php } ?>
                <div class="change" data-id="1">
                  <p>Change</p>
                </div>
              </div>
            </div>


            <div class="item-adv">
              <div class="wrap-img">
                <?php if(Helper::getNeChannel(2)->image == ''){ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
                <?php }else{ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(2)->image ?>" alt="">
                <?php } ?>
                <div class="change" data-id="2">
                  <p>Change</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <h4 class="text-center">Advertisment Channel Page</h4>

            <div class="item-adv">
              <div class="wrap-img">

                <?php if(Helper::getNeChannel(3)->image != '' and Helper::getNeChannel(3)->expired >= date('Y-m-d H:i:s')){ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(3)->image ?>" alt="">
                <?php }else{ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">

                <?php } ?>
                <div class="change" data-id="3">
                  <p>Change</p>
                </div>
              </div>
            </div>

            <div class="item-adv">
              <div class="wrap-img">
                <?php if(Helper::getNeChannel(4)->image == ''){ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>demos/800x800.png" alt="">
                <?php }else{ ?>
                  <img  src="<?= Yii::$app->params['base_url'] ?>media/advertisment/<?= Helper::getNeChannel(4)->image ?>" alt="">
                <?php } ?>
                <div class="change" data-id="4">
                  <p>Change</p>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>

  </div>
</div>

<style media="screen">
  .item-adv{
    text-align: center;
    margin-top: 20px;
  }
  .item-adv .wrap-img{
    width: 250px;
    height: 250px;
    margin: auto;
    position: relative;
  }
  .item-adv img{
    width: 100%;
    height: 250px;
    margin: auto;
  }
  .item-adv .change{
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.5);
    border:3px dashed #fff;
    cursor: pointer;
    display: none
  }
  .item-adv .wrap-img:hover .change{
    display: block
  }
  .item-adv .change p{
    position: absolute;
    left: 0;
    top:45%;
    width: 100%;
    color:#fff;
    text-transform: uppercase;
letter-spacing: 6px;
font-size: 20px;
  }
</style>
