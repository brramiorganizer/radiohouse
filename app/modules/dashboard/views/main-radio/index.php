<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">


      <table id="main-radio">
        <thead>
          <tr>
            <th>#</th>
            <th>Radio Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($radios as $s){ ?>

            <?php
            $active = '';
            if($now and $s->id == $now->radio_id and $now->expired >= date('Y-m-d H:i:s')){
              $active = 'thismain';
            }
            ?>
          <tr class="<?= $active ?>">
            <td></td>
            <td><?= $s->radio_name ?></td>
            <td class="action">
              <?php if($now and $s->id == $now->radio_id and $now->expired >= date('Y-m-d H:i:s')){ ?>
                <a href="#" data-id="<?= $s->id ?>" class="unset-main-radio">unset main radio</a>
            <?php }else{ ?>
                  <a href="#" data-id="<?= $s->id ?>" data-title="<?= $s->radio_name ?>" class="set-main-radio">set main radio</a>
            <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>

    </div>

  </div>
</div>


<div id="modalmainradio" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Set Main Radio</h4>
      </div>
      <div class="modal-body" style="overflow:hidden">
        <form id="main-radio-form">
          <div class="form-group">
            <h4 id="title-radio"></h4>
          </div>
          <div class="form-group">
            <input type="hidden" name="radio_id" value="">
            <label for="">Date Expired</label>
            <input type="text" readonly class="form-control dateya" name="expired" value="">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="button">save</button>
          </div>
        </form>
      </div>
    </div>

  </div>
</div>

<style media="screen">
  .action a{
    color:blue;
    border-bottom: 1px dotted blue
  }
  .thismain{
    background: rgba(0, 255, 0,0.3);
  }
</style>
