<?php

use app\helper\Helper;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
        <p style="text-align:right"><a id="add-new-bank" href="#" class="btn btn-primary">Add New</a> </p>
        <table id="data-bank">
          <thead>
            <tr>
              <th>#</th>
              <th>Bank</th>
              <th>Rekening Name</th>
              <th>Rekening Number</th>
              <th>Logo</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($banks as $bank){ ?>
            <tr>
              <td></td>
              <td><?= $bank->name ?></td>
              <td><?= $bank->rekening_name ?></td>
              <td><?= $bank->rekening_number ?></td>
              <td><img src="<?= Yii::$app->params['base_url'].'media/bank/'. $bank->image ?>" width="70"> </td>
              <td>
                <a href="#" class="edit edit-bank" data-id="<?= $bank->id ?>">edit</a>
                <a href="#" class="hapus delete-bank" data-id="<?= $bank->id ?>">delete</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>

    </div>

  </div>
</div>
