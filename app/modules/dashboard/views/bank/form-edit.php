<form id="form-bank-edit">
  <input type="hidden" name="id" value="<?= $model->id ?>">
  <div class="form-group">
    <label for="">Bank Name</label>
    <input type="text" class="form-control" name="name" value="<?= $model->name ?>">
  </div>
  <div class="form-group">
    <label for="">Rekening Name</label>
    <input type="text" class="form-control" name="rekening_name" value="<?= $model->rekening_name ?>">
  </div>
  <div class="form-group">
    <label for="">Rekening Number</label>
    <input type="text" class="form-control" name="rekening_number" value="<?= $model->rekening_number ?>">
  </div>
  <div class="form-group">
    <label for="">Logo</label>
    <input type="file" class="setpreview" name="logo" value="">
    <img src="<?= Yii::$app->params['base_url'] . 'media/bank/'. $model->image ?>" alt="" class="getpreview">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="button">Save</button>
  </div>
</form>
<style media="screen">
  .getpreview{
    width: 70px !important;
    height: auto !important;
    margin-top: 10px
  }
</style>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.getpreview').attr('src', e.target.result).css('margin-bottom','15px');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".setpreview").change(function(){
    readURL(this);
});
$('#form-bank-edit').validate({
  rules : {
    name : {
      required : true
    },
    rekening_name : {
      required : true
    },
    rekening_number : {
      required : true,
      digits : true
    }
  },
  messages : {
    name : {
      required : 'please fill the name of Bank'
    },
    rekening_name : {
      required : 'please fill the rekening name'
    },
    rekening_number : {
      required : 'please fill the rekening number',
      digits : 'please fill the right value'
    }
  },
  submitHandler : function(form){
    $('#form-bank-edit button').prop('disabled',true).html('saving ...');
    var kuy = $('#form-bank-edit')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('#form-bank-edit input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());

    $.ajax({
      url : base + 'bank/create',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'bank';
      }
    });
    return false;
  }
});
</script>
