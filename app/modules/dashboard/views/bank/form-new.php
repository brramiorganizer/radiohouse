<form id="form-bank">
  <div class="form-group">
    <label for="">Bank Name</label>
    <input type="text" class="form-control" name="name" value="">
  </div>
  <div class="form-group">
    <label for="">Rekening Name</label>
    <input type="text" class="form-control" name="rekening_name" value="">
  </div>
  <div class="form-group">
    <label for="">Rekening Number</label>
    <input type="text" class="form-control" name="rekening_number" value="">
  </div>
  <div class="form-group">
    <label for="">Logo</label>
    <input type="file" name="logo" value="">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="button">Save</button>
  </div>
</form>

<script type="text/javascript">
$('#form-bank').validate({
  rules : {
    name : {
      required : true
    },
    rekening_name : {
      required : true
    },
    rekening_number : {
      required : true,
      digits : true
    },
    logo : {
      required : true
    }
  },
  messages : {
    name : {
      required : 'please fill the name of Bank'
    },
    rekening_name : {
      required : 'please fill the rekening name'
    },
    rekening_number : {
      required : 'please fill the rekening number',
      digits : 'please fill the right value'
    },
    logo : {
      required : 'please fill the logo'
    }
  },
  submitHandler : function(form){
    $('#form-bank button').prop('disabled',true).html('saving ...');
    var kuy = $('#form-bank')[0];
    var formData = new FormData(kuy);

    formData.append('banner', $('#form-bank input[type=file]')[0].files[0]);
    formData.append('_csrf', $('#gils').val());

    $.ajax({
      url : base + 'bank/create',
      type : 'POST',
      contentType: false,
      cache: false,
      processData:false,
      data : formData,
      success : function(data){
        window.location = base + 'bank';
      }
    });
    return false;
  }
});
</script>
