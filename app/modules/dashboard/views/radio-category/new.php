<form id="newcat">
  <div class="form-group">
    <label for="">Category Name</label>
    <input type="hidden" name="id" value="">
    <input type="text" class="form-control" name="name" value="">
  </div>
  <div class="form-group">
    <button type="submit" name="button" class="btn btn-primary">Save</button>
  </div>
</form>


<script type="text/javascript">
  $('#newcat').validate({
    rules : {
      name : {
        required : true
      }
    },
    messages : {
      name : {
        required : 'please fill category name'
      }
    },
    submitHandler : function(form){
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/radio-category/new',
        type : 'POST',
        data : $(form).serialize()+'&_csrf='+$('#gils').val(),
        success : function(data){
          location.reload();
        }
      });
      return false;
    }
  });
</script>
