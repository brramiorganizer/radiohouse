<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">


      <p style="text-align:right">  <?= Html::a('Create New', ['#'], ['class' => 'btn btn-primary addnewradiocat']) ?></p>
      <table id="datatable-radio-category">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($datas as $data){ ?>
          <tr>
            <td></td>
            <td><?= $data->name ?></td>
            <td>
              <?php if($data->id != 100){ ?>
              <a href="#" data-value="<?= $data->name ?>" data-id="<?= $data->id ?>" class="edit-radio-category edit">edit</a>
              <a href="#" class="hapus">delete</a>
            <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>


    </div>

  </div>
</div>
<style media="screen">
.active-user , .inactive-user{
  display: inline-block;
  width: 15px;
  height: 15px;
  border-radius: 20px
}
.active-user{
  background: green
}
.inactive-user{
  background: red
}
</style>
