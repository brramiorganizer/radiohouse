<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\SliderHomeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Slider Homes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
        <p style="text-align:right">  <?= Html::a('Create New', ['create',], ['class' => 'btn btn-primary']) ?></p>
        <table id="datatable-slider-home">
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Type</th>
              <th>Radio</th>
              <th>Status</th>
              <th>Sort</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($dataProvider as $data){ ?>
              <?php $expired = (date('Y-m-d H:i:s') > $data->expired ? 'red-oke' : ''); ?>
            <tr class="<?= $expired ?>">
              <td></td>
              <td><?= $data->title ?></td>
              <td>
                <?php
                if($data->type == 'radio'){
                  echo 'Radio Member';
                }else{
                  echo 'Advertisment or other';
                }
                ?>
              </td>
              <td>
                <?php
                if($data->type == 'radio'){
                  echo $data->radio->radio_name;
                }else{
                  echo '-';
                }
                ?>
              </td>
              <td>
                <?php if($data->status == 'active'){ ?>
                  <span style="color:green">active</span>
                <?php }else{ ?>
                  <span style="color:red">inactive</span>
                <?php } ?>
              </td>
              <td><?= $data->sort ?></td>
              <td>
                <a href="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home/update?id=<?= $data->id ?>" class="edit">edit</a>
                <a href="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home/view?id=<?= $data->id ?>" class="lihat">lihat</a>
                <a href="#" class="hapus delete-slider" data-id="<?= $data->id ?>">delete</a>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>


        <div class="row">
          <div class="col-md-12 ">
            <h3 class="h3sort">SORT SLIDER</h3>
            <ul id="sortableslider">
              <?php foreach($dataProvider as $datax){ ?>
              <li id="set_<?= $datax->id ?>" class="ui-state-default">
                <img src="<?= Yii::$app->params['base_url'] ?>media/slider-home/<?= $datax->file ?>" alt="">
                <p class="text-center" style="margin:0"><?= $datax->title ?></p>
              </li>
              <?php } ?>
            </ul>
          </div>
        </div>

    </div>



  </div>
</div>

<style media="screen">
  .h3sort{
    margin: 30px 0 10px;
    text-align: center;
    color: #fff;
    font-size: 16px;
    background: #333;
    padding: 15px
  }
  ul{
    padding: 0
  }
  .ui-state-default{
    background: #ccc;
    padding: 5px;
    border:1px dotted #aaa;
    list-style: none;
    margin-bottom: 5px;
    cursor: move;
    text-align: center;
  }
  .ui-state-default img{
    height: 70px;
    margin: auto;
  }
  .red-oke{background-color: rgba(255, 0, 0,0.5)}
</style>
