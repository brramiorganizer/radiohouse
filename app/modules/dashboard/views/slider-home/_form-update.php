<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\SliderHome */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-home-form">
<form id="slider-home" enctype="multipart/form-data" method="post" action="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home/do-update">
  <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
  <input type="hidden" name="id" id="updateya" value="<?= $model->id ?>">
  <div class="form-group">
    <label for="">File Banner <span class="required">* size must be 1600px X 800px</span></label><br/>
    <input type="file" name="file" value="" class="setpreview">
    <img src="<?= Helper::base_url() ?>media/slider-home/<?= $model->file ?>" class="getpreview" alt="" style="display:inline-block !important">
  </div>
  <div class="form-group">
    <label for="">Title</label><br/>
    <input class="form-control" type="text" name="title" value="<?= $model->title ?>">
  </div>
  <div class="form-group">
    <label for="">Description</label><br/>
    <textarea class="form-control" name="description" style="resize:none" name="description"><?= $model->description ?></textarea>
  </div>
  <div class="form-group">
    <label for="">Link <span class="required">* use http:// or https://</span></label><br/>
    <input class="form-control" type="text" name="link" value="<?= $model->link ?>">
  </div>
  <div class="form-group">
    <label for="">Type</label><br/>
    <select id="sliderhome-type" class="form-control" name="type">
      <option value="">-- slider for --</option>
      <?php
      $radio_is = ($model->type == 'radio' ? 'selected="selected"' : '');
      $other_is = ($model->type == 'other' ? 'selected="selected"' : '');
      ?>
      <option <?= $radio_is ?> value="radio">Radio Member</option>
      <option <?= $other_is ?> value="other">Radio House</option>
    </select>
  </div>
  <?php
  $empty_ornot = ($model->radio_id != 0 ? '' : 'style="display:none"');
  ?>
  <div class="form-group field-sliderhome-radio_id" <?= $empty_ornot ?>>
    <label for="">Radio</label><br/>
    <select class="form-control" name="radio">
      <option value="">-- select radio --</option>
      <?php foreach($radios as $radio){ ?>
        <?php if($model->radio_id != 0){ ?>
          <?php $selected = ($model->radio_id == $radio->id ? 'selected="selected"' : '') ?>
          <option <?= $selected ?> value="<?= $radio->id ?>"><?= $radio->radio_name ?></option>
        <?php }else{ ?>
          <option value="<?= $radio->id ?>"><?= $radio->radio_name ?></option>
        <?php } ?>
      <?php } ?>
    </select>
  </div>
  <div class="form-group">
    <label for="">Date Expired</label><br/>
    <input class="form-control dateya" type="text" name="expired" readonly value="<?= substr($model->expired,0,10) ?>">
  </div>
  <div class="form-group">
    <button type="submit" name="button" class="btn btn-primary">Save</button>
  </div>
</form>

</div>
