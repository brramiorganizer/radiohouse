<?php

use yii\helpers\Html;
use app\helper\Helper;
?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="slider-home-create wrapindex">
    <?= $this->render('_form', [
        'radios' => $radios
    ]) ?>
  </div>
  </div>

</div>
