<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\SliderHome */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-home-form">

<form id="slider-home" enctype="multipart/form-data" method="post" action="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home/do-create">
  <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken?>">
  <div class="form-group">
    <label for="">File Banner <span class="required">* size must be 1600px X 800px</span></label><br/>
    <input type="file" name="file" value="" class="setpreview">
    <img src="" class="getpreview" alt="">
  </div>
  <div class="form-group">
    <label for="">Title</label><br/>
    <input class="form-control" type="text" name="title" value="">
  </div>
  <div class="form-group">
    <label for="">Description</label><br/>
    <textarea class="form-control" name="description" style="resize:none" name="description"></textarea>
  </div>
  <div class="form-group">
    <label for="">Link <span class="required">* use http:// or https://</span></label><br/>
    <input class="form-control" type="text" name="link" value="">
  </div>
  <div class="form-group">
    <label for="">Type</label><br/>
    <select id="sliderhome-type" class="form-control" name="type">
      <option value="">-- slider for --</option>
      <option value="radio">Radio Member</option>
      <option value="other">Radio House</option>
    </select>
  </div>
  <div class="form-group field-sliderhome-radio_id" style="display:none">
    <label for="">Radio</label><br/>
    <select class="form-control" name="radio">
      <option value="">-- select radio --</option>
      <?php foreach($radios as $radio){ ?>
      <option value="<?= $radio->id ?>"><?= $radio->radio_name ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group">
    <label for="">Date Expired</label><br/>
    <input class="form-control dateya" type="text" name="expired" readonly value="">
  </div>
  <div class="form-group">
    <button type="submit" name="button" class="btn btn-primary">Save</button>
  </div>
</form>

</div>
