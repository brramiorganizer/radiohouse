<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helper\Helper;

?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="slider-home-view wrapindex">

    <h1><?= Html::encode($this->title) ?></h1>

    <p style="text-align:right">
        <?= Html::a('<i class="icon icon-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="icon icon-trash"></i>', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger delete-slider-home',
            'data-id' => $model->id
        ]) ?>


        <?php $isstatus = ($model->status == 'active' ? 'inactive' : 'active');  ?>
        <?php $isbtn = ($model->status == 'active' ? 'btn-danger' : 'btn-success');  ?>
        <a href="<?= Helper::base_url() ?>dashboard/slider-home/switch?id=<?= $model->id ?>" class="btn <?= $isbtn ?>"><?= $isstatus ?></a>
    </p>
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <img src="<?= Helper::base_url() ?>media/slider-home/<?= $model->file ?>" class="img-responsive" alt="">
      </div>
      <div class="col-md-4 col-sm-4">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'title',
                  'description',
                [
                  'attribute' => 'type',
                  'value' => function($model){
                    if($model->type == 'other'){
                      return 'Advertisment or Other';
                    }else{
                      return 'Radio Member';
                    }
                  }
                ],
                [
                  'attribute' => 'radio_id',
                  'value' => function($model){
                    if($model->radio_id == 0){
                      return '-';
                    }else{
                      return $model->radio->name;
                    }
                  }
                ],
                [
                  'attribute' => 'expired',
                  'value' => function($model){
                    return Helper::convertDateTime($model->expired);
                  }
                ],
                [
                  'attribute' => 'created_on',
                  'value' => function($model){
                    return Helper::convertDateTime($model->created_on);
                  }
                ],
                [
                  'attribute' => 'updated_on',
                  'value' => function($model){
                    return Helper::convertDateTime($model->updated_on);
                  }
                ],
            ],
        ]) ?>
      </div>
    </div>


</div>
</div>
</div>
