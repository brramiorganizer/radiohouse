<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

        <table id="datatable-data">
        	<thead>
        		<tr>
        			<th>#</th>
        			<th>Page</th>
        			<th>Title</th>
        			<th>Content</th>
        			<th>action</th>
        		</tr>
        	</thead>
        	<tbody>
        		<?php foreach($datas as $data){ ?>
        		<tr>
        			<td></td>
        			<td><?= $data->page->name ?></td>
        			<td><?= $data->title ?></td>
        			<td><?= $data->content ?></td>
        			<td>
        				<a href="#" data-id="<?= $data->id ?>" data-title="<?= $data->title ?>" data-content="<?= $data->content ?>" class="edit edit-data">edit</a>
        			</td>
        		</tr>
        		<?php } ?>
        	</tbody>
        </table>

    </div>

  </div>
</div>


<script type="text/javascript">
	function all_act_data(){
		$('.edit-data').on('click',function(e){
			e.preventDefault();
			var id = $(this).data('id');
			var title = $(this).data('title');
			var content = $(this).data('content');

			$.ajax({
				url : '<?= Yii::$app->params['base_url'] ?>dashboard/data/form',
				type : 'GET',
				success : function(data){
					$('#generalmodal').modal('show');
					$('#generalmodal .modal-body').html(data);
					$('#generalmodal #form-data-content input[name=title]').val(title);
					$('#generalmodal #form-data-content textarea[name=content]').val(content);
					$('#generalmodal #form-data-content input[name=id]').val(id);
				}
			});


		});
	}
	setTimeout(function(){
		all_act_data();
	},1000);

</script>
