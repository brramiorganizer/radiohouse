<table id="event-ticket">
  <thead>
    <tr>
      <th>#</th>
      <th>Code</th>
      <th>total price</th>
      <th>Event</th>
      <th>Name</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($datum as $data){ ?>
    <tr>
      <td></td>
      <td>RH-<?= $data->code ?></td>
      <td>Rp. <?= number_format($data->total_price) ?></td>
      <td><?= $data->event->title ?></td>
      <td><?= $data->name?></td>
      <td>
        <a href="<?= Yii::$app->params['base_url'] ?>dashboard/event/view?id=<?= $data->id ?>" class="edit">detail</a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
</table>
