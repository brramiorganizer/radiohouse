<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <div id="wrap-event">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <button type="button" class="btn btn-primary hidden" id="approve">Approve</button>
            <button type="button"  class="btn btn-danger hidden" id="unapprove">Unapprove</button>

            <h4 style="margin:20px 0 10px;">Data Acara</h4>
            <table style="margin-bottom:25px" class="standarttb">
            	<tr>
            		<td>Nama Acara</td>
            		<td>:</td>
            		<td><?= $join->event->title ?></td>
            	</tr>
            	<tr>
            		<td>Url Detail Acara</td>
            		<td>:</td>
            		<td><a href="<?= Yii::$app->params['base_url'].'event/'.$join->event->slug ?>"><?= Yii::$app->params['base_url'].'event/'.$join->event->slug ?></a> </td>
            	</tr>
            </table>

            <h4 style="margin:20px 0 10px;">Data Informasi Pembeli</h4>
            <table style="margin-bottom:25px" class="standarttb">
            	<tr>
            		<td>Nama Lengkap</td>
            		<td>:</td>
            		<td><?= $join->name?></td>
            	</tr>
            	<tr>
            		<td>Email</td>
            		<td>:</td>
            		<td><?= $join->email ?></td>
            	</tr>
            	<tr>
            		<td>Nomer Telephone</td>
            		<td>:</td>
            		<td><?= $join->no_phone ?></td>
            	</tr>
            	<tr>
            		<td>Alamat</td>
            		<td>:</td>
            		<td><?= $join->address ?></td>
            	</tr>
            </table>

            <h4 style="margin:0px 0 10px;">Data Informasi Tiket</h4>
            <table style="margin-bottom:25px" class="standarttb">
            	<tr>
            		<td>Kategori Tiket</td>
            		<td>:</td>
            		<td><?= $join->ticket->category ?></td>
            	</tr>
            	<tr>
            		<td>Harga Satuan</td>
            		<td>:</td>
            		<td>Rp. <?= number_format($join->ticket->price) ?></td>
            	</tr>
            	<tr>
            		<td>Quantity</td>
            		<td>:</td>
            		<td><?= $join->quantity ?></td>
            	</tr>
            	<tr>
            		<td>Total Price</td>
            		<td>:</td>
            		<td>Rp. <?= number_format($join->total_price) ?></td>
            	</tr>
            </table>


            <h4 style="margin:0px 0 10px;">Data Informasi Konfirmasi Pembayaran</h4>
            <table style="margin-bottom:15px" class="standarttb">
            	<tr>
            		<td>Tujuan Bank</td>
            		<td>:</td>
            		<td><?= $confirmation->bank->name ?> (<?= $confirmation->bank->rekening_number ?>)</td>
            	</tr>
            	<tr>
            		<td>Nomer Rekening</td>
            		<td>:</td>
            		<td><?= $confirmation->rekening_number ?></td>
            	</tr>
            	<tr>
            		<td>Atas Nama Rekening</td>
            		<td>:</td>
            		<td><?= $confirmation->rekening_name ?></td>
            	</tr>
              <tr>
            		<td>Catatan</td>
            		<td>:</td>
            		<td><?= $confirmation->description ?></td>
            	</tr>
            	<tr>
            		<td>Total</td>
            		<td>:</td>
            		<td>Rp. <?= number_format($confirmation->total) ?></td>
            	</tr>
              <?php if($confirmation->image != ''){ ?>
                <tr>
                  <td>Bukti Transfer</td>
                  <td>:</td>
                  <td><a target="_blank" style="color:blue !important; text-decoration:underline" href="<?= Yii::$app->params['base_url'] ?>media/transfer/<?= $confirmation->image ?>">klik disini</a> </td>
                </tr>
              <?php } ?>
            </table>




          </div>
          <div class="col-md-6 col-sm-6">

          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<style media="screen">
  .standarttb{
    width: 100%
  }
  .standarttb td{
    padding: 4px
  }
</style>

<script type="text/javascript">
  setTimeout(function(){
    $('#approve , #unapprove').removeClass('hidden');

    $('#unapprove').on('click',function(){

      if(confirm('Are you sure to unapprove this ticket ?')){
        $('button').prop('disabled',true);
        $(this).html('proccessing ...');
        $.ajax({
          url : '<?= Yii::$app->params['base_url'] ?>dashboard/event/unapprove',
          type : 'POST',
          data : {
            ejc : '<?= $confirmation->id ?>',
            _csrf : $('#gils').val()
          },
          success : function(data){
            window.location = '<?= Yii::$app->params['base_url'] ?>dashboard/event';
          }
        });
      }
    });



    $('#approve').on('click',function(){

      if(confirm('Are you sure to approve this ticket ?')){
        $('button').prop('disabled',true);
        $(this).html('proccessing ...');
        $.ajax({
          url : '<?= Yii::$app->params['base_url'] ?>dashboard/event/approve',
          type : 'POST',
          data : {
            ejc : '<?= $confirmation->id ?>',
            _csrf : $('#gils').val()
          },
          success : function(data){
            window.location = '<?= Yii::$app->params['base_url'] ?>dashboard/event';
          }
        });
      }
    });

  },2000);
</script>
