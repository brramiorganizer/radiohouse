<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <div id="wrap-event">

      </div>

    </div>
  </div>
</div>


<script type="text/javascript">
function load_table(){

  $.ajax({
    url : '<?= Helper::base_url() ?>dashboard/event/data',
    success : function(data){
      $('#wrap-event').html(data);

      setTimeout(function(){
        var t = jQuery('#event-ticket').DataTable( {
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": [0,5]
            } ],
        } );

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
      },500);
    }
  });

}

setTimeout(function(){
  load_table();
},1000);




</script>
