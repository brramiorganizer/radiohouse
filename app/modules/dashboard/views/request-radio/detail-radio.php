<?php
use app\helper\Helper;
?>
<div class="loading">
  <p>please wait ...</p>
</div>
<div style="oveflow:hidden">


<div class="col-md-12">
  <div class="itemin">
    <h3>Data Pribadi</h3>
    <table id="detail-radio-request">
      <tr>
        <td>Name</td>
        <td>:</td>
        <td><?= $pribadi->name ?></td>
      </tr>

      <tr>
        <td>Email</td>
        <td>:</td>
        <td><?= $pribadi->email ?></td>
      </tr>
      <tr>
        <td>Address</td>
        <td>:</td>
        <td><?= $pribadi->address ?></td>
      </tr>
      <tr>
        <td>Telepon</td>
        <td>:</td>
        <td><?= $pribadi->telephone ?></td>
      </tr>
      <tr>
        <td>Handphone</td>
        <td>:</td>
        <td><?= $pribadi->handphone ?></td>
      </tr>

    </table>
  </div>

  <div class="itemin">
    <h3>Data Radio</h3>
    <table id="detail-radio-request">
      <tr>
        <td>Number ISR & IPP</td>
        <td>:</td>
        <td><?= $data->number_isr_ipp ?></td>
      </tr>

      <tr>
        <td>Nama Radio</td>
        <td>:</td>
        <td><?= $data->radio_name ?></td>
      </tr>

      <tr>
        <td>Company</td>
        <td>:</td>
        <td><?= $data->company_name ?></td>
      </tr>

      <tr>
        <td>Telepon</td>
        <td>:</td>
        <td><?= $data->telepon ?></td>
      </tr>

      <tr>
        <td>Address</td>
        <td>:</td>
        <td><?= $data->address ?></td>
      </tr>
      <tr>
        <td>Package Plan</td>
        <td>:</td>
        <td><?= Helper::getPlan($data->id)->name ?></td>
      </tr>
      <tr>
        <td>Price</td>
        <td>:</td>
        <td>Rp. <?= number_format($plan->plan->price) ?></td>
      </tr>

    </table>
  </div>


  <div class="itemin">
    <h3>Data Pengurus</h3>
    <table id="detail-radio-request">
      <?php foreach($pengurus as $x){?>
      <tr>
        <td><b><?= $x->position->name ?></b></td>
        <td>:</td>
        <td><?= $x->name  ?>  , tlp : <?= $x->telephone ?> , address : <?= $x->address ?> </td>
      </tr>
    <?php } ?>


    </table>
  </div>


  <div class="itemin">
    <h3>Data Pembayaran</h3>
    <table id="detail-radio-request">
      <tr>
        <td>Sent To</td>
        <td>:</td>
        <td><?= $payment->bank->name ?> - <?= $payment->bank->rekening_name ?> (<?= $payment->bank->rekening_number ?>)</td>
      </tr>
      <tr>
        <td>Rekening Name</td>
        <td>:</td>
        <td><?= $payment->rekening_name ?></td>
      </tr>

      <tr>
        <td>Rekening Number</td>
        <td>:</td>
        <td><?= $payment->rekening_number ?></td>
      </tr>

      <tr>
        <td>Description</td>
        <td>:</td>
        <td><?= $payment->description ?></td>
      </tr>


    </table>
  </div>

  <!-- <p style="margin-top:20px">
    <a href="#" class="btn btn-success approve" data-id="<?= $data->id ?>"> Approve</a>
    <a href="#" class="btn btn-danger disapprove" data-id="<?= $data->id ?>"> Disapprove</a>
  </p> -->
</div>
</div>

<style media="screen">
  #detail-radio-request td{
    padding: 8px
  }
  #detail-radio-request{
    width: 100%
  }
  .loading{
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: rgba(0,0,0,0.6);
    z-index: 100;
    display: none
  }
  .loading p{
    position: absolute;
    left: 0;
    top: 45%;
    font-size: 16px;
    text-align: center;
    width: 100%;
    color: #fff;
    letter-spacing: 3px;
  }
  .itemin{
    border:1px solid #ccc;
    margin-bottom: 15px
  }
  .itemin h3{
    font-size: 17px;
    background: #ccc;
    color:#333;
    text-transform: uppercase;
    letter-spacing: 1px;
    margin-top:0;
    padding: 5px 10px
  }
  .modal{
    overflow-y: scroll;
  }
</style>

<script type="text/javascript">
  // $('.approve').on('click',function(){
  //   var id = $(this).data('id');
  //   if(confirm('Apakah anda yakin untuk menyetujuinya ?')){
  //     $('.loading').show();
  //     $.ajax({
  //       url : '<?= Yii::$app->params['base_url'] ?>dashboard/request-radio/approve-disapprove',
  //       type : 'POST',
  //       data : {
  //         id : id,
  //         type : 'approve',
  //         _csrf : $('#gils').val()
  //       },
  //       success : function(data){
  //         location.reload();
  //       }
  //     });
  //   }
  // });
  //
  // $('.disapprove').on('click',function(){
  //   var id = $(this).data('id');
  //   if(confirm('Apakah anda yakin untuk tidak menyetujuinya ?')){
  //     $('.loading').show();
  //     $.ajax({
  //       url : '<?= Yii::$app->params['base_url'] ?>dashboard/request-radio/approve-disapprove',
  //       type : 'POST',
  //       data : {
  //         id : id,
  //         type : 'disapprove',
  //         _csrf : $('#gils').val()
  //       },
  //       success : function(data){
  //         location.reload();
  //       }
  //     });
  //   }
  // });
</script>
