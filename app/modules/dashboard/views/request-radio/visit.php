<?php
use app\helper\Helper;
?>
<table id="visitor-tables">
  <thead>
    <tr>
      <th>#</th>
      <th>Date</th>
      <th>IP</th>
      <th>Device</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($model as $m){ ?>
    <tr>
      <td></td>
      <td><?= Helper::convertDateTime($m->created_on); ?></td>
      <td><?= $m->ip ?></td>
      <td><?= $m->device ?></td>
    </tr>
  <?php } ?>
  </tbody>
</table>


<script type="text/javascript">
var qa = $('#visitor-tables').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0]
    } ]
} );

qa.on( 'order.dt search.dt', function () {
    qa.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();
</script>
