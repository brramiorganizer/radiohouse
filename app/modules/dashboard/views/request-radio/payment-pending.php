<div class="loading">
  <p>please wait ...</p>
</div>
<div style="padding:15px">
  <table id="table-payment-pending">
    <thead>
      <tr>
        <th>#</th>
        <th>Radio</th>
        <th>Rekening Name</th>
        <th>Rekening Number</th>
        <th>Description</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

        <?php foreach($datum as $data){ ?>
          <tr>
        <td></td>
        <td><?= $data->radio->radio_name ?></td>
        <td><?= $data->rekening_name ?></td>
        <td><?= $data->rekening_number ?></td>
        <td><?= strip_tags($data->description) ?></td>
        <td class="action">
          <?php if($data->image != ''){ ?>
            <p><a href="<?= Yii::$app->params['base_url'] ?>media/transfer/<?= $data->image ?>" target="_blank">evidence of transfer</a></p>
          <?php } ?>
          <p><a href="#" data-id="<?= $data->id ?>" class="approve">approve</a></p>
          <p><a href="#" data-id="<?= $data->id ?>" class="disapprove">disapprove</a></p>
          <p><a href="#" data-id="<?= $data->radio_id; ?>" class="detailradio">detail radio</a></p>

        </td>
        </tr>
      <?php } ?>

    </tbody>
  </table>
</div>

<style media="screen">
.loading{
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  background: rgba(0,0,0,0.6);
  z-index: 100;
  display: none
}
.loading p{
  position: absolute;
  left: 0;
  top: 45%;
  font-size: 16px;
  text-align: center;
  width: 100%;
  color: #fff;
  letter-spacing: 3px;
}
.action a{
  border-bottom: 1px dotted;
}
  .approve{
    color:green;
  }
  .disapprove{
    color:red
  }
  .detailradio{
    color:blue
  }
</style>

<script type="text/javascript">
var tabs = jQuery('#table-payment-pending').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,4]
    } ],
    //"order": [[ 1, 'asc' ]]
} );

tabs.on( 'order.dt search.dt', function () {
    tabs.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();
jQuery('.modal-dialog').addClass('modal-lg');

$('.detailradio').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  general_modal('Detail Radio',base + 'request-radio/getone?id='+id);
});

$('.approve').on('click',function(){
  var id = $(this).data('id');
  if(confirm('Apakah anda yakin untuk menyetujuinya ?')){
    $('.loading').show();
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/request-radio/approve-disapprove',
      type : 'POST',
      data : {
        id : id,
        type : 'approve',
        _csrf : $('#gils').val()
      },
      success : function(data){
        location.reload();
      }
    });
  }
});

$('.disapprove').on('click',function(){
  var id = $(this).data('id');
  if(confirm('Apakah anda yakin untuk tidak menyetujuinya ?')){
    $('.loading').show();
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/request-radio/approve-disapprove',
      type : 'POST',
      data : {
        id : id,
        type : 'disapprove',
        _csrf : $('#gils').val()
      },
      success : function(data){
        location.reload();
      }
    });
  }
});
</script>
