<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <p style="text-align:right"><a class="btn btn-primary" id="payment-pending" href="#">Confirm Payment (<?= $count ?>)</a></p>


      <table id="datatable-request-radio">
        <thead>
          <tr>
            <th>No</th>
            <th>Radio Name</th>
            <th>Company</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($datas as $data){ ?>
            <tr>
              <td></td>
              <td><?= $data->radio_name ?></td>
              <td><?= $data->company_name ?></td>
              <td>
                <a href="#" class="lihat detailradio" data-id="<?= $data->id ?>">lihat</a>
                <a href="#" class="hapus hapusradioreq" data-id="<?= $data->id ?>">hapus</a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>


    </div>

  </div>
</div>
