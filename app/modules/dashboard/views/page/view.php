<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\Page */

// $this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
<div class="page-view wrapindex">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
      <div class="col-md-8 col-sm-8 col-lg-8">
        <?php if(!in_array($model->id,$no_banner)){ ?>
        <div class="text-center">
          <?php $image = ($model->banner != '' ? Yii::$app->params['base_url'].'media/banner-page/'.$model->banner : ''); ?>
          <?php if($image != ''){ ?>
            <img src="<?= $image ?>" class="img-responsive">
          <?php }else{ ?>
            <div class="noimage">
                <p>No Banner</p>
            </div>
          <?php } ?>
        </div>
        <?php } ?>

        <?php if(!in_array($model->id,$no_description)){ ?>
        <div class="description">
          <?php $description = ($model->description != '' ? $model->description : '<p><i>no description</i> '.Html::a('update here', ['update', 'id' => $model->id], ['class' => 'updatehere'])) ?>
          <?php echo $description ?>
        </div>
        <?php } ?>
        <div class="metas">
          <?= DetailView::widget([
              'model' => $model,
              'attributes' => [

                  'meta_title',
                  'meta_description',
                  'meta_keywords'
              ],
          ]) ?>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 col-lg-4">
        <?php echo Html::a('update', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-block mg-10']) ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'created_on',
                'updated_on',
            ],
        ]) ?>
      </div>
    </div>


</div>
</div>
</div>

<style media="screen">
  .description{
    margin-top:20px
  }
  .mg-10{
    margin-bottom: 20px
  }
  .metas{
    margin-top: 50px
  }
  .noimage{
    border: 1px dashed gray;
    background: #eee;
    padding: 50px 0;
  }
  .noimage p{
    margin-bottom: 0;
text-transform: uppercase;
letter-spacing: 1px;
font-size: 17px;
font-weight: 100;
  }
  .updatehere{
    color:blue;
    text-decoration: underline;
  }
</style>
