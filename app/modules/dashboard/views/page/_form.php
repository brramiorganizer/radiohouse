<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if(!in_array($model->id, $no_banner)){ ?>
         <?= $form->field($model, 'banner')->fileInput(['class'=>'fileinform setpreview']) ?>
         <label class="error">* image size 1920px X 378px</label>
         <?php $image = ($model->banner == ''  ? '' : Yii::$app->params['base_url'].'media/banner-page/'.$model->banner); ?>
         <img src="<?= $image ?>" class="getpreview" alt="" style="margin-bottom:15px">
     <?php } ?>
     <?php if(!in_array($model->id, $no_description)){ ?>
    <?= $form->field($model, 'description')->textarea(['class' => 'editortextarea']) ?>
    <?php } ?>
    <?php if(!in_array($model->id, $no_title)){ ?>
    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
    <?php } ?>
    <?php if(!in_array($model->id, $no_meta_description)){ ?>
    <?= $form->field($model, 'meta_description')->textarea() ?>
  <?php } ?>
  <?php if(!in_array($model->id, $no_meta_keyword)){ ?>
    <?= $form->field($model, 'meta_keywords')->textInput(['class'=>'taginput']) ?>
  <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>



    <?php if($model->id == 6){ ?>

    <form style="margin-top:50px; display:none" id="contact-form">
      <h4 style="margin-bottom:15px">Data Content Page</h4>
      <div class="form-group">
        <label for="">Alamat</label>
        <input type="text" name="alamat" class="form-control" value="<?= Helper::getDataContent(1) ?>">
      </div>
      <div class="form-group">
        <label for="">Phone 1</label>
        <input type="text" name="phone1" class="form-control" value="<?= Helper::getDataContent(2) ?>">
      </div>
      <div class="form-group">
        <label for="">Phone 2</label>
        <input type="text" name="phone2" class="form-control" value="<?= Helper::getDataContent(3) ?>">
      </div>
      <div class="form-group">
        <label for="">Email</label>
        <input type="text" name="email" class="form-control" value="<?= Helper::getDataContent(4) ?>">
      </div>
      <div class="form-group">
        <label for="">Link Maps</label>
        <input type="text" name="maps" class="form-control" value="<?= Helper::getDataContent(22) ?>">
      </div>
      <div class="form-group">
      <button type="submit" name="button" class="btn btn-primary">Save</button>
      </div>
    </form>
    <script type="text/javascript">
    setTimeout(function(){

      $('#contact-form').fadeIn();
      $('#contact-form').validate({
        rules : {
          alamat : {
            required : true
          },
          phone1 : {
            required : true
          },
          phone2 : {
            required : true
          },
          email : {
            required : true
          },
          maps : {
            required : true
          }
        },
        messages : {
          alamat : {
            required : 'please fill the address'
          },
          phone1 : {
            required : 'please fill the phone 1'
          },
          phone2 : {
            required : 'please fill the phone 2'
          },
          email : {
            required : 'please fill the email'
          },
          maps : {
            required : 'please fill the maps'
          }
        },
        submitHandler : function(form){
          $('#contact-form button').html('Saving ...').prop('disabled',true);
          var dataform = $(form).serialize() + '&_csrf='+ $('#gils').val();
          $.ajax({
            url : '<?= Yii::$app->params['base_url'] ?>dashboard/page/content-contact',
            type : 'POST',
            data : dataform,
            dataType : 'JSON',
            success : function(data){
              $('#contact-form button').html('Save').prop('disabled',false);
              $('#contact-form').prepend('<div class="alert alert-success">Data has been updated</div>');
              setTimeout(function(){
                $('#contact-form .alert').remove();
              },3000);
            }
          });
          return false;
        }
      });
      },1000);
    </script>
  <?php } ?>



</div>
