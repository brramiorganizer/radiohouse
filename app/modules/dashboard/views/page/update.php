<?php

use yii\helpers\Html;

use app\helper\Helper;
/* @var $this yii\web\View */
/* @var $model app\modules\dashboard\models\Page */

//$this->title = 'Update Page: ' . $model->name;
?>
<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
<div class="page-update wrapindex">

  <?php if(Yii::$app->session->hasFlash('yes')){ ?>
    <div class="alert alert-success">
      Data has been updated
    </div>
  <?php } ?>

    <?= $this->render('_form', [
        'model' => $model,
        'no_banner' => $no_banner,
        'no_description' => $no_description,

        'no_title' => $no_title,
        'no_meta_description' => $no_meta_description,
        'no_meta_keyword' => $no_meta_keyword
    ]) ?>

</div>
</div>
</div>
