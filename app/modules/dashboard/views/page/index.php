<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">

        <table id="datatable-page">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Page</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($dataProvider as $data){ ?>
            <tr>
              <td></td>
              <td><?= $data->page->name ?></td>
              <td>
                <a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=<?= $data->id ?>" class="edit">edit</a>
                <a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/view?id=<?= $data->id ?>" class="lihat">lihat</a>
              </td>
            </tr>
          <?php } ?>
          </tbody>
        </table>

    </div>

  </div>
</div>
