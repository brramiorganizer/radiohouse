<?php foreach($data as $a){ ?>
<tr>
  <td></td>
  <td><?= $a->name ?></td>
  <td>
    <a href="#" data-value="<?= $a->name ?>" data-id="<?= $a->id ?>" class="edit">edit</a>
    <a href="#" data-id="<?= $a->id ?>" class="hapus">delete</a>
  </td>
</tr>
<?php } ?>


<script type="text/javascript">
  $('.edit').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var valuenya = $(this).data('value');
    $('#idnya').val(id);
    $('#namenya').val(valuenya);
  });
  $('.hapus').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    if(confirm('Are you sure to delete this feature ?')){
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/delete-feature',
        type : 'POST',
        data :  { id : id , _csrf : $('#gils').val()},
        success : function(){
          location.reload();
        }
      });
    }
  });
</script>
