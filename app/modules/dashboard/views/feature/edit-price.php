<form id="edit-price-plan">
  <div class="form-group">
    <input type="hidden" name="id" value="<?= $model->id ?>">
    <label for="">Price</label>
    <input type="text" class="form-control" name="price" value="<?= $model->price ?>">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="button">save</button>
  </div>
</form>

<script type="text/javascript">
  $('#edit-price-plan').validate({
    rules: {
      price: {
        required: true,
        digits: true
      }
    },
    messages : {
      price: {
        required: 'please fill the price',
        digits: 'please fill the valid price'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf='+$('#gils').val();
      $('#edit-price-plan button').prop('disabled',true);
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/update-price-plan',
        type : 'POST',
        data : dataform,
        success : function(data){
          location.reload();
        }
      });
      return false;
    }
  })



</script>
