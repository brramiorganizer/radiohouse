<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">


      <!-- <p style="text-align:right">  <?php // Html::a('Create New', ['create',], ['class' => 'btn btn-primary']) ?></p> -->
      <div class="col-md-5 col-sm-5">
        <form id="managefeature" style="margin-top:20px"  method="post">
          <div class="form-group">
            <label for="">Name Feature</label><br/>
            <input type="hidden" id="idnya" name="id" value="">
            <input type="text" id="namenya" class="form-control" name="name" value="">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name="button">Save</button>
          </div>
        </form>
      </div>
      <div class="col-md-7 col-sm-7">
        <table id="datatable-feature">
          <thead>
            <tr>
              <th>No</th>
              <th>Feature</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>


      <div class="col-md-12 plans">
        <div class="row">
          <?php foreach($plans as $plan){ ?>
          <div class="col-md-4">
            <table>
              <thead>
                <tr>
                  <th colspan="3"><?= $plan->name ?> (Rp. <?= number_format($plan->price) ?>) <a href="#" class="edit-price" data-id="<?= $plan->id ?>">edit price</a> </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>No</td>
                  <td>Feature</td>
                  <td>Check List</td>
                </tr>

                <?php
                $no =1;
                foreach($dataProvider as $x){ ?>
                <tr>
                  <td><?= $no ?></td>
                  <td><?= $x->name ?></td>
                  <?php $checked = (Helper::checkPlan($plan->id,$x->id) ? 'checked="checked"' : ''); ?>
                  <td><input <?= $checked ?> type="checkbox"  data-parent="<?= $plan->id ?>" data-child="<?= $x->id ?>" class="pick_plan" value="ya"></td>
                </tr>
                <?php $no++;} ?>
              </tbody>
            </table>
          </div>
          <?php } ?>
        </div>
      </div>



    </div>

  </div>
</div>
<style media="screen">
  .plans{
    margin-top: 50px;
    display: none
  }
  .plans table{
    width: 100%
  }
  .plans thead th{
    background: #333;
    color: white;
    padding: 5px 10px;
    border:1px solid #333;
  }
  .plans tbody tr:first-child td , .plans tbody tr td:first-child , .plans tbody tr td:last-child{
    text-align: center;
  }
  .plans tbody tr td{
    padding: 4px;
    border:1px solid #333;
  }
  .plans tbody tr td input{
    cursor: pointer;
  }
  .edit-price{
    float:right;
    color:#bbb;
    border-bottom: 1px dotted #bbb
  }
</style>
<script type="text/javascript">
function refresh_table(){
  jQuery('#datatable-feature').DataTable().destroy();
  $.ajax({
    url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/data',
    type : 'GET',
    success : function(data){
      $('#datatable-feature tbody').html(data);

      setTimeout(function(){
        var t = jQuery('#datatable-feature').DataTable( {
          "columnDefs": [ {
              "searchable": false,
              "orderable": false,
              "targets": [0,2]
          } ],
          "order": [[ 1, 'asc' ]]
      } );

      t.on( 'order.dt search.dt', function () {
          t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
          } );
      } ).draw();
      },500);
    }
  });
}
setTimeout(function(){

refresh_table();


$('#managefeature').validate({
  rules : {
    name : {
      required : true
    },
  },
  messages : {
    name : {
      required : 'please fill name of feature'
    },
  },
  submitHandler : function(form){
    var data = $(form).serialize();

    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/proses',
      type : 'POST',
      dataType : 'JSON',
      data : data + '&_csrf='+$('#gils').val(),
      success : function(data){
        location.reload();
      }
    });
    return false;
  }
});
  $('.pick_plan').on('click',function(){
    var parent = $(this).data('parent');
    var child = $(this).data('child');

    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/choose-plan',
      type : 'POST',
      data : {
        parent : parent,
        child : child,
        _csrf : $('#gils').val()
      }
    });
  });
  $('.plans').fadeIn('fast');

  $('.edit-price').on('click',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $('#generalmodal').modal('show');
    $('#generalmodal .modal-title').html('Edit Price');
    $.ajax({
      url : '<?= Yii::$app->params['base_url'] ?>dashboard/feature/edit-price-plan',
      type : 'POST',
      data : {
        id : id,
        _csrf : $('#gils').val()
      },
      success : function(data){
        $('#generalmodal .modal-body').html(data);
      }
    });
  });

},2000);







</script>
