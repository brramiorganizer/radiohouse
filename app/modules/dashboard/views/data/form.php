<form id="form-data-content">
	<div class="form-group">
		<label>Title</label>
		<input type="text" name="title" class="form-control">
	</div>
	<div class="form-group">
		<label>Content</label>

		<textarea name="content" class="form-control"></textarea>
	</div>
	<div class="form-group">
		<input type="hidden" name="id">
		<button type="submit" class="btn btn-primary">save</button>
	</div>

</form>

<script type="text/javascript">
	$('#form-data-content').validate({
		rules : {
			title : {
				required : true
			},
			content : {
				required : true
			}
		},
		messages : {
			title : {
				required : 'please fill the title'
			},
			content : {
				required : 'please fill the content'
			}
		},
		submitHandler : function(form){

			$.ajax({
				url : '<?= Yii::$app->params['base_url'] ?>dashboard/data/update',
				type : 'POST',
				data : $(form).serialize() + '&_csrf='+$('#gils').val(),
				dataType : 'JSON',
				success : function(data){
					if(data.status){
						location.reload();
					}
				}
			});
			return false;
		}
	})

</script>
