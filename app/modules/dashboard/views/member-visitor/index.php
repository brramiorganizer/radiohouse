<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">


      <!-- <p style="text-align:right">  <?php // Html::a('Create New', ['create',], ['class' => 'btn btn-primary']) ?></p> -->
      <table id="datatable-member-visitor">
        <thead>
          <tr>
            <th>No</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Type</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($dataProvider as $data){ ?>
          <tr>
            <td></td>
            <td><?= $data->firstname ?></td>
            <td><?= $data->lastname ?></td>
            <td><?= $data->email ?></td>
            <td>
              <?php if($data->type == 'web'){ ?>
                <i class="icon icon-globe"></i>
              <?php }elseif($data->type == 'facebook'){ ?>
                <i class="icon icon-facebook"></i>
              <?php }else{ ?>
                <i class="icon icon-google-plus"></i>
              <?php } ?>
            </td>
            <td>
              <?php if($data->is_active == 0){ ?>
                <span class="active-user"></span>
              <?php }else{ ?>
                <span class="inactive-user"></span>
              <?php } ?>
            </td>
            <td>
              <a href="<?= Yii::$app->params['base_url'] ?>dashboard/member-visitor/update?id=<?= $data->id ?>" class="edit">edit</a>
              <a href="#" data-id="<?= $data->id ?>" class="lihat lihat-visitor">lihat</a>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>


    </div>

  </div>
</div>
<style media="screen">
.active-user , .inactive-user{
  display: inline-block;
  width: 15px;
  height: 15px;
  border-radius: 20px
}
.active-user{
  background: green
}
.inactive-user{
  background: red
}
</style>
