<?php
  use app\helper\Helper;
?>
<div style="overflow:hidden">
  <div class="col-md-12 col-sm-12">
    <table class="ket-detail-visitor" style="margin-left:-19px">
      <tr>
        <td>First Name</td>
        <td>:</td>
        <td><?= $model->firstname ?></td>
      </tr>
      <tr>
        <td>Last Name</td>
        <td>:</td>
        <td><?= $model->lastname ?></td>
      </tr>
      <tr>
        <td>Email</td>
        <td>:</td>
        <td><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a> </td>
      </tr>
      <tr>
        <td>Register On</td>
        <td>:</td>
        <td>
          <?php if($model->type == 'web'){ ?>
            <i class="icon icon-globe"></i> Website
          <?php }elseif($model->type == 'facebook'){ ?>
            <i class="icon icon-facebook"></i> Facebook
          <?php }else{?>
            <i class="icon icon-google-plus"></i> Google
          <?php } ?>
        </td>
      </tr>
      <tr>
        <td>Status</td>
        <td>:</td>
        <td>
          <?php if($model->is_active == '0'){ ?>
            Active
          <?php }else{ ?>
            Inactive
          <?php } ?>
        </td>
      </tr>
      <tr>
        <td>Register Date</td>
        <td>:</td>
        <td><?php echo Helper::convertDate($model->created_on); ?></td>
      </tr>
      <tr>
        <td>Last Update</td>
        <td>:</td>
        <td><?php echo Helper::convertDate($model->updated_on); ?></td>
      </tr>
    </table>
  </div>

  <div class="col-md-12 ketlogged">
    <h4 style="margin-left:-15px">History Login</h4>
    <table id="history-login">
      <thead>
        <tr>
          <th>#</th>
          <th>IP</th>
          <th>Device</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($loggeds as $logged){ ?>
        <tr>
          <td></td>
          <td><?= $logged->ip ?></td>
          <td><?= $logged->device ?></td>
          <td><?= Helper::convertDateTime($logged->created_on) ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<style media="screen">

  .ket-detail-visitor td{
    padding: 4px 8px
  }
  .noimageprofile{
    width: 130px;
    border-radius: 100px;
    margin-right: 20px
  }
  .ketlogged{
    border-top: 1px dotted gray;
margin-top: 40px;
padding-top: 20px;
  }
</style>

<script type="text/javascript">
var xa = jQuery('#history-login').DataTable( {
    "columnDefs": [ {
        "searchable": false,
        "orderable": false,
        "targets": [0,1,2,3]
    } ]
} );

xa.on( 'order.dt search.dt', function () {
    xa.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
} ).draw();
</script>
