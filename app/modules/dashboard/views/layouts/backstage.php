
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/favicon.png" rel="shortcut icon" type="image/png">
     <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon.png" rel="apple-touch-icon">
     <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
     <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
     <link href="<?= Yii::$app->params['base_url'] ?>vendor/frontstage/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/fullcalendar.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/matrix-style.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/matrix-media.css" />
    <link href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/jquery.gritter.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/datatables/datatables.min.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/overwrite.css" />
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/taginput.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= Yii::$app->params['base_url'] ?>vendor/backstage/css/custom.css" />
  </head>
  <body>
  <?= $this->render('backstage-header') ?>
<?= $this->render('backstage-sidebar') ?>
<?= $content ?>

<?= $this->render('backstage-footer') ?>
