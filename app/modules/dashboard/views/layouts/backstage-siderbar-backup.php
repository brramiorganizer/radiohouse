<?php
use app\helper\Helper;
?>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="submenu">
      <a href="#"><i class="icon icon-inbox"></i> <span>Beranda</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home">SEO</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/main-radio">Logo</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-under-slider">Slider</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-before-footer">Radio Before Footer (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/news-event-home">News Event (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-ten">10 Radio (menu)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/banner-news-event-channel">Banner News Event & Channel</a></li>
      </ul>
    </li>
    <li> <a href="<?= Yii::$app->params['base_url'] ?>dashboard/page"><i class="icon icon-file"></i> <span>Page</span></a> </li>
    <li class="submenu">
      <a href="#"><i class="icon icon-inbox"></i> <span>Advertisement</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home">Slider (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/main-radio">Main Radio Player (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-under-slider">Radio Under Slider (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-before-footer">Radio Before Footer (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/news-event-home">News Event (home)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-ten">10 Radio (menu)</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/banner-news-event-channel">Banner News Event & Channel</a></li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#"><i class="icon icon-file"></i> <span>News & Events</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/data-news-events">Data</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/news">News & Events</a></li>

      </ul>
    </li>
    <li class="submenu">
      <a href="#"><i class="icon icon-user"></i> <span>User</span></a>
      <ul>
        <!-- <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/admin">Admin</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio">Member Radio</a></li> -->
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/member-visitor">Member Visitor</a></li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#"><i class="icon icon-play"></i> <span>Radio</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio">Data</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/request-radio">Request</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/feature">Features</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-category">Category</a></li>
      </ul>
    </li>
    <li>
      <a href="<?= Yii::$app->params['base_url'] ?>dashboard/data"><i class="icon icon-file"></i> <span>Data</span></a>
      <ul>
        <!-- <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/request-radio">Province</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/feature">Features</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-category">Category</a></li> -->
      </ul>
    </li>
    <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/event"><i class="icon icon-file"></i> <span>Event Ticket (<?= Helper::buyingTicketCount() ?>)</span></a></li>
    <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/contact"><i class="icon icon-file"></i> <span>Contact</span></a></li>
    <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/faq"><i class="icon icon-file"></i> <span>Faq</span></a></li>
    <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/bank"><i class="icon icon-file"></i> <span>Bank</span></a></li>
    <!-- <li><a href="tables.html"><i class="icon icon-th"></i> <span>Tables</span></a></li>
    <li><a href="grid.html"><i class="icon icon-fullscreen"></i> <span>Full width</span></a></li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <span class="label label-important">3</span></a>
      <ul>
        <li><a href="form-common.html">Basic Form</a></li>
        <li><a href="form-validation.html">Form with Validation</a></li>
        <li><a href="form-wizard.html">Form with Wizard</a></li>
      </ul>
    </li>
    <li><a href="buttons.html"><i class="icon icon-tint"></i> <span>Buttons &amp; icons</span></a></li>
    <li><a href="interface.html"><i class="icon icon-pencil"></i> <span>Eelements</span></a></li>
    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Addons</span> <span class="label label-important">5</span></a>
      <ul>
        <li><a href="index2.html">Dashboard2</a></li>
        <li><a href="gallery.html">Gallery</a></li>
        <li><a href="calendar.html">Calendar</a></li>
        <li><a href="invoice.html">Invoice</a></li>
        <li><a href="chat.html">Chat option</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Error</span> <span class="label label-important">4</span></a>
      <ul>
        <li><a href="error403.html">Error 403</a></li>
        <li><a href="error404.html">Error 404</a></li>
        <li><a href="error405.html">Error 405</a></li>
        <li><a href="error500.html">Error 500</a></li>
      </ul>
    </li>
    <li class="content"> <span>Monthly Bandwidth Transfer</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: 77%;" class="bar"></div>
      </div>
      <span class="percent">77%</span>
      <div class="stat">21419.94 / 14000 MB</div>
    </li>
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li> -->
  </ul>
</div>
