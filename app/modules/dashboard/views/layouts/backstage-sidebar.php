<?php
use app\helper\Helper;
?>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="submenu">
      <a href="#"><span>Beranda</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=1">SEO</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/logo">Logo</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/slider-home">Slider</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/main-radio">Main Radio Player</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-under-slider">Radio Under Slider Home</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/news-event-home">Berita & Event</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-before-footer">Radio Before Footer</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio-ten">List Radio Menu</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/banner-news-event-channel">Banner Berita Event & Channel</a></li>
      </ul>
    </li>

    <li class="submenu">
      <a href="#"><span>Tentang Kami</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=3">Content & SEO</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/data-tentang">Data</a></li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#"><span>Menjadi Member</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=7">Content & SEO</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/feature">Package Member</a></li>
      </ul>
    </li>
    <li class="submenu">
      <a href="#"><span>Radio Member</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=4">Content Channel</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/request-radio">Request Become Member</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/radio">Data Radio Member</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/content-email">Content Email Notification</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/message-tutorial">Message Download File Tutorial</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/tutorial">File Tutorial</a></li>
      </ul>
    </li>

    <li class="submenu">
      <a href="#"><span>News & Event</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=5">Content</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=12">Banner Page Detail</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/news">News & Event</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/data-news-events">Data News & Event</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/event">Data Purchase Ticket</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/content-email-ticket">Content Email Notification</a></li>

      </ul>
    </li>

    <li class="submenu">
      <a href="#"><span>Contact Us</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=6">Content</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/contact">Data Contact</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/content-email-contact">Content Email Notification</a></li>
      </ul>
    </li>

    <li> <a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=8"><span>Terms and Condition</span></a> </li>

    <li class="submenu">
      <a href="#"><span>FAQ</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=9">Content & SEO</a></li>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/faq">Data</a></li>
      </ul>
    </li>
    <li> <a href="<?= Yii::$app->params['base_url'] ?>dashboard/page/update?id=10"><span>Privacy</span></a> </li>
    <li class="submenu">
      <a href="#"><span>Others</span></a>
      <ul>
        <li><a href="<?= Yii::$app->params['base_url'] ?>dashboard/bank">Data bank</a></li>

      </ul>
    </li>
</div>
