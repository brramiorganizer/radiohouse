<div class="row-fluid">
  <div id="footer" class="span12"> <?= date('Y') ?> &copy; Lumbung School</div>
</div>


<div id="generalmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body" style="overflow:hidden">
        <p>loading ...</p>
      </div>
    </div>

  </div>
</div>

<!--end-Footer-part-->


  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/excanvas.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.min.js"></script>
  <!-- <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.ui.custom.js"></script> -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/bootstrap.min.js"></script>
  <!-- <script src="<?php //Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.flot.min.js"></script>
  <script src="<?php // Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.flot.resize.min.js"></script> -->
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.peity.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/fullcalendar.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.dashboard.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.gritter.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.interface.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.chat.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.validate.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.form_validation.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.wizard.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/jquery.uniform.js"></script>

  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.popover.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/datatables/datatables.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/matrix.tables.js"></script>
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/taginput.js"></script>
  <script src="<?= Yii::$app->params['base_url'] ?>vendor/backstage/js/main.js"></script>
  <script type="text/javascript">

      $( ".dateya" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'yy-mm-dd',
      minDate : 1
    });
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
              document.location.href = newURL;
            }
        }
    }

  // resets the menu selection upon entry to this page:
  function resetMenu() {
     document.gomenu.selector.selectedIndex = 2;
  }
  </script>
  </body>
</html>
