<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <p style="text-align:right"><a href="#" id="add-new" class="btn btn-primary hidden">add new</a></p>
      <ul id="sortable-faq">
        <?php foreach($datum as $datax){ ?>
        <li id="set_<?= $datax->id ?>" class="ui-state-default">
          <h4 class="text-center" style="margin:0"><?= $datax->question ?></h4>
          <p class="text-center"><?= $datax->answer ?></p>
          <div class="action text-center">
            <a href="#" class="hapus delete-faq hidden" data-id="<?= $datax->id ?>" style="color: #e74c3c !important;">hapus</a>
            <a href="#" class="edit edit-faq hidden" data-answer="<?= $datax->answer  ?>" data-question="<?= $datax->question  ?>" data-id="<?= $datax->id ?>" style="color: #35A5D8 !important;">edit</a>
          </div>
        </li>
        <?php } ?>
      </ul>

    </div>

  </div>
</div>
<script type="text/javascript">
  setTimeout(function(){
    $('.edit , .hapus , #add-new').removeClass('hidden');
    $('.delete-faq').on('click',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      if(confirm('Are you sure to delete this data ?')){
        $.ajax({
          type : 'post',
          url : base + 'faq/delete',
          data : {id : id, _csrf : $('#gils').val()},
          success : function(data){
            location.reload();
          }
        });
      }
    });


    $('#add-new').on('click',function(e){
      e.preventDefault();
      $('#generalmodal').modal('show');
      $('#generalmodal .modal-title').html('New Data');
      $.ajax({
        url : base + 'faq/form',
        success : function(data){
          $('#generalmodal .modal-body').html(data);
        }
      });
    });


    $('.edit-faq').on('click',function(e){
      e.preventDefault();
      var id = $(this).data('id');
      var question = $(this).data('question');
      var answer = $(this).data('answer');
      $('#generalmodal').modal('show');
      $('#generalmodal .modal-title').html('New Data');
      $.ajax({
        url : base + 'faq/form',
        success : function(data){
          $('#generalmodal .modal-body').html(data);
          $('#generalmodal input[name=id]').val(id);
          $('#generalmodal textarea[name=question]').val(question);
          $('#generalmodal textarea[name=answer]').val(answer);
        }
      });
    });


  },2000);
</script>
<style media="screen">
#sortable-faq li{
  margin-bottom: 5px;
  list-style: none;
  padding-top: 10px;
  padding-bottom: 10px;
  cursor: cursor
}
#sortable-faq h4{
  font-weight: 700;
  font-size: 16px;
  margin-bottom: 5px !important
}
#sortable-faq p{
  margin-bottom: 0
}
</style>
