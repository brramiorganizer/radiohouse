<form id="form-faq">
  <?php $is = (isset($id) ? $id : ''); ?>
  <input type="hidden" name="id" value="<?= $is ?>">
  <div class="form-group">
    <label for="">Question</label>
    <textarea name="question" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <label for="">Answer</label>
    <textarea name="answer" class="form-control"></textarea>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary" name="button">save</button>
  </div>
</form>

<script type="text/javascript">
  $('#form-faq').validate({
    rules : {
      question : {
        required : true
      },
      answer : {
        required : true
      }
    },
    messages : {
      question : {
        required : 'please fill the question'
      },
      answer : {
        required : 'please fill the answer'
      }
    },
    submitHandler : function(form){
      var dataform = $(form).serialize() + '&_csrf=' + $('#gils').val();
      $('#form-faq button').prop('disabled',true);
      $.ajax({
        url : '<?= Yii::$app->params['base_url'] ?>dashboard/faq/insert',
        data : dataform,
        type : 'post',
        success : function(data){
          location.reload();
        }
      });
    }
  });
</script>
