<?php


use app\helper\Helper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
  <?= Helper::breadcrumb($breadcrumb); ?>
  <div class="container-fluid">
    <div class="page-index wrapindex">
      <div class="row">
        <div class="col-md-4 col-sm-4"></div>
        <div class="col-md-4 col-sm-4">
          <?php if(Yii::$app->session->hasFlash('tuto')){ ?>
            <div class="alert alert-success">
              file has been changed
            </div>
          <?php } ?>
          <form id="edittutorial">

            <input  type="file" name="image" value="">
            <a href="#" data-file="<?= $model ?>" class="download-tutorial">current file tutorial</a>
            <div class="form-group" style="margin-top:10px">
              <button type="submit" class="btn btn-primary btn-block">save</button>
            </div>
          </form>
        </div>
        <div class="col-md-4 col-sm-4"></div>
      </div>


    </div>

  </div>
</div>
<style media="screen">
  .download-tutorial{
    color:blue;
    display: inline-block;
    margin: 15px 0 10px;
    width: 100%;
    border:1px dashed blue;
    text-align: center;
  }
  .download-tutorial:hover{
    background: blue;
    color:#fff
  }
</style>
