<?php

namespace app\modules\dashboard\controllers;
use app\models\AdvertismentNeChannel;
use Yii;
class BannerNewsEventChannelController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'Advertisment News Event Page & Channel Page',
          'url' => '#',
          'current' => true
        ]
      ];

      return $this->render('index',[
        'breadcrumb' => $breadcrumb
      ]);
    }

    public function actionChange(){
      $this->layout = false;
      $id = Yii::$app->request->post('id');
      $data = AdvertismentNeChannel::findOne($id);
      return $this->render('change',['data'=>$data]);
    }

    public function actionInsert(){
      $model = AdvertismentNeChannel::findOne(Yii::$app->request->post('id'));
      $model->link = Yii::$app->request->post('link');
      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/advertisment/".$newfilename);
        $model->image = $newfilename;
      }
      $model->expired = Yii::$app->request->post('expired') . ' 01:00:00';
      $model->save();
    }


}
