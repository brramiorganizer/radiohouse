<?php

namespace app\modules\dashboard\controllers;

use app\models\RadioCategory;
use Yii;

class RadioCategoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $data = RadioCategory::find()->all();
      $breadcrumb = [
        [
          'page' => 'Radio Category',
          'url' => '#',
          'current' => true
        ]
      ];
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'datas' => $data
        ]);
    }

    public function actionNew(){
      $this->layout = false;
      if(Yii::$app->request->post()){
        if(Yii::$app->request->post('id') != ''){
          $model = RadioCategory::findOne(Yii::$app->request->post('id'));
          $model->name = Yii::$app->request->post('name');
          $model->save();
        }else{
          $model = new RadioCategory();
          $model->name = Yii::$app->request->post('name');
          $model->save();
        }

      }else{
        return $this->render('new');
      }

    }

}
