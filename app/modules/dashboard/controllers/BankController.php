<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\Bank;

class BankController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'Bank',
          'url' => '#',
          'current' => true
        ]
      ];

      $banks = Bank::find()->andWhere(['is_delete'=>0])->all();

      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'banks' => $banks
      ]);
    }

    public function actionFormNew(){
      $this->layout = false;
      return $this->render('form-new');
    }
    public function actionFormEdit(){
      $this->layout = false;
      $edit = Bank::findOne(Yii::$app->request->get('id'));
      return $this->render('form-edit',[
        'model' => $edit
      ]);
    }
    public function actionDelete(){
      $this->layout = false;
      $id = Yii::$app->request->post('id');
      $bank = Bank::findOne($id);
      $bank->is_delete = 1;
      $bank->save();
    }
    public function actionCreate(){
      if(Yii::$app->request->post('id')){
        $model = Bank::findOne(Yii::$app->request->post('id'));
      }else{
        $model = new Bank();
      }

      $model->name = strip_tags(Yii::$app->request->post('name'));
      $model->rekening_name = strip_tags(Yii::$app->request->post('rekening_name'));
      $model->rekening_number = strip_tags(Yii::$app->request->post('rekening_number'));
      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/bank/".$newfilename);
        $model->image = $newfilename;

      }
      $model->save();
    }

}
