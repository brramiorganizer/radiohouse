<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\Radio;
use app\models\AdvertismentRadioMenu;

class RadioTenController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $breadcrumb = [
      [
        'page' => 'Radio on Menu',
        'url' => '#',
        'current' => true
      ]
    ];
    $radios = Radio::find()->andWhere(['status'=>'active'])->orderBy(['radio_name'=>SORT_ASC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'radios' => $radios,
        'count' => AdvertismentRadioMenu::find()->count()
      ]);
  }

  public function actionInsert(){
    if(Yii::$app->request->isAjax and $id = Yii::$app->request->post('radio_id')){
      $model = new AdvertismentRadioMenu();
      $model->radio_id = Yii::$app->request->post('radio_id');
      $model->expired= Yii::$app->request->post('expired') . ' 01:00:00';
      $model->save();

    }else{
      return $this->goHome();
    }
  }

  public function actionDelete(){
    if(Yii::$app->request->isAjax and $id = Yii::$app->request->post('id')){
      $model = AdvertismentRadioMenu::findOne(['radio_id'=>$id]);
      $model->delete();
    }else{
      return $this->goHome();
    }
  }

}
