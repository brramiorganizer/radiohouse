<?php

namespace app\modules\dashboard\controllers;
use app\models\Contact;
use Yii;
class ContactController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $breadcrumb = [
      [
        'page' => 'Contact',
        'url' => '#',
        'current' => true
      ]
    ];
    $datum = Contact::find()->orderBy(['id'=>SORT_DESC])->all();
    return $this->render('index',[
      'breadcrumb' => $breadcrumb,
      'datum' => $datum
    ]);

  }

  public function actionDelete(){
    $model = Contact::findOne(Yii::$app->request->post('id'))->delete();
  }
  public function actionView(){
    $this->layout = false;
    $model = Contact::findOne(Yii::$app->request->post('id'));
    return $this->render('view',['model'=>$model]);
  }

}
