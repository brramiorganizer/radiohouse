<?php

namespace app\modules\dashboard\controllers;

class AdvertisementController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'Advertisement',
          'url' => 'advertisement',
          'current' => true
        ]
      ];
        return $this->render('index',[
          'breadcrumb' => $breadcrumb
        ]);
    }

}
