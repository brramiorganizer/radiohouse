<?php

namespace app\modules\dashboard\controllers;

use Yii;
use app\models\PlanFeatures;
use app\models\Plan;
use app\models\PlanPackage;

class FeatureController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $data = PlanFeatures::find()->all();
      $plans = Plan::find()->all();
      $breadcrumb = [
        [
          'page' => 'Feature',
          'url' => '#',
          'current' => true
        ]
      ];
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'dataProvider' => $data,
          'plans' => $plans
        ]);
    }

    public function actionData(){
      $this->layout = false;
      $data = PlanFeatures::find()->all();
      return $this->render('data',[
        'data' => $data
      ]);
    }

    public function actionProses(){
      $this->layout = false;
      $id = Yii::$app->request->post('id');
      $name = Yii::$app->request->post('name');
      if($id == ''){
        $model = new PlanFeatures();
        $model->name = $name;
        $model->save();
      }else{
        $model = PlanFeatures::findOne($id);
        $model->name = $name;
        $model->save();
      }
      echo json_encode(['status'=>true]);
    }

    public function actionChoosePlan(){
      $plan_id = Yii::$app->request->post('parent');
      $plan_feature_id = Yii::$app->request->post('child');

      $check = PlanPackage::findOne(['plan_id'=>$plan_id,'plan_features_id'=>$plan_feature_id]);

      if($check){
        $check->delete();
      }else{
        $model = new PlanPackage();
        $model->plan_id = $plan_id;
        $model->plan_features_id = $plan_feature_id;
        $model->save();
      }

    }

    public function actionDeleteFeature(){
      $id = Yii::$app->request->post('id');

      $model = PlanFeatures::findOne($id);
      $model->delete();
      PlanPackage::deleteAll(['plan_features_id' => $id]);

    }

    public function actionEditPricePlan(){
      $this->layout =false;
      $model = Plan::findOne(['id'=>Yii::$app->request->post('id')]);
      return $this->render('edit-price',['model'=>$model]);
    }
    public function actionUpdatePricePlan(){
      $model = Plan::findOne(['id'=>Yii::$app->request->post('id')]);
      $model->price = Yii::$app->request->post('price');
      $model->save();
    }

}
