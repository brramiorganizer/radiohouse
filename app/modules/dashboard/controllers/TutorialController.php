<?php

namespace app\modules\dashboard\controllers;
use app\helper\Helper;
use app\models\Data;
use Yii;
class TutorialController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
      [
        'page' => 'File Tutorial',
        'url' => '#',
        'current' => true
      ]
    ];
    $model = Helper::getDataContent(17);
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'model' => $model
        ]);
    }

    public function actionChange(){
      $model = Data::findOne(17);
      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = 'tutorial.'. $file_ext;

        move_uploaded_file($file_tmp,"images/".$newfilename);
        $model->content = $newfilename;
      }
      $model->save();
      Yii::$app->session->setFlash('tuto','oke');
    }

}
