<?php

namespace app\modules\dashboard\controllers;
use app\helper\Helper;
use app\models\Data;
class LogoController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
      [
        'page' => 'Logo',
        'url' => '#',
        'current' => true
      ]
    ];
    $model = Helper::getDataContent(16);
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'model' => $model
        ]);
    }

    public function actionChange(){
      $model = Data::findOne(16);
      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = 'logo-radiohouse.'. $file_ext;

        move_uploaded_file($file_tmp,"images/".$newfilename);
        $model->content = $newfilename;
      }
      $model->save();
    }

}
