<?php

namespace app\modules\dashboard\controllers;

use app\models\Faq;
use Yii;

class FaqController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'Faq',
          'url' => '#',
          'current' => true
        ]
      ];
      $datum = Faq::find()->orderBy(['sort'=>SORT_ASC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'datum' => $datum
      ]);
    }


    public function actionSort(){
      $datas = Yii::$app->request->post();
      $no = 1;
      foreach($datas['set'] as $data){
        $model = Faq::findOne(['id'=>$data]);
        $model->sort = $no;
        $model->save(false);
        $no++;
      }
    }

    public function actionDelete(){
      $id = Yii::$app->request->post('id');
      Faq::findOne($id)->delete();
    }

    public function actionForm(){
      $this->layout =  false;
      return $this->render('form');
    }
    public function actionInsert(){
      if(Yii::$app->request->post('id') == ''){
        $model = new Faq();
        $model->sort = Faq::find()->count() + 1;
      }else{
        $model = Faq::findOne(Yii::$app->request->post('id'));
      }
      $model->question = Yii::$app->request->post('question');
      $model->answer = Yii::$app->request->post('answer');

      $model->save();
    }
}
