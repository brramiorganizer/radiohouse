<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\Radio;

class RadioController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $datas = Radio::find()
            ->where(['in', 'status', ['active','inactive']])
            ->all();


    $breadcrumb = [
      [
        'page' => 'Data Radio Active',
        'url' => '#',
        'current' => true
      ]
    ];
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'datum' => $datas
      ]);
  }

  public function actionInactive(){
    $model = Radio::findOne(Yii::$app->request->post('id'));
    $model->status = 'inactive';
    $model->save();
  }

  public function actionActive(){
    $model = Radio::findOne(Yii::$app->request->post('id'));
    $model->status = 'active';
    $model->save();
  }

}
