<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\EventJoin;
use app\models\EventJoinConfirmation;
use app\models\NewsEvent;

class EventController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionData(){
      $this->layout = false;
      $datum = EventJoin::find()
      ->andWhere(['status'=>'buying'])
      ->orderBy(['id'=>SORT_DESC])->all();
      return $this->render('data',[
        'datum' => $datum
      ]);
    }
    public function actionView($id){

      $data = EventJoin::findOne($id);
      $confirmation = EventJoinConfirmation::findOne(['event_join_id'=>$data->id]);
      return $this->render('view',[
        'confirmation' => $confirmation,
        'join' => $data
      ]);
    }

    public function actionUnapprove(){
      $confirmation = EventJoinConfirmation::findOne(Yii::$app->request->post('ejc'));
      $confirmation->status = 'failed';
      $confirmation->save();

      $join = EventJoin::findOne($confirmation->event_join_id);
      $join->status = 'failed';
      $join->save();
      $return_email = [
        'confirmation' => $confirmation,
        'join' => $join
      ];
      Yii::$app->mail->compose('failed-confirmation',$return_email)
        ->setFrom(['radiohouse.id@gmail.com' =>'Failed Confirmation Ticket Event ('.ucwords($join->event->title).' - Radio House)'])
        ->setTo($join->email)
        ->setSubject('Failed Confirmation Ticket Event ('.ucwords($join->event->title).' - Radio House)')
        ->send();
    }


    public function actionApprove(){
      $confirmation = EventJoinConfirmation::findOne(Yii::$app->request->post('ejc'));
      $confirmation->status = 'success';
      $confirmation->save();

      $join = EventJoin::findOne($confirmation->event_join_id);
      $join->status = 'success';
      $join->save();

      $return_email = [
        'confirmation' => $confirmation,
        'join' => $join
      ];
      Yii::$app->mail->compose('success-ticket',$return_email)
        ->setFrom(['radiohouse.id@gmail.com' =>'Ticket Event ('.ucwords($join->event->title).' - Radio House)'])
        ->setTo($join->email)
        ->setSubject('Ticket Event ('.ucwords($join->event->title).' - Radio House)')
        ->send();
    }
}
