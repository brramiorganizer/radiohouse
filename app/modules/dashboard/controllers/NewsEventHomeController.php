<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\NewsEvent;
class NewsEventHomeController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $breadcrumb = [
      [
        'page' => 'News event',
        'url' => '#',
        'current' => true
      ]
    ];
    $newsevents = NewsEvent::find()->andWhere(['is_delete'=>'0'])->orderBy(['title'=>SORT_ASC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'newsevents' => $newsevents
      ]);
  }

}
