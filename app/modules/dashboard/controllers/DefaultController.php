<?php

namespace app\modules\dashboard\controllers;

use yii\web\Controller;
use Yii;

/**
 * Default controller for the `dashboard` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
     public function __construct($id, $module, $config = [])
     {
       parent::__construct($id, $module, $config);
       $this->layout = 'backstage';
     }
     public function beforeAction($action)
     {
       if (!parent::beforeAction($action)) {
         return false;
        }
        if(!Yii::$app->session->get('domtoken')){
             $this->redirect('/backstage');
             return false;
        }

        return true;

     }
     public function actionIndex()
     {
         return $this->render('index');
     }
}
