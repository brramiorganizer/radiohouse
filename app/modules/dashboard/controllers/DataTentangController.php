<?php

namespace app\modules\dashboard\controllers;
use app\models\NewsEvent;
use app\models\EventTicket;
use Yii;

class DataTentangController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'News',
          'url' => '#',
          'current' => true
        ]
      ];
      $datum = NewsEvent::find()->andWhere(['is_delete'=>0])->orderBy(['id'=>SORT_DESC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'datum' => $datum
      ]);
    }

    public function actionEdit($id){
      $breadcrumb = [
        [
          'page' => 'News',
          'url' => 'news',
          'current' => false
        ],
        [
          'page' => 'Edit',
          'url' => '#',
          'current' => true
        ]
      ];
      if($data = NewsEvent::findOne(['id'=>$id,'is_delete'=>0])){
        return $this->render('edit',[
          'breadcrumb' => $breadcrumb,
          'model' => $data
        ]);
      }else{
        return $this->redirect('index');
      }

    }



    
}
