<?php

namespace app\modules\dashboard\controllers;

use Yii;
use app\modules\dashboard\models\SliderHome;
use app\modules\dashboard\models\SliderHomeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Radio;

/**
 * SliderHomeController implements the CRUD actions for SliderHome model.
 */
class SliderHomeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SliderHome models.
     * @return mixed
     */
     public function actionIndex()
     {
         $data = SliderHome::find()->orderBy('sort asc')->all();
         $breadcrumb = [
           [
             'page' => 'Slider Home',
             'url' => '#',
             'current' => true
           ]
         ];
         return $this->render('index', [
             'dataProvider' => $data,
             'breadcrumb' => $breadcrumb
         ]);
     }

    /**
     * Displays a single SliderHome model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $breadcrumb = [
        [
          'page' => 'Slider Home',
          'url' => 'slider-home',
          'current' => false
        ],
        [
          'page' => 'View',
          'url' => '#',
          'current' => true
        ]
      ];
        return $this->render('view', [
            'model' => $this->findModel($id),
            'breadcrumb' => $breadcrumb
        ]);
    }

    /**
     * Creates a new SliderHome model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
          $breadcrumb = [
            [
              'page' => 'Slider Home',
              'url' => 'slider-home',
              'current' => false
            ],
            [
              'page' => 'Create New',
              'url' => 'slider-home/create',
              'current' => true
            ]
          ];

          $radios = Radio::find()->andWhere(['=','status','active'])->all();
            return $this->render('create', [
                'breadcrumb' => $breadcrumb,
                'radios' => $radios
            ]);

    }

    public function actionDoCreate(){
      if(Yii::$app->request->post()){
        $model = new SliderHome();
        $model->title = strip_tags(Yii::$app->request->post('title'));
        $model->description = strip_tags(Yii::$app->request->post('description'));
        $model->link = strip_tags(Yii::$app->request->post('link'));
        $model->type = Yii::$app->request->post('type');
        $model->radio_id = (Yii::$app->request->post('radio') == '' ? 0 : Yii::$app->request->post('radio'));

        $file_name = $_FILES['file']['name'];
        $file_tmp =$_FILES['file']['tmp_name'];

        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);

        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/slider-home/".$newfilename);
        $model->file = $newfilename;

        //sort
        $totalslider = count(SliderHome::find()->all());
        if($totalslider == 0){
          $sort = 1;
        }else{
          $is = SliderHome::find()->orderBy('sort DESC')->one();
          $sort = $is->sort + 1;
        }
        $model->sort = $sort;
        $model->expired = Yii::$app->request->post('expired') . ' 00:01:00';
        $model->save(false);
        return $this->redirect(['view', 'id' => $model->id]);
      }else{
        return $this->goHome();
      }
    }
    /**
     * Updates an existing SliderHome model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      $breadcrumb = [
        [
          'page' => 'Slider Home',
          'url' => 'slider-home',
          'current' => false
        ],
        [
          'page' => 'Update',
          'url' => '#',
          'current' => true
        ]
      ];
      $model = $this->findModel($id);
        $radios = Radio::find()->andWhere(['=','status','active'])->all();
        return $this->render('update', [
            'breadcrumb' => $breadcrumb,
            'radios' => $radios,
            'model' => $model
        ]);
    }


    public function actionDoUpdate(){
      // echo var_dump(Yii::$app->request->post());
      // die;

      if(Yii::$app->request->post()){
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->title = strip_tags(Yii::$app->request->post('title'));
        $model->description = strip_tags(Yii::$app->request->post('description'));
        $model->link = strip_tags(Yii::$app->request->post('link'));
        $model->type = Yii::$app->request->post('type');
        $model->expired = Yii::$app->request->post('expired') . ' 00:01:00';
        if(Yii::$app->request->post('type') == 'other'){
          $model->radio_id = 0;
        }else{
          $model->radio_id = Yii::$app->request->post('radio');
        }
        if(isset($_FILES['file']['name']) and $_FILES['file']['name'] != ''){
          $file_name = $_FILES['file']['name'];
          $file_tmp =$_FILES['file']['tmp_name'];

          $tmp = explode('.', $file_name);
          $file_ext = end($tmp);

          $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

          unlink("media/slider-home/".$model->file);

          move_uploaded_file($file_tmp,"media/slider-home/".$newfilename);
          $model->file = $newfilename;

        }
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
      }else{
        return $this->goHome();
      }
    }

    /**
     * Deletes an existing SliderHome model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        unlink("media/slider-home/".$model->file);
        $model->delete();
    }

    public function actionSort(){
       $datas = Yii::$app->request->post();
       $no = 1;
       foreach($datas['set'] as $data){
         $model = SliderHome::findOne(['id'=>$data]);
         $model->sort = $no;
         $model->save(false);
         $no++;
       }
     }
     public function actionSwitch($id){
        $model = SliderHome::findOne(['id'=>$id]);
        $val = ($model->status == 'active' ? 'inactive' : 'active');
        $model->status = $val;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
      }

    /**
     * Finds the SliderHome model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SliderHome the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SliderHome::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
