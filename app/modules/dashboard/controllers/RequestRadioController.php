<?php

namespace app\modules\dashboard\controllers;

use Yii;
use app\models\Radio;
use app\models\RadioToken;
use app\models\RadioContent;
use app\models\RadioPersonal;
use app\models\RadioCommittee;
use app\models\RadioConfirmPayment;
use app\models\RadioPlan;
use app\models\RadioVisit;

class RequestRadioController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $datas = Radio::find()
              ->andWhere(['status'=>'pending'])
              ->all();

      $count_payment_pending = RadioConfirmPayment::find()->andWhere(['status'=>'pending'])->count();
      $breadcrumb = [
        [
          'page' => 'Request Radio',
          'url' => '#',
          'current' => true
        ]
      ];
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'datas' => $datas,
          'count' => $count_payment_pending
        ]);
    }

    public function actionGetone($id){
      $this->layout = false;
      $data = Radio::findOne($id);
      $pribadi  = RadioPersonal::findOne(['radio_id'=>$data->id]);
      $pengurus = RadioCommittee::find()->andWhere(['radio_id'=>$data->id])->all();
      $plan = RadioPlan::findOne(['radio_id'=>$data->id]);
      $payment = RadioConfirmPayment::findOne(['radio_id'=>$data->id]);
      return $this->render('detail-radio',[
        'data' => $data,
        'pribadi' => $pribadi,
        'pengurus' => $pengurus,
        'plan' => $plan,
        'payment' => $payment
      ]);
    }

    public function actionApproveDisapprove(){

      $id = RadioConfirmPayment::findOne(Yii::$app->request->post('id'))->radio_id;
      $type = Yii::$app->request->post('type');

      $model = Radio::findOne(['id'=>$id]);
      $model->status = $type;
      $model->save();

      $personal = RadioPersonal::findOne(['radio_id'=>$id]);





      if($type == 'approve'){
        $token = RadioToken::findOne(['radio_id'=>$id]);
        $token->expired_date = date('Y-m-d H:i:s', strtotime('+30 day', time()));
        $token->save();

        $a = RadioConfirmPayment::findOne(['radio_id'=>$id]);
        $a->status = 'success';
        $a->save();

        RadioConfirmPayment::deleteAll(['radio_id' => $id,'status' => 'pending']);

        $content = new RadioContent();
        $content->radio_id = $id;
        $content->save();
        $view = 'request-radio-approve';
        $return_email = [
          'token' => $token->token,
          'model' => $model,
          'expired' => $token->expired_date,
          'personal' => $personal
        ];
        $tit = 'Success! ';
      }else{
        $a = RadioConfirmPayment::findOne(['radio_id'=>$id]);
        $a->status = 'failed';
        $a->save();

        $view = 'request-radio-disapprove';
        $return_email = [
          'model' => $personal
        ];
        $tit = 'Failed! ';
      }


      Yii::$app->mail->compose($view,$return_email)
        ->setFrom(['radiohouse.id@gmail.com'=>$tit.'Confirm Payment Radio House'])
        ->setTo($personal->email)
        ->setSubject($tit.'Confirm Payment Radio House')
        ->send();
    }

    public function actionPaymentPending(){

      $this->layout = false;
      $datum  = RadioConfirmPayment::find()->orderBy(['id'=>SORT_DESC])->andWhere(['status'=>'pending'])->all();
      return $this->render('payment-pending',[
        'datum' => $datum
      ]);
    }

    public function actionDeleteRadio(){
      $id = Yii::$app->request->post('id');
      Radio::findOne($id)->delete();
      RadioPersonal::findOne(['radio_id'=>$id])->delete();
      RadioCommittee::deleteAll(['radio_id'=>$id]);
      RadioToken::findOne(['radio_id'=>$id])->delete();
      RadioPlan::findOne(['radio_id'=>$id])->delete();
      RadioConfirmPayment::deleteAll(['radio_id'=>$id]);
    }


    public function actionVisit($id){
      $this->layout = false;
      $model = RadioVisit::find()->where(['radio_id'=>$id])->orderBy(['created_on'=>SORT_DESC])->all();
      return $this->render('visit',[
        'model' => $model
      ]);
    }
}
