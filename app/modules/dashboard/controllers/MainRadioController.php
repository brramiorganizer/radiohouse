<?php

namespace app\modules\dashboard\controllers;
use Yii;
use app\models\AdvertismentRadio;
use app\models\Radio;

class MainRadioController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'Main Radio',
          'url' => '#',
          'current' => true
        ]
      ];
      $now = AdvertismentRadio::findOne(['status'=>'active']);
      $radios = Radio::find()->andWhere(['status'=>'active'])->orderBy(['radio_name'=>SORT_ASC])->all();
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'radios' => $radios,
          'now' => $now
        ]);
    }

    public function actionSet(){
      $id = Yii::$app->request->post('radio_id');
      AdvertismentRadio::updateAll(['status' => 'inactive']);
      $model = new AdvertismentRadio();
      $model->radio_id = $id;
      $model->expired = Yii::$app->request->post('expired') . ' 01:00:00';
      $model->save();
      Radio::updateAll(['main' => 0]);
      $radio = Radio::findOne($id);
      $radio->main = 1;
      $radio->save();
    }

    public function actionUnset(){
      AdvertismentRadio::updateAll(['status' => 'inactive']);
      Radio::updateAll(['main' => 0]);
    }

}
