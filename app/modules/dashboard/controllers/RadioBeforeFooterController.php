<?php

namespace app\modules\dashboard\controllers;use Yii;
use app\models\Radio;
use app\models\RadioContent;
use app\models\AdvertismentRadioTen;
use app\models\AdvertismentNewsEvent;

class RadioBeforeFooterController extends \yii\web\Controller
{
  public function actionIndex()
  {
    $breadcrumb = [
      [
        'page' => 'Radio Before Footer',
        'url' => '#',
        'current' => true
      ]
    ];
    $radios = Radio::find()->andWhere(['status'=>'active'])->orderBy(['radio_name'=>SORT_ASC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'radios' => $radios
      ]);
  }


  public function actionGetOneRadio(){
    $id = Yii::$app->request->post('id');
    $model = RadioContent::findOne(['radio_id'=>$id]);
    $return  = [
      'photo_profile' => $model->photo_profile
    ];
    echo json_encode($return);
  }

  public function actionAdvertismentOne(){
    $model = AdvertismentRadioTen::findOne(['sort'=>Yii::$app->request->post('sort')]);
    $model->banner_type = (Yii::$app->request->post('image') == 'new' ? 'custom' : 'profil');
    $model->radio_id = Yii::$app->request->post('radio');
    $model->expired_date = Yii::$app->request->post('date') . ' 00:00:00';
    if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
      $file_name = $_FILES['banner']['name'];
      $file_tmp =$_FILES['banner']['tmp_name'];
      $tmp = explode('.', $file_name);
      $file_ext = end($tmp);
      $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

      move_uploaded_file($file_tmp,"media/advertisment/".$newfilename);
      $model->banner = $newfilename;
    }

    $model->save();

  }
  public function actionAdvertismentNe(){
    $update = AdvertismentNewsEvent::findOne(['sort'=>Yii::$app->request->post('sort')]);
    if($update){
      $update->status = 'inactive';
      $update->save();
    }
    $model = new AdvertismentNewsEvent();
    $model->sort = Yii::$app->request->post('sort');
    $model->news_event_id = Yii::$app->request->post('ne');
    $model->expired_date = Yii::$app->request->post('date');
    $model->save();


  }
}
