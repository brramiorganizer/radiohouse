<?php

namespace app\modules\dashboard\controllers;
use app\modules\dashboard\models\Data;
use Yii;

class ContentEmailTicketController extends \yii\web\Controller
{
  public function actionIndex()
  {
      $breadcrumb = [
      [
        'page' => 'Data',
        'url' => 'data',
        'current' => true
      ]
    ];
    $datum = [15,19,20,21,9,8];
    $data = Data::find()
    ->joinWith('page')
    ->where(['in','data.id',$datum])
    ->orderBy(['id'=>SORT_DESC])
    ->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'datas' => $data
      ]);
  }

  public function actionForm(){
    $this->layout = false;
    return $this->render('form');
  }
  public function actionUpdate(){
    if(Yii::$app->request->isAjax){
      $model = Data::findOne(['id'=>Yii::$app->request->post('id')]);
      if($model){
        $model->title = Yii::$app->request->post('title');
        $model->content = Yii::$app->request->post('content');
        $model->save();
      }
      $return =  ['status'=>true];
      echo json_encode($return);
    }else{
      echo 'not allowed';
      die;
    }
  }

}
