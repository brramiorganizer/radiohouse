<?php

namespace app\modules\dashboard\controllers;

use Yii;

use app\modules\dashboard\models\AccountMember;
use app\models\AccountLogged;

class MemberVisitorController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $data = AccountMember::find()->all();
      $breadcrumb = [
        [
          'page' => 'Member Visitor',
          'url' => '#',
          'current' => true
        ]
      ];
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'dataProvider' => $data
        ]);
    }
    public function actionUpdate($id)
    {
      $model = AccountMember::findOne($id);
      $breadcrumb = [
        [
          'page' => 'Member Visitor',
          'url' => 'member-visitor',
          'current' => false
        ],
        [
          'page' => 'Edit',
          'url' => '#',
          'current' => true
        ]
      ];
      if ($model->load(Yii::$app->request->post()) ) {
        // if($model->banner = UploadedFile::getInstance($model,'banner')){
        //     $imageName = $model->banner.date("Y-m-d H-i-s");
        //     $model->banner->saveAs('media/banner-page/'.$imageName.'.'.$model->banner->extension);
        //     $model->banner = $imageName.'.'.$model->banner->extension;
        // }else{
        //   $model->banner = $this->findModel($id)->banner;
        // }
        $model->save();
        Yii::$app->session->setFlash('success', 'Data Berhasil Terbaharui');
        return $this->redirect(['update', 'id' => $model->id]);
      } else {
        return $this->render('edit',[
          'breadcrumb' => $breadcrumb,
          'model' => $model
        ]);
      }
    }

    public function actionView($id){
      $this->layout = false;
      $model = AccountMember::findOne($id);

      $loggeds = AccountLogged::find()
      ->andWhere(['account'=>'member','account_id'=>'4'])
      ->orderBy('id desc')
      ->all();
      return $this->render('view',[
        'model' => $model,
        'loggeds' => $loggeds
      ]);
    }

}
