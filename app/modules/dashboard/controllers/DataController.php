<?php

namespace app\modules\dashboard\controllers;

use app\modules\dashboard\models\Data;
use Yii;

class DataController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $breadcrumb = [
        [
          'page' => 'Data',
          'url' => 'data',
          'current' => true
        ]
      ];
      $data = Data::find()
      ->joinWith('page')
      ->orderBy(['id'=>SORT_DESC])
      ->all();
        return $this->render('index',[
          'breadcrumb' => $breadcrumb,
          'datas' => $data
        ]);
    }

    public function actionForm(){
    	$this->layout = false;
    	return $this->render('form');
    }
    public function actionUpdate(){
    	if(Yii::$app->request->isAjax){
    		$model = Data::findOne(['id'=>Yii::$app->request->post('id')]);
    		if($model){
    			$model->title = Yii::$app->request->post('title');
    			$model->content = Yii::$app->request->post('content');
    			$model->save();
    		}
    		$return =  ['status'=>true];
    		echo json_encode($return);
    	}else{
    		echo 'not allowed';
    		die;
    	}
    }

}
