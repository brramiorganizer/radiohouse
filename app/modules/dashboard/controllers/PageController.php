<?php

namespace app\modules\dashboard\controllers;

use Yii;
use app\modules\dashboard\models\PageContent;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Data;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{

    public function actionIndex()
    {
        $data = PageContent::find()->joinWith('page')->all();
        $breadcrumb = [
          [
            'page' => 'Page',
            'url' => 'page',
            'current' => true
          ]
        ];


        return $this->render('index', [
            'dataProvider' => $data,
            'breadcrumb' => $breadcrumb
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $model = $this->findModel($id);
      $breadcrumb = [
        [
          'page' => 'Page',
          'url' => 'page',
          'current' => false
        ],
        [
          'page' => $model->page->name,
          'url' => '#',
          'current' => true
        ],

      ];
      $no_banner = [1];
      $no_description = [1,6,12,11,9];
      $no_title = [12];
      $no_meta_description = [12];
      $no_meta_keyword = [12];
        return $this->render('view', [
            'model' => $model,
            'breadcrumb' => $breadcrumb,
                'no_banner' => $no_banner,
                'no_description' => $no_description,
                'no_title' => $no_title,
                'no_meta_description' => $no_meta_description,
                'no_meta_keyword' => $no_meta_keyword
        ]);
    }


    public function actionUpdate($id)
    {
      $model = $this->findModel($id);
      $breadcrumb = [
        [
          'page' => 'SEO ' . $model->page->name,
          'url' => '#',
          'current' => true
        ]

      ];


        if ($model->load(Yii::$app->request->post()) ) {
          if($model->banner = UploadedFile::getInstance($model,'banner')){
              $imageName = $model->banner.date("Y-m-d H-i-s");
              $model->banner->saveAs('media/banner-page/'.$imageName.'.'.$model->banner->extension);
              $model->banner = $imageName.'.'.$model->banner->extension;
          }else{
            $model->banner = $this->findModel($id)->banner;
          }
          $model->save();
          Yii::$app->session->setFlash('yes','oke');
          return $this->redirect(['update', 'id' => $model->id]);
        } else {

          $no_banner = [1];
          $no_description = [1,6,12,11,9];
          $no_title = [12];
          $no_meta_description = [12];
          $no_meta_keyword = [12];
            return $this->render('update', [
                'model' => $model,
                'breadcrumb' => $breadcrumb,
                'no_banner' => $no_banner,
                'no_description' => $no_description,
                'no_title' => $no_title,
                'no_meta_description' => $no_meta_description,
                'no_meta_keyword' => $no_meta_keyword
            ]);
        }
    }


    public function actionContentContact(){
      $address = Data::findOne(1);
      $address->content = Yii::$app->request->post('address');
      $address->save();

      $phone1 = Data::findOne(2);
      $phone1->content = Yii::$app->request->post('phone1');
      $phone1->save();

      $phone2 = Data::findOne(3);
      $phone2->content = Yii::$app->request->post('phone2');
      $phone2->save();


      $email = Data::findOne(4);
      $email->content = Yii::$app->request->post('email');
      $email->save();

      $maps = Data::findOne(22);
      $maps->content = Yii::$app->request->post('maps');
      $maps->save();
      echo json_encode(['status'=>true]);

    }

    public function actionContentAbout(){
      $icon1 = Data::findOne(23);
      $icon1->content = Yii::$app->request->post('icon1');
      $icon1->save();

      $icon2 = Data::findOne(24);
      $icon2->content = Yii::$app->request->post('icon2');
      $icon2->save();

      $icon3 = Data::findOne(25);
      $icon3->content = Yii::$app->request->post('icon3');
      $icon3->save();

      echo json_encode(['status'=>true]);

    }


    protected function findModel($id)
    {
        if (($model = PageContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
