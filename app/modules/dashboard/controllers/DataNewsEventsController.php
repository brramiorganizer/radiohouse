<?php

namespace app\modules\dashboard\controllers;
use app\models\NewsEvent;
use app\models\EventTicket;
use Yii;

class DataNewsEventsController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $breadcrumb = [
        [
          'page' => 'News',
          'url' => '#',
          'current' => true
        ]
      ];
      $datum = NewsEvent::find()->andWhere(['is_delete'=>0])->orderBy(['id'=>SORT_DESC])->all();
      return $this->render('index',[
        'breadcrumb' => $breadcrumb,
        'datum' => $datum
      ]);
    }

    public function actionEdit($id){
      $breadcrumb = [
        [
          'page' => 'News',
          'url' => 'news',
          'current' => false
        ],
        [
          'page' => 'Edit',
          'url' => '#',
          'current' => true
        ]
      ];
      if($data = NewsEvent::findOne(['id'=>$id,'is_delete'=>0])){
        return $this->render('edit',[
          'breadcrumb' => $breadcrumb,
          'model' => $data
        ]);
      }else{
        return $this->redirect('index');
      }

    }



    public function actionCreate(){
      if($id = Yii::$app->request->post('id')){
        $model =  NewsEvent::findOne($id);
      }else{
        $model = new NewsEvent();
        $model->author = 'admin';
        $model->radio_id =  0;
        $model->slug = $this->convertSlug(strip_tags(Yii::$app->request->post('title')));
      }
      $model->type = Yii::$app->request->post('type');
      $model->title =  strip_tags(Yii::$app->request->post('title'));
      $model->description =  Yii::$app->request->post('content');
      $model->meta_keywords =  strip_tags(Yii::$app->request->post('meta_keywords'));
      $model->meta_description =  strip_tags(Yii::$app->request->post('meta_description'));


      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/banner-news-event/".$newfilename);
        $model->banner = $newfilename;

      }

      $model->save();
      // Yii::$app->session->setFlash('success_news','oke');
    }



    public function actionView($id){
      $breadcrumb = [
        [
          'page' => 'News & Event',
          'url' => 'news',
          'current' => false
        ],
        [
          'page' => 'View',
          'url' => '#',
          'current' => true
        ]
      ];
      if($data = NewsEvent::findOne(['id'=>$id,'is_delete'=>0])){
        return $this->render('view',[
          'breadcrumb' => $breadcrumb,
          'model' => $data
        ]);
      }else{
        return $this->redirect('index');
      }

    }


    public function actionTicket($id){
      $breadcrumb = [
        [
          'page' => 'News & Event',
          'url' => 'news',
          'current' => false
        ],
        [
          'page' => 'Ticket',
          'url' => '#',
          'current' => true
        ]
      ];
      if($data = NewsEvent::findOne(['id'=>$id,'is_delete'=>0,'type'=>'event'])){
        $tickets = EventTicket::find()->andWhere(['news_event_id'=>$data->id,'is_delete'=>0])->all();
        return $this->render('ticket',[
          'breadcrumb' => $breadcrumb,
          'model' => $data,
          'tickets' => $tickets
        ]);
      }else{
        return $this->redirect('index');
      }
    }

    public function actionInsertTicket(){
      if(Yii::$app->request->post('id') == ''){
        $model = new EventTicket();
        $model->news_event_id = Yii::$app->request->post('news_event_id');
      }else{
        $model = EventTicket::findOne(Yii::$app->request->post('id'));
      }
      $model->category = Yii::$app->request->post('category');
      $model->price = Yii::$app->request->post('price');
      $model->save();
    }

    public function actionEditTicket(){
      $model = EventTicket::findOne(Yii::$app->request->post('id'));
      $return = [
        'category' => $model->category,
        'price' => $model->price
      ];
      echo json_encode($return);
    }

    public function actionDeleteTicket(){
      $model = EventTicket::findOne(Yii::$app->request->post('id'));
      $model->is_delete = 1;
      $model->save();
    }
    public function actionSoldTicket(){
      $model = EventTicket::findOne(Yii::$app->request->post('id'));
      $model->is_sold = 1;
      $model->available = 'no';
      $model->save();
    }
}
