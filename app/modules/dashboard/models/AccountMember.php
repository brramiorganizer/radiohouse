<?php

namespace app\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "account_member".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $image
 * @property string $password
 * @property string $token
 * @property string $type
 * @property integer $is_active
 * @property integer $is_delete
 * @property string $created_on
 * @property string $updated_on
 */
class AccountMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token'], 'required'],
            [['type'], 'string'],
            [['is_active', 'is_delete'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['firstname', 'lastname', 'email'], 'string', 'max' => 50],
            [['image', 'password', 'token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'image' => 'Image',
            'password' => 'Password',
            'token' => 'Token',
            'type' => 'Type',
            'is_active' => 'Is Active',
            'is_delete' => 'Is Delete',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
