<?php

namespace app\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "page_content".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $description
 * @property string $banner
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $created_on
 * @property string $updated_on
 */
class PageContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'required'],
            [['page_id'], 'integer'],
            [['description', 'meta_description', 'meta_keywords'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['banner', 'meta_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'description' => 'Description',
            'banner' => 'Banner',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }

    public function getPage(){
        return $this->hasOne(Page::className(), ['id'=>'page_id']);
    }
}
