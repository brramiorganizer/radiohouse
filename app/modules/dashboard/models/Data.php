<?php

namespace app\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $title
 * @property string $content
 * @property string $created_on
 * @property string $updated_on
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'content'], 'required'],
            [['page_id'], 'integer'],
            [['content'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'title' => 'Title',
            'content' => 'Content',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
    public function getPage(){
        return $this->hasOne(Page::className(), ['id'=>'page_id']);
    }
}
