<?php

namespace app\controllers;
use Yii;
use app\helper\Helper;
use app\models\Province;
use app\models\AccountMember;
use app\models\AccountMemberForgot;
use app\models\AccountChangePassword;
class MemberController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if(Yii::$app->session->get('fronttoken')){
          $user = Helper::checkMemberLogged();
          $provinces = Province::find()->all();
          return $this->render('index',[
            'user' => $user,
            'provinces' => $provinces
          ]);
        }else{
          return $this->goHome();
        }
    }

    public function actionChangeProfile(){
      $data['status'] = false;
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $firstname = strip_tags(Yii::$app->request->post('firstname'));
        $lastname = strip_tags(Yii::$app->request->post('lastname'));
        $gender = Yii::$app->request->post('gender');
        $place_of_birth = strip_tags(Yii::$app->request->post('place_of_birth'));
        $date_of_birth = Yii::$app->request->post('date_of_birth');
        $ktp = strip_tags(Yii::$app->request->post('ktp'));
        $number_phone = strip_tags(Yii::$app->request->post('number_phone'));
        $address = strip_tags(Yii::$app->request->post('address'));
        $city = strip_tags(Yii::$app->request->post('city'));
        $province = strip_tags(Yii::$app->request->post('province'));
        $zip_code = strip_tags(Yii::$app->request->post('zip_code'));

        $model = AccountMember::findOne(['id'=>$id]);
        $model->firstname = $firstname;
        $model->lastname = $lastname;
        $model->gender = $gender;
        $model->place_of_birth = $place_of_birth;
        $model->date_of_birth = $date_of_birth;
        $model->ktp = $ktp;
        $model->number_phone = $number_phone;
        $model->address = $address;
        $model->city = $city;
        $model->province = $province;
        $model->zip_code = $zip_code;
        if($model->save()){
          $data['status'] = true;
          Yii::$app->session->setFlash('success_change_profile','yes');
        }
      }
      echo json_encode($data);
    }

    public function actionChangePassword(){
      $data['status'] = false;
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $old_password = Yii::$app->request->post('old_password');
        $new_password = Yii::$app->request->post('new_password');

        $member = AccountMember::findOne(['id'=>$id]);
        if (password_verify($old_password, $member->password)) {

          $member->password = password_hash($new_password, PASSWORD_DEFAULT);
          $member->save();

          $change = new AccountChangePassword();
          $change->account_type = 'member';
          $change->old_password = $old_password;
          $change->new_password = $new_password;
          $change->save();
          $data['old_valid'] = true;
          Yii::$app->session->setFlash('success_change_password','yes');
        }else{
          $data['old_valid'] = false;
        }
          $data['status'] = true;

      }
      echo json_encode($data);

    }
    public function actionCheckEmail(){
      $data = AccountMember::findOne(['email'=>Yii::$app->request->post('email')]);
      if($data){
        echo 'true';
      }else{
        echo 'false';
      }
    }
    public function actionForgotPassword(){
      return $this->render('forgot-password');
    }
    public function actionInsertForgot(){
      date_default_timezone_set('Asia/Jakarta');
      if(Yii::$app->request->isAjax){

        $data = AccountMember::findOne(['email'=>Yii::$app->request->post('email')]);
        AccountMemberForgot::updateAll(['status' => 'fail'], 'status = "pending" AND account_member_id = '.$data->id);
        $model = new AccountMemberForgot();
        $model->account_member_id = $data->id;
        $model->token = md5(date('Y-m-d H:i:s'));
        $model->expired_date = date('Y-m-d H:i:s', strtotime(' + 7 days'));
        $model->save();
        $return_email = [
          'model' => $model
        ];
        Yii::$app->mail->compose('forgot-password-member',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'Forgot Password Radio House'])
          ->setTo($data->email)
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('Forgot Password Radio House')
          ->send();
      }

    }


    public function actionChangePasswordForgot($id){
      $validate = AccountMemberForgot::findOne(['token'=>$id,'status'=>'pending']);
      if($validate){
        return $this->render('change-password-forgot-member',[
          'model' => $validate
        ]);
      }else{
        throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
      }
    }

    public function actionNewForgot(){

      if(Yii::$app->request->post('account_member_id') and Yii::$app->request->isAjax){
        $model = AccountMemberForgot::findOne(['status'=>'pending','account_member_id'=>Yii::$app->request->post('account_member_id')]);
        $model->status = 'used';
        $model->save();

        $data = AccountMember::findOne($model->account_member_id);
        $data->password = password_hash(Yii::$app->request->post('password'), PASSWORD_DEFAULT);
        $data->save();
      }else{
        return $this->goHome();
      }
    }

}
