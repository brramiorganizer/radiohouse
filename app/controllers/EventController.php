<?php

namespace app\controllers;
use Yii;
use app\models\NewsEvent;
use app\models\EventTicket;
use app\models\AccountMember;

class EventController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->goHome();
    }

    public function actionDetail($slug){
      $data = NewsEvent::findOne(['slug'=>$slug,'type'=>'event']);
      $latestNewsEvent = NewsEvent::find()
      ->orderBy(['id'=>SORT_DESC])
      ->limit(4)
      ->all();
      if($data){
        $tickets  = EventTicket::find()->andWhere(['news_event_id'=>$data->id,'available'=>'yes','is_delete'=>0,'is_sold'=>0])->all();
        //if(isset($_GET['join']) and $_GET['join'] == 'true' and count($tickets) > 0 and Yii::$app->session->get('fronttoken')){
        if(isset($_GET['join']) and $_GET['join'] == 'true' and count($tickets) > 0){
          // $user = AccountMember::findOne(['token'=>Yii::$app->session->get('fronttoken')]);
          return $this->render('join',[
            'model' => $data,
            'tickets' => $tickets,
            // 'user' => $user
          ]);
        }else{
          return $this->render('detail',[
            'model' => $data,
            'latest' => $latestNewsEvent,
            'ticket' => count($tickets)
          ]);
        }
      }else{
        return $this->goHome();
      }

    }




}
