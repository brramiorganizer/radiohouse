<?php

namespace app\controllers;
use app\models\NewsEvent;
use app\models\Radio;
use Yii;

class NewsEventsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $where = ['is_delete'=>0];
        if(isset($_GET['type'])){
          if($_GET['type'] == 'event'){
            $where = ['is_delete'=>0,'type'=>'event'];
          }else if($_GET['type'] == 'news'){
            $where = ['is_delete'=>0,'type'=>'news'];
          }
        }
        $count = NewsEvent::find()->andWhere($where)->all();

        $perpage = 6;
        $total = count($count);
        $offset = 0;

        $pagination = $total / $perpage;
        if(isset($_GET['page']) and $_GET['page'] != '' and $_GET['page'] > 1){
          $offset = ($_GET['page'] - 1) * $perpage;
        }

        $newsevent = NewsEvent::find()
        ->andWhere($where)
        ->orderBy(['id'=>SORT_DESC])
        ->limit($perpage)
		    ->offset($offset)
        ->all();
        $radios = Radio::find()->andWhere(['status'=>'active'])->all();
        return $this->render('index',[
          'newsevents' => $newsevent,
          'pagination' => $pagination,
          'radios' => $radios
        ]);
    }

    public function actionDelete(){
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $model = NewsEvent::findOne($id);
        $model->is_delete = 1;
        $model->save();
        echo json_encode(['status'=>true]);
      }else{
        return $this->goHome();
      }
    }
}
