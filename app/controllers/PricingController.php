<?php

namespace app\controllers;
use Yii;
use app\models\Plan;
use app\models\Radio;
use app\models\RadioPlan;
use app\models\RadioPersonal;
use app\models\RadioCommittee;
use app\models\RadioToken;
use app\models\Bank;
use app\models\PlanPackage;
use app\models\RadioConfirmPayment;
use app\helper\Helper;
class PricingController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $plans = Plan::find()->all();
        return $this->render('index',[
          'plans' => $plans
        ]);
    }

    public function actionCheckUsername(){
      $data = Radio::findOne(['username'=>Yii::$app->request->post('username')]);
      if($data){
        echo 'false';
      }else{
        echo 'true';
      }
    }
    public function actionCheckEmail(){
      $data = RadioPersonal::findOne(['email'=>Yii::$app->request->post('email')]);
      if($data){
        echo 'false';
      }else{
        echo 'true';
      }
    }

    public function actionCheckName(){
      $data = Radio::findOne(['radio_name'=>Yii::$app->request->post('email')]);
      if($data){
        echo 'false';
      }else{
        echo 'true';
      }
    }
    public function actionRequest(){
      if(Yii::$app->request->isAjax){
        $model = new Radio();
        $model->company_name = strip_tags(Yii::$app->request->post('company_name'));
        $model->radio_name =  strip_tags(Yii::$app->request->post('radio_name'));
        $model->telepon =  strip_tags(Yii::$app->request->post('telepon'));
        $model->address =  strip_tags(Yii::$app->request->post('address'));
        $model->city =  strip_tags(Yii::$app->request->post('kota'));
        $model->number_isr_ipp =  strip_tags(Yii::$app->request->post('isr_ipp'));
        $model->slug = Helper::slugify($model->radio_name);
        $model->save();


        $personal = new RadioPersonal();
        $personal->radio_id = $model->id;
        $personal->name = strip_tags(Yii::$app->request->post('p_name'));
        $personal->email =  strip_tags(Yii::$app->request->post('p_email'));
        $personal->address = strip_tags(Yii::$app->request->post('p_address'));
        $personal->telephone = strip_tags(Yii::$app->request->post('p_telephone'));
        $personal->handphone = strip_tags(Yii::$app->request->post('p_handphone'));
        $personal->save();

        for($i = 1; $i <= 3; $i++){
          $komite = new RadioCommittee();
          $komite->radio_id = $model->id;
          $komite->position_id = $i;
          $komite->name = strip_tags(Yii::$app->request->post('name_p'.$i));
          $komite->address = strip_tags(Yii::$app->request->post('alamat_p'.$i));
          $komite->telephone = strip_tags(Yii::$app->request->post('handphone_p'.$i));
          $komite->save();
        }

        $token = new RadioToken();
        $token->radio_id = $model->id;
        $token->token = md5(date('Y-m-d H:i:s'));
        $token->save();

        $plan = new RadioPlan();
        $plan->radio_id = $model->id;
        $plan->plan_id = Yii::$app->request->post('plan_id');
        $plan->save();

        $banks = Bank::find()->all();
        $return_email = [
          'banks' => $banks,
          'personal' => $personal,
          'token' => $token->token,
          'plan' => Plan::findOne($plan->plan_id),
          'packages' => PlanPackage::find()->andWhere(['plan_id'=>$plan->plan_id])->all(),
          'model' => $model
        ];
        Yii::$app->mail->compose('radio-request',$return_email)
          ->setFrom(['radiohouse.id@gmail.com'=>'Request join Radio House'])
          ->setTo($personal->email)
          ->setSubject('Request join Radio House')
          ->send();

          Yii::$app->mail->compose('radio-request-admin',$return_email)
            ->setFrom(['radiohouse.id@gmail.com'=>'Request join Radio House'])
            ->setTo(Yii::$app->params['email'])
            ->setSubject('Request join Radio House')
            ->send();

        Yii::$app->session->setFlash('success_regis_radio',$token->token);
      }else{
        return $this->goHome();
      }

    }

    public function actionFinishRequest(){
      if(Yii::$app->session->hasFlash('success_regis_radio')){
        return $this->render('thanks-page',['token'=>Yii::$app->session->getFlash('success_regis_radio')]);
      }else{
        return $this->goHome();
      }

    }

    public function actionConfirm($token){
      if($token = RadioToken::findOne(['token'=>$token])){
        if($radio = Radio::findOne(['id'=>$token->radio_id,'status'=>'pending'])){
          $return = [
            'banks' => Bank::find()->andWhere(['is_delete'=>0])->all(),
            'plan' => RadioPlan::findOne(['radio_id'=>$radio->id])
          ];
          return $this->render('confirm-payment',$return);
        }else{
          return $this->goHome();
        }

      }else{
        return $this->goHome();
      }
    }

    public function actionInsertConfirm(){
      if(Yii::$app->request->isAjax){
        $model = new RadioConfirmPayment();
        $model->radio_id = Yii::$app->request->post('id');
        $model->bank_id = Yii::$app->request->post('bank');
        $model->rekening_name = Yii::$app->request->post('rekening_name');
        $model->rekening_number = Yii::$app->request->post('rekening_number');
        $model->description = Yii::$app->request->post('keterangan');

        if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
          $file_name = $_FILES['banner']['name'];
          $file_tmp =$_FILES['banner']['tmp_name'];
          $tmp = explode('.', $file_name);
          $file_ext = end($tmp);
          $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

          move_uploaded_file($file_tmp,"media/transfer/".$newfilename);
          $model->image = $newfilename;

        }

        $model->save();
        $return_email = [
          'model' => $model
        ];
        Yii::$app->mail->compose('confirm-payment-admin',$return_email)
          ->setFrom(['radiohouse.id@gmail.com'=>'Confirm Payment'])
          ->setTo(Yii::$app->params['email'])
          ->setSubject('Confirm Payment')
          ->send();

      }else{
        return $this->goHome();
      }
    }

}
