<?php

namespace app\controllers;
use app\models\Contact;
use Yii;
use app\helper\Helper;

class HubungiKamiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionPost(){
    	if(Yii::$app->request->isAjax){
    		$model = new Contact();
    		$model->name  = Yii::$app->request->post('name');
    		$model->email = Yii::$app->request->post('email');
    		$model->message = Yii::$app->request->post('comment');
    		$model->device = Helper::getDevice();
    		$model->save();

    		$return_email = [
	          'model' => $model,
	        ];
	        Yii::$app->mail->compose('contact',$return_email)
	          ->setFrom([$model->email=>'(Contact Page Radio House)'])
	          ->setTo('ryza.brramio@gmail.com')
	          ->setSubject('(Contact Page Radio House)')
	          ->send();
	          Yii::$app->session->setFlash('contact','yes');
	          echo json_encode(['status'=>true]);

    	}else{
    		echo 'not allowed';
    		die;
    	}
    }

}
