<?php

namespace app\controllers;

use Yii;

use app\models\Radio;
use app\models\RadioForgot;
use app\models\RadioProgram;
use app\models\RadioProgramUnggulan;
use app\models\RadioToken;
use app\models\RadioPlan;
use app\models\RadioPersonal;
use app\models\RadioCommittee;
use app\models\PlanPackage;
use app\models\RadioContent;
use app\models\EventTicket;
use app\models\NewsEvent;
use app\models\EventJoin;
use app\models\EventJoinPayment;
use app\models\Bank;
use app\models\AccountLogged;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helper\Helper;
class RadioController extends \yii\web\Controller
{

  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete' => ['POST'],
                  'post-form-news' => ['POST'],
                  'list-news-event' => ['POST']
              ],
          ],
      ];
  }

    public function actionIndex()
    {
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
        $radio = $this->findModel($id);
        $personal = RadioPersonal::findOne(['radio_id'=>$radio->id]);
        $content = RadioContent::findOne(['radio_id'=>$radio->id]);
        $committees = RadioCommittee::find()->andWhere(['radio_id'=>$radio->id])->all();
        $newsevents = NewsEvent::find()
        ->andWhere(['radio_id'=>$radio->id,'is_delete'=>0])
        ->orderBy(['id'=>SORT_DESC])
        ->all();
        return $this->render('index',[
          'radio' => $radio,
          'content' => $content,
          'newsevents' => $newsevents,
          'personal' => $personal,
          'committees' => $committees
        ]);
    }

    public function actionActivate($token){
      $token = RadioToken::findOne(['token'=>$token]);
      if($token and Radio::findOne(['id'=>$token->radio_id,'status'=>'approve'])){
        return $this->render('activate',['radio_id' => $token->radio_id]);
      }else{
        throw new NotFoundHttpException();
      }
    }
    public function actionSetActivate(){
      if(Yii::$app->request->isAjax){
        $radio = Radio::findOne(Yii::$app->request->post('radio_id'));
        $radio->status = 'active';
        $radio->password = password_hash(Yii::$app->request->post('password'), PASSWORD_DEFAULT);
        $radio->save();
        $personal = RadioPersonal::findOne(['radio_id'=>$radio->id]);
        $plan = RadioPlan::findOne(['radio_id'=>$radio->id]);
        $plan->status = 'active';
        $plan->active_date = date('Y-m-d H:i:s');
        $plan->expired_date = date('Y-m-d H:i:s', strtotime('+364 day', time()));
        $plan->save();
        $return_email = [
          'model' => $radio,
          'plan' => $plan,
          'packages' => PlanPackage::find()->andWhere(['plan_id'=>$plan->plan_id])->all()
        ];
        Yii::$app->mail->compose('radio-active',$return_email)
          ->setFrom(['radiohouse.id@gmail.com'=>'Success active your radio in Radio House'])
          ->setTo($personal->email)
          ->setSubject('Success active your radio in Radio House')
          ->send();
        $data['status'] = true;
      }else{
        $data['status'] = false;
        throw new NotFoundHttpException();
      }
      echo json_encode($data);
    }

    public function actionLogin(){
      $this->layout = false;
      if(Yii::$app->session->get('radiotoken')){
        Yii::$app->session->setFlash('download_file','oke');
        return $this->redirect(['index']);
      }
      return $this->render('login');
    }
    public function actionLogout(){
      Yii::$app->session->remove('radiotoken');
      return $this->goHome();
    }
    public function actionLogining(){
      $request = Yii::$app->request;
      $return['status'] = false;
      if($request->isAjax and $email = $request->post('email') and $password = $request->post('password')) {
        $em = RadioPersonal::findOne(['email'=>$email]);
        if($em){
          $check = Radio::findOne(['id'=>$em->radio_id,'status'=>'active']);
          if($check){
            if (password_verify($password, $check->password)) {
                $return['status'] = true;
                Yii::$app->session->set('radiotoken', $check->id);
                $logged = new AccountLogged();
                $logged->account = 'radio';
                $logged->account_id = $check->id;
                $logged->ip = Helper::get_client_ip();
                $logged->device = Helper::getDevice();
                $logged->save();
            }
          }
        }

      }

      echo json_encode($return);
    }


    public function actionChangeIklan(){
      if(Yii::$app->request->post()){
        $id = Yii::$app->request->post('id');
        $model = RadioContent::findOne(['radio_id'=>$id]);
        $file_name = $_FILES['file']['name'];
        $file_tmp =$_FILES['file']['tmp_name'];

        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);

        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/radio/iklan/".$newfilename);
        if(Yii::$app->request->post('iklan') == 1){
          $model->advertisment_1 = $newfilename;
        }else{
          $model->advertisment_2 = $newfilename;
        }

        $model->save();
        return $this->redirect(['index']);
      }else{
        return $this->goHome();
      }
    }

    public function actionChangeBanner(){
      if(Yii::$app->request->post()){
        $id = Yii::$app->request->post('id');
        $model = RadioContent::findOne(['radio_id'=>$id]);
        $file_name = $_FILES['file']['name'];
        $file_tmp =$_FILES['file']['tmp_name'];

        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);

        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/radio/banner/".$newfilename);
        $model->banner = $newfilename;
        $model->save();
        return $this->redirect(['index']);
      }else{
        return $this->goHome();
      }
    }

    public function actionChangeProfileImage(){
      if(Yii::$app->request->post()){
        $id = Yii::$app->request->post('id');
        $model = RadioContent::findOne(['radio_id'=>$id]);
        $file_name = $_FILES['file']['name'];
        $file_tmp =$_FILES['file']['tmp_name'];

        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);

        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/radio/profile/".$newfilename);
        $model->photo_profile = $newfilename;
        $model->save();
        return $this->redirect(['index']);
      }else{
        return $this->goHome();
      }
    }

    public function actionChangeProfileContent(){
      if(Yii::$app->request->post()){
        $id = Yii::$app->request->post('id');
        $content = Yii::$app->request->post('description');
        $model = RadioContent::findOne(['radio_id'=>$id]);

        $model->description = $content;
        $model->save();
        return $this->redirect(['index']);
      }else{
        return $this->goHome();
      }
    }

    public function actionGetFormNews(){
      $this->layout = false;
      return $this->render('form-news');
    }

    public function actionGetFormEvent(){
      $this->layout = false;
      return $this->render('form-event');
    }

    public function actionPostFormNews(){
      $model = new NewsEvent();
      $model->type = 'news';
      $model->radio_id =  Yii::$app->request->post('radio_id');
      $model->title =  strip_tags(Yii::$app->request->post('title'));
      $model->description =  Yii::$app->request->post('content');
      $model->meta_keywords =  strip_tags(Yii::$app->request->post('meta_keywords'));
      $model->meta_description =  strip_tags(Yii::$app->request->post('meta_description'));
      $model->slug = $this->convertSlug(strip_tags(Yii::$app->request->post('title')));

      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/banner-news-event/".$newfilename);
        $model->banner = $newfilename;

      }

      $model->save();
      Yii::$app->session->setFlash('success_news','oke');
    }



    public function actionPostFormEvent(){
      $tickets = json_decode(stripslashes(Yii::$app->request->post('tickets')));
      $total_ticket = count($tickets);
      $model = new NewsEvent();
      $model->type = 'event';
      $model->radio_id =  Yii::$app->request->post('radio_id');
      $model->title =  strip_tags(Yii::$app->request->post('title'));
      $model->description =  Yii::$app->request->post('content');
      $model->meta_keywords =  strip_tags(Yii::$app->request->post('meta_keywords'));
      $model->meta_description =  strip_tags(Yii::$app->request->post('meta_description'));
      $model->join_active = ($total_ticket > 0 ? 'yes' : 'no');
      $model->slug = $this->convertSlug(strip_tags(Yii::$app->request->post('title')));

      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/banner-news-event/".$newfilename);
        $model->banner = $newfilename;

      }
      $model->save();

      foreach($tickets as $ticket){
        $is = new EventTicket();
        $is->category = $ticket[0];
        $is->price = $ticket[1];
        $is->news_event_id = $model->id;
        $is->save();
      }
      Yii::$app->session->setFlash('success_news','oke');

    }

    public function actionListNewsEvent(){
      $this->layout = false;
      $id = Yii::$app->request->post('gen');
      $newsevents = NewsEvent::find()
      ->andWhere(['radio_id'=>$id,'is_delete'=>0])
      ->orderBy(['id'=>SORT_DESC])
      ->all();

      return $this->render('list-news-event',[
        'newsevents' => $newsevents
      ]);
    }


    public function actionModalTicket(){
      $this->layout = false;
      if(Yii::$app->request->isAjax){
        $id = Yii::$app->request->post('id');
        $datum = EventTicket::find()
        ->andWhere(['news_event_id'=>$id,'is_delete'=>0])
        ->orderBy(['id'=>SORT_DESC])
        ->all();
        return $this->render('ticket-modal',[
          'datum' => $datum
        ]);
      }else{
        return $this->goHome();
      }
    }

    public function actionDeleteTicket(){
      $this->layout = false;
      if(Yii::$app->request->isAjax){
        $model =  EventTicket::findOne(Yii::$app->request->post('id'));
        $model->is_delete = 1;
        $model->save();
        echo json_encode(['status'=>true]);
      }else{
        return $this->goHome();
      }
    }

    public function actionSoldTicket(){
      $this->layout = false;
      if(Yii::$app->request->isAjax){
        $model =  EventTicket::findOne(Yii::$app->request->post('id'));
        $model->is_sold = 1;
        $model->save();
        echo json_encode(['status'=>true]);
      }else{
        return $this->goHome();
      }
    }
    public function actionGetNewsEvent(){
      $this->layout = false;
      if(Yii::$app->request->isAjax){
        $model =  NewsEvent::findOne(Yii::$app->request->post('id'));
        $data = [
          'id' => $model->id,
          'title' => $model->title,
          'banner' => $model->banner,
          'description' => $model->description,
          'meta_keywords' => $model->meta_keywords,
          'meta_description' => $model->meta_description
        ];
        echo json_encode(['status'=>true,'data'=>$data]);
      }else{
        return $this->goHome();
      }
    }



    public function actionUpdateNewsEvent(){
      $model = NewsEvent::findOne(['id'=>Yii::$app->request->post('id')]);
      $model->title =  strip_tags(Yii::$app->request->post('title'));
      $model->description =  Yii::$app->request->post('content');
      $model->meta_keywords =  strip_tags(Yii::$app->request->post('meta_keywords'));
      $model->meta_description =  strip_tags(Yii::$app->request->post('meta_description'));

      if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
        $file_name = $_FILES['banner']['name'];
        $file_tmp =$_FILES['banner']['tmp_name'];
        $tmp = explode('.', $file_name);
        $file_ext = end($tmp);
        $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

        move_uploaded_file($file_tmp,"media/banner-news-event/".$newfilename);
        $model->banner = $newfilename;

      }

      $model->save();
      Yii::$app->session->setFlash('update_oke','oke');
    }

    public function actionBuyTicket(){
      if(Yii::$app->request->isAjax){
        $total_data = count(EventJoin::find()->all()) + 1;
        $total_today = EventJoin::find()->where(['like','created_on',date('Y-m-d')])->count() + 1;
        $code = substr(date('Y-m-d'),0,4) . substr(date('Y-m-d'),5,2) . substr(date('Y-m-d'),8,2) . $total_data;
        $model = new EventJoin();
        $model->code = (int)$code;
        // $model->account_member_id = Helper::getUserLogged()->id;
        $model->account_member_id = 0;
        $model->news_event_id = strip_tags(Yii::$app->request->post('news_event_id'));
        $model->name = strip_tags(Yii::$app->request->post('name'));
        $model->email = strip_tags(Yii::$app->request->post('email'));
        $model->no_phone = strip_tags(Yii::$app->request->post('handphone'));
        $model->address = strip_tags(Yii::$app->request->post('address'));
        $model->event_ticket_id = strip_tags(Yii::$app->request->post('category_ticket'));
        $model->quantity = strip_tags(Yii::$app->request->post('total_ticket'));

        $price = EventTicket::findOne(['id'=>$model->event_ticket_id ])->price;
        $model->total_price = ((int)strip_tags(Yii::$app->request->post('total_ticket')) * $price) + $total_today;
        $model->save(false);
        $return_email = [
          'join' => $model
        ];
        Yii::$app->mail->compose('order-ticket',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'Pembelian Tiket (Event : '.$model->event->title.' | Radio House)'])
          ->setTo($model->email)
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('Pembelian Tiket (Event : '.$model->event->title.' | Radio House)')
          ->send();

        echo json_encode(['status'=>true,'code'=>$code]);
      }else{
        return $this->goHome();
      }
    }


    public function actionEditTicket(){
      if(Yii::$app->request->isAjax){
        $total_data = count(EventJoin::find()->all()) + 1;
        $total_today = EventJoin::find()->where(['like','created_on',date('Y-m-d')])->count() + 1;

        $model = EventJoin::findOne(['id'=>Yii::$app->request->post('id')]);
        $model->name = strip_tags(Yii::$app->request->post('name'));
        //$model->email = strip_tags(Yii::$app->request->post('email'));
        $model->no_phone = strip_tags(Yii::$app->request->post('no_phone'));
        $model->address = strip_tags(Yii::$app->request->post('address'));
        $model->event_ticket_id = strip_tags(Yii::$app->request->post('category_ticket'));
        $model->quantity = strip_tags(Yii::$app->request->post('total_ticket'));

        $price = EventTicket::findOne(['id'=>$model->event_ticket_id ])->price;

        $model->total_price = ((int)strip_tags(Yii::$app->request->post('total_ticket')) * $price) + $total_today;
        $model->save(false);
        echo json_encode(['status'=>true]);
      }else{
        return $this->goHome();
      }
    }

    public function actionChoosePayment(){
      if(Yii::$app->request->isAjax){
        $model = new EventJoinPayment();
        $model->event_join_id = Yii::$app->request->post('id');
        $model->bank_id = Yii::$app->request->post('bank');
        $model->save();

        $edit = EventJoin::findOne($model->event_join_id);
        $edit->status = 'choose';
        $edit->save();
        $return_email = [
          'join' => $edit
        ];
        Yii::$app->mail->compose('order-ticket-choose-payment',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'Pembelian Tiket (Event : '.$edit->event->title.' | Radio House)'])
          ->setTo($edit->email)
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('Pembelian Tiket (Event : '.$edit->event->title.' | Radio House)')
          ->send();
        echo json_encode(['status'=>true]);
        Yii::$app->session->setFlash('token_complete',$model->event_join_id);
      }else{
        return $this->goHome();
      }
    }

    public function actionComplete(){
      if(Yii::$app->session->getFlash('token_complete')){
        $eventjoin = EventJoin::findOne(['id'=>Yii::$app->session->get('token_complete'),'status'=>'choose']);
        $ticket = EventTicket::findOne(['id'=>$eventjoin->event_ticket_id]);
        $event = NewsEvent::findOne(['id'=>$ticket->news_event_id]);
        $payment = EventJoinPayment::findOne(['event_join_id'=>$eventjoin->id]);
        $bank = Bank::findOne(['id'=>$payment->bank_id]);
        return $this->render('complete',[
          'event' => $eventjoin,
          'ticket' => $ticket,
          'model'=>$event,
          'bank' => $bank
        ]);
      }else{
        return $this->goHome();
      }
    }


    public function actionDataPersonal(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['personal']  = RadioPersonal::findOne(['radio_id'=>$id]);
      return $this->render('data-personal',$return);
    }
    public function actionChangePersonal(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->session->get('radiotoken')){
        $model = RadioPersonal::findOne(['radio_id'=>$id]);
        $model->name = strip_tags(Yii::$app->request->post('name'));
        $model->address = strip_tags(Yii::$app->request->post('address'));
        $model->telephone = strip_tags(Yii::$app->request->post('telephone'));
        $model->handphone = strip_tags(Yii::$app->request->post('handphone'));
        $model->save();
      }else{
        return $this->goHome();
      }
    }
    public function actionDataCompany(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['model']  = Radio::findOne(['id'=>$id]);
      return $this->render('data-company',$return);
    }

    public function actionChangeCompany(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->session->get('radiotoken')){
        $model = Radio::findOne(['id'=>$id]);
        $model->number_isr_ipp = strip_tags(Yii::$app->request->post('number_isr_ipp'));
        $model->company_name = strip_tags(Yii::$app->request->post('company_name'));
        $model->radio_name = strip_tags(Yii::$app->request->post('radio_name'));
        $model->telepon = strip_tags(Yii::$app->request->post('telepon'));
        $model->address = strip_tags(Yii::$app->request->post('address'));
        $model->description = Yii::$app->request->post('description');
        $model->save();
      }else{
        return $this->goHome();
      }
    }
    public function actionDataProgram(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['model']  = RadioProgram::findOne(['radio_id'=>$id]);
      return $this->render('data-program',$return);
    }

    public function actionChangeProgram(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->session->get('radiotoken')){
        if(!$model = RadioProgram::findOne(['radio_id'=>$id])){
          $model = new RadioProgram();
          $model->radio_id = $id;
        }
        $model->format_siaran = Yii::$app->request->post('format_siaran');
        $model->target_pendengar = Yii::$app->request->post('target_pendengar');
        $model->coverage_area = Yii::$app->request->post('coverage_area');
        $model->musik = Yii::$app->request->post('musik');
        $model->save();
      }else{
        return $this->goHome();
      }
    }
    public function actionDataCommittee(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['direktur']  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>1]);
      $return['marketing']  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>2]);
      $return['personal']  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>3]);
      return $this->render('data-committee',$return);
    }
    public function actionChangeCommittee(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->session->get('radiotoken')){
        $direktur  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>1]);
        $direktur->name = Yii::$app->request->post('name_1');
        $direktur->address = Yii::$app->request->post('address_1');
        $direktur->telephone = Yii::$app->request->post('telephone_1');
        $direktur->save();
        $marketing  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>2]);
        $marketing->name = Yii::$app->request->post('name_2');
        $marketing->address = Yii::$app->request->post('address_2');
        $marketing->telephone = Yii::$app->request->post('telephone_2');
        $marketing->save();
        $personal  = RadioCommittee::findOne(['radio_id'=>$id,'position_id'=>3]);
        $personal->name = Yii::$app->request->post('name_3');
        $personal->address = Yii::$app->request->post('address_3');
        $personal->telephone = Yii::$app->request->post('telephone_3');
        $personal->save();
      }else{
        return $this->goHome();
      }
    }
    public function actionProgramSuperior(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['datum']  = RadioProgramUnggulan::find()->andWhere(['radio_id'=>$id])->all();
      return $this->render('data-program-unggulan',$return);
    }
    public function actionDataProgramSuperior(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      $return['datum']  = RadioProgramUnggulan::find()->andWhere(['radio_id'=>$id])->all();
      return $this->render('data-program-unggulan-data',$return);
    }

    public function actionInsertProgramUnggulan(){
      $this->layout = false;
      if(!$id = Yii::$app->session->get('radiotoken')){
        return $this->goHome();
      }
      if(Yii::$app->request->post('id') == ''){
        $model = new RadioProgramUnggulan();
        $model->radio_id = $id;
      }else{
        $model = RadioProgramUnggulan::findOne(Yii::$app->request->post('id'));
      }
      $model->name = strip_tags(Yii::$app->request->post('name'));
      $model->listener = strip_tags(Yii::$app->request->post('listener'));
      $model->kind_program = strip_tags(Yii::$app->request->post('kind_program'));
      $model->music = strip_tags(Yii::$app->request->post('music'));
      $model->content_information = Yii::$app->request->post('description');
      $model->average_listener = strip_tags(Yii::$app->request->post('average_listener'));
      $model->save();
    }

    public function actionDeleteProgram(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->request->post('id')){
        $model = RadioProgramUnggulan::findOne($id);
        $model->delete();
      }else{
        return $this->goHome();
      }
    }

    public function actionEditProgram(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->request->post('id')){
        $model = RadioProgramUnggulan::findOne($id);
        $return = [
          'id' => $model->id,
          'name' => $model->name,
          'listener' => $model->listener,
          'kind_program' => $model->kind_program,
          'music' => $model->music,
          'content_information' => $model->content_information,
          'average_listener' => $model->average_listener,
        ];
        echo json_encode($return);
      }else{
        return $this->goHome();
      }
    }


    public function actionApiRadio(){
      if(Yii::$app->request->isAjax and $id = Yii::$app->session->get('radiotoken')){
        $model = Radio::findOne($id);
        $model->api = Yii::$app->request->get('apinya');
        $model->save();
      }else{
          return $this->goHome();
        }
    }


    protected function convertSlug($text){
      $converted =  Helper::slugify($text);
      $model = NewsEvent::findOne(['slug'=>$converted]);
      if($model){
        $get_last_one = substr($converted,-1);
        if(is_numeric($get_last_one)){
          $count = intval($get_last_one) + 1;
          $ret = substr($converted,0,-1) .$count;
        }else{
          $ret = $converted.'-2';
        }
        return $ret;
      }else{
        return $converted;
      }

    }

    public function actionForgotPassword(){
      $this->layout = false;
      return $this->render('forgot-password');
    }
    public function actionCheckEmail(){
      $data = RadioPersonal::findOne(['email'=>Yii::$app->request->post('email')]);
      if($data){
        echo 'true';
      }else{
        echo 'false';
      }
    }

    public function actionInsertForgot(){
      if(Yii::$app->request->isAjax){

        $data = RadioPersonal::findOne(['email'=>Yii::$app->request->post('email')]);
        RadioForgot::updateAll(['status' => 'fail'], 'status = "pending" AND radio_id = '.$data->radio_id);
        $model = new RadioForgot();
        $model->radio_id = $data->radio_id;
        $model->token = md5(date('Y-m-d H:i:s'));
        $model->save();
        $return_email = [
          'model' => $model
        ];
        Yii::$app->mail->compose('forgot-password-radio',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'Forgot Password Radio House'])
          ->setTo($data->email)
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('Forgot Password Radio House')
          ->send();
      }

    }

    public function actionChangePasswordForgot($id){
      $validate = RadioForgot::findOne(['token'=>$id,'status'=>'pending']);
      if($validate){
        return $this->render('change-password-forgot-radio',[
          'model' => $validate
        ]);
      }else{
        throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
      }
    }

    public function actionNewForgot(){

      if(Yii::$app->request->post('radio_id') and Yii::$app->request->isAjax){
        $model = RadioForgot::findOne(['status'=>'pending','radio_id'=>Yii::$app->request->post('radio_id')]);
        $model->status = 'used';
        $model->save();

        $data = Radio::findOne($model->radio_id);
        $data->password = password_hash(Yii::$app->request->post('password'), PASSWORD_DEFAULT);
        $data->save();
      }else{
        return $this->goHome();
      }
    }

    public function actionDataTicket(){
      if(Yii::$app->request->isAjax){
        $this->layout = false;
        $radioid = Yii::$app->session->get('radiotoken');
        $news_events = NewsEvent::find()->where(['radio_id'=>$radioid])->all();
        $news_events_data = [];
        foreach($news_events as $a){
          array_push($news_events_data,$a->id);
        }

        $datum = EventJoin::find()
        ->andWhere(['in','news_event_id',$news_events_data])
        ->andWhere(['in','status',['success','failed','cancel']])
        ->orderBy(['id'=>SORT_DESC])
        ->all();

        return $this->render('data-ticket',[
          'datum' => $datum
        ]);
      }else{
        return $this->goHome();
      }
    }


    public function actionDataTicketDetail(){
      if(Yii::$app->request->isAjax){
        $this->layout = false;
        $data = EventJoin::findOne(Yii::$app->request->post('id'));
        return $this->render('data-ticket-detail',[
          'data' => $data
        ]);
      }else{
        return $this->goHome();
      }
    }

    protected function findModel($id)
    {
        if (($model = Radio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
