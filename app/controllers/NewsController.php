<?php

namespace app\controllers;
use app\models\NewsEvent;

class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->goHome();
    }

    public function actionDetail($slug){
      $data = NewsEvent::findOne(['slug'=>$slug,'type' => 'news']);
      $latestNewsEvent = NewsEvent::find()
      ->orderBy(['id'=>SORT_DESC])
      ->limit(4)
      ->all();
      if($data){
        return $this->render('detail',[
          'model' => $data,
          'latest' => $latestNewsEvent
        ]);
      }else{
        return $this->goHome();
      }

    }

}
