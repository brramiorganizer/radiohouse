<?php

namespace app\controllers;

use Yii;
use app\models\AccountAdmin;
use app\models\AccountLogged;
use app\helper\Helper;

class BackstageController extends \yii\web\Controller
{
    public function __construct($id, $module, $config = [])
    {
      parent::__construct($id, $module, $config);
      $this->layout = 'backstage';
    }
    public function actionIndex()
    {
      $this->layout = false;
      if(Yii::$app->session->get('domtoken')){
        return $this->redirect(['dashboard/']);
      }
      return $this->render('login');
    }

    public function actionLogining(){
      $request = Yii::$app->request;
      $return['status'] = false;
      if($request->isAjax and $username = $request->post('username') and $password = $request->post('password')) {
        $check = AccountAdmin::findOne(['username'=>$username]);
        if($check){
          if (password_verify($password, $check->password)) {
              $return['status'] = true;
              Yii::$app->session->set('domtoken', $check->token);
              $logged = new AccountLogged();
              $logged->account = 'admin';
              $logged->account_id = $check->id;
              $logged->ip = Helper::get_client_ip();
              $logged->device = Helper::getDevice();
              $logged->save();
          }
        }
      }

      echo json_encode($return);
    }
    public function actionLogout(){
      Yii::$app->session->remove('domtoken');
      return $this->redirect(['index']);
    }

}
