<?php
namespace app\controllers;

use Yii;
use app\models\AccountMember;
use app\models\AccountLogged;
use app\helper\Helper;
use app\models\SliderHome;
use app\models\Radio;
use app\models\AdvertismentRadio;
class SiteController extends \yii\web\Controller{


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
              'class' => 'yii\authclient\AuthAction',
              'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }


    public function actionIndex()
    {
      $sliders = SliderHome::find()
        ->andWhere(['status'=>'active'])
        ->orderBy('sort asc')
        ->all();

      $main = AdvertismentRadio::find()
      ->andWhere(['status'=>'active'])
      ->andWhere(['>=','expired',date('Y-m-d H:i:s')])->one();
        return $this->render('index',[
          'sliders' => $sliders,
          'main' => $main
         ]);
    }

    public function actionTest(){
      $return_email = [
        'agung' => 'oke'
      ];
      Yii::$app->mail->compose('test',$return_email)
        ->setFrom(['radiohouse.id@gmail.com'=>'New Member  Radio House'])
        ->setTo('agungseptiyadi.74@gmail.com')
        ->setSubject('New Member  Radio House')
        ->send();
    }

    public function actionRegisterMember(){
      $request = Yii::$app->request;
      $return['status'] = false;
      if($request->isAjax){
        $model = new AccountMember();
        $model->firstname = strip_tags($request->post('firstname'));
        $model->lastname = strip_tags($request->post('lastname'));
        $model->email = $request->post('email');
        $model->password = password_hash($request->post('password'), PASSWORD_DEFAULT);
        $model->token = md5(date('Y-m-d H:i:s'));
        $model->type = 'web';
        $model->save();

        $return_email = [
          'name' => $model->firstname . ' ' . $model->lastname,
          'token' => $model->token
        ];
        Yii::$app->mail->compose('active-account',$return_email)
          ->setFrom(['radiohouse.id@gmail.com'=>'New Member  Radio House'])
          ->setTo($model->email)
          ->setSubject('New Member  Radio House')
          ->send();
          $return['status'] = true;
      }

      echo json_encode($return);
    }

    public function actionLoginMember(){
      $request = Yii::$app->request;
      $return['status'] = false;
      if($request->isAjax and $email = $request->post('email') and $password = $request->post('password')) {
        $check = AccountMember::findOne(['email'=>$email,'is_active'=>0]);
        if($check){
          if (password_verify($password, $check->password)) {
              $return['status'] = true;
              $data = [
                'type' => 'member',
                'token' => $check->token
              ];

              $logged = new AccountLogged();
              $logged->account = 'member';
              $logged->account_id = $check->id;
              $logged->ip = Helper::get_client_ip();
              $logged->device = Helper::getDevice();
              $logged->save();

              Yii::$app->session->set('fronttoken',$data);
          }
        }
      }

      echo json_encode($return);
    }

    public function actionLogoutMember(){
      Yii::$app->session->remove('fronttoken');
      return $this->redirect('/');
    }


    public function oAuthSuccess($client) {

      $userAttributes = $client->getUserAttributes();

      if(substr($client->authUrl,8,16) == 'www.facebook.com'){
        if(!$member = AccountMember::findOne(['email'=>$userAttributes['email'],'type'=>'facebook'])){
          $token = md5(date('Y-m-d H:i:s'));
          $newmember = new AccountMember();
          $newmember->firstname = $userAttributes['first_name'];
          $newmember->lastname = $userAttributes['last_name'];
          $newmember->email = $userAttributes['email'];
          $newmember->password = '-';
          $newmember->token = $token;
          $newmember->type = 'facebook';
          $newmember->save();

        }else{
          $token = $member->token;
        }
      }else{

          $email = $userAttributes['emails'][0]['value'];
          $firstname = ($userAttributes['name']['givenName'] ? $userAttributes['name']['givenName'] : '');
          $lastname = ($userAttributes['name']['familyName'] ? $userAttributes['name']['familyName'] : '');
          if(!$member = AccountMember::findOne(['email'=>$email,'type'=>'google'])){
              $token = md5(date('Y-m-d H:i:s'));
              $newmember = new AccountMember();
              $newmember->firstname = $firstname;
              $newmember->lastname = $lastname;
              $newmember->email = $email;
              $newmember->password = '-';
              $newmember->token = $token;
              $newmember->type = 'google';
              $newmember->save();

              $user = Member::findOne(['token'=>$token]);
              $bank = new Bank();
              $bank->member_id = $user->id;
              $bank->save();
          }else{
              $token = $member->token;
          }
      }

      // Yii::$app->session->set('gtoken', $token);
      //
      return $this->redirect('/');

    }

    public function actionMemberActive($token){
      if($check = AccountMember::findOne(['token'=>$token])){
        $check->is_active = 0;
        $check->save();
        Yii::$app->session->setFlash('success_regis_member',$check->token);
      }
      return $this->goHome();
    }

    public function actionError(){
      return $this->render('error');
    }

}
