<?php

namespace app\controllers;
use Yii;
use app\helper\Helper;
use app\models\EventJoin;
use app\models\AccountMember;
use app\models\NewsEvent;

class HistoryController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if(Yii::$app->session->get('fronttoken')){
          $user = AccountMember::findOne(['token'=>Yii::$app->session->get('fronttoken')]);
          $transactions = EventJoin::find()
          ->orderBy(['id'=>SORT_DESC])
          ->andWhere(['account_member_id'=>$user->id])->all();
          return $this->render('index',[
            'transactions' => $transactions
          ]);
        }else{
          return $this->goHome();
        }

    }

}
