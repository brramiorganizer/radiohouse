<?php

namespace app\controllers;
use Yii;
use app\models\EventJoin;
use app\models\EventTicket;
use app\models\NewsEvent;
use app\models\Bank;
class PembayaranController extends \yii\web\Controller
{
    public function actionIndex()
    {
        //if(isset($_GET['token']) and $user = Yii::$app->session->get('fronttoken')){
        if(isset($_GET['token'])){
          $eventjoin = EventJoin::findOne(['code'=>$_GET['token'],'status'=>'pending']);
          if($eventjoin){
            $ticket = EventTicket::findOne(['id'=>$eventjoin->event_ticket_id]);
            $event = NewsEvent::findOne(['id'=>$ticket->news_event_id]);
            $banks = Bank::find()->andWhere(['is_delete'=>0])->all();

            $tickets = EventTicket::find()->andWhere(['news_event_id'=>$eventjoin->news_event_id])->all();
            return $this->render('index',[
              'event' => $eventjoin,
              'ticket' => $ticket,
              'model'=>$event,
              'banks' => $banks,
              'tickets' => $tickets

            ]);
          }else{
            return $this->goHome();
          }

        }else{
          return $this->goHome();
        }

    }

}
