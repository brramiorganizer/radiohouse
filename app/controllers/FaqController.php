<?php

namespace app\controllers;
use app\models\Faq;
class FaqController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $datum = Faq::find()->orderBy(['sort'=>SORT_ASC])->all();
        return $this->render('index',[
          'datum' => $datum
        ]);
    }

}
