<?php

namespace app\controllers;
use Yii;
use app\models\EventJoin;
use app\models\Bank;
use app\models\EventJoinPayment;
use app\models\EventJoinConfirmation;
class KonfirmasiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if(isset($_GET['token'])){
          $check = EventJoin::findOne(['code'=>$_GET['token'],'status'=>'choose']);
          if($check){
            $banks = Bank::find()->all();
            $payment = EventJoinPayment::findOne(['event_join_id'=>$check->id]);
            return $this->render('index',[
              'model' => $check,
              'banks' => $banks,
              'payment' => $payment
            ]);
          }else{
            return $this->goHome();
          }

        }else {
          return $this->goHome();
        }

    }

    public function actionPost(){
      if(Yii::$app->request->isAjax){
        $model = new EventJoinConfirmation();
        $model->event_join_id = Yii::$app->request->post('id');
        $model->bank_id = Yii::$app->request->post('bank');
        $model->rekening_name = Yii::$app->request->post('rekening_name');
        $model->rekening_number = Yii::$app->request->post('rekening_number');
        $model->total = Yii::$app->request->post('total_price');
        $model->description = Yii::$app->request->post('keterangan');
        if(isset($_FILES['banner']['name']) and $_FILES['banner']['name'] != ''){
          $file_name = $_FILES['banner']['name'];
          $file_tmp =$_FILES['banner']['tmp_name'];
          $tmp = explode('.', $file_name);
          $file_ext = end($tmp);
          $newfilename = md5(date('Y-m-d H:i:s')) .'.'. $file_ext;

          move_uploaded_file($file_tmp,"media/transfer/".$newfilename);
          $model->image = $newfilename;

        }
        $model->save();

        $edit = EventJoin::findOne($model->event_join_id);
        $edit->status = 'buying';
        $edit->save();

        $return_email = [
          'confirmation' => $model,
          'join' => $edit
        ];
        Yii::$app->mail->compose('purchase-ticket',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'(Purchase of Event Tickets)'])
          ->setTo('ryza.brramio@gmail.com')
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('(Purchase of Event Tickets)')
          ->send();

        Yii::$app->mail->compose('order-ticket-payment-confirmation',$return_email)
          ->setFrom(['radiohouse.id@gmail.com' =>'(Konfirmasi Pembayaran)'])
          ->setTo($edit->email)
          //->setTo('agungseptiyadi.74@gmail.com')
          ->setSubject('(Konfirmasi Pembyaran)')
          ->send();
        echo json_encode(['status'=>true]);
      }else{
        return $this->goHome();
      }
    }

}
