<?php

namespace app\controllers;
use app\models\Radio;
use app\models\RadioContent;
use app\models\NewsEvent;
use app\models\RadioPersonal;
use app\models\RadioCommittee;
use app\models\RadioProgram;
use app\models\RadioProgramUnggulan;
use yii\web\NotFoundHttpException;
use app\models\RadioVisit;
use app\helper\Helper;
class ChannelController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $search = (isset($_GET['s']) ? $_GET['s'] : '');
      $search_city = (isset($_GET['city']) ? $_GET['city'] : '');

      $count = Radio::find()
      ->andWhere(['status'=>'active'])
      ->andWhere(['LIKE', 'radio_name', $search])
      ->all();

      $perpage = 12;
      $total = count($count);
      $offset = 0;

      $pagination = $total / $perpage;
      if(isset($_GET['page']) and $_GET['page'] != '' and $_GET['page'] > 1){
        $offset = ($_GET['page'] - 1) * $perpage;
      }


      $radios = Radio::find()->andWhere(['status'=>'active'])->all();
      $radio_cities = Radio::find()
      ->groupBy(['city'])
      ->orderBy(['city'=>SORT_ASC])
      ->andWhere(['status'=>'active'])->all();

        $channels = Radio::find()
        ->joinWith('content')
        ->andWhere(['status'=>'active'])
        ->andWhere(['LIKE', 'radio_name', $search])
        ->andWhere(['LIKE', 'city', $search_city])
        ->orderBy(['id' => SORT_DESC])
        ->limit($perpage)
		    ->offset($offset)
        ->all();

        return $this->render('index',[
          'channels' => $channels,
          'pagination' => $pagination,
          'radios' => $radios,
          'cities' => $radio_cities
        ]);
    }

    public function actionDetail($slug)
    {

      if($radio = Radio::findOne(['slug'=>$slug])){
        $personal = RadioPersonal::findOne(['radio_id'=>$radio->id]);
        $committees = RadioCommittee::find()->andWhere(['radio_id'=>$radio->id])->all();
        $content = RadioContent::findOne(['radio_id'=>$radio->id]);
        $newsevents = NewsEvent::find()
        ->andWhere(['radio_id'=>$radio->id,'is_delete'=>0])
        ->orderBy(['id'=>SORT_DESC])
        ->all();
        $program = RadioProgram::findOne(['radio_id'=>$radio->id]);
        $unggulan = RadioProgramUnggulan::find()->andWhere(['radio_id'=>$radio->id])->all();

        $model = new RadioVisit();
        $model->ip = Helper::get_client_ip();
        $model->device = Helper::getDevice();
        $model->radio_id = $radio->id;
        $model->save();

        return $this->render('detail',[
          'radio' => $radio,
          'content' => $content,
          'newsevents' => $newsevents,
          'personal' => $personal,
          'committees' => $committees,
          'program' =>$program,
          'unggulan' => $unggulan
        ]);
      }else{
        throw new NotFoundHttpException();
      }

    }



}
