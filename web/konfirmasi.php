<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img whiteText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Konfirmasi Pembayaran</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Konfirmasi Pembayaran</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="pageAccount pt-70 pb-60 pt-sm-60">
    
        <div class="container page-information-process page-konfirmasi">
            <div class="success-message text-center">
                <h1 class="mt-0 font-26">Konfirmasi Pembayaran</h1>
            </div>
            <div class="col-sm-8 col-sm-offset-2 main-konfirmasi well margintop-30">
                <form method="" action="">
                    <p>Silahkan Melakukan Konfirmasi pembayaran jika sudah mentransfer pembayarannya.</p>
                    <div class="form-group">
                        <label>Invoice</label>
                        <p class="well oID p-10"><strong>#123456</strong></p>
                    </div>
                    <div class="form-group">
                        <label>Transfer Ke</label>
                        <select class="form-control" id="destination">
                            <option>-- Pilih Bank --</option>
                            <option>Bank BCA (0987654321 a/n Radio House)</option>
                            <option>Bank Mandiri (0987654321 a/n Radio House)</option>
                            <option>Bank BNI (0987654321 a/n Radio House)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>No Rekening Anda</label>
                        <input type="text" name="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Nama Pemilik Rekening</label>
                        <input type="text" name="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Jumlah yang di Transfer</label>
                        <input type="text" name="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btnKonfrim">Konfirmasi Pembayaran</button>
                </form>
            </div>
		</div><!-- end container -->
    
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>