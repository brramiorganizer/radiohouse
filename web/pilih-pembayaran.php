<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img whiteText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Pilih Pembayaran</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Pilih Pembayaran</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="pagePembayaran pt-70 pb-70 pt-sm-60">
    
        <div class="container page-information-process page-payment">
            <div id="processRent">
                <form method="" action="terima-kasih.php">
                    <div class="col-md-7">
                        <h2 class="mt-0 mb-0">Informasi Pemesanan <a href="" class="text-green pull-right font-14 editInfo" data-toggle="modal" data-target="#editInformation"><i class="fa fa-edit"></i> Edit</a></h2>
                        <div class="table-responsive">
                            <h3 class="mt-30"><i class="fa fa-user mr-10"></i>Personal Data</h3>
                            <table class="table">
                                <tr>
                                    <td class="pl-25"><strong>Nama Lengkap</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Lorem Ipsum</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Email</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">example@email.com</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>No Identitas</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">0987654321</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>No Handphone</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">09870321332</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Alamat Lengkap</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Jl. Mendawai I No.21, RT.4/RW.7, Kramat Pela, Kby. Baru, Jakarta, Daerah Khusus Ibukota Jakarta 12130</td>
                                </tr>
                            </table>
                            <h3 class="mt-15"><i class="fa fa-ticket mr-10"></i>Tiket</h3>
                            <table class="table">
                                <tr>
                                    <td class="pl-25"><strong>Event</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Lorem Ipsum</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Tanggal Event</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">9 November 2017</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Kategori Tiket</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">VIP</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Jumlah</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">2 Tiket</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Kursi</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">VIP A 01</td>
                                </tr>
                            </table>
                        </div>
                        
                        <h2 class="mt-40">Pilih Pembayaran</h2>
                        <div class="contentPilihPembayaran mt-20">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="mainPembayaran text-center">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>                                       
                                            </label>
                                        </div>
                                        <div class="contBank">
                                            <img src="images/logo-bca.png" class="" />
                                            <p>Radio House</p>
                                            <p>0987654321</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="mainPembayaran text-center">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>                                       
                                            </label>
                                        </div>
                                        <div class="contBank">
                                            <img src="images/logo-mandiri.png" class="" />
                                            <p>Radio House</p>
                                            <p>0987654321</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="mainPembayaran text-center">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>                                       
                                            </label>
                                        </div>
                                        <div class="contBank">
                                            <img src="images/logo-bni.png" class="" />
                                            <p>Radio House</p>
                                            <p>0987654321</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-5">
                        <div class="sidebar-booking discount">
                            <h3 class="m-0 p-0 text-green">Invoice</h3>
                            <p class="text-right">RH-170414001</p>
                        </div>
                        <div class="sidebar-booking">
                            <h3 class="mt-0 mb-20 p-0 text-green">Keranjang</h3>
                            <div class="main-sidebar-booking">
                                <div class="row noteRent">
                                    <div class="col-xs-5">
                                        <h4 class="mb-5 pr-0">Nama Event</h4>
                                        <p class="small">Tanggal Event : <strong>09/11/2017</strong></p>
                                    </div>
                                    <div class="col-xs-1 text-center">1</div>
                                    <div class="col-xs-6 pl-0">IDR 1.000.000</div>
                                </div>
                                <hr />
                                <!--<div class="row">
                                    <div class="col-xs-5">
                                        <p class="font-15">Shipping</p>
                                    </div>
                                    <div class="col-xs-1 text-center">
                                    </div>
                                    <div class="col-xs-6">
                                        <p>IDR 100.000</p>
                                    </div>
                                </div>
                                <hr />-->
                                <span class="totalPrice"><strong>Total</strong> IDR 1.000.000</span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="sidebar-booking discount sidebarKodeVoucher">
                                <h3>Kode Voucher</h3>
                                <div class="form-group">
                                    <label class="sr-only">Kode Voucher</label>
                                    <input type="text" class="form-control" placeholder=""/>
                                    <button type="submit" class="btn btn-primary pull-right btn-disc">Apply</button>
                                </div>
                            </div>
                        </div>
                        <div class="btnProses">
                            <button type="submit" class="btn btn-primary pull-right btn-lg">Pesan Sekarang</a>
                        </div>
                    </div>                    
                </form>
            </div>
		</div><!-- end container -->
        
        
        <!-- Modal Edit Information -->
        <div class="modal fade s1" id="editInformation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <form action="" method="">
                      <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Pemesanan</h4>
                      </div>
                      <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="" action="pilih-pembayaran.php">
                                        <p>Silahkan mengisi form pembelian tiket dibawah ini.</p>
                                        <div class="headTiket mt-20 mb-20">
                                            <h2 class="mt-0 pb-0">Personal Data</h2>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Lengkap <span class="sparator">*</span></label>
                                            <input type="text" name="" class="form-control"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Email <span class="sparator">*</span></label>
                                            <input type="email" name="" class="form-control"/>
                                        </div>
                                        <div class="form-group">
                                            <label>No Identitas <span class="sparator">*</span></label>
                                            <input type="text" name="" class="form-control"/>
                                        </div>
                                        <div class="form-group">
                                            <label>No Handphone <span class="sparator">*</span></label>
                                            <input type="text" name="" class="form-control"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Lengkap <span class="sparator">*</span></label>
                                            <textarea class="form-control" rows="3"></textarea>
                                        </div>
                                        
                                        <div class="headTiket mt-40 mb-20">
                                            <h2 class="mt-0 pb-0">Tiket</h2>
                                        </div>
                                        <div class="form-group">
                                            <label>Event <span class="sparator">*</span></label>
                                            <select class="form-control">
                                                <option>-- Pilih Event --</option>
                                                <option>Lorem Ipsum - 9 September 2017</option>
                                                <option>Lorem Ipsum - 10 November 2017</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Kategori Tiket <span class="sparator">*</span></label>
                                            <select class="form-control">
                                                <option>-- Pilih Kategori --</option>
                                                <option>Umum</option>
                                                <option>VIP</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Jumlah <span class="sparator">*</span></label>
                                            <input type="text" name="" class="form-control"/>
                                        </div>
                                        <!--<div class="form-group mb-10">
                                            <label>Kursi <span class="sparator">*</span></label>
                                            <div class="mainTiket">
                                                <div class="contentLayar"> 
                                                    <h3 class="m-0 p-0">Layar / Panggung</h3>
                                                </div>
                                                <div class="contentSheet">
                                                    <div class="row">
                                                        <div class="col-xs-5 text-left pr-0 pr-sm-15">
                                                            <div class="radio-group">
                                                                <input type="radio" id="option-one" name="selector"><label for="option-one">VIP A 01</label>
                                                                <input type="radio" id="option-two" name="selector"><label for="option-two">VIP A 02</label>
                                                                <input type="radio" id="option-three" name="selector"><label for="option-three">VIP A 03</label>
                                                                <input type="radio" id="option-four" name="selector"><label for="option-four">VIP A 04</label>
                                                                <input type="radio" id="option-five" name="selector"><label for="option-five">VIPA 05</label>
                                                                <input type="radio" id="option-six" name="selector"><label for="option-six">VIP A 06</label>
                                                            </div>
                                                            <div class="radio-group">
                                                                <input type="radio" id="option-seven" name="selector"><label for="option-seven">VIP A 07</label>
                                                                <input type="radio" id="option-eight" name="selector"><label for="option-eight">VIP A 08</label>
                                                                <input type="radio" id="option-nine" name="selector"><label for="option-nine">VIP A 09</label>
                                                                <input type="radio" id="option-ten" name="selector"><label for="option-ten">VIP A 10</label>
                                                                <input type="radio" id="option-11" name="selector"><label for="option-11">VIP A 11</label>
                                                                <input type="radio" id="option-12" name="selector"><label for="option-12">VIP A 12</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 text-right pl-0 pl-sm-15">
                                                            <div class="radio-group">
                                                                <input type="radio" id="option-one-b1" name="selector"><label for="option-one-b1">VIP B 01</label>
                                                                <input type="radio" id="option-two-b2" name="selector"><label for="option-two-b2">VIP B 02</label>
                                                                <input type="radio" id="option-three-b3" name="selector"><label for="option-three-b3">VIP B 03</label>
                                                                <input type="radio" id="option-four-b4" name="selector"><label for="option-four-b4">VIP B 04</label>
                                                                <input type="radio" id="option-five-b5" name="selector"><label for="option-five-b5">VIP B 05</label>
                                                                <input type="radio" id="option-six-b6" name="selector"><label for="option-six-b6">VIP B 06</label>
                                                            </div>
                                                            <div class="radio-group">
                                                                <input type="radio" id="option-seven-b7" name="selector"><label for="option-seven-b7">VIP B 07</label>
                                                                <input type="radio" id="option-eight-b8" name="selector"><label for="option-eight-b8">VIP B 08</label>
                                                                <input type="radio" id="option-nine-b9" name="selector"><label for="option-nine-b9">VIP B 09</label>
                                                                <input type="radio" id="option-ten-b10" name="selector"><label for="option-ten-b10">VIP B 10</label>
                                                                <input type="radio" id="option-ten-b11" name="selector"><label for="option-ten-b11">VIP B 11</label>
                                                                <input type="radio" id="option-ten-b12" name="selector"><label for="option-ten-b12">VIP B 12</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                    </form>
                                </div>
                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="button" class="btn btn-primary">Update</button>
                      </div>
                  </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>