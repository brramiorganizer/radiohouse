<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_1.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Tentang Radio House</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-60 pb-0">
    	<div class="container mb-100">
        	
            <div class="content-about-up mb-60">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
            </div>
            
            <div class="content-about-middel mb-80">
                <div class="row m-0">
                    <div class="stat f-container">
    					<div class="f-element col-lg-4 col-sm-4">
    						<div class="milestone-counter mb-sm-30">
                            	<i class="fa fa-user fa-3x text-green"></i>
                                <div class="milestone-details text-4e4e4e mt-30">Happy Customers</div>
    						</div>
    					</div>
    					<div class="f-element col-lg-4 col-sm-4">
    						<div class="milestone-counter mb-sm-30">
                            	<i class="fa fa-coffee fa-3x text-green"></i>
                                <div class="milestone-details text-4e4e4e mt-30">Ordered Coffee's</div>
    						</div>
    					</div>
    					<div class="f-element col-lg-4 col-sm-4 ">
    						<div class="milestone-counter mb-sm-30">
                            	<i class="fa fa-trophy fa-3x text-green"></i>
                                <div class="milestone-details text-4e4e4e mt-30">Awards Win</div>
    						</div>
    					</div>
    				</div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="contentVideo mb-80">
                <div class="row m-0">
                    <div class="col-sm-6">
                        <div class="videoAbout embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="http://player.vimeo.com/video/72960194?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff"></iframe>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contentAbout">
                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="bannerProductAbout">
                <a href="#"><img src="demos/1170x378.png" class="img-responsive" /></a>
            </div> 
    	</div><!-- end container -->
                
        <!--<div class="row ml-0 mr-0 secAnyQuestion">
            <div class="col-sm-12 p-0">
                <div class="bg-gray pt-50 pb-50 pl-xs-15 pr-xs-15">
                    <h1 class="m-0 p-0 text-center text-white font-36"><span class="font-familly-proximareg">ANY QUESTION?</span> <strong><a href="<?php echo 'contact.php';?>" class="contact-about">CONTACT US</a></strong></h1>
                </div>
            </div>
        </div>-->
    </section><!--end white-wrapper -->
    
    <section id="one-parallax" class="parallax category-channel" style="background-image: url('demos/parallax_04.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay pt-125 pb-95">
            <div class="container-fluid p-0">
                <div class="row ml-0 mr-0 secAnyQuestion">
                    <div class="col-sm-12 p-0">
                        <div class="pl-xs-15 pr-xs-15">
                            <h1 class="m-0 p-0 text-center text-white font-36"><span class="font-familly-proximareg">ANY QUESTION?</span> <strong><a href="<?php echo 'contact.php';?>" class="contact-about">CONTACT US</a></strong></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<?php include 'layout/footer.php'; ?>