<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>

	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_3.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Hubungi Kami</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Hubungi Kami</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->

    <section class="white-wrapper nopadding">
    	<div id="map">
            <iframe src="https://snazzymaps.com/embed/23679" width="100%" height="450px" style="border:none;"></iframe>
        </div>
        
        <div class="clearfix"></div>
        
        <div class="container pt-60 pb-40">
        
            <div class="contact_form m-0">
                <div id="message"></div>
                <form id="contactform" action="contact.php" name="contactform" method="post">
    				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    					<input type="text" name="name" id="name" class="form-control" placeholder="Name"> 
    					<input type="text" name="email" id="email" class="form-control" placeholder="Email">
    				</div>
    				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
    					<textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>
    					<button type="submit" value="SEND" id="submit" class="btn btn-lg btn-primary pull-right mt-20">Kirim</button>
    				</div>
                </form>    
            </div><!-- end contact-form -->
            
            <div class="clearfix"></div>
            
            <div class="row padding-top margin-top">
    			<div class="contact_details">
    				<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
    					<div class="text-center">
    						<div class="">
    							<div class="contact-icon">
    								<a href="#" class=""> <i class="fa fa-map-marker fa-3x text-green"></i> </a>
    							</div><!-- end dm-icon-effect-1 -->
                                 <p>Jl. Mendawai I No.21, RT.4/RW.7, Kramat Pela, Kby. Baru, Jakarta, Daerah Khusus Ibukota Jakarta 12130</p>
    						</div><!-- end service-icon -->
    					</div><!-- end miniboxes -->
    				</div><!-- end col-lg-4 -->
                    
    				<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
    					<div class="text-center">
    						<div class="">
    							<div class="contact-icon">
    								<a href="#" class=""> <i class="fa fa-phone fa-3x text-green"></i> </a>
    							</div><!-- end dm-icon-effect-1 -->
                                 <p><strong>Phone:</strong> (1005) 5999 4446<br>
    							<strong>Phone:</strong> (0422) 5999 4446</p>
    						</div><!-- end service-icon -->
    					</div><!-- end miniboxes -->
    				</div><!-- end col-lg-4 -->  
    
    				<div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
    					<div class="text-center">
    						<div class="">
    							<div class="contact-icon">
    								<a href="#" class=""> <i class="fa fa-envelope-o fa-3x text-green"></i> </a>
    							</div><!-- end dm-icon-effect-1 -->
                                 <p><strong>Email:</strong> info@radiohouse.id</p>
    						</div><!-- end service-icon -->
    					</div><!-- end miniboxes -->
    				</div><!-- end col-lg-4 -->                  
    			</div><!-- end contact_details -->
            </div><!-- end margin-top -->
        </div><!-- end container -->
    </section><!-- end map wrapper -->

	<?php include 'layout/footer.php'; ?>