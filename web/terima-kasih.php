<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<!-- <section class="banner-img whiteText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Pilih Pembayaran</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Pilih Pembayaran</h1>
                </div>
            </div>
        </div>
    </section>end post-wrapper-top -->
    
    <section class="notification pt-60">
        <div class="container">
            <div class="row m-0">
                <div class="col-md-12 success-message text-center bg-success">
                    <p><i class="fa fa-check"></i></p>
                    <p>Terima Kasih telah melakukan pembelian tiket di Radio House</p>
                </div>
            </div>
        </div>
    </section>
    
    <section class="pagePembayaran pt-60 pb-70 pt-sm-60">
    
        <div class="container page-information-process page-payment">
            <div id="processRent">
                <form method="" action="">
                    <div class="col-md-7">
                        <h2 class="mt-0 mb-0">Detail Pemesanan</h2>
                        <div class="table-responsive">
                            <h3 class="mt-30"><i class="fa fa-user mr-10"></i>Personal Data</h3>
                            <table class="table">
                                <tr>
                                    <td class="pl-25"><strong>Nama Lengkap</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Lorem Ipsum</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Email</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">example@email.com</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>No Identitas</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">0987654321</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>No Handphone</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">09870321332</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Alamat Lengkap</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Jl. Mendawai I No.21, RT.4/RW.7, Kramat Pela, Kby. Baru, Jakarta, Daerah Khusus Ibukota Jakarta 12130</td>
                                </tr>
                            </table>
                            <h3 class="mt-15"><i class="fa fa-ticket mr-10"></i>Tiket</h3>
                            <table class="table">
                                <tr>
                                    <td class="pl-25"><strong>Event</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">Lorem Ipsum</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Tanggal Event</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">9 November 2017</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Kategori Tiket</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">VIP</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Jumlah</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">2 Tiket</td>
                                </tr>
                                <tr>
                                    <td class="pl-25"><strong>Kursi</strong></td>
                                    <td class="text-center">:</td>
                                    <td class="" style="width: 450px;">VIP A 01</td>
                                </tr>
                            </table>
                        </div>
                        
                        <h2 class="mt-40 mb-30">Jumlah Transfer</h2>
                        <div class="TotalTrasfer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="text-green T1">IDR 1.100.123</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="well">
                                        Mohon transfer sesuai jumlah yang tertera (termasuk 3 digit terakhir)
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <h2 class="mt-40">Bank Tujuan Transfer</h2>
                        <div class="contentPilihPembayaran mt-20">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="mainPembayaran text-center">
                                        <img src="images/logo-bca.png" class="" />
                                        <p >Radio House</p>
                                        <p>0987654321</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h2 class="mt-40">Konfirmasi Pembayaran</h2>
                        <div class="konfTrans marginbottom-40">
                            <p>Setelah Anda melakukan transfer, segera konfirmasikan dengan menekan tombol di bawah ini.</p>
                            <a class="btn btn-primary display-block font-18" href="konfirmasi.php">Konfirmasi Pembayaran</a>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-5">
                        <div class="sidebar-booking discount">
                            <h3 class="m-0 p-0 text-green">Invoice</h3>
                            <p class="text-right">RH-170414001</p>
                        </div>
                        <div class="sidebar-booking">
                            <h3 class="mt-0 mb-20 p-0 text-green">Keranjang</h3>
                            <div class="main-sidebar-booking">
                                <div class="row noteRent">
                                    <div class="col-xs-5">
                                        <h4 class="mb-5 pr-0">Nama Event</h4>
                                        <p class="small">Tanggal Event : <strong>09/11/2017</strong></p>
                                    </div>
                                    <div class="col-xs-1 text-center">1</div>
                                    <div class="col-xs-6 pl-0">IDR 1.000.000</div>
                                </div>
                                <hr />
                                <!--<div class="row">
                                    <div class="col-xs-5">
                                        <p class="font-15">Shipping</p>
                                    </div>
                                    <div class="col-xs-1 text-center">
                                    </div>
                                    <div class="col-xs-6">
                                        <p>IDR 100.000</p>
                                    </div>
                                </div>
                                <hr />-->
                                <span class="totalPrice"><strong>Total</strong> IDR 1.000.000</span>
                            </div>
                        </div>
                        <!--<div class="sidebar-booking discount">
                            <h3>KODE PROMOSI</h3>
                            <div class="form-group">
                                <label class="sr-only">Kode Promosi</label>
                                <input type="text" class="form-control" placeholder="Kode Promosi"/>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Apply</button>
                        </div>-->
                    </div>                    
                </form>
            </div>
		</div><!-- end container -->
    
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>