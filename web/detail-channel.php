<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    <audio class="hidden" controls="controls"><source src="http://45.64.98.181:8323/stream" type="audio/mpeg" /></audio>
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_1.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner detail-page-other">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Tentang Radio House</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <!-- Radio Player -->
    <section class="radio-palyer bg-green-primary">
        <div class="container">
            <div class="row m-0">
                <div class="col-sm-4 img-thumbRadioChannel hidden-mobileTablet">
                    <div class="widget pl-15 pr-60">
                        <img src="images/channel/imges-channel-radio-34.jpg" class="img-responsive" />
                    </div>
                </div>
                <div class="col-sm-8 pt-15 pb-15 pr-sm-0 pl-sm-0 overflowMobile playerDetailChannel">
                    <div id="audio-player">
                        <div id="buttons">
            				<span>
            					<button id="play"></button>
            					<button id="pause"></button>
            				</span>
            			</div>
            			<div id="tracker">
                            <div class="hidden-dekstop">
                                <h3 class="p-0 m-0">Radio House</h3>
                                <p class="">Jakarta</p>
                            </div>
            				<div id="progress-bar">
            					<span id="progress"></span>
                                <p class="timer-player">0:00</p>
            				</div>
            			</div>
                        <div id="volume">
                            <span class="dropdown-hover">
                                <button id="volumeup"></button>
            					<button id="volumemute"></button>
                            </span>
                            <div class="dropdown-menu-volume">
                                <div id="master" style="width:100px;"></div>
                            </div>
                        </div>
                        <div class="button-share-radio ml-xs-10">
                            <span class="dropdown">
                                <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span></span></a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="#" class="goog"><i class="fa fa-google-plus"></i></li></a>
                                    <li><a href="#" class="face"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twit"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end Radio Player -->
    
    <section class="page-under pt-60 pb-60 detail-channel">
    	<div class="container">
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mt-165 mt-sm-0 pr-sm-15">
            
                <!-- images Radio Channel Mobile -->
                <div class="imgRadioChannel">
                    <div class="widget mb-30 mb-sm-40 hidden-dekstop bannerRadio">
                        <img src="images/channel/imges-channel-radio-34.jpg" class="img-responsive" />
                    </div>
                </div>
                <!-- end image radio channel mobile -->
                
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale hiddenMobile">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
                
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 radio-channel pb-0">
                <div class="row m-0 header-detail-channel">
                    <div class="breadcrumb-content">
                        <ul class="m-0">
                            <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                            <li><a href="<?php echo 'channel.php'; ?>">Channel</a></li>
                            <li class="text-green">Raya FM</li>
                        </ul>
                    </div>
                    <div class="title">
                        <h2 class="mt-10 p-0 mb-0">Raya FM</h2>
                        <p>Banyuwangi</p>
                    </div><!-- end title -->
                    <div class="description">
                        <h2 class="p-0 mb-5">Description <span class="text-gray-silver">Radio Name</span></h2>
                        <p class="p-grid">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus posuere eget turpis a feugiat. Nullam pharetra ac ex vulputate semper. Morbi bibendum nulla eget nibh lobortis, a sollicitudin ante rutrum. In euismod lectus at varius pulvinar. Nunc vitae ante aliquam, porta lacus sit amet, scelerisque odio. Quisque ut fringilla erat. Phasellus molestie sollicitudin nulla eu congue. Donec mollis sed neque venenatis bibendum.</p>
                        <p class="p-grid">Suspendisse non lacus at metus tincidunt hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas dapibus mi quis pulvinar pharetra. Donec urna velit, suscipit ut rhoncus eu, dapibus quis dui. Vestibulum ipsum leo, auctor ac purus volutpat, gravida feugiat quam. Pellentesque facilisis rhoncus libero non fermentum. Nulla ut tortor ultricies, pellentesque magna non, viverra tortor. Sed imperdiet tincidunt lacus a placerat. Curabitur ultrices, enim ut volutpat pharetra, nulla tortor porttitor libero, ut congue nibh metus sit amet ante. Ut gravida, sem in sagittis ecitur, urna tellus rhoncus ante, sed lobortis neque sapien bibendum turpis. Ut leo diam, rutrum et turpis venenatis, commodo mollis magna. Phasellus hendrerit est sed eleifend ullamcorper. Phasellus facilisis sapien mollis, malesuada diam sed, pulvinar sem.</p>
                        <p class="p-grid">Suspendisse non lacus at metus tincidunt hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas dapibus mi quis pulvinar pharetra. Donec urna velit, suscipit ut rhoncus eu, dapibus quis dui. Vestibulum ipsum leo, auctor ac purus volutpat, gravida feugiat quam. Pellentesque facilisis rhoncus libero non fermentum. Nulla ut tortor ultricies, pellentesque magna non, viverra tortor. Sed imperdiet tincidunt lacus a placerat. Curabitur ultrices, enim ut volutpat pharetra, nulla tortor porttitor libero, ut congue nibh metus sit amet ante. Ut gravida, sem in sagittis ecitur, urna tellus rhoncus ante, sed lobortis neque sapien bibendum turpis. Ut leo diam, rutrum et turpis venenatis, commodo mollis magna. Phasellus hendrerit est sed eleifend ullamcorper. Phasellus facilisis sapien mollis, malesuada diam sed, pulvinar sem.</p>
                        <p class="p-grid">Suspendisse non lacus at metus tincidunt hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas dapibus mi quis pulvinar pharetra. Donec urna velit, suscipit ut rhoncus eu, dapibus quis dui. Vestibulum ipsum leo, auctor ac purus volutpat, gravida feugiat quam. Pellentesque facilisis rhoncus libero non fermentum. Nulla ut tortor ultricies, pellentesque magna non, viverra tortor. Sed imperdiet tincidunt lacus a placerat. Curabitur ultrices, enim ut volutpat pharetra, nulla tortor porttitor libero, ut congue nibh metus sit amet ante. Ut gravida, sem in sagittis ecitur, urna tellus rhoncus ante, sed lobortis neque sapien bibendum turpis. Ut leo diam, rutrum et turpis venenatis, commodo mollis magna. Phasellus hendrerit est sed eleifend ullamcorper. Phasellus facilisis sapien mollis, malesuada diam sed, pulvinar sem.</p>
                        <p class="p-grid">Suspendisse non lacus at metus tincidunt hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas dapibus mi quis pulvinar pharetra. Donec urna velit, suscipit ut rhoncus eu, dapibus quis dui. Vestibulum ipsum leo, auctor ac purus volutpat, gravida feugiat quam. Pellentesque facilisis rhoncus libero non fermentum. Nulla ut tortor ultricies, pellentesque magna non, viverra tortor. Sed imperdiet tincidunt lacus a placerat. Curabitur ultrices, enim ut volutpat pharetra, nulla tortor porttitor libero, ut congue nibh metus sit amet ante. Ut gravida, sem in sagittis ecitur, urna tellus rhoncus ante, sed lobortis neque sapien bibendum turpis. Ut leo diam, rutrum et turpis venenatis, commodo mollis magna. Phasellus hendrerit est sed eleifend ullamcorper. Phasellus facilisis sapien mollis, malesuada diam sed, pulvinar sem.</p>
                        <p class="p-grid">Suspendisse non lacus at metus tincidunt hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas dapibus mi quis pulvinar pharetra. Donec urna velit, suscipit ut rhoncus eu, dapibus quis dui. Vestibulum ipsum leo, auctor ac purus volutpat, gravida feugiat quam. Pellentesque facilisis rhoncus libero non fermentum. Nulla ut tortor ultricies, pellentesque magna non, viverra tortor. Sed imperdiet tincidunt lacus a placerat. Curabitur ultrices, enim ut volutpat pharetra, nulla tortor porttitor libero, ut congue nibh metus sit amet ante. Ut gravida, sem in sagittis ecitur, urna tellus rhoncus ante, sed lobortis neque sapien bibendum turpis. Ut leo diam, rutrum et turpis venenatis, commodo mollis magna. Phasellus hendrerit est sed eleifend ullamcorper. Phasellus facilisis sapien mollis, malesuada diam sed, pulvinar sem.</p>
                        <a href="#" class="text-green" id="viewMore"><i class="fa fa-plus-square-o"></i> View More</a>
                    </div>
                </div>
                <div class="row news-detail-channel m-0">
                    <div class="well mt-40 pr-xs-10 pl-xs-10">
                        <div class="title-well">
                            <h2 class="p-0 mb-5 mt-0">News &amp; Events <span class="text-gray-silver">Radio Name</span></h2>
                        </div>
                        <div class="slide-news-event">
                            <div id="owl-blog" class="owl-carousel mt-35 mt-xs-70">
                            
                                <div class="blog-carousel">
                                    <div class="entry">
                                        <a title="" href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_1.jpg" alt="" class="img-responsive"></a>
                                    </div><!-- end entry -->
                                    <div class="bg-white p-20">
                                        <div class="blog-carousel-header">
                                            <h3 class="p-0 mt-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">New Graphic Designer on the block</a></h3>
                                            <div class="blog-carousel-meta">
                                                <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                            </div><!-- end blog-carousel-meta -->
                                        </div><!-- end blog-carousel-header -->
                                        <div class="blog-carousel-desc">
                                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                            <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                        </div><!-- end blog-carousel-desc -->
                                    </div>
                                </div><!-- end blog-carousel -->
                                <div class="blog-carousel">
                                    <div class="entry">
                                        <a title="" href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_2.jpg" alt="" class="img-responsive"></a>
                                    </div><!-- end entry -->
                                    <div class="bg-white p-20">
                                        <div class="blog-carousel-header">
                                            <h3 class="p-0 mt-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">New Graphic Designer on the block</a></h3>
                                            <div class="blog-carousel-meta">
                                                <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                            </div><!-- end blog-carousel-meta -->
                                        </div><!-- end blog-carousel-header -->
                                        <div class="blog-carousel-desc">
                                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                            <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                        </div><!-- end blog-carousel-desc -->
                                    </div>
                                </div><!-- end blog-carousel -->
                                <div class="blog-carousel">
                                    <div class="entry">
                                        <a title="" href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_3.jpg" alt="" class="img-responsive"></a>
                                    </div><!-- end entry -->
                                    <div class="bg-white p-20">
                                        <div class="blog-carousel-header">
                                            <h3 class="p-0 mt-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">New Graphic Designer on the block</a></h3>
                                            <div class="blog-carousel-meta">
                                                <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                            </div><!-- end blog-carousel-meta -->
                                        </div><!-- end blog-carousel-header -->
                                        <div class="blog-carousel-desc">
                                            <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                            <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                        </div><!-- end blog-carousel-desc -->
                                    </div>
                                </div><!-- end blog-carousel -->
                                     
                            </div><!-- end owl-blog -->
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    
                    <!-- images banner sale -->
                    <div class="imageBannerSale hidden-dekstop viewMobile mt-40 border-top-1px">
                        <div class="widget mb-30">
                            <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                        </div>
                        <div class="widget mb-30">
                            <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                        </div>
                    </div>
                    <!-- end images banner sale -->
                    
                </div>
            </div><!-- end content -->
                    
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>