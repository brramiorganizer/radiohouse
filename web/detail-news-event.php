<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner detail-page-other">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Tentang Radio House</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-70 pb-60 pt-sm-60">
    	<div class="container">
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 detail-news col-xs-12 pr-xs-15 pr-sm-0">
                <div class="breadcrumb-sidebar">
                    <ul class="m-0">
                        <li><a href="<?php echo 'detail-news-event.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="title-detail-news text-green">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                    </ul>
                </div>
                <div class="widget mt-20 mb-40 pageChannel hiddenMobile">
                    <div class="title">
                        <h2 class="mt-10 p-0 mb-5">The Latest News &amp; Events</h2>
                    </div><!-- end title -->
                    <div class="value-latest mt-30 pageChannel hiddenMobile">
                        <div class="row media mb-20">
                            <a href="<?php echo 'detail-news-event.php'; ?>">
                                <div class="media-left">
                                    <img class="media-object ml-15" src="images/newsevent/news_event_sample_5.jpg" style="width: 100px;" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                </div>
                            </a>
                        </div>
                        <div class="row media mb-20">
                            <a href="<?php echo 'detail-news-event.php'; ?>">
                                <div class="media-left">
                                    <img class="media-object ml-15" src="images/newsevent/news_event_sample_7.jpg" style="width: 100px;" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                </div>
                            </a>
                        </div>
                        <div class="row media mb-20">
                            <a href="<?php echo 'detail-news-event.php'; ?>">
                                <div class="media-left">
                                    <img class="media-object ml-15" src="images/newsevent/news_event_sample_8.jpg" style="width: 100px;" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                </div>
                            </a>
                        </div>
                        <div class="row media mb-20">
                            <a href="<?php echo 'detail-news-event.php'; ?>">
                                <div class="media-left">
                                    <img class="media-object ml-15" src="images/newsevent/news_event_sample_11.jpg" style="width: 100px;" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hiddenMobile">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
                
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-detail-news-event pb-0 mt-xs-40 mb-xs-40">
                
                <div class="row content-news-event m-0">
                    <div class="heading-detail-ne">
                        <h2 class="p-0 mb-0 mt-0 font-26">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                        <div class="blog-carousel-meta">
                            <span><i class="fa fa-user text-green"></i> Administrator</span>
                            <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                            <span class="dropdown">
                                <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share text-green"></i> Share</a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="#" class="goog"><i class="fa fa-google-plus"></i></li></a>
                                    <li><a href="#" class="face"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twit"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </span>
                        </div><!-- end blog-carousel-meta -->
                    </div>
                </div>
                <div class="ne-carousel-desc mt-20">
                    <div class="desc-img mb-20">
                        <img src="images/newsevent/news_event_sample_2.jpg" class="img-responsive" />
                    </div>
                    <div class="desc-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus posuere eget turpis a feugiat. Nullam pharetra ac ex vulputate semper. Morbi bibendum nulla eget nibh lobortis, a sollicitudin ante rutrum. In euismod lectus at varius pulvinar. Nunc vitae ante aliquam, porta lacus sit amet, scelerisque odio. Quisque ut fringilla erat. Phasellus molestie sollicitudin nulla eu congue. Donec mollis sed neque venenatis bibendum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus posuere eget turpis a feugiat. Nullam pharetra ac ex vulputate semper. Morbi bibendum nulla eget nibh lobortis, a sollicitudin ante rutrum. In euismod lectus at varius pulvinar. Nunc vitae ante aliquam, porta lacus sit amet, scelerisque odio. Quisque ut fringilla erat. Phasellus molestie sollicitudin nulla eu congue. Donec mollis sed neque venenatis bibendum.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus posuere eget turpis a feugiat. Nullam pharetra ac ex vulputate semper. Morbi bibendum nulla eget nibh lobortis, a sollicitudin ante rutrum. In euismod lectus at varius pulvinar. Nunc vitae ante aliquam, porta lacus sit amet, scelerisque odio. Quisque ut fringilla erat. Phasellus molestie sollicitudin nulla eu congue. Donec mollis sed neque venenatis bibendum.</p>
                        
                        <a href="" data-toggle="modal" data-target="#myModal" class="btn btn-primary hvr-sweep-to-right btn-lg mt-20" data-toggle="modal" data-target="#myModalJoinEvent">Join Event</a>
                    </div>
                </div>
            </div><!-- end content -->
            
            <!-- Modal -->
            <div class="modal fade" id="myModalJoinEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pb-0" id="myModalLabel">Join Event <span class="text-grey text-gray-silver ml-5">Nama Event</span></h4>
                  </div>
                  <form method="" action="join-event.php">
                      <div class="modal-body">
                        <p>Silahkan isi form pendaftaran di bawah ini untuk berpartisipasi dalam event ini.</p>
                        <div class="form-group mt-15">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="" placeholder="Jhon Doe" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Email</label>
                            <input type="email" class="form-control" name="" placeholder="email@example.com" />
                        </div>
                        <div class="form-group mt-15">
                            <label>No Telepon / HP</label>
                            <input type="text" class="form-control" name="" placeholder="0***" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Alamat Lengkap</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group mt-15">
                            <label>Password</label>
                            <input type="password" class="form-control" name="" placeholder="***" />
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" name="" class="btn btn-primary btn-lg pull-left">Kirim</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="row m-0 pl-15 pr-15">
                <div class="title hidden-dekstop viewMobile pageChannel detailNews border-top-1px">
                    <h2 class="mt-10 p-0 mb-5">The Latest News &amp; Events</h2>
                </div><!-- end title -->
                <div class="value-latest mt-30 pageChannel hidden-dekstop viewMobile">
                    <div class="row media mb-20">
                        <a href="<?php echo 'detail-news-event.php'; ?>">
                            <div class="media-left">
                                <img class="media-object ml-15" src="images/newsevent/news_event_sample_1.jpg" style="width: 100px;" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            </div>
                        </a>
                    </div>
                    <div class="row media mb-20">
                        <a href="<?php echo 'detail-news-event.php'; ?>">
                            <div class="media-left">
                                <img class="media-object ml-15" src="images/newsevent/news_event_sample_3.jpg" style="width: 100px;" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            </div>
                        </a>
                    </div>
                    <div class="row media mb-20">
                        <a href="<?php echo 'detail-news-event.php'; ?>">
                            <div class="media-left">
                                <img class="media-object ml-15" src="images/newsevent/news_event_sample_2.jpg" style="width: 100px;" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            </div>
                        </a>
                    </div>
                    <div class="row media mb-20">
                        <a href="<?php echo 'detail-news-event.php'; ?>">
                            <div class="media-left">
                                <img class="media-object ml-15" src="images/newsevent/news_event_sample_3.jpg" style="width: 100px;" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                            </div>
                        </a>
                    </div>
                </div>
                
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
            </div>
                    
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>