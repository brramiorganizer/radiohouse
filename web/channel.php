<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_1.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner detail-page-other">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Tentang Radio House</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-80 pt-sm-60 pb-40">
    	<div class="container">
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pr-sm-15">
                <div class="breadcrumb-sidebar">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Channel</li>
                    </ul>
                </div>
                <div class="widget mb-30">
                    <div class="title">
                        <h2 class="mt-10 p-0 mb-5">Channel</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris.</p>
                    </div><!-- end title -->
                </div>
                <div class="widget mb-30">
                    <div id="accordion-second" class="clearfix">
                        <div class="accordion" id="accordion3">
                          <div class="accordion-group">
                            <div class="accordion-heading active">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseToggle1">
                                <em class="fa fa-minus icon-fixed-width"></em>Channel
                              </a>
                            </div>
                            <div id="collapseToggle1" class="accordion-body collapse in">
                              <div class="accordion-inner">
                                <div class="engine-search">
                                    <form method="" accept-charset="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="" placeholder="Search" />
                                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="accordion-group">
                            <div class="accordion-heading">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseToggle2">
                                <em class="fa fa-plus icon-fixed-width"></em>Kategori Channel
                              </a>
                            </div>
                            <div id="collapseToggle2" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <div class="engine-search">
                                    <form method="" accept-charset="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="" placeholder="Search" />
                                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="accordion-group">
                            <div class="accordion-heading">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseToggle3">
                                <em class="fa fa-plus icon-fixed-width"></em>Daerah / Kota
                              </a>
                            </div>
                            <div id="collapseToggle3" class="accordion-body collapse">
                              <div class="accordion-inner">
                                <div class="engine-search">
                                    <form method="" accept-charset="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="" placeholder="Search" />
                                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <ul>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                    <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Prambors</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div><!-- end accordion -->
                    </div><!-- end accordion first -->
                </div>
                
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hiddenMobile">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
                
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 radio-channel pb-0">
                <div class="row img-radio-channel">
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-18.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-19.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-20.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-21.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-22.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-23.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-24.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-25.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-26.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-27.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-28.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-29.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-30.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-31.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-32.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-33.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-34.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-1.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-4 col-md-4 col-grid">
                        <div class="portfolio_item">
            				<div class="entry">
                                <a href="<?php echo 'detail-channel.php'; ?>">
                					<img src="images/channel/imges-channel-radio-4.jpg" alt="" class="img-responsive">
                					<div class="magnifier">
                						<div class="buttons">
                							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                            <h4 class="p-0 m-0">Jakarta</h4>
                						</div><!-- end buttons -->
                					</div><!-- end magnifier -->
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                </div>
                
                <div class="pagination_wrapper text-center">
                    <!-- Pagination Normal -->
                    <ul class="pagination">
                        <li><a href="#">«</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div><!-- end pagination_wrapper -->
                
            </div><!-- end content -->
            
            <div class="clearfix"></div>
            
            <div class="row m-0 pl-15 pr-15">
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40 border-top-1px">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_2.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
            </div>
            
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>