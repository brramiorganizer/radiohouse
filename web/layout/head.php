<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <title>Radio House</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Streaming Radio">
  <meta name="keywords" content="Radio House">
  <meta name="author" content="Radio House">

  <!-- Favicon and Touch Icons -->
  <link href="images/favicon.png" rel="shortcut icon" type="image/png">
  <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
  <link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
  <link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
  <link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

  <!-- Bootstrap Styles -->
  <link href="css/bootstrap.css" rel="stylesheet">

  <!-- Styles -->
  <link href="css/style.css" rel="stylesheet">

  <!-- Carousel Slider -->
  <link href="css/owl-carousel.css" rel="stylesheet">

  <!-- CSS Animations -->
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- CSS Responsive -->
  <link href="css/responsive.css" rel="stylesheet">

  <!-- CSS Customs -->
  <link href="css/customs.css" rel="stylesheet">
  
  <!-- Flex Slider -->
  <link href="css/flexslider.css" rel="stylesheet">

  <!-- CSS Bootstrap Margin & Padding -->
  <link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet">

  <!-- CSS Untility Classes -->
  <link href="css/utility-classes.css" rel="stylesheet">

  <link href="css/jquery-ui.css" rel="stylesheet">

  <link href="fonts/fontawesome/css/font-awesome.min.css" rel="stylesheet">

  <!-- Google Fonts 
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>-->

  <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!-- Support for HTML5 -->
  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <!-- Enable media queries on older bgeneral_rowsers -->
  <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>  <![endif]-->

</head>
<body>
