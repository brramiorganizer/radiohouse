<div class="animationload">
<div class="loader">Loading...</div>
</div>
    <div id="topbar" class="clearfix">

    </div><!-- end topbar -->

    <header id="header-style-1">
		<div class="container-fluid">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                
                    <div class="searchMobile hidden-dekstop">
                        <div class="dropdown">
                            <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-search"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li>
                                    <div class="search-header">
                                        <form method="" action="">
                                            <div class="form-group m-0">
                                                <input class="form-control input-lg" type="text" placeholder="Search...">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <a href="<?php echo 'index.php'?>" class="navbar-brand m-0 pl-50 pl-xs-0 pl-sm-20">
                        <img src="images/logo.png" alt="Radiohouse" class="m-0" width="180" />
                    </a>
                    <div class="main-menu hidden-xs">
                        <div class="dropdown">
                          <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MENU
                            <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <!--<li><a href="<?php echo 'index.php';?>">HOME</a></li>-->
                            <li><a href="<?php echo 'about.php';?>">TENTANG RADIO HOUSE</a></li>
                            <li class="dropdown-submenu">
                                <a href="#">CHANNELS <span class="caret ml-10"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo 'channel.php';?>">ALL CHANNELS</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo 'news-event.php';?>">NEWS &amp; EVENTS</a></li>
                          </ul>
                        </div>
                    </div>
                    <div class="search-header hiddenMobile">
                        <form method="" action="">
                            <div class="form-group m-0">
                                <input class="form-control input-lg" type="text" placeholder="Search...">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
        		</div><!-- end navbar-header -->

				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right mr-20 mr-xs-0">
                    <ul class="nav navbar-nav hidden-xs navDesktop">
                        <li><a href="<?php echo 'menjadi-member.php';?>" class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li style="display: none;"><a href="#" class="hvr-underline-reveal" data-toggle="modal" data-target="#myModal">Masuk / Register</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Hi, Nama User </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'informasi-akun.php'?>">Informasi Akun</a></li>
                                <li><a href="<?php echo 'history.php'?>">History Transaksi</a></li>
                                <li><a href="<?php echo 'konfirmasi.php'?>">Konfirmasi Pembayaran</a></li>
                                <li><a href="">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
					<ul class="nav navbar-nav hidden-dekstop">
						<!--<li><a href="<?php echo 'index.php'?>">Home</a></li>-->
                        <li><a href="<?php echo 'about.php';?>">TENTANG RADIO HOUSE</a></li>
						<li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">CHANNELS <span class="caret ml-10"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'channel.php';?>">ALL CHANNELS</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>"><i class="fa fa-microphone mr-10"></i>RADIO CHANNEL (Jakarta)</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo 'news-event.php'?>">NEWS &amp; EVENTS</a></li>
                        <li><a href="<?php echo 'menjadi-member.php';?>">MENJADI MEMBER</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">MASUK / REGISTER</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Hi, Nama User</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'informasi-akun.php'?>">Informasi Akun</a></li>
                                <li><a href="<?php echo 'history.php'?>">History Transaksi</a></li>
                                <li><a href="<?php echo 'konfirmasi.php'?>">Konfirmasi Pembayaran</a></li>
                                <li><a href="">Log Out</a></li>
                            </ul>
                        </li>
					</ul><!-- end navbar-nav -->
				</div><!-- #navbar-collapse-1 -->
            </nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->
	</header><!-- end header-style-1 -->