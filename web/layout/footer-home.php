  <!-- Main Scripts-->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/jquery.ui.touch-punch.min.js"></script>
  <script src="js/menu.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.parallax-1.1.3.js"></script>
  <script src="js/jquery.simple-text-rotator.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/custom.js"></script>

  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/custom-portfolio.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script type="text/javascript">
  (function($) {
    "use strict";
  $(window).load(function(){
    $('.flexslider').flexslider({
      animation: "fade",
      controlNav: false,
      start: function(slider){
        $('body').removeClass('loading');
      }
    });
  });
  })(jQuery);

  var audioplaying = $('audio');
    $( function() {
        // setup master volume
    
        $('#master').slider({
          orientation: "horizontal",
          value: audioplaying.prop('volume'),
          min: 0,
          max: 1,
          range: 'min',
          animate: true,
          step: .1,
          slide: function(e, ui) {
              audioplaying.prop('volume',ui.value);
              if(ui.value > 0){
                $('#volumeup').css('display','inline-block');
                $('#volumemute').css('display','none');
              }else{
                $('#volumemute').css('display','inline-block');
                $('#volumeup').css('display','none');
              }
          }
      });
    
    });
    
    //var audio = new Audio('media/Linkin Park - Papercut.mp3');
    //Hide Pause
    $('#pause').hide();
    
    //Play button
    $('#play').click(function(){
        $('audio').trigger('play');
        $('#play').hide();
        $('#pause').show();
        showDuration();
    });
    
    //Pause button
    $('#pause').click(function(){
        $('audio').trigger('pause');
        $('#play').show();
        $('#pause').hide();
    });
    
    //hide volumemute
    $('#volumemute').hide();
    
    //volume
    var volume = 0;
    $('#volumeup').click(function(){
        volume = audioplaying.prop('volume');
        audioplaying.prop('volume',0);
        $("#master").slider('value',0);
        $('#volumeup').hide();
        $('#volumemute').show();
        showDuration();
    });
    
    //volume
    $('#volumemute').click(function(){
        audioplaying.prop('volume',volume);
        $("#master").slider('value',volume);
        $('#volumeup').show();
        $('#volumemute').hide();
    });
    



  $('#widget').draggable();


  $(document).ready(function () {

      //$('#play').trigger('click');
      var sildeNum = $('.page').length,
          wrapperWidth = 100 * sildeNum,
          slideWidth = 100/sildeNum;
      //$('.wrapper').width(wrapperWidth + '%');
      //$('.page').width(slideWidth + '%');

      $('a.scrollitem').click(function(){
          $('a.scrollitem').removeClass('selected');
          $(this).addClass('selected');

          var slideNumber = $($(this).attr('href')).index('.page'),
              margin = slideNumber * -100 + '%';

          $('.wrapper').animate({marginLeft: margin},100);
          return false;
      });
  });

  $(document).ready(function(){

      $(".filter-button").click(function(){
          var value = $(this).attr('data-filter');

          if(value == "all")
          {
              //$('.filter').removeClass('hidden');
              $('.filter').show('1000');
          }
          else
          {
  //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
  //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
              $(".filter").not('.'+value).hide('3000');
              $('.filter').filter('.'+value).show('3000');

          }
      });

      if ($(".filter-button").removeClass("active")) {
  $(this).removeClass("active");
  }
  $(this).addClass("active");

  });
  </script>

  </body>
</html>
