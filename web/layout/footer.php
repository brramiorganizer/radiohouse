<footer id="footer-style-1" class="p-0 ">
    	<div class="container-fluid p-0">
            <div class="row menu-footer pl-70 pr-70 m-0 m-xs-0 pl-md-30 pr-md-30 pt-xs-20 pb-xs-20">
                <div class="nav-foot col-sm-10 col-xs-8 p-0">
                    <ul class="main-menu-foot mb-0">
                        <li><a href="<?php echo 'about.php'?>" class="hvr-underline-reveal">Tentang Radio House</a></li>
                        <li><a href="<?php echo 'channel.php'?>" class="hvr-underline-reveal">Channel</a></li>
                        <li><a href="<?php echo 'news-event.php'?>" class="hvr-underline-reveal">News &amp; Event</a></li>
                        <li><a href="<?php echo 'contact.php'; ?>" class="hvr-underline-reveal">Hubungi Kami</a></li>
                        <li><a href="<?php echo 'menjadi-member.php'; ?>" class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li><a href="<?php echo 'syaratketentuan.php'; ?>" class="hvr-underline-reveal">Syarat &amp; Ketentuan</a></li>
                        <li><a href="<?php echo 'faq.php'; ?>" class="hvr-underline-reveal">FAQ</a></li>
                        <li><a href="<?php echo 'privacy.php'; ?>" class="hvr-underline-reveal">Privacy</a></li>
                    </ul>
                </div>
                <div class="social-media col-sm-2 col-xs-4 p-0">
                    <ul class="pull-right mb-0">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        	<div id="copyrights" class="bg-green-primary">
            	<div class="container p-0 m-0">
                	<div class="copyright-text pl-70 pr-70 pl-md-15 pr-md-15">
                        <p>Copyright &copy; 2017. RADIOHOUSE.com</p>
                    </div><!-- end copyright-text -->
                </div><!-- end container -->
            </div><!-- end copyrights -->
    	</div><!-- end container -->
	   <div class="dmtop">Scroll to Top</div>

    </footer><!-- end #footer-style-1 -->

    <!--Modal Login & Register-->
    <div class="modal fade loginnReg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <!--<h4 class="modal-title pb-0" id="myModalLabel">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-right" aria-hidden="true"></i></a> </h4>-->
          </div>
          <div class="modal-body">
            <div class="wrapper">
                <div id="mask">
                    <div id="page-1" class="page">
                        <div class="widget">
                            <div class="title">
                            	<h3 class="mt-0 text-capitalize pt-0 pb-10">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-circle-right ml-5"></i></a></h3>
                             </div>
                            <form id="loginform" method="post" name="loginform">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group mt-25">
                                    <button type="submit" class="btn btn-primary btn-lg">Masuk</button>
                                    <a href="" class="lp-password">Lupa Password</a>
                                </div>
                            </form>
                            <div class="orLogin">
                                <div class="lineLogin">
                                    <hr />
                                    <span>Atau</span>
                                </div>
                                <div class="row option-login">
                                    <div class="col-sm-6 pl-xs-0 pr-xs-0 mRes">
                                        <a title="Gmail" href="" class="gmail"><i class="fa fa-google"></i><label>Masuk dengan Google</label></a>
                                    </div>
                                    <div class="col-sm-6 pl-xs-0 pr-xs-0 mRes">
                                        <a title="Facebook" href="" class="facebook"><i class="fa fa-facebook"></i><label>Masuk dengan Facebook</label></a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end widget -->
                    </div>
                    <div id="page-2" class="page">
                        <div class="page-login">
                            <!--<a name="item2"></a>-->
                            <div class="widget last">
                            	<div class="title">
                                	<h3 class="mt-0 pt-0 pb-10 text-capitalize">Register</h3>
                                </div><!-- end title -->
                                <form id="registerform" method="post" name="registerform" action="detail-lelang.html">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Re-enter password">
                                    </div>
                                    <div class="form-group mb-0">
                                        <a href="#page-1" class="scrollitem bc-login"><i class="fa fa-angle-left mr-5"></i>Back Login</a>
                                        <button type="submit" class="btn btn-primary btn-lg pull-right mt-15">Register</button>
                                    </div>
                                </form>
                            </div><!-- end widget -->
                        </div><!-- end page-login -->
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Modal Login & Register-->

  <!-- Main Scripts-->
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/jquery.ui.touch-punch.min.js"></script>
  <script src="js/menu.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.parallax-1.1.3.js"></script>
  <script src="js/jquery.simple-text-rotator.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/custom.js"></script>

  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/custom-portfolio.js"></script>

  <script src="js/jquery.flexslider.js"></script>
  <script type="text/javascript">
    (function($) {
      "use strict";
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "fade",
    	controlNav: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
    })(jQuery);

    var audioplaying = $('audio');
    $( function() {
        // setup master volume
    
        $('#master').slider({
          orientation: "horizontal",
          value: audioplaying.prop('volume'),
          min: 0,
          max: 1,
          range: 'min',
          animate: true,
          step: .1,
          slide: function(e, ui) {
              audioplaying.prop('volume',ui.value);
              if(ui.value > 0){
                $('#volumeup').css('display','inline-block');
                $('#volumemute').css('display','none');
              }else{
                $('#volumemute').css('display','inline-block');
                $('#volumeup').css('display','none');
              }
          }
      });
    
    });
    
    //var audio = new Audio('media/Linkin Park - Papercut.mp3');
    //Hide Pause
    $('#pause').hide();
    
    //Play button
    $('#play').click(function(){
        $('audio').trigger('play');
        $('#play').hide();
        $('#pause').show();
        showDuration();
    });
    
    //Pause button
    $('#pause').click(function(){
        $('audio').trigger('pause');
        $('#play').show();
        $('#pause').hide();
    });
    
    //hide volumemute
    $('#volumemute').hide();
    
    //volume
    var volume = 0;
    $('#volumeup').click(function(){
        volume = audioplaying.prop('volume');
        audioplaying.prop('volume',0);
        $("#master").slider('value',0);
        $('#volumeup').hide();
        $('#volumemute').show();
        showDuration();
    });
    
    //volume
    $('#volumemute').click(function(){
        audioplaying.prop('volume',volume);
        $("#master").slider('value',volume);
        $('#volumeup').show();
        $('#volumemute').hide();
    });
    



    $('#widget').draggable();


    $(document).ready(function () {

        //$('#play').trigger('click');
        var sildeNum = $('.page').length,
            wrapperWidth = 100 * sildeNum,
            slideWidth = 100/sildeNum;
        //$('.wrapper').width(wrapperWidth + '%');
        //$('.page').width(slideWidth + '%');

        $('a.scrollitem').click(function(){
            $('a.scrollitem').removeClass('selected');
            $(this).addClass('selected');

            var slideNumber = $($(this).attr('href')).index('.page'),
                margin = slideNumber * -100 + '%';

            $('.wrapper').animate({marginLeft: margin},100);
            return false;
        });
    });

    $(document).ready(function(){

        $(".filter-button").click(function(){
            var value = $(this).attr('data-filter');

            if(value == "all")
            {
                //$('.filter').removeClass('hidden');
                $('.filter').show('1000');
            }
            else
            {
    //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
    //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');

            }
        });

        if ($(".filter-button").removeClass("active")) {
    $(this).removeClass("active");
    }
    $(this).addClass("active");

    });

  </script>
  </body>
</html>
