<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img whiteText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Informasi Akun</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Informasi Akun</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="pageAccount pt-70 pb-60 pt-sm-60">
    	<div class="container">
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <ul class="list-group">
                    <li class="list-group-item"><span class="text-green"><strong>Hi Nama User,</strong></span></li>
                    <li class="list-group-item"><a href="<?php echo 'informasi-akun.php'?>" class="text-black-444">Informasi Akun</a></li>
                    <li class="list-group-item"><a href="<?php echo 'history.php'?>" class="text-black-444">History Transaksi</a></li>
                    <li class="list-group-item"><a href="<?php echo 'konfirmasi.php'?>" class="text-black-444">Konfirmasi Pembayaran</a></li>
                </ul>
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-detail-news-event pb-0 mt-xs-40 mb-xs-40">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h4 class="m-0 p-0">Informasi Akun</h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <a href="#" class="text-green" data-toggle="modal" data-target="#editAccount">
                                            <span class="fa fa-edit"></span>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item">
                                <div class="dsk">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Nama Depan</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">Lipsum</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Nama Belakang</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">Lipsum</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Tempat, Tanggal Lahir</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">Jakarta, 19 Oct 1990</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">No Handphone</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">081 2890 9356</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Email</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">example@gmail.com</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Alamat</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">Jl. Mendawai I No.21, RT.4/RW.7, Kramat Pela, Kby. Baru, Jakarta, Daerah Khusus Ibukota Jakarta 12130</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Modal Edit Account -->
                        <div class="modal fade change" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <form action="" method="">
                                      <div class="modal-header">
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title p-0">Edit Informasi Akun</h4>
                                      </div>
                                      <div class="modal-body pb-0">
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Nama Depan</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Nama Belakang</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Jenis Kelamin</label>
                                                <div class="col-sm-8">
                                                    <div class="radio">
                                                      <label class="mr-20">
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                        Pria
                                                      </label>
                                                      <label>
                                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                        Wanita
                                                      </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Tanggal Lahir</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control datepicker"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentaddress">Email</label>
                                                <div class="col-sm-8">
                                                    <input type="email" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">No KTP</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentaddress">No. Handphone</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentaddress">Alamat</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Kota</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Provinsi</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control">
                                                        <option>-- Pilih Provinsi --</option>
                                                        <option>DKI Jakarta</option>
                                                        <option>Jawa Barat</option>
                                                        <option>Jawa Timur</option>
                                                        <option>Jawa Tengah</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentname">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="" class="form-control"/>
                                                </div>
                                            </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="button" class="btn btn-primary">Save</button>
                                      </div>
                                  </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                    <div class="col-sm-6">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h4 class="m-0 p-0">Password</h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <a href="" class="text-green" data-toggle="modal" data-target="#editPassword">
                                            <span class="fa fa-edit"></span>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item">
                                <div class="dsk">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-3 col-sm-6 mb-10">
                                            <p class="m-0 font-weight-900">Password</p>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-6 mb-10">
                                            <p class="m-0">*****</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Modal Edit Account -->
                        <div class="modal fade change" id="editPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <form action="" method="">
                                      <div class="modal-header">
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title p-0">Edit Password</h4>
                                      </div>
                                      <div class="modal-body pb-0">
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentaddress">New Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group mb-15">
                                                <label class="col-sm-4" for="#currentaddress">Verify Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" name="" class="form-control"/>
                                                </div>
                                            </div>
                                      </div>
                                      <div class="clearfix"></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="button" class="btn btn-primary">Save</button>
                                      </div>
                                  </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>
                </div>
            </div><!-- end content -->
            
            <!-- Modal -->
            <div class="modal fade" id="myModalJoinEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pb-0" id="myModalLabel">Join Event <span class="text-grey text-gray-silver ml-5">Nama Event</span></h4>
                  </div>
                  <form method="" action="">
                      <div class="modal-body">
                        <p>Silahkan isi form pendaftaran di bawah ini untuk berpartisipasi dalam event ini.</p>
                        <div class="form-group mt-15">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="" placeholder="Jhon Doe" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Email</label>
                            <input type="email" class="form-control" name="" placeholder="email@example.com" />
                        </div>
                        <div class="form-group mt-15">
                            <label>No Telepon / HP</label>
                            <input type="text" class="form-control" name="" placeholder="0***" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Alamat Lengkap</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group mt-15">
                            <label>Password</label>
                            <input type="password" class="form-control" name="" placeholder="***" />
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" name="" class="btn btn-primary btn-lg pull-left">Kirim</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
                
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>