<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner detail-page-other">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Tentang Radio House</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Tentang Radio House</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-60 pb-40 newsEvent">
    	<div class="container">
            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pr-sm-15">
                <div class="breadcrumb-sidebar">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>News &amp; Events</li>
                    </ul>
                </div>
                <div class="widget mb-30">
                    <div class="title">
                        <h2 class="mt-10 p-0 mb-5">News &amp; Events</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris.</p>
                    </div><!-- end title -->
                </div>
                <div class="widget mb-30">
                    <h3 class="mt-0 mb-10 pb-5">Radio Channel</h3>
                    <div class="search-news-event">
                        <form method="" action="">
                            <div class="form-group">
                                <input type="text" name="" class="form-control" placeholder="Search" />
                                <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="list-radioChannel">
                        <ul>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li class="r-grid"><a href=""><i class="fa fa-angle-right"></i>Prambors</a></li>
                            <li><a href="" id="viewMore"><i class="fa fa-plus"></i>View More  </a></li>
                        </ul>
                    </div>
                </div>
                
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hiddenMobile">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
                
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-news-event pb-0 mt-xs-40">
                
                <div class="row portfolio-filter mt-0 ">
                    <div class="text-left">
                        <a class="filter-button" data-filter="all">All</a>
                        <a class="filter-button" data-filter="news">News</a>
                        <a class="filter-button" data-filter="events">Events</a>
                    </div>
                </div>
                
                <div class="row content-news-event">
                    <div class="grid mainEqual">
                    
                        <div class="col-sm-6 equal-1 mb-30 filter news">
                            <div class="blog-carousel" >
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_3.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 News</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-sm-6 equal-1 mb-30 filter news">
                            <div class="blog-carousel">
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_1.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 News</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-sm-6 equal-1 mb-30 filter events">
                            <div class="blog-carousel">
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_2.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 Events</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-sm-6 equal-1 mb-30 filter events">
                            <div class="blog-carousel">
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_6.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 Events</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-sm-6 equal-1 mb-30 filter news">
                            <div class="blog-carousel">
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_7.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 News</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-sm-6 equal-1 mb-30 filter events">
                            <div class="blog-carousel">
                                <div class="entry">
                                    <a href="<?php echo 'detail-news-event.php'; ?>"><img src="images/newsevent/news_event_sample_8.jpg" alt="" class="img-responsive"></a>
                                </div><!-- end entry -->
                                <div class="blog-carousel-header">
                                    <h3 class="p-0 mb-0"><a title="" href="<?php echo 'detail-news-event.php'; ?>">Work Office - 2014 Events</a></h3>
                                    <div class="blog-carousel-meta">
                                        <span><i class="fa fa-calendar text-green"></i> April 01, 2014</span>
                                    </div><!-- end blog-carousel-meta -->
                                </div><!-- end blog-carousel-header -->
                                <div class="blog-carousel-desc">
                                    <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel. </p>
                                    <a href="<?php echo 'detail-news-event.php'; ?>" class="btn btn-primary hvr-sweep-to-right">Read More</a>
                                </div><!-- end blog-carousel-desc -->
                            </div><!-- end blog-carousel -->
                        </div><!-- end col-lg-4 -->
                        
                    </div><!-- end blog-masonry --> 
                    
                    <div class="clearfix"></div>
                    
                    <div class="pagination_wrapper text-center">
                        <!-- Pagination Normal -->
                        <ul class="pagination">
                            <li><a href="#">«</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div><!-- end pagination_wrapper -->
                    
                </div>
            </div><!-- end content -->
            
            <div class="clearfix"></div>
            
            <div class="row m-0 pl-15 pr-15">
                <!-- images banner sale Dekstop -->
                <div class="imageBannerSale pageChannel hidden-dekstop viewMobile mt-40 border-top-1px">
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_1.jpg" class="img-responsive" /></a>
                    </div>
                    <div class="widget mb-30">
                        <a href=""><img src="images/sidebar/sample_sidebar_3.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <!-- end images banner sale -->
            </div>
                    
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>