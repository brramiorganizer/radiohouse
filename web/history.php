<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img whiteText" style="background-image: url(images/breadcumb/breadcumb_sample_2.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li>Histori Transaksi</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Histori Transaksi</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="pageAccount pt-70 pb-60 pt-sm-60">
    	<div class="container">
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <ul class="list-group">
                    <li class="list-group-item"><span class="text-green"><strong>Hi Nama User,</strong></span></li>
                    <li class="list-group-item"><a href="<?php echo 'informasi-akun.php'?>" class="text-black-444">Informasi Akun</a></li>
                    <li class="list-group-item"><a href="<?php echo 'history.php'?>" class="text-black-444">History Transaksi</a></li>
                    <li class="list-group-item"><a href="<?php echo 'konfirmasi.php'?>" class="text-black-444">Konfirmasi Pembayaran</a></li>
                </ul>
            </div><!-- end left-sidebar -->
            
        	<div id="content" class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-detail-news-event pb-0 mt-xs-40 mb-xs-40">
                <div class="content-history">
                    <h3 class="mt-0">Histori Transaksi</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Invoice</th>
                                    <th>Nama Event</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="" class="text-green">#123456</a></td>
                                    <td>Lorem Ipsum is simply dummy text of the printing.</td>
                                    <td>09 September 2017</td>
                                    <td class="bg-success text-center">Success</td>
                                </tr>
                                <tr>
                                    <td><a href="" class="text-green">#123456</a></td>
                                    <td>Lorem Ipsum is simply dummy text of the printing.</td>
                                    <td>09 September 2017</td>
                                    <td><a href="<?php echo 'konfirmasi.php'?>" class="btn btn-primary text-white text-transform-none">Konfirmasi Pembayaran</a></td>
                                </tr>
                                <tr>
                                    <td><a href="" class="text-green">#123456</a></td>
                                    <td>Lorem Ipsum is simply dummy text of the printing.</td>
                                    <td>09 September 2017</td>
                                    <td><a href="<?php echo 'konfirmasi.php'?>" class="btn btn-primary text-white text-transform-none">Konfirmasi Pembayaran</a></td>
                                </tr>                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- end content -->
            
            <!-- Modal -->
            <div class="modal fade" id="myModalJoinEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title pb-0" id="myModalLabel">Join Event <span class="text-grey text-gray-silver ml-5">Nama Event</span></h4>
                  </div>
                  <form method="" action="">
                      <div class="modal-body">
                        <p>Silahkan isi form pendaftaran di bawah ini untuk berpartisipasi dalam event ini.</p>
                        <div class="form-group mt-15">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="" placeholder="Jhon Doe" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Email</label>
                            <input type="email" class="form-control" name="" placeholder="email@example.com" />
                        </div>
                        <div class="form-group mt-15">
                            <label>No Telepon / HP</label>
                            <input type="text" class="form-control" name="" placeholder="0***" />
                        </div>
                        <div class="form-group mt-15">
                            <label>Alamat Lengkap</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group mt-15">
                            <label>Password</label>
                            <input type="password" class="form-control" name="" placeholder="***" />
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" name="" class="btn btn-primary btn-lg pull-left">Kirim</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
                
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>