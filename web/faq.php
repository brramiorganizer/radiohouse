<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_3.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">FAQ</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">FAQ</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-70 pb-50">
    	<div class="container">
        	
            <div class="col-md-12 p-0 p-sm-0">
                <div class="doc">
                    <div class="widget">
                        <div class="about_tabbed">     
                            <div class="panel-group" id="accordion2">
                                <div class="panel panel-default">
                                    <div class="panel-heading active">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Why Jollyany superb for your business?</a>
                                        </h4>
                                    </div><!-- end panel-heading -->
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p>Nam sem ligula, fringilla id pulvinar eu, laoreet at ligula. Donec blandit ligula a cursus tempor. Ut sit amet nibh nulla. Quisque nec augue eget mi sodales posuere. Integer vitae vehicula mi. Mauris orci velit, tempor ut sem vel, placerat mattis orci. Curabitur lacinia ac sapien ullamcorper fermentum. Nunc interdum, dui eget facilisis convallis.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end collapseOne -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Total how much page template?</a>
                                        </h4>
                                    </div><!-- end panel-heading -->
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Nam sem ligula, fringilla id pulvinar eu, laoreet at ligula. Donec blandit ligula a cursus tempor. Ut sit amet nibh nulla. Quisque nec augue eget mi sodales posuere. Integer vitae vehicula mi. Mauris orci velit, tempor ut sem vel, placerat mattis orci. Curabitur lacinia ac sapien ullamcorper fermentum. Nunc interdum, dui eget facilisis convallis.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end collapseOne -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">How to change color schemes?</a>
                                        </h4>
                                    </div><!-- end panel-heading -->
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Nam sem ligula, fringilla id pulvinar eu, laoreet at ligula. Donec blandit ligula a cursus tempor. Ut sit amet nibh nulla. Quisque nec augue eget mi sodales posuere. Integer vitae vehicula mi. Mauris orci velit, tempor ut sem vel, placerat mattis orci. Curabitur lacinia ac sapien ullamcorper fermentum. Nunc interdum, dui eget facilisis convallis.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end collapseOne -->
                                </div><!-- end panel -->
                            </div><!-- end panel-group -->
                        </div><!-- end about tabbed -->
                    </div><!-- end widget -->
                </div><!-- end doc -->
            </div>
            
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>