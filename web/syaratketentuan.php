<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_3.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Syarat dan Ketentuan</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Syarat dan Ketentuan</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-60 pb-60">
    	<div class="container">
        	
            <div class="content-about-up">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
            </div> 
    	</div><!-- end container -->
    </section><!--end white-wrapper -->

	<?php include 'layout/footer.php'; ?>