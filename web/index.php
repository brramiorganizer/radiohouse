<?php 

    include 'layout/head.php';
    
?>

<div class="animationload">
<div class="loader">Loading...</div>
</div>
    <audio class="hidden" controls="controls"><source src="http://45.64.98.181:8323/stream" type="audio/mpeg" /></audio>
    <div id="topbar" class="clearfix">

    </div><!-- end topbar -->

    <header id="header-style-1">
		<div class="container-fluid">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                
                    <div class="searchMobile hidden-dekstop">
                        <div class="dropdown">
                            <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-search"></i>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li>
                                    <div class="search-header">
                                        <form method="" action="">
                                            <div class="form-group m-0">
                                                <input class="form-control input-lg" type="text" placeholder="Search...">
                                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <a href="<?php echo 'index.php'?>" target="_blank" class="navbar-brand m-0 pl-50 pl-xs-0 pl-sm-20">
                        <img src="images/logo.png" alt="Radiohouse" class="m-0" width="180" />
                    </a>
                    <div class="main-menu hidden-xs">
                        <div class="dropdown">
                          <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MENU
                            <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <!--<li><a href="<?php echo 'index.php';?>">HOME</a></li>-->
                            <li><a href="<?php echo 'about.php';?>" target="_blank">Tentang Radio House</a></li>
                            <li class="dropdown-submenu">
                                <a href="#">Channels <span class="caret ml-10"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo 'channel.php';?>" target="_blank">All Channels</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                    <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo 'news-event.php';?>" target="_blank" >Berita &amp; Event</a></li>
                          </ul>
                        </div>
                    </div>
                    <div class="search-header hiddenMobile">
                        <form method="" action="">
                            <div class="form-group m-0">
                                <input class="form-control input-lg" type="text" placeholder="Search...">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
        		</div><!-- end navbar-header -->

				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right mr-20 mr-xs-0">
                    <ul class="nav navbar-nav hidden-xs navDesktop">
                        <li><a href="<?php echo 'menjadi-member.php';?>" target="_blank"  class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li style="display: none;"><a href="#" class="hvr-underline-reveal" data-toggle="modal" data-target="#myModal">Masuk / Register</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Hi, Nama User </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'informasi-akun.php'?>" target="_blank">Informasi Akun</a></li>
                                <li><a href="<?php echo 'history.php'?>" target="_blank">History Transaksi</a></li>
                                <li><a href="<?php echo 'konfirmasi.php'?>" target="_blank">Konfirmasi Pembayaran</a></li>
                                <li><a href="">Log Out</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="#" class="hvr-underline-reveal" data-toggle="modal" data-target="#myModal">Register</a></li>-->
                    </ul>
					<ul class="nav navbar-nav hidden-dekstop">
						<!--<li><a href="<?php echo 'index.php'?>">Home</a></li>-->
                        <li><a href="<?php echo 'about.php';?>" target="_blank" >Tentang Radio House</a></li>
						<li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Channels <span class="caret ml-10"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'channel.php';?>" target="_blank" >All Channels</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                                <li><a href="<?php echo 'detail-channel.php';?>" target="_blank" ><i class="fa fa-microphone mr-10"></i>Radio Channel (Jakarta)</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo 'news-event.php'?>" target="_blank" >Berita &amp; Event</a></li>
                        <li><a href="<?php echo 'menjadi-member.php';?>" target="_blank" >Menjadi Member</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Masuk / Register</a></li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Hi, Nama User</a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo 'informasi-akun.php'?>" target="_blank">Informasi Akun</a></li>
                                <li><a href="<?php echo 'history.php'?>" target="_blank">History Transaksi</a></li>
                                <li><a href="<?php echo 'konfirmasi.php'?>" target="_blank">Konfirmasi Pembayaran</a></li>
                                <li><a href="">Log Out</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="#" data-toggle="modal" data-target="#myModal">REGISTER</a></li>-->
					</ul><!-- end navbar-nav -->
				</div><!-- #navbar-collapse-1 -->
            </nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->
	</header><!-- end header-style-1 -->

    <section class="slider-wrapper">
    
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/slider/slider-1.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-2.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-3.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-4.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-5.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-6.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <li>
                            <img src="images/slider/slider-7.jpg" alt="" class="img-responsive">
                            <div class="flex-caption">
                                <h2 class="mb-10">Lorem Ipsum is simply dummy text of the printing.</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                                <a href="" class="btn btn-primary mt-10">View More</a>
                            </div>
                            <div class="overlay"></div>
                        </li>
                    </ul><!-- end slides -->
                </div><!-- end post-slider -->
            </div>
        </div>
    </section><!-- end slider-wrapper -->

    <!-- Radio Player -->
    <section class="radio-palyer bg-green-primary wow fadeInDown">
        <div class="container">
            <div class="row m-xs-0">
                <div class="col-sm-3 col-sm-offset-2 border-right pt-20 pb-15 hidden-xs">
                    <div class="img-radio">
                        <img src="images/channel/images-streaming-home.jpg" class="img-responsive" />
                    </div>
                    <div class="title-radio">
                        <h3 class="p-0 m-0">Raya FM</h3>
                        <p class="">Banyuwangi</p>
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12 pt-20 pt-xs-30 pb-xs-30 pl-40 pl-xs-0 pr-xs-0">
                    <div id="audio-player">
                        <div id="buttons">
            				<span>
            					<button id="play"></button>
            					<button id="pause"></button>
            				</span>
            			</div>
            			<div id="tracker">
                            <div class="titlePlayer hidden-dekstop">
                                <h3 class="p-0 m-0">Radio House</h3>
                                <p class="">Jakarta</p>
                            </div>
            				<div id="progress-bar">
            					<span id="progress"></span>
                                <p class="timer-player">0:00</p>
            				</div>
            			</div>
                        <div id="volume">
                            <span class="dropdown-hover">
                                <button id="volumeup"></button>
            					<button id="volumemute"></button>
                            </span>
                            <div class="dropdown-menu-volume">
                                <div id="master" style="width:100px;"></div>
                            </div>
                        </div>
                        <div class="button-share-radio ml-xs-10">
                            <span class="dropdown">
                                <a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span></span></a>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="#" target="_blank" class="goog"><i class="fa fa-google-plus"></i></li></a>
                                    <li><a href="#" target="_blank" class="face"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" target="_blank" class="twit"><i class="fa fa-twitter"></i></a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end Radio Player -->

    <!-- Radio Channel-->
    <section class="radio-channel pt-125 pb-95 pt-sm-60 pb-sm-60">
        <div class="container">
            <div class="row">
                <div class="img-radio-channel">
                    <div class="col-sm-4">
                        <div class="portfolio_item wow fadeInLeft">
            				<div class="entry">
                                <a href="detail-channel.php" target="_blank" >
                                    <div class="square-img-bc bg-img" style="background-image: url(images/channel/imges-channel-radio-1.jpg);">
                    					<div class="magnifier">
                    						<div class="buttons">
                    							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                <h4 class="p-0 m-0">Jakarta</h4>
                    						</div><!-- end buttons -->
                    					</div><!-- end magnifier -->
                                    </div>
                                </a>
            				</div><!-- end entry -->
            			</div><!-- end portfolio_item -->
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-2.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-3.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInLeft">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-4.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-5.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-6.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                            <div class="col-sm-4">
                                <div class="portfolio_item wow fadeInRight">
                    				<div class="entry">
                                        <a href="detail-channel.php" target="_blank" >
                        					<img src="images/channel/imges-channel-radio-7.jpg" alt="" class="img-responsive">
                        					<div class="magnifier">
                        						<div class="buttons">
                        							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                                    <h4 class="p-0 m-0">Jakarta</h4>
                        						</div><!-- end buttons -->
                        					</div><!-- end magnifier -->
                                        </a>
                    				</div><!-- end entry -->
                    			</div><!-- end portfolio_item -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="text-radio-channel text-center mt-10">
                    <div class="row m-xs-0">
                        <div class="col-sm-offset-2 col-sm-8">
                            <p class="wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris, sit amet pretium enim placerat non. Donec ac massa accumsan quam fringilla lacinia.</p>
                            <a href="channel.php" target="_blank"  class="btn btn-default btn-lg mt-30 hvr-sweep-to-right-green wow fadeInUp">Radio Channel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end Radio Channel -->

    <!-- News & Event -->
	<section class="radio-news-event">
        <div class="container-fluid p-0">
            <div class="row m-0 mainEqual m-xs-0">
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item title-news-event bg-green-primary p-40 wow fadeInLeft" data-wow-offset="200" data-wow-duration="1500ms">
                        <h1 class="m-0 p-0 text-white">Berita &amp; Event</h1>
                        <hr class="m-0" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
                        <a href="news-event.php" target="_blank"  class="btn btn-default btn-lg hvr-sweep-to-right">Lihat Semua Berita &amp; Event</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item wow fadeInLeft" data-wow-offset="260" data-wow-duration="1500ms">
        				<div class="entry">
        					<img src="images/newsevent/news_event_sample_1.jpg" alt="" class="img-responsive">
        					<div class="magnifier m-15 m-xs-0">
        						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
        							<a href="detail-news-event.php" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
        						</div><!-- end buttons -->
        					</div><!-- end magnifier -->
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item wow fadeInLeft" data-wow-offset="320" data-wow-duration="1500ms">
        				<div class="entry">
        					<img src="images/newsevent/news_event_sample_3.jpg" alt="" class="img-responsive">
        					<div class="magnifier m-15 m-xs-0">
        						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
        							<a href="detail-news-event.php" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
        						</div><!-- end buttons -->
        					</div><!-- end magnifier -->
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item wow fadeInRight" data-wow-offset="260" data-wow-duration="1500ms">
        				<div class="entry">
        					<img src="images/newsevent/news_event_sample_2.jpg" alt="" class="img-responsive">
        					<div class="magnifier m-15 m-xs-0">
        						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
        							<a href="detail-news-event.php" target="_blank"  class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
        						</div><!-- end buttons -->
        					</div><!-- end magnifier -->
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item wow fadeInRight" data-wow-offset="300" data-wow-duration="1500ms">
        				<div class="entry">
        					<img src="images/newsevent/news_event_sample_5.jpg" alt="" class="img-responsive">
        					<div class="magnifier m-15 m-xs-0">
        						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
        							<a href="detail-news-event.php" target="_blank" class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
        						</div><!-- end buttons -->
        					</div><!-- end magnifier -->
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-6 col-md-4 p-0 equal-1">
                    <div class="portfolio_item wow fadeInRight" data-wow-offset="350" data-wow-duration="1500ms">
        				<div class="entry">
        					<img src="images/newsevent/news_event_sample_4.jpg" alt="" class="img-responsive">
        					<div class="magnifier m-15 m-xs-0">
        						<div class="buttons p-20 text-left pt-xs-0 pb-xs-0">
                                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat ecitur mauris,  sit amet pretium enim placerat non.</p>
        							<a href="detail-news-event.php" target="_blank" class="st btn btn-default hvr-sweep-to-right" rel="bookmark" href="">Read More</a>
        						</div><!-- end buttons -->
        					</div><!-- end magnifier -->
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
            </div>
        </div>

    </section><!-- end white-wrapper -->

    <!-- Category Channel 
	<section id="one-parallax" class="parallax category-channel" style="background-image: url('demos/parallax_04.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay pt-125 pb-95">
        	<div class="container">
				<div class="stat f-container">
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-30">
                        	<i class="fa fa-user fa-3x text-green wow flipInX" data-wow-offset="200"></i>
                            <div class="milestone-details wow fadeInLeft" data-wow-offset="200">Happy Customers</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-30">
                        	<i class="fa fa-coffee fa-3x text-green wow flipInX" data-wow-offset="200"></i>
                            <div class="milestone-details wow fadeInLeft" data-wow-offset="200">Ordered Coffee's</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-20">
                        	<i class="fa fa-trophy fa-3x text-green wow flipInX" data-wow-offset="200"></i>
                            <div class="milestone-details wow fadeInRight" data-wow-offset="200">Awards Win</div>
						</div>
					</div>
					<div class="f-element col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="milestone-counter mb-sm-30">
                        	<i class="fa fa-camera fa-3x text-green wow flipInX" data-wow-offset="200"></i>
                            <div class="milestone-details wow fadeInRight" data-wow-offset="200">Photos Taken</div>
						</div>
					</div>-->
				<!--</div> stat -->
            <!--</div> end container -->
    	<!--</div> end overlay -->
    <!--</section> end transparent-bg -->

    <!-- Logo-Logo Channel -->
	<section class="white-wrapper channel-logo radio-channel pt-125 pb-95">
    	<div class="container">
        	<div class="row text-center m-xs-0 img-radio-channel">
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-8.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-9.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-10.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-11.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInLeft" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank">
            					<img src="images/channel/imges-channel-radio-12.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-3.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-21.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-5.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-25.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
                <div class="col-sm-2">
                    <div class="portfolio_item mb-0 wow fadeInRight" data-wow-offset="200">
        				<div class="entry">
                            <a href="detail-channel.php" target="_blank" >
            					<img src="images/channel/imges-channel-radio-17.jpg" alt="" class="img-responsive">
            					<div class="magnifier">
            						<div class="buttons">
            							<h3 class="p-0 mt-0 mb-5">Radio Channels</h3>
                                        <h4 class="p-0 m-0">Jakarta</h4>
            						</div><!-- end buttons -->
            					</div><!-- end magnifier -->
                            </a>
        				</div><!-- end entry -->
        			</div><!-- end portfolio_item -->
                </div>
            </div>
		</div><!-- end container -->
    </section><!-- end grey-wrapper -->
    
    <section id="one-parallax" class="parallax category-channel" style="background-image: url('demos/parallax_04.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
		<div class="overlay pt-125 pb-95">
            <div class="container-fluid p-0">
                <div class="row ml-0 mr-0 secAnyQuestion">
                    <div class="col-sm-12 p-0">
                        <div class="pl-xs-15 pr-xs-15">
                            <h1 class="m-0 p-0 text-center text-white font-36"><span class="font-familly-proximareg">ANY QUESTION?</span> <strong><a href="<?php echo 'contact.php';?>" class="contact-about">CONTACT US</a></strong></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer id="footer-style-1" class="p-0 ">
    	<div class="container-fluid p-0">
            <div class="row menu-footer pl-70 pr-70 m-0 m-xs-0 pl-md-30 pr-md-30 pt-xs-20 pb-xs-20">
                <div class="nav-foot col-sm-10 col-xs-8 p-0">
                    <ul class="main-menu-foot mb-0">
                        <li><a href="<?php echo 'about.php'?>" target="_blank" class="hvr-underline-reveal">Tentang Radio House</a></li>
                        <li><a href="<?php echo 'channel.php'?>" target="_blank" class="hvr-underline-reveal">Channel</a></li>
                        <li><a href="<?php echo 'news-event.php'?>" target="_blank" class="hvr-underline-reveal">News &amp; Event</a></li>
                        <li><a href="<?php echo 'contact.php'; ?>" target="_blank" class="hvr-underline-reveal">Hubungi Kami</a></li>
                        <li><a href="<?php echo 'menjadi-member.php'; ?>" target="_blank" class="hvr-underline-reveal">Menjadi Member</a></li>
                        <li><a href="<?php echo 'syaratketentuan.php'; ?>" target="_blank" class="hvr-underline-reveal">Syarat &amp; Ketentuan</a></li>
                        <li><a href="<?php echo 'faq.php'; ?>" target="_blank" class="hvr-underline-reveal">FAQ</a></li>
                        <li><a href="<?php echo 'privacy.php'; ?>" target="_blank" class="hvr-underline-reveal">Privacy</a></li>
                    </ul>
                </div>
                <div class="social-media col-sm-2 col-xs-4 p-0">
                    <ul class="pull-right mb-0">
                        <li><a href="" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="" target="_blank" ><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        	<div id="copyrights" class="bg-green-primary">
            	<div class="container p-0 m-0">
                	<div class="copyright-text pl-70 pr-70 pl-md-15 pr-md-15">
                        <p>Copyright &copy; 2017. RADIOHOUSE.com</p>
                    </div><!-- end copyright-text -->
                </div><!-- end container -->
            </div><!-- end copyrights -->
    	</div><!-- end container -->
	   <div class="dmtop">Scroll to Top</div>

    </footer><!-- end #footer-style-1 -->
    
    <!--Modal Login & Register-->
    <div class="modal fade loginnReg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <!--<h4 class="modal-title pb-0" id="myModalLabel">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-right" aria-hidden="true"></i></a> </h4>-->
          </div>
          <div class="modal-body">
            <div class="wrapper">
                <div id="mask">
                    <div id="page-1" class="page">
                        <div class="widget">
                            <div class="title">
                            	<h3 class="mt-0 text-capitalize pt-0 pb-10">Masuk / <a href="#page-2" class="register_m scrollitem">Register <i class="fa fa-arrow-circle-right ml-5"></i></a></h3>
                             </div>
                            <form id="loginform" method="post" name="loginform">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group mt-25">
                                    <button type="submit" class="btn btn-primary btn-lg">Masuk</button>
                                    <a href="" class="lp-password">Lupa Password</a>
                                </div>
                            </form>
                            <div class="orLogin">
                                <div class="lineLogin">
                                    <hr />
                                    <span>Atau</span>
                                </div>
                                <div class="row option-login">
                                    <div class="col-sm-6 pl-xs-0 pr-xs-0 mRes">
                                        <a title="Gmail" href="" class="gmail"><i class="fa fa-google"></i><label>Masuk dengan Google</label></a>
                                    </div>
                                    <div class="col-sm-6 pl-xs-0 pr-xs-0 mRes">
                                        <a title="Facebook" href="" class="facebook"><i class="fa fa-facebook"></i><label>Masuk dengan Facebook</label></a>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end widget -->
                    </div>
                    <div id="page-2" class="page">
                        <div class="page-login">
                            <!--<a name="item2"></a>-->
                            <div class="widget last">
                            	<div class="title">
                                	<h3 class="mt-0 pt-0 pb-10 text-capitalize">Register</h3>
                                </div><!-- end title -->
                                <form id="registerform" method="post" name="registerform" action="detail-lelang.html">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Last name">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Re-enter password">
                                    </div>
                                    <div class="form-group mb-0">
                                        <a href="#page-1" class="scrollitem bc-login"><i class="fa fa-angle-left mr-5"></i>Back Login</a>
                                        <button type="submit" class="btn btn-primary btn-lg pull-right mt-15">Register</button>
                                    </div>
                                </form>
                            </div><!-- end widget -->
                        </div><!-- end page-login -->
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Modal Login & Register-->

	<?php include 'layout/footer-home.php'; ?>
