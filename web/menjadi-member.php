<?php 

    include 'layout/head.php';
    include 'layout/header.php';
    
?>
    
	<section class="banner-img withText" style="background-image: url(images/breadcumb/breadcumb_sample_3.jpg);">
        <div class="bg-overlay"></div>
        <div class="container">
            <div class="row m-0">
                <div class="breadcrumb-banner">
                    <ul class="m-0">
                        <li><a href="<?php echo 'index.php'; ?>"><i class="fa fa-home"></i></a></li>
                        <li class="text-green">Menjadi Member</li>
                    </ul>
                    <h1 class="m-0 p-0 text-white font-36 font-familly-proximareg">Menjadi Member</h1>
                </div>
            </div>
        </div>
    </section><!-- end post-wrapper-top -->
    
    <section class="page-under pt-60 pb-0">
    	<div class="container">
            <div class="content-about-up">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus venenatis, est eu gravida iaculis, mauris leo sagittis lorem, euismod laoreet ante purus feugiat arcu. Praesent posuere semper scelerisque. Sed interdum sollicitudin metus, vel pellentesque nisi commodo sed. Aliquam vitae risus iaculis, blandit ex non, ultrices enim. Ut quis suscipit lorem. In tempus dolor pharetra ultrices volutpat. Nam finibus elit in eros maximus, sagittis pellentesque dui ultricies. Pellentesque nulla lacus, fermentum vitae malesuada non, vehicula ut velit. Maecenas sit amet ullamcorper enim.</p>
            </div>
        </div>
    </section><!--end white-wrapper -->
    
    <div class="wrapper-section pt-60 pb-60">
        <section class="white-wrapper pb-40 pt-0 sec-member">
        	<div class="container">
            	<div class="general-title pb-70 pb-sm-60">
                	<h2 class="mt-0 p-0">Harga Member</h2>
                    <hr class="border-green"/>
                </div><!-- end general title -->
                <div class="doc member-price">
                    <div id="owl-testimonial-widget" class="owl-carousel pricing_boxes mainEqual p-0">
                        <div class="owl-item col-lg-4 col-sm-4">
                            <div class="pricing_detail">
                                <!--<span class="priceamount">Rp 1.000.000<br>/Tahun</span>-->
                                <header>
                                    <h3 class="p-0">SILVER</h3>
                                    <h2 class="mt-0 p-0 mb-30">Rp 1.000.000 / Tahun</h2>
                                </header><!-- end header -->
                                <div class="pricing_info text-center">
                                    <ul class="equal-1">
                                        <li class="pb-5">Streaming Radio</li>
                                        <li class="pb-5">Profile Radio</li>
                                        <li class="pb-5">Share Sosial Media</li>
                                        <li class="pb-5">Berita &amp; Event</li>
                                    </ul>
                                    <footer class="mt-20">
                                        <a href="#basic" class="btn btn-primary btn-lg get-form-besic">Get Member</a>
                                    </footer>
                                </div><!-- end pricing-info -->
                            </div><!-- end pricing_detail -->
                        </div><!-- end col-lg-3 -->
        
                        <div class="owl-item col-lg-4 col-sm-4">
                            <div class="pricing_detail">
                                <!--<span class="priceamount">Rp 3.000.000<br>/Tahun</span>-->
                                <header>
                                    <h3 class="p-0">GOLD</h3>
                                    <h2 class="mt-0 p-0 mb-30">Rp 3.000.000 / Tahun</h2>
                                </header><!-- end header -->
                                <div class="pricing_info text-center">
                                    <ul class="equal-1">
                                        <li class="pb-5">Streaming Radio</li>
                                        <li class="pb-5">Profile Radio</li>
                                        <li class="pb-5">Share Sosial Media</li>
                                        <li class="pb-5">Berita &amp; Event</li>
                                        <li class="pb-5"><a class="getMemberGold text-green">Detail...</a></li>
                                    </ul>
                                    <footer class="mt-20">
                                        <a href="#gold" class="btn btn-primary btn-lg getMemberGold">Get Member</a>
                                    </footer>
                                </div><!-- end pricing-info -->
                            </div><!-- end pricing_detail -->
                        </div><!-- end col-lg-3 -->
                        
                        <div class="owl-item col-lg-4 col-sm-4">
                            <div class="pricing_detail">
                                <!--<span class="priceamount">Rp 5.000.000<br>/Tahun</span>-->
                                <header>
                                    <h3 class="p-0">PREMIUM</h3>
                                    <h2 class="mt-0 p-0 mb-30">Rp 5.000.000 / Tahun</h2>
                                </header><!-- end header -->
                                <div class="pricing_info text-center">
                                    <ul class="equal-1">
                                        <li class="pb-5">Streaming Radio</li>
                                        <li class="pb-5">Profile Radio</li>
                                        <li class="pb-5">Share Sosial Media</li>
                                        <li class="pb-5">Berita &amp; Event</li>
                                        <li class="pb-5"><a class="getMemberPremium text-green">Detail...</a></li>
                                    </ul>
                                    <footer class="mt-20">
                                        <a href="#premium" class="btn btn-primary btn-lg getMemberPremium">Get Member</a>
                                    </footer>
                                </div><!-- end pricing-info -->
                            </div><!-- end pricing_detail -->
                        </div><!-- end col-lg-3 -->
                    </div>
    			</div><!-- end doc -->
    		</div><!-- end container -->
        </section><!-- end white-wrapper -->
        
        <section id="basic" class="white-wrapper pb-0 pt-0">
            <div class="container">
                <div class="col-md-12 section-form-besic pb-20">
                    <p>Isi form di bawah ini untuk mendaftarkan stasiun radio Anda</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-member">
                                <form method="" action="">
                                    <h2 class="mt-10 mb-10 p-0">Data Perusahaan</h2>
                                    <div class="form-group">
                                        <label>Nama <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Username <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Email <em>(wajib)</em></label>
                                        <input type="email" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Password <em>(wajib)</em></label>
                                        <input type="password" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Perusahaan <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Segmentasi Pendengar <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>-->
                                    <div class="form-group">
                                        <label>No Telepon <em>(wajib)</em></label>
                                        <input type="number" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat <em>(wajib)</em></label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-100">
                                            <input type="checkbox" class="mr-10" required=""/><a href="" style="color: blue;" target="_blank">Syarat dan Ketentuan.</a>
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Kirim</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="gold" class="white-wrapper pb-0 pt-0 benefitPre">
            <div class="container">
                <div class="col-md-12 section-form-gold pb-20">
                    <div class="benefit-goldPremium">
                        <h2 class="mt-0 p-0">Benefit Gold</h2>
                        <ul class="mb-20">
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                        </ul>
                    </div>
                    <p>Isi form di bawah ini untuk mendaftarkan stasiun radio Anda</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-member">
                                <form method="" action="">
                                    <h2 class="mt-10 mb-10 p-0">Data Perusahaan</h2>
                                    <div class="form-group">
                                        <label>Nama <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Username <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Email <em>(wajib)</em></label>
                                        <input type="email" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Password <em>(wajib)</em></label>
                                        <input type="password" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Perusahaan <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Segmentasi Pendengar <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>-->
                                    <div class="form-group">
                                        <label>No Telepon <em>(wajib)</em></label>
                                        <input type="number" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat <em>(wajib)</em></label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-100">
                                            <input type="checkbox" class="mr-10" required=""/><a href="" style="color: blue;" target="_blank">Syarat dan Ketentuan.</a>
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Kirim</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="premium" class="white-wrapper pb-0 pt-0 benefitPre">
            <div class="container">
                <div class="col-md-12 section-form-premium pb-20">
                    <div class="benefit-goldPremium">
                        <h2 class="mt-0 p-0">Benefit Premium</h2>
                        <ul class="mb-20">
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                            <li><i class="fa fa-check text-green mr-10"></i>Streaming Radio</li>
                        </ul>
                    </div>
                    <p>Isi form di bawah ini untuk mendaftarkan stasiun radio Anda</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-member">
                                <form method="" action="">
                                    <h2 class="mt-10 mb-10 p-0">Data Perusahaan</h2>
                                    <div class="form-group">
                                        <label>Nama <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Username <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Email <em>(wajib)</em></label>
                                        <input type="email" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Password <em>(wajib)</em></label>
                                        <input type="password" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Perusahaan <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <!--<div class="form-group">
                                        <label>Nama Radio <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>-->
                                    <div class="form-group">
                                        <label>Segmentasi Pendengar <em>(wajib)</em></label>
                                        <input type="text" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>No Telepon <em>(wajib)</em></label>
                                        <input type="number" class="form-control" name="" />
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat <em>(wajib)</em></label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="font-weight-100">
                                            <input type="checkbox" class="mr-10" required=""/><a href="" style="color: blue;" target="_blank">Syarat dan Ketentuan.</a>
                                        </label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg">Kirim</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>

	<?php include 'layout/footer.php'; ?>
    
    <!--<script>
        $('a[href*="#"]')
          .not('[href="#"]')
          .not('[href="#0"]')
          .click(function(event) {
            if (
              location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
              &&
              location.hostname == this.hostname
            ) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                  scrollTop: target.offset().top - 120
                }, 1000, function() {
                  var $target = $(target);
                  $target.focus();
                  if ($target.is(":focus")) { 
                    return false;
                  } else {
                    $target.attr('tabindex','-1'); 
                    $target.focus(); 
                  };
                });
              }
            }
          });
      </script>-->
